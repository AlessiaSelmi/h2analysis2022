#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 28 11:38:40 2023

@author: ale
"""
from functionale import *
import numpy as np
import matplotlib.pyplot as plt
from lmfit.models import GaussianModel
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
model = GaussianModel()
from scipy.optimize import curve_fit
#iniziamo con fascio ortogonale al cristallo. Cioè le due matrici di SiPM sono 
#messe a lato --> Posso equalizzare, studiare l'uniformità di risposta, e 
# la risposta in funzione a dove si trova il fascio rispetto al SiPM 
#Dimensioni cristallo → 3.1x3.1 x4.1 cm
#Posizione motori iniziale: X 860.5 mm Y 208.1 mm
#Distanza dal centro del Tele3 al centro del cristallo: 45.5 cm
#Bias: 32 V
#canali letti ch0+ch3
#ch1+ch2


###########################################################################
#############     equalizzazione #################ààà
### ATTENZIONE CHE X E Y POTREBBERO ESSERE INVERTITE 
#apro i dati
distancesfromT1 = [0.45]
xlim = [[0,2]]*4
p0divx = 0.1
p0divy = 0.5
rangex = [[-1,1]]
rangey =  [[-1,1.5]]
xfitrangeinf=[-1]*2
xfitrangesup=[1]*2

matfitrangeinf = [0,500, 400, 0]
matfitrangesup = [4000,2000, 2000, 1500]

sumfitrangeinf = [0, 0, 1000, 1000]
sumfitrangesup = [5000, 4000, 3000, 4000]
color = ['hotpink', 'limegreen', 'limegreen', 'hotpink']
colorfit = ['black']*4

#guardo forma fascio + divergenza di tutte le run a varie energia (qui il crustallo è perpendicolare alle matric)
# calcolo media e sigma dei segnali e faccio rapporto--> non cambia nulla se copro o meno tutto il cristallo
#quindi nel test beam 2023 possimao mettere matrici che non ricoprono tutta la superficie 

labelmat = ['John, mat2+mat3', 'Paul mat2+mat3', 'George, mat2+mat3', 'Ringo, mat2+mat3']
datapath = r'C:\Users\aselmi\Desktop\Dottorato\analysis\BTF2023\data\run400'
h5keys = ['xpos', 'digi_ph', 'digi_time']
energies = [450, 300, 250]
runnumber = {450: 395, 300: 402, 250:403}
#, 200: 404
mumatrix = []
errmumatrix = []
ratioJohn = {}
ratioGeorge = {}
ratioPaul = {}
ratioRingo = {}
ratiosum = {}

errratioJohn = {}
errratioGeorge = {}
errratioPaul = {}
errratioRingo = {}

errratioJR = {}
errratioPG = {}
sigmamatrix= []
ratioJR = {}
ratioPG = {}
musum = []
errmusum = []
sigmasum = []
ratiosum = []
for j, run in enumerate(energies): 
    data = opendata(runnumber[run], datapath, h5keys)
    #guardiamo le camere 
    labels = ['T1', 'T1', 'T2','T2']
    #limite su asse x per hiostogrammi 1d
    
    histpos1d(data[0],2, labels, xlim)
    histpos2d(data[0], 2, Range = [[0,2],[0,2]])




    divx, divy, sigma, errsigma, mu, ax = divgaussianfit(data[0], 2, distancesfromT1, p0divx, p0divy, rangex, rangey)
    datashift, shift = aligntracker(data[0], 2, mu, distancesfromT1)
    divsx,divsy, sigmas, errsigmas, mus, ax = divgaussianfit(datashift, 2, distancesfromT1, p0divx, p0divy, rangex, rangey)




    fig, ax = plt.subplots(2,2, figsize=(10,10))
    ax = ax.flat
    for i in range(4):
        hph, binsph, _ = ax[i].hist(data[1][:,i],bins = 50, histtype ='stepfilled', 
                     color = color[i], alpha = 0.3, edgecolor = color[i],
                     lw =3, label=f'{labelmat[i]}, {energies[j]} MeV')
        # ax.hist(data[1][:,3],bins=50, histtype ='stepfilled', 
        #              color = 'navy', alpha = 0.3, edgecolor='black', 
        #              lw = 2, label='mat3')
        
        xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
        phcondition =  ((xdata > matfitrangeinf[j]) & (xdata<matfitrangesup[j]))
        paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
        resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                            weights = np.sqrt(hph[phcondition]))
        to_plot = np.linspace(matfitrangeinf[j],matfitrangesup[j],10000)
        energyadcfit = resultx.params['center'].value
        mumatrix.append(energyadcfit)
        energyadcfiterr = resultx.params['center'].stderr
        
        sigma = resultx.params['sigma'].value
        sigmaerr = resultx.params['sigma'].stderr
        sigmamatrix.append(sigma)
        y_eval = model.eval(resultx.params, x= to_plot)
        # ax.plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = Colorfit)
        errratio =  (energyadcfit / sigma) * np.sqrt((energyadcfiterr/energyadcfit)**2 + (sigmaerr/sigma)**2)
            
        if i == 0: 
            ratioJohn[run] = energyadcfit / sigma
            errratioJohn[run] = errratio
        elif i== 1:
            ratioPaul[run] = energyadcfit / sigma
            errratioPaul[run] = errratio
        elif i==2:
            ratioGeorge[run] = energyadcfit / sigma
            errratioGeorge[run] = errratio
        else: 
            ratioRingo[run] = energyadcfit / sigma
            errratioRingo[run] = errratio
        ax[i].plot(to_plot, y_eval, linewidth = 2, color = colorfit[i], label = f'$\mu$ = {round(energyadcfit,2)} $\pm$ {round(energyadcfiterr,2)} \n $\sigma$ = {round(sigma,2)} $\pm$ {round(sigmaerr,2)} \n $\mu/\sigma$ = {round(energyadcfit/sigma,2)} $\pm$ {round(errratio,2)}')
            
        ax[i].set_xlabel('PH [a.u.]')
        ax[i].set_ylabel('Entries')
        ax[i].legend(fontsize = 10)
        ax[i].grid()
    plt.tight_layout()
    plt.show()
        
        
    eqfactor = []
    for k in range(4):
        eqfactor.append(mumatrix[0]/mumatrix[k])
    
    fig, ax = plt.subplots(1,1, figsize=(8,8))
    for i in range(4):
        ax.hist(data[1][:,i]*eqfactor[i],bins = 50, histtype ='step', 
                      color = color[i], alpha = 0.7, 
                      lw =3, label=f'mat{i} equalized')
        ax.set_xlabel('PH [a.u.]')
        ax.set_ylabel('Entries')
        ax.grid()
        ax.legend()
    plt.show()
    
    SiPM1 = data[1][:,0]*eqfactor[0] +data[1][:,3]*eqfactor[3]
    SiPM2 = data[1][:,2]*eqfactor[2] + data[1][:,1]*eqfactor[1]
    
    fig, ax = plt.subplots(1,1, figsize=(8,8))
    ax.hist((SiPM1-SiPM2),bins = 50, histtype ='step', 
                   color = 'navy', alpha = 0.7, 
                   lw =3)
    ax.set_xlabel('PH1-PH2')
    ax.set_ylabel('Entries')
    ax.grid()


    SiPM = [data[1][:,0]*eqfactor[0] +data[1][:,3]*eqfactor[3], data[1][:,2]*eqfactor[2] + data[1][:,1]*eqfactor[1]]
    labelsum = [f'John + Ringo, {energies[j]} MeV ', f'Paul + George, {energies[j]} MeV']
    fig, ax = plt.subplots(1,2, figsize=(10,8))
    for i in range(2):
        hph, binsph, _ = ax[i].hist(SiPM[i],bins = 50, histtype ='stepfilled', 
                   color = color[i], alpha = 0.3,edgecolor=color[i], 
                   lw =3, label = labelsum[i])
        xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
        phcondition =  ((xdata > sumfitrangeinf[j]) & (xdata<sumfitrangesup[j]))
        paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
        resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                            weights = np.sqrt(hph[phcondition]))
        to_plot = np.linspace(sumfitrangeinf[j],sumfitrangesup[j],10000)
        energyadcfit = resultx.params['center'].value
  
        energyadcfiterr = resultx.params['center'].stderr
        
        sigma = resultx.params['sigma'].value
        sigmaerr = resultx.params['sigma'].stderr
    
        y_eval = model.eval(resultx.params, x= to_plot)
        # ax.plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = Colorfit)
        ax[i].plot(to_plot, y_eval, linewidth = 2, color = colorfit[i], label = f'$\mu$ = {round(energyadcfit,2)} $\pm$ {round(energyadcfiterr,2)} \n $\sigma$ = {round(sigma,2)} $\pm$ {round(sigmaerr,2)}\n $\mu/\sigma = {round(energyadcfit/sigma,2)} $\pm$ {round(errratio,2)}')
        errratio =  (energyadcfit / sigma) * np.sqrt((energyadcfiterr/energyadcfit)**2 + (sigmaerr/sigma)**2)
        if i == 0: 
            ratioJR[run] = energyadcfit / sigma
            errratioJR[run] = errratio
        elif i== 1:
            ratioPG[run] = energyadcfit / sigma
            errratioPG[run] = errratio
        ax[i].set_xlabel('PH [a.u]')
        ax[i].set_ylabel('Entries')
        ax[i].legend()
        ax[i].grid()
    plt.tight_layout()
    plt.show()
    # #%%
    # fig, ax = plt.subplots(1,1, figsize=(8,8))
    # ax.hist((SiPM1+SiPM2),bins = 50, histtype ='step', 
    #                color = 'navy', alpha = 0.7, 
    #                lw =3)
    # ax.set_xlabel('PH1+PH2')
    # ax.set_ylabel('Entries')
    # ax.grid()
fig, ax = plt.subplots(1,2, figsize=(8,8))


for j, ene in enumerate(energies): 
    ax[0].errorbar(energies[j], ratioJohn[ene], errratioJohn[ene], color = 'red', markersize = 10, marker = '*', label = 'Jhon')
    ax[0].errorbar(energies[j], ratioRingo[ene],errratioRingo[ene], color = 'black', markersize = 10, marker = '^', label = 'Ringo')
    ax[0].errorbar(energies[j], ratioJR[ene],errratioJR[ene],  color = 'green', markersize = 10, marker = '*', label = 'Jhon + Ringo')
    
    ax[1].errorbar(energies[j], ratioPaul[ene], errratioPaul[ene], color = 'red', markersize = 10, marker = '*', label = 'Paul')
    ax[1].errorbar(energies[j], ratioGeorge[ene],errratioGeorge[ene] , color = 'black', markersize = 10, marker = '^', label = 'George' )
    ax[1].errorbar(energies[j], ratioPG[ene], errratioPG[ene], color = 'green', markersize = 10, marker = '*', label = 'Paul+George')
    if j == 0: 
        ax[0].legend()
        ax[1].legend()
ax[0].set_xlabel('MeV')
ax[1].set_xlabel('MeV')
ax[0].set_ylabel('$\mu$/$\sigma$')
ax[1].set_ylabel('$\mu$/$\sigma$')
ax[0].grid()
ax[1].grid()
#%% eff

    # PHtot = SiPM1+SiPM2
    # zcryst = distancesfromT1[0]+0.44
    # xcryst = data[0][:,0] + zcryst*np.tan(divx)
    # ycryst = data[0][:,1] + zcryst*np.tan(divy)
    
    # sel = PHtot>1000
    # Range2d = [[0,2], [0,2]]
    # fig, ax = plt.subplots(1,1, figsize=(8,8))
    # zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(
    #     xcryst[0], ycryst[0], cmap=plt.cm.jet, bins=[100, 100], range=Range2d)
    # xcrystcut = xcryst[0][sel]
    # ycrystcut = ycryst[0][sel]
    # fig, ax = plt.subplots(1,1, figsize=(8,8))
    # zhist2dcut, xhist2dcut , yhist2dcut , mapable = ax.hist2d(xcrystcut, ycrystcut, cmap=plt.cm.jet, bins = [100,100], range = Range2d)
    # ax.set_xlim([0,2])
    # ax.set_ylim([0,2])
    # rect = patches.Rectangle((3.6, 3), 3.1, 3.1, linewidth=6, edgecolor='white', facecolor='none')
    # ax.add_patch(rect)
    
    
    
    #%% 
    # np.seterr(divide='ignore', invalid = 'ignore')
    # eff = np.divide(zhist2dcut, zhist2d)
    # eff[np.isnan(eff)]=0
    
    
    # fig, ax = plt.subplots(1,1, figsize=(12,6))
    # plt.rcParams['font.size'] = '15'
    # divider = make_axes_locatable(ax)
    # cax = divider.append_axes('right', size='5%', pad=0.1)
    # im = ax.imshow(np.flip(np.transpose(eff)), cmap=plt.cm.jet, extent=[0,2, 0, 2])
    # fig.colorbar(im, cax=cax, orientation='vertical')
    # rect = patches.Rectangle((3.4, 3), 3.1, 3.1, linewidth=6, edgecolor='darkred', facecolor='none')
    # ax.add_patch(rect)
    # ax.set_xlim([0,2])
    # ax.set_ylim([0,2])
    # ax.set_xlabel('x [cm]', fontsize = 14)
    # ax.set_ylabel('y [cm]', fontsize = 14)
    # ax.set_title('SiPMs efficiency')
    # plt.show()


#%%proiezione eff

# xcoordmin = 0.7
# xcoordmax = 1.25
# ycoordmin = 0.75
# ycoordmax = 1.25
# minx = np.nonzero(xhist2dcut > xcoordmin)[0][0]
# maxx = np.nonzero(xhist2dcut > xcoordmax)[0][0]
# miny = np.nonzero(yhist2dcut > ycoordmin)[0][0]
# maxy = np.nonzero(yhist2dcut> ycoordmax)[0][0]

# xvect = xhist2dcut[:-1] + (xhist2dcut[1]-xhist2dcut[0])/2
# yvect = yhist2dcut[:-1] + (yhist2dcut[1]-yhist2dcut[0])/2
# proiezy = np.nanmean(eff[minx:maxx, :], axis = 0)
# proiezx = np.nanmean(eff[:, miny:maxy], axis = 1)

# xliminf = 0.75
# xlimsup = 1.50
# yliminf = 0.75
# ylimsup = 1.50
# def line(x,b):
#     return 0*x+b
# poptx, pcovx = curve_fit(line, xvect[minx+xliminf:maxx-xlimsup], proiezx[minx+xliminf:maxx-xlimsup], 
#                               p0 = 0.8)
# popty, pcovy = curve_fit(line, yvect[miny+yliminf:maxy-ylimsup], proiezy[miny+yliminf:maxy-ylimsup], 
#                               p0 = 0.8)

# fig, ax = plt.subplots(2,1)
# plt.rcParams['font.size'] = '10'
# ax[0].plot(xvect, proiezx, 
#                    ls = "", marker = ".", c = "navy", zorder = 1, 
#                    label = "Proiezione x")
# ax[1].plot(yvect, proiezy,  
#                    ls = "", marker  = ".", c = "navy", zorder = 1, 
#                    label = "Proiezione y")

# ax[0].plot(xvect[minx+xliminf:maxx-xlimsup], line(xvect[minx+xliminf:maxx-xlimsup], *poptx), ls = "--", c = "r", 
#                 zorder = 2, label = f"p0 = {np.round(poptx,3)}")
# ax[1].plot(yvect[miny+yliminf:maxy-ylimsup], line(yvect[miny+yliminf:maxy-ylimsup], *popty), ls = "--", c = "r", 
#                 zorder = 2, label = f"p0 = {np.round(popty,3)}")
# for i in range(2):
#         ax[i].grid()
#         ax[i].legend()
#         ax[i].set_ylabel("Efficiency", fontsize = 14)
#         ax[i].set_xlabel("Position (cm)", fontsize = 14)
# ax[0].set_title('Top SiPMs efficiency (John | Ringo)')


#%%
## profilo efficienza con altre run 

nruns = []
data = {}
cut = [0,0,0,0,1000, 900,900,900, 900]
nrunstart = 382
eff = []
for i in range(9): 
    nruns.append(nrunstart+i)
    data[nruns[i]] = opendata(nruns[i], datapath, h5keys)
    digiph = data[nruns[i]][1]
    SiPM1 = digiph[:,0]*eqfactor[0]+digiph[:,3]*eqfactor[3]
    SiPM2 = digiph[:,1]*eqfactor[1]+digiph[:,2]*eqfactor[2]
    # histpos1d(data[nruns[i]][0],2, labels, xlim)
    # histpos2d(data[nruns[i]][0], 2, Range = [[0,2],[0,2]])
    fig, ax = plt.subplots(1,1, figsize=(8,8))
    ph = SiPM1+SiPM2

        
    hph, binsph, _ =  ax.hist(ph, bins=100, range = [0,8000], histtype ='stepfilled', 
            color = 'hotpink', alpha = 0.5, edgecolor='black', lw =2, density = True)
    ax.axvline(x=cut[i], color="black", linestyle = '--', label = 'Threshold', lw=3)
    # xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
    # phcondition =  ((xdata > xfitrangeinf[i]) & (xdata<xfitrangesup[i]))
    # paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
    # resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
    #                     weights = np.sqrt(hph[phcondition]))
    # to_plot = np.linspace(0,5000,10000)
    # energyadcfit = resultx.params['center'].value
    # energyadcfiterr = resultx.params['center'].stderr
    # y_eval = model.eval(resultx.params, x= to_plot)
    # # ax.plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = Colorfit)
    # ax[i].plot(to_plot, y_eval, linewidth = 2, color = 'black', label = f'$\mu$ = {energyadcfit}')
    

    phcut = hph[binsph[:-1]>cut[i]]
    bin_width = binsph[1] - binsph[0]

    integral = bin_width * sum(phcut)
    print(integral)
    eff.append(integral)
    ax.legend()    
    


    ax.grid()
x  = [0,0.5, 1.0, 1.5, 2.0, 2.5, 3.0,-0.5,-1]

fig, ax = plt.subplots(1,1, figsize=(8,8))
ax.plot(x, eff, '.', markersize = 20, color = 'navy')
ax.grid()
ax.set_xlabel('cm')
ax.set_ylabel('eff')      

#%% ## prova retta di calibrazione
# # mu = []
# # muerr = []
# # nruns = [395, 402, 403,404]
# # ene = [450, 300, 250, 200]
# # for j, nrun in enumerate(nruns):
# #     data = opendata(nrun, datapath, h5keys)
# #     digiph = data[1]
# #     SiPM1 = digiph[:,0]+digiph[:,3]
# #     SiPM2 = digiph[:,1]+digiph[:,2]
# #     histpos1d(data[0],2, labels, xlim)
# #     histpos2d(data[0], 2, Range = [[0,2],[0,2]])
#     #fig, ax = plt.subplots(1,2, figsize=(8,8))
#     #ph = [SiPM1,SiPM2]
#     # for i in range(2): 
        
#     #     hph, binsph, _ =  ax[i].hist(ph[i], bins=100, range = [0,5000], histtype ='stepfilled', 
#     #             color = 'hotpink', alpha = 0.5, edgecolor='black', lw =2)
        
#     #     xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
#     #     phcondition =  ((xdata > xfitrangeinf) & (xdata<xfitrangesup))
#     #     paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
#     #     resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
#     #                         weights = np.sqrt(hph[phcondition]))
#     #     to_plot = np.linspace(0,5000,10000)
#     #     mu = resultx.params['center'].value
#     #     muerr= resultx.params['center'].stderr
#     #     y_eval = model.eval(resultx.params, x= to_plot)
#     #     # ax.plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = Colorfit)
#     #     ax[i].plot(to_plot, y_eval, linewidth = 2, color = 'black', label = f'$\mu$ = {round(mu,2)} $\pm$ {round(muerr,2)}')
        
#     #     ax[i].legend()    
        
#     #     ax[i].grid()
# #     fig, ax = plt.subplots(1,1, figsize=(8,8))
# #     ph = SiPM1+SiPM2
# #     # for i in range(2): 
        
# #     hph, binsph, _ =  ax.hist(ph, bins=100, range = [0,10000], histtype ='stepfilled', 
# #             color = 'hotpink', alpha = 0.5, edgecolor='black', lw =2)
    
# #     xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
# #     phcondition =  ((xdata > xfitrangeinf) & (xdata<xfitrangesup))
# #     paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
# #     resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
# #                         weights = np.sqrt(hph[phcondition]))
# #     to_plot = np.linspace(0,10000,10000)
# #     mu.append(resultx.params['center'].value)
# #     muerr.append(resultx.params['center'].stderr)
# #     y_eval = model.eval(resultx.params, x= to_plot)
# #     # ax.plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = Colorfit)
# #     ax.plot(to_plot, y_eval, linewidth = 2, color = 'black', label = f'$\mu$ = {round(mu,2)} $\pm$ {round(mu,2)}')
    
# #     ax.legend()    
    
# #     ax.grid()


# # #%%
# # fig, ax = plt.subplots(1,1, figsize=(8,8))
# # from lmfit.models import LinearModel
# # linearmodel = LinearModel()     
# # linearparams = linearmodel.guess(energies, x=mucalib)
# # linearresult = linearmodel.fit(energies, linearparams, x= mucalib, weights = errcalib)

# # ax.errorbar(mu, energies, xerr= errcalib, 
# #             marker='D',  linestyle = 'none', color = 'midnightblue',
# #             label = f'Lead Glass CC', markersize = 6)
# # ax.plot(mucalib,linearresult.best_fit, color = 'red', lw = 2,
# #         label = f'a.u. = ({round(linearresult.params["slope"].value,3)} $\pm$ {round(linearresult.params["slope"].stderr,3)})$\cdot$ GeV +  ({round(linearresult.params["intercept"].value,3)}  $\pm$ {round(linearresult.params["intercept"].stderr,3)})')
# # ax.legend(fontsize = 14)
# # ax.set_ylabel('GeV')
# # ax.set_xlabel('PH [a.u.]')
# # ax.grid()
# # slope=(linearresult.params['slope'].value)
# # errslope = (linearresult.params['slope'].stderr)
# # intercept= (linearresult.params['intercept'].value)
# # errintercept= (linearresult.params['intercept'].stderr)
# # plt.show()

#%%
###################### eff 
