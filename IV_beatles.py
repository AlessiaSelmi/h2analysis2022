#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 14 17:11:40 2023

@author: ale
"""


##* V,uA	 Ringo	George	Paul	John
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches


plt.rcParams['font.size'] = '20'
data = np.loadtxt('beatles_scan.dat')
V = data[:,0]
I_Ringo = data[:,1]
I_George = data[:,2]
I_Paul = data[:,3]
I_John = data[:,4]

fig, ax = plt.subplots(1, 1, figsize=(8, 6))
ax.plot(V,I_Ringo, 'o', linestyle = '--', color = 'navy', markersize = '10', label = 'Ringo')
ax.plot(V,I_George, 'D', linestyle = '--', color = 'limegreen', markersize = '10', label = 'George')
ax.plot(V,I_Paul, 's', linestyle = '--', color = 'hotpink', markersize = '10', label = 'Paul')
ax.plot(V,I_John, 'X', linestyle = '--', color = 'orangered', markersize = '10', label = 'John')
ax.set_xlabel('V')
ax.set_ylabel('I [$\mu$A]')
ax.grid()
ax.legend()
plt.tight_layout()
plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/IV_curve.pdf')
plt.show()