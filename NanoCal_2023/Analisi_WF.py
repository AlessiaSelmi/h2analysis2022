#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 17 17:44:02 2023

@author: ale
"""
import Wave_fun as Wfa
from scipy.signal import peak_prominences
import numpy as np
import sys, os, re, glob
import uproot
from matplotlib import pyplot as plt
import scipy as sp
from scipy.signal import butter, lfilter, freqz, filtfilt

def butter_lowpass(data, cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    filtered_signal = lfilter(b, a, data)
    return filtered_signal

# def butter_lowpass_filter(data, cutoff, fs, order=5):
#     b, a = butter_lowpass(cutoff, fs, order=order)
#     y = lfilter(b, a, data)
#     return y


#-------------------------------
#find start and stop

def get_pulses(wf, thrUP=0.1, thrDOWN=-0.1):
    starts,  = np.where((wf>thrUP) & (np.roll(wf,1)<=thrUP) )  
    stops,  = np.where((wf<thrDOWN) & (np.roll(wf,1)>=thrDOWN) )
    
    real_starts, = np.where(np.diff(np.searchsorted(stops, starts), prepend=-1))
    real_stops, = np.where(np.diff( np.searchsorted(starts, stops), prepend=0) )
    
    return np.vstack((starts[real_starts], stops[real_stops]))
path = "/home/ale/Desktop/Dottorato/wf_OREOprova"
numRun = 680408

nameFile = f"run{numRun}_00010.root"

fileToLoad = os.path.join(path, nameFile)
with uproot.open(fileToLoad) as f:
    for k in f:
        print(k)
        try:
            for kk in f[k]:
                print(kk)
        except:
            pass
with uproot.open(fileToLoad)["h3"] as f:
    for k in f:
        print(k)
    Nwdigi_742a = f["Nwdigi_742a"].arrays(library = "np")["Nwdigi_742a"]
    Idigi_742a = f["Idigi_742a"].arrays(library = "np")["Idigi_742a"]
    
    print(Nwdigi_742a.shape, Idigi_742a.shape)
    
# Condizione da soddisfare, (direi che è sempre vera)
conda = (Nwdigi_742a == 8248)#32992)
print(f"Good events: {conda.sum()}\nTotal: {conda.shape}")

#Nwdigi_742a = Nwdigi_742a[conda]
Idigi_742a = Idigi_742a[conda]
print(type(Idigi_742a))
print(Idigi_742a.shape, Idigi_742a.dtype)

#%%

nPtsDigi = 1024
nWordSingleChannel = 5+nPtsDigi+2
nChannels = 8 # 32


def getWaveform(matrice, canale, evento):
    """
    matrice: matrice dei dati
    canale: numero di canale, contando da 0
    evento: numero di evento (riga), contando da 0
    """
    i = evento
    j = canale
    return matrice[i, (5 + nWordSingleChannel*j) : (5 + nWordSingleChannel*j + nPtsDigi)]


#%%
xVect = (np.arange(0, nPtsDigi) * 0.4)#ns, sampling 5 GHz

chanToPlot = 6
allAmplitudes = []
for i in range(50,100):
    fig, ax = plt.subplots()
    fig.set_size_inches(12, 5)
    wf = getWaveform(Idigi_742a, chanToPlot, i)
    
    ax.plot(xVect, wf, ls = ":", c = "tab:green")
    
    ax.grid()
    ax.set_xlabel("Time [ns]", fontsize = 14)
    ax.set_ylabel("[ADC]", fontsize = 14)
    
    ax.set_title(f"Chan. {chanToPlot} -- Ev. {i}", fontsize = 16)
    
    plt.show()

#     #baseline subtraction
    # baseline = np.average(wf[0:100])
    # wf_shift = (wf.transpose() - baseline).transpose()
    # # Filter requirements.
    # order = 5
    # fs = 1 # sample rate
    # cutoff = 1/100  # desired cutoff frequency of the filter

    # #filter
    # wf_smooth = butter_lowpass_filter((wf_shift), cutoff, fs, order)
    # wf_smoothder = np.diff(wf_smooth,1)
    # # fig, ax = plt.subplots()
    # # ax.plot(xVect, wf_smooth, ls = ":", c = "tab:green")
    # # fig, ax = plt.subplots()
    # # ax.plot(xVect[:-1], wf_smoothder, ls = ":", c = "tab:green")

    # #selecting the first part of the waveform (pre trigger for single photon serach)
    # pretr = wf_smoothder[0:90]
    # wf_pretr = pretr.flatten()

    # #merging of the all event in one vector 
    # wf_flat = wf_smoothder.flatten()
    # # fig, ax = plt.subplots()
    # # ax.plot(xVect[:-1], wf_flat, ls = ":", c = "tab:green")
    # #getting the start and stop point on the smooth derivative
    # pulses = get_pulses(wf_flat, 0.2, 0)
    # # creating an empty array with 2 x start shape (better to force the dtype since it has to be used later as index array)
    # alternated = np.zeros(pulses.shape[1]*2, dtype='int')

    # # filling the array alternatively with start and stop, start and stop, ...
    # alternated = pulses.flatten('F')
    # amplitudes = np.add.reduceat(wf_flat, alternated)[::2]
    # allAmplitudes.append(amplitudes)
#%%############################################################################
################################################################################
###############################################################################
wf = getWaveform(Idigi_742a, chanToPlot, 90)
fig, ax = plt.subplots()
ax.plot(xVect, wf, ls = ":", c = "tab:green")
#baseline subtraction
baseline = np.average(wf[0:50])
wf_shift = (wf.transpose() - baseline).transpose()

#%%
## proviamo the FFT per capire i parametri ottimali per il filtro 
fft_signal = np.fft.fft(wf_shift)
# Compute the frequency vector that corresponds to the FFT
freq_vector = np.fft.fftfreq(len(wf_shift), d=1/5)

fig, ax = plt.subplots()
# Plot the magnitude of the FFT signal as a function of frequency
plt.plot(freq_vector, np.abs(fft_signal))
plt.xlabel('Frequency (Hz)')
plt.ylabel('Magnitude')
plt.show()
#%%


#Filter requirements.
order = 5
fs = 5# sample rate
cutoff = 1/(100)# desired cutoff frequency of the filter

# #filter
wf_smooth = butter_lowpass(wf_shift, cutoff, fs)
wf_smoothder = np.diff(wf_smooth,1)
fig, ax = plt.subplots()
ax.plot(xVect, wf_smooth, ls = ":", c = "tab:green", label = 'wf filtrata')
# axR = ax.twinx()
# axR.plot(xVect, wf_smooth, ls = ":", c = "tab:red", label = 'wf filtrata', lw=3)
# ax.legend()
# ax.set_ylim((312, 342))



# #%%
# fig, ax = plt.subplots()
# ax.plot(xVect[:-1], wf_smoothder, ls = ":", c = "tab:green", label = 'derivata su wf filtrata')
# ax.legend()

# #selecting the first part of the waveform (pre trigger for single photon serach)
# pretr = wf_smoothder[0:90]
# wf_pretr = pretr.flatten()


# # #merging of the all event in one vector 
# wf_flat = wf_smoothder.flatten()
# #getting the start and stop point on the smooth derivative
# pulses = get_pulses(wf_flat, 0.2, 0)
# # creating an empty array with 2 x start shape (better to force the dtype since it has to be used later as index array)
# alternated = np.zeros(pulses.shape[1]*2, dtype='int')

# # # filling the array alternatively with start and stop, start and stop, ...
# alternated = pulses.flatten('F')
# amplitudes = np.add.reduceat(wf_flat, alternated)[::2]
# print(amplitudes)

# # #%%
# # fig, ax = plt.subplots()
# # ax.hist(allAmplitudes, 100, log=True)
# # plt.show()