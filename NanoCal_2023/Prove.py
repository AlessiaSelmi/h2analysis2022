#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  4 10:21:16 2023

@author: ale
"""
from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
## prova analisi dati NanoCal 
#struttura ascii file 
# x1,y1,x2,y2
# 8 baselines V1720 
# 8 PH V1720
# 8 time V1720
# 64 baseline V1742
# 64 time V1742
# 64 time50% V1742
# 64 PH V1742
# event nr
  	
# format(4(e12.5,1x),(8(i4.4,1x)),(8(i4.4,1x)),(8(i4.4,1x)),(64(i4.4,1x)),(64(i4.4,1x)),(64(i4.4,1x)),(64(i4.4,1x)),(i9.9,1x))

# (numbering from 1)
# Channels V1720
# ch1 - trigger
# ch2 - cherenkov 1
# ch3 - cherenkov 2

# Channels V1742
# ch1 - trigger
# ch2 - nanocal
# ch3 - empty
# ch 4 - trigger scintillator
# ch 5 - empty
# ch 6 - empty
# ch 7 - lucite 1
# ch 8 - lucite 2
plt.rcParams['font.size'] = '15'
#datapath = '/home/ale/Desktop/Dottorato/analysis/NanoCal_2023/Dati_NanoCal/run650'
runnumber = 320
datapath = '/home/ale/Desktop/Dottorato/analysis/NanoCal_2023/datiHDF5NanoCal/run650'
#%%
h5keys = ['ph1720', 'ph1742', 'time1720', 'time1742','time501742', 'xpos']
data = opendata(runnumber, datapath, h5keys)
xpos = data[5]


nanoCal = data[1][:,1]
TimeNanoCal = data[3][:,1]
Time50NanoCal = data[4][:,1]
cerry1 = data[0][:,2]
cerry2 = data[0][:,3]
#%%
#apertura file ascii

# plt.rcParams['font.size'] = '15'
# data = np.loadtxt(datapath +f'{runnumber}.dat')
# xpos = data[:,0:4]
# base = data[:, 4:4+8]
# ph1720 = data[:, 4+8:4+8+8]
# time1720 = data[:, 4+8+8:4+8+8+8]
# base1742 = data[: , 4+8+8+8: 4+8+8+8+64]
# time1742 = data[:, 4+8+8+8+64:4+8+8+8+64+64]
# time50 = data[:,4+8+8+8+64+64:4+8+8+8+64+64+64]
# ph1742 = data[:,4+8+8+8+64+64+64: 4+8+8+8+64+64+64+64]



#%%
#camere 
xlabels = ['BC1',' BC1', 'BC2', 'BC2']
xlim = [[-0,10]]*4
histpos1d(xpos, 2, xlabels = xlabels, xlim = xlim, Range = [0,10])
histpos2d(xpos, numberoftrackers = 2)
fig, ax = plt.subplots(1,2 , figsize=(8,8))
ax = ax.flat
ax[0].hist2d(xpos[:,0], xpos[:,2], bins = [100,100], range = [[0,10], [0,10]])
ax[1].hist2d(xpos[:,1],xpos[:,3] , bins = [100,100], range = [[0,10], [0,10]])
plt.show()




#%%
#divergenze
distancesfromT1 = [72.6]
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]

divex, divey, sigma, errsigma, mu, _ = divgaussianfit(xpos, 2, distancesfromT1,
                                                       p0divx, p0divy,
                                                       Rangex=[[-0.02,0.02]],
                                                       Rangey=[[-0.02,0.02]])


datashift, shift = aligntracker(xpos, 2, mu, distancesfromT1)
divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                               distancesfromT1,
                                                                                p0divx, p0divy,
                                                                                Rangex=[[-0.02,0.02]],
                                                                                Rangey=[[-0.02,0.02]], 
                                                                                Color='navy', 
                                                                                Figsize = (8,6))

#%%
#nanoCal = data[:,221]
#nanoCal = ph1742[:,1]
phcut = (nanoCal>60)
### Ph NanoCal
fig, ax = plt.subplots(1,1, figsize=(8,8))
ax.hist(nanoCal, bins = 100, 
        histtype = 'stepfilled', color = 'lime', edgecolor = 'lime'
        ,alpha = 0.6,lw =3, label = f'NanoCal, run {runnumber}', range=[0,1000])
ax.legend()
ax.set_xlabel('PH [a.u]')
ax.set_ylabel('Entries')
ax.grid()
ax.set_yscale('log')
plt.tight_layout()
plt.show()#%%


#%%

# crillin = data[1][:,32]
# fig, ax = plt.subplots(1,1, figsize=(8,8))
# ax.hist(crillin , bins = 100, 
#         histtype = 'stepfilled', color = 'lime', edgecolor = 'lime'
#         ,alpha = 0.6,lw =3, label = f'Crillin, run {runnumber}')
# ax.legend()
# ax.set_xlabel('PH [a.u]')
# ax.set_ylabel('Entries')
# ax.grid()
# ax.set_yscale('log')
# plt.tight_layout()
# plt.show()#%%


#%%

#Time NanoCal
Time50thsup = 1350
Time50thinf = 1250

noisethinf = 1100
noisethsup = 1200
fig, ax = plt.subplots(1,1, figsize=(8,8))
ax.hist(TimeNanoCal, bins = 100, histtype = 'step', color = 'indigo' 
        ,alpha = 1,lw =3, label = f'NanoCal Time, run {runnumber}')
ax.set_xlabel('NanoCal Time')
ax.set_ylabel('Entries')
ax.legend()
ax.grid()
#%%
## Time 50 
fig, ax = plt.subplots(1,1, figsize=(8,8))
ax.hist(Time50NanoCal, bins = 100, histtype = 'step', color = 'indigo' 
        ,alpha = 1,lw =3, label = f'NanoCal Time 50, run {runnumber}',
                             range = [900,1400])
ax.axvline(x = Time50thsup, color="red", label = 'threshold', ls = '--')
ax.axvline(x = Time50thinf, color="red", label = 'threshold', ls = '--')


ax.set_xlabel('NanoCal Time 50 ')
ax.set_ylabel('Entries')
ax.legend()
ax.grid()


#%%
#PH e tempo 
fig, ax = plt.subplots(1,1, figsize=(8,8))
ax.hist2d(nanoCal,TimeNanoCal, bins= [100,100], norm=colors.LogNorm()) 
ax.set_xlabel('NanoCal PH')
ax.set_ylabel('NanoCal Time')
ax.grid()
plt.show()
#PH e tempo  50 

fig, ax = plt.subplots(1,1, figsize=(8,8))
ax.hist2d(nanoCal,Time50NanoCal, bins= [100,100], norm=colors.LogNorm()) 
ax.axhline(y = Time50thsup, color="red", label = 'threshold', ls = '--')
ax.axhline(y = Time50thinf, color="red", label = 'threshold', ls = '--')

ax.axhline(y = noisethinf, color="black", label = 'threshold', ls = '--')
ax.axhline(y = noisethsup, color="black", label = 'threshold', ls = '--')
ax.set_xlabel(f'NanoCal PH, run {runnumber}')
ax.set_ylabel(f'NanoCal Time 50, run {runnumber}')
ax.grid()
plt.show()
#%% NanoCal cut
cuttime = (Time50NanoCal>Time50thinf) 
cutbackground = (Time50NanoCal>noisethinf)&(Time50NanoCal<noisethsup)
### Ph NanoCal con Cut 
fig, ax = plt.subplots(1,1, figsize=(8,8))
ax.hist(nanoCal[cuttime], bins = 100, 
        histtype = 'stepfilled', color = 'lime' 
        ,alpha = 0.5,edgecolor= 'lime',lw =3, label = f'NanoCal, run {runnumber}', range = [0,1000])
ax.legend()
ax.set_xlabel('PH [a.u]')
ax.set_ylabel('Entries')
ax.grid()
ax.set_yscale('log')
plt.tight_layout()
plt.show()#%%
#%%   efficienza 
zdec = 171
xdec = xpos[:,0] + zdec*np.tan(divex)
ydec = xpos[:,1] + zdec*np.tan(divey)
fig, ax = plt.subplots(1,1, figsize=(8,8))
zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xdec[0], ydec[0], cmap=plt.cm.jet, bins=[100, 100], range=[[0,10], [0,10]])
plt.colorbar(mapable, ax=ax)
ax.set_xlabel('x [cm]')
ax.set_xlabel('y [cm]')
ax.set_title('Proiezione su piano del cristallo')
plt.tight_layout()
plt.show()

cutdec = cuttime
zhist2dcut, xhist2dcut , yhist2dcut , mapable = ax.hist2d(xdec[0][cutdec], ydec[0][cutdec], 
                                                             cmap=plt.cm.jet, bins = [100,100],
                                                             range = [[0,10], [0,10]])
plt.rcParams['font.size'] = '20'
fig, ax = plt.subplots(1, 1, figsize=(8,8))
np.seterr(divide='ignore', invalid = 'ignore')
eff = np.divide(zhist2dcut, zhist2d)
eff[np.isnan(eff)]=0
divider = make_axes_locatable(ax)
cax = divider.append_axes('right', size='5%', pad=0.1)
im = ax.imshow(np.flip((np.transpose(eff))), cmap=plt.cm.jet, extent=[0,10, 0, 10])
fig.colorbar(im, cax=cax, orientation='vertical')
ax.set_xlim([0,10])
ax.set_ylim([0,10])
ax.set_xlabel('x [cm]')
ax.set_ylabel('y [cm]')
ax.set_title(f'Efficency run {runnumber}')
plt.tight_layout()
plt.show()
# #cherenkov uno vede per elettroni e l'altro per elettroni e muoni 
# #se vantano entrambi vedi elettroni 
# fig, ax = plt.subplots(1,2, figsize=(8,8))
# ax[0].hist(cerry1, bins = 100, 
#         histtype = 'step', color = 'indigo' 
#         ,alpha = 1,lw =3, label = f'NanoCal, run {runnumber}', range = [0,100])
# ax[1].hist(cerry2, bins = 100, 
#         histtype = 'step', color = 'indigo' 
#         ,alpha = 1,lw =3, label = f'NanoCal, run {runnumber}', range = [0,300])
# #%%
# for i in range(2): 
    
#     ax[i].set_xlabel('PH cerry [a.u]')
#     ax[i].set_ylabel('Entries')
#     ax[i].grid()
# plt.tight_layout()
# plt.show()#%%


#%%


# choose a window size for the rolling mean
window_size = 1000

# compute the rolling mean of the signal
rolling_mean = np.convolve(nanoCal, np.ones(window_size)/window_size, mode='same')

# subtract the rolling mean from the signal to obtain the background-subtracted signal
bg_subtracted_signal = nanoCal - rolling_mean

# plot the original signal and the background-subtracted signal
fig, ax = plt.subplots(1,2, figsize=(8,8))
ax = ax.flat
ax[0].hist(nanoCal[cuttime], bins = 100, label='Original Signal')
ax[1].hist(rolling_mean, bins = 100, label='Rolling Mean')
plt.legend()
plt.show()
fig, ax = plt.subplots(1,1, figsize=(8,8))
ax.hist(bg_subtracted_signal, bins = 100,  label='Background-Subtracted Signal', range = [0,1000])
ax.hist(nanoCal[cuttime], bins = 100, 
        histtype = 'stepfilled', color = 'lime' 
        ,alpha = 0.5,edgecolor= 'lime',lw =3, label = f'NanoCal, run {runnumber}', range = [0,1000])
ax.set_yscale('log')
