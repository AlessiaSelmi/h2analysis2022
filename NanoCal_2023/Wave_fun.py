# Lets try to make a file.py with all my function for the Waveform analysis 
import numpy as np



def wf_derivative(wf) :
    ''' perform the derivative (DeltaY/DeltaX) of the waveform'''
    derivative = []
    for i in range(len(wf)-1):
        increment = (wf[i+1]-wf[i])
        derivative.append(increment)
    return np.array(derivative)


def RC_filter(der_wf,RC):
    
    '''making RC filter on waveform '''
    
    der = der_wf.copy()
    der_smooth = der_wf.copy()
    for i in range(len(der_wf)-1):
        der_smooth[i+1] = der_smooth[i] + RC*(der[i+1]-der_smooth[i])
    return der_smooth




def find_crossings(der_RC, thr):
    '''finding the starting point time of the signal'''
    start= []
    
    for i in range(len(der_RC)):
        
        if i > 0 and der_RC[i]>thr and der_RC[i-1]<=thr :
            start.append(i)
            
    return np.array(start) 

 

def find_amplitude(wf, starts):
    
    '''find the amplitude corresponding to the peak time in the original waveform '''
    peaks = []
    
    for i in range(len(starts)-1):
        peaks.append(
                [wf[starts[i] : starts[i+1]].argmax() + starts[i] , 
                  wf[starts[i] : starts[i+1]].max()
                 
                 ])
    return np.array(peaks)
    
    
    
    
    
def Delta_time(time) : 
    Delta = []
    '''making Delta time'''
    for i in range (len(time)-1): 
            time_diff = time[i+1]-time[i]
            Delta.append(time_diff)
    return np.array(Delta)