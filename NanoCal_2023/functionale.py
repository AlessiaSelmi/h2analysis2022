#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 21 17:33:31 2022

@author: ale
"""

import numpy as np
import h5py
import matplotlib.pyplot as plt
from lmfit.models import GaussianModel

#%%  
def opendata(runnumber, datapath, h5keys): 
    '''
    load data in H5 format
    INPUT: 
        - runnumber --> data run you want to load
        - datapath--> a string with the path of where to find the data
        - h5keys --> list containing the name of the variable to upload
        
    Returns
    -------
    list containing numpy array corresponding to the keys value in inpunt 
    
    EXAMPLE: 
        runleadcc = 124
        datapath = '/home/ale/Desktop/datiH5remoti/HDF5/run520'
        h5keys = ['xpos', 'digi_ph', 'digi_time', 'info_plus']
        datarunleadcc  = open_data(runleadcc, datapath, h5keys)
        datarunleadcc = list with size 4 containing 4 numpy array of xpos, digi_ph etc..
        

    '''
    outdata = []
    with h5py.File(datapath +f'{runnumber}.h5', 'r', libver='latest', swmr=True) as hf: 
            print("opening %s" % runnumber)
            print(hf.keys())
            for j in h5keys: 
                outdata.append(np.array(hf[j]))
            return outdata
        
#%%          

def histpos1d(datatoplot, numberoftrackers, xlabels, xlim, Bins = 150, 
              Range = [0,10],Figsize = (8,8), Histtype ='stepfilled', 
              Color = 'lightpink',Alpha = 0.7, Edgecolor='black', Lw =2):
    '''
     
    This function takes as input the (x,y) hit potions and plot the one dimention histogram 
    INPUT: 
        - dattoplot --> trackers hit potions (x,y)
        - numberoftrackers--> number of silicon trackers on the beam line
        - xlabels--> labels on the x axis (hist are plotted as couple (x,y) on the same line) 
        so it should be [silicon1. silicon1, silicon 2, silicon 2 etc..]
          y label is entries by defeault
        - xlim --> list of x limitis i.e. [xliminf, xlimsup]
        - Bins --> defeault 150
        - Range --> defeault [0,10]
        - Figsize --> defeault (8,8)
        others --> hist beauty cose di defeault
        Histtype ='stepfilled', Color = 'lightpink',Alpha = 0.7, Edgecolor='black', Lw =2
        
    Returns
    -------
     Pink 1D histogram of the silicon trackers hit position
    
    '''
    
    fig, ax = plt.subplots(numberoftrackers,2 , figsize=Figsize)
    ax = ax.flat
    for i in range(numberoftrackers*2):
        ax[i].hist(datatoplot[:,i], bins = Bins, range = Range, 
                   histtype = Histtype, color = Color ,alpha = Alpha, 
                   edgecolor= Edgecolor, lw =Lw)
        if i%2 ==0 :
            
            ax[i].set_xlabel(f' {xlabels[i]} x [cm]')
        else :
            ax[i].set_xlabel(f'{xlabels[i]} y [cm]')
            
        ax[i].grid()
        ax[i].set_xlim(xlim[i])
        ax[i].set_ylabel('Entries')
        plt.tight_layout() 
    return   plt.show()

#%%   
def histpos2d(datatoplot, numberoftrackers,  Bins = 100, 
              Range = [[0,10],[0,10]],Figsize = (8,4), Cmap = plt.cm.jet): 
    '''
     
    This function takes as input the (x,y) hit potions and plot the two dimention histogram 
    INPUT: 
        - dattoplot --> trackers hit potions (x,y)
        - numberoftrackers--> number of silicon trackers on the beam line
        - Bins --> defeault 100
        - Range --> defeault [[0,10],[0,10]]
        - Figsize --> defeault (8,4)
        - Cmap --> defeault plt.cm.jet
        
    Returns
    -------
     2D (x,y) histogram of the silicon trackers hit position 
    
    '''
    fig, ax = plt.subplots(1,numberoftrackers , figsize=Figsize)
    ax = ax.flat
    j = 0 #step on x (0,2,4,6 etc...)
    k = 1 #step on y (1,3,5,7 etc..)
    for i in range(numberoftrackers):
        _,_,_, mapable = ax[i].hist2d(datatoplot[:,j], datatoplot[:,k], 
                                      bins = Bins, range = Range, cmap = Cmap)
        plt.colorbar(mapable, ax=ax[i])
        ax[i].set_xlabel(f'BC{i+1} x[cm]')  
        ax[i].set_ylabel(f'BC{i+1} y[cm]') 
        plt.tight_layout() 
        j = j+2
        k = k+2
        
    return  plt.show()
#%%   

def divergence(datatrackers, numberoftrackers, distancesfromT1): 
    
    '''
      INPUT: 
        - datatrackers --> trackers hit potions (x,y)
        - numberoftrackers--> number of silicon trackers on the beam line
        - distancesfromT1--> list with the distances in m from the first tracker
        
    Returns
    -------
     divx, divy --> list containing array of divergence for each tracker with respect to the first one
    
    '''
    divex = []
    divey = []
    # j = 2
    # k = 3
    for i in range(numberoftrackers-1): 
        # divex.append(10**6* np.arctan((datatrackers[:,j]- datatrackers[:,0] )/(distancesfromT1[i])))
        # divey.append(10**6* np.arctan((datatrackers[:,k]- datatrackers[:,1] )/(distancesfromT1[i])))
        divex.append(np.arctan((datatrackers[:,2*(i+1)]- datatrackers[:,0] )/(distancesfromT1[i])))
        divey.append(np.arctan((datatrackers[:,2*(i+1)+1]- datatrackers[:,1] )/(distancesfromT1[i])))
        # j = j+2
        # k = k+2
        
    divex = np.vstack(divex)
    divey = np.vstack(divey)
    
    return divex, divey

#%%   

def divgaussianfit(datatrackers, numberoftrackers, distancesfromT1, p0divx, p0divy,
                   Rangex, Rangey,
                   model = GaussianModel(),  Figsize = (8,6), 
                   Bins = 80, Lw = 2, Histtype='stepfilled', 
                   Color = 'forestgreen',Alpha=0.9, Edgecolor='black', 
                   Fontsize = 12): 
    '''
      INPUT: 
        - datatrackers --> trackers hit potions (x,y)
        - numberoftrackers--> number of silicon trackers on the beam line
        - distancesfromT1--> list with the distances in cm from the first tracker
        - p0divx--> guess parameter for guassian fit, [amplitude, meanvalue, sigma] 
                    possible values are [2000, 0, 500]
        - p0divy--> same for y 
        - model --> lmfit.models, defeault gaussian
        - Range --> range on x for the i-esimo tracker i.e. range = [[xinfTi, xsupTi]]
        - Bins --> defeault 80
        - others matplotlib belle cose
        
    Returns
    -------
    -divx, divy in radianti
     -mu --> contiene i valori medi delle divergenze in coppia (x,y) ottenuti dai fit delle divergenze 
             i.e [mudivxT1T2, mudivyT1T2, mudivxT1T3, etc...]
     -errmu--> errori dei valori  medi restituiti da lmfit
     -plot delle divergenze con i fit
     
    
    '''
    
    
    divex = []
    divey = []
    mu = []
    sigma = []
    errsigma = []
    #serie di indici poco intelligenti per avere i plot in ordine (x,y)

    l = 0
    h = 1
    fig, ax = plt.subplots(numberoftrackers-1,2, figsize=Figsize) 
    ax = ax.flat
    for i in range(numberoftrackers-1): 
        # divex.append(10**6* np.arctan((datatrackers[:,j]- datatrackers[:,0] )/(distancesfromT1[i])))
        # divey.append(10**6* np.arctan((datatrackers[:,k]- datatrackers[:,1] )/(distancesfromT1[i])))
        divex.append(np.arctan((datatrackers[:,2*(i+1)]- datatrackers[:,0] )/(distancesfromT1[i])))
        divey.append(np.arctan((datatrackers[:,2*(i+1)+1]- datatrackers[:,1] )/(distancesfromT1[i])))

        #istogramma per div x
        hdivx, binsx,_ = ax[l].hist(divex[i],
                                    bins = Bins, lw = Lw, histtype = Histtype, 
                                    color = Color, alpha = Alpha, edgecolor = Edgecolor, 
                                     range = Rangex[i])
        xstepdivx = np.array([(binsx[k+1]+binsx[k])/2 for k in range(len(binsx)-1)])
        
        #istogramma per div y
        hdivy, binsy,_ = ax[h].hist(divey[i],
                                    bins = Bins, lw = Lw, histtype = Histtype, 
                                    color = Color, alpha = Alpha, edgecolor = Edgecolor, 
                                     range = Rangey[i])
        xstepdivy = np.array([(binsy[k+1]+binsy[k])/2 for k in range(len(binsy)-1)])
        #cerco parametri per fit x e y
        paramsx = model.guess(hdivx, x=xstepdivx)
        resultx = model.fit(hdivx, paramsx, x=xstepdivx) #, weights = errx)
        paramsy = model.guess(hdivy, x=xstepdivy)
        resulty = model.fit(hdivy, paramsy, x=xstepdivy) #, weights = erry)
        #plot
        # sigmamuradx = (resultx.params.get("sigma")).value*10**6
        # sigmaerrmuradx = (resultx.params["sigma"].stderr)*10**6
        # sigmamurady = (resulty.params.get("sigma")).value*10**6
        # sigmaerrmurady = (resulty.params["sigma"].stderr)*10**6
        sigmamuradx = (resultx.params.get("sigma")).value
        sigmaerrmuradx = (resultx.params["sigma"].stderr)
        sigmamurady = (resulty.params.get("sigma")).value
        sigmaerrmurady = (resulty.params["sigma"].stderr)
        ax[l].plot(xstepdivx, resultx.best_fit, label=f'$\sigma$ = {round(sigmamuradx,4)} $\pm$ {round(sigmaerrmuradx,4)} rad ', color = 'red', lw = Lw)
        ax[h].plot(xstepdivy, resulty.best_fit, label=f'$\sigma$ = {round(sigmamurady,4)}  $\pm$ {round(sigmaerrmurady,4)} rad', color = 'red', lw = Lw)
        ax[l].set_xlabel('x divergence [rad]',fontsize=Fontsize)
        ax[h].set_xlabel('y divergence [rad]',fontsize=Fontsize)
        ax[l].set_ylabel('Counts',fontsize=Fontsize)
        ax[h].set_ylabel('Counts',fontsize=Fontsize)
        ax[l].grid()
        ax[h].grid()
     
        ax[l].legend(fontsize=Fontsize)
        ax[h].legend(fontsize=Fontsize)
        plt.tight_layout()
        #appendo a sigma prima sigmax e poi sigmay così da averli a coppie
        sigma.append(resultx.params.get("sigma").value)
        sigma.append(resulty.params.get("sigma").value)
        errsigma.append(resultx.params["sigma"].stderr)
        errsigma.append(resulty.params["sigma"].stderr)
        
        mu.append(resultx.params.get("center").value)
        mu.append(resulty.params.get("center").value)
        l = l+2
        h = h+2
        
        plt.tight_layout()
     
    return np.array(divex), np.array(divey), sigma, errsigma, mu, ax
    
 #%%   
def aligntracker(datatrackers, numberoftrackers, mudiv, distancesfromT1):
    '''
      INPUT: 
        - datatrackers --> trackers hit potions (x,y)
        - numberoftrackers--> number of silicon trackers on the beam line
        - mudiv --> valori medi delle divergenze (a coppie x,y--> [muxT1T2, muyT1T2, muxT1T3 etc..]
                                                  così come restituiti dalla funzione  divgaussianfit
        - distancesfromT1--> list with the distances in m from the first tracker
        
        
    Returns
    -------
    - datashift --> coordinate x e y di hit sui tracciatori già allineate 
    - shift--> i fattori di shift applicati 
     
    
    '''
    shift = []
    zBC = []
    for k in distancesfromT1:
        zBC.extend([k,k])
    shift.append(0)
    shift.append(0)
    datashift = np.zeros([len(datatrackers), numberoftrackers*2])
    for i in range(len(mudiv)): 
        shift.append(np.tan(mudiv[i])*zBC[i])
         
    for j in range(numberoftrackers*2):
        datashift[:,j] = (datatrackers[:,j]-shift[j])
         
         
    return datashift, shift
 #%%   
    
def timephplot(time, ph, ax, timethresholdinf, timethresholdsup, Bins = [100,100], 
              Range = [[0,500],[0,15000]], Cmap = plt.cm.jet, Title = ''):
    '''
      INPUT: 
        - time --> array with time
        - ph ---> array pulse high
        - ax--> matplotlib subplots: e.g fig, ax =  plt.subplots(1,1 , figsize=(15,5))
        - timethresholdinf --> int value of time inf threshold
        - timethresholdsup--> int value of time sup th
        - bins--> bins of the 2d hist
        - range --> [[xrage], [yrange]]
        - Cmap--> defeault jet
        - title--> string 
        
    Returns
    -------
    2d histogram of PH and time with time threshold 
     
    
    '''
    _,_,_, mapable = ax.hist2d(time, ph, 
                                  bins = Bins, range = Range, cmap = Cmap)
    plt.colorbar(mapable, ax=ax)
    ax.axvline(x=timethresholdinf, color="red", label = 'threshold')
    ax.axvline(x = timethresholdsup, color="red")
    ax.set_xlabel('Time [ns]')
    ax.set_ylabel('PH')
    ax.set_title(Title)
    ax.legend(fontsize = 10)
    ax.grid()
    plt.tight_layout()
    return plt.show()
    
def calibgaussianfit(ph, ax, xfitrangeinf, xfitrangesup, Range = [0,15000], Bins = 100, 
                     Label = '', model = GaussianModel(),Histtype='stepfilled', Color = 'midnightblue', Alpha=0.9,
                     Edgecolor='black', Colorfit = 'red', labelfit = 'fit'):
    
    '''
      INPUT: 
        - ph ---> array pulse high
        - ax--> matplotlib ax
        - xfitrangeinf ---> int inferiore da  cui parte il fit 
        - xfitrangesup --> int superiore dove finisce il fit
        - Range --> hist x range
        - Bins --> defeault = 100
        - Label--> string
        - model --> modello fit, defeault GaussianModel()
        
    Returns
    -------
    hist ph with gaussian fit, return the gaussian mean value 
     
    
    '''
    hph, binsph, _ = ax.hist(ph, bins = Bins, label = Label, range = Range,
                             color = Color, alpha = Alpha, edgecolor = Edgecolor,
                             histtype = Histtype)
    
    xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
    phcondition =  ((xdata > xfitrangeinf) & (xdata<xfitrangesup))
    paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
    resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                        weights = np.sqrt(hph[phcondition]))
    to_plot = np.linspace(0,14000,10000)
    y_eval = model.eval(resultx.params, x= to_plot)
    # ax.plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = Colorfit)
    ax.plot(to_plot, y_eval, linewidth = 2, color = Colorfit, label = labelfit)
    energyadcfit = resultx.params['center'].value
    energyadcfiterr = resultx.params['center'].stderr
    ax.legend()    

    ax.grid()
    
    return (energyadcfit, energyadcfiterr)



def calibrationCalo(lglabelch, data, intercept, slope):
    phcalo = {}
    for i in lglabelch:
        phcalo[i] = data[1][:,lglabelch[i]]
    
    phcaloGeV = {}
    for label in (lglabelch):
        phcaloGeV[label] = (phcalo[label]-intercept[label])/slope[label]
        
    return phcaloGeV

 #%%   

def hist2dToProfile(
        hist2d,
        errType = ""
):

    xVal = []
    yVal = []
    yErr = []
    for i in range(len(hist2d[1])-1):
        yNum = 0
        yErrNum = 0
        yDenom = 0
        for j in range(len(hist2d[2])-1):
            yNum += hist2d[0][i][j] * (hist2d[2][j] + (hist2d[2][j+1] - hist2d[2][j])/2)
            yDenom += hist2d[0][i][j]
        if yDenom != 0:
            for j in range(len(hist2d[2])-1):
                yErrNum += hist2d[0][i][j] * \
                           ((hist2d[2][j] + (hist2d[2][j + 1] - hist2d[2][j]) / 2) - yNum/yDenom) ** 2
            yErrDenom = yDenom - 1
            xVal.append(hist2d[1][i] + (hist2d[1][i+1] - hist2d[1][i])/2)
            yVal.append(yNum/yDenom)
            if yDenom > 1:
                if errType == "std":
                    yErr.append(np.sqrt(yErrNum/yErrDenom))
                elif errType == "mean":
                    yErr.append(np.sqrt(yErrNum/yErrDenom) / np.sqrt(yDenom))
                else:
                    yErr.append(0)
            else:
                yErr.append(0)
    return np.array(xVal), np.array(yVal), np.array(yErr)
#%%
def truebins(bins):
    # Shift the binning edges and remove the last point
    step = np.abs(bins[1]-bins[0])/2
    tbins = bins[:-1]+step
    return(tbins)


