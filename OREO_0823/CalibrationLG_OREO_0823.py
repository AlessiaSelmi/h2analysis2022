#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 13 10:29:27 2023

@author: ale
"""
### calibrazione unico LG con tagli in divergenza e tempo 
## c'è un po' di casino per avere le due rette sia a 0.5 di range che a 2
from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
model = GaussianModel()
plt.rcParams['font.size'] = '20'
#triggere APC + Cherry
# Ch 0 -->  XCET 48 (Cherry 2)
# Ch 1 --> LG1 (Door / T10 side)
# Ch 2 --> George 1
# Ch 3 --> George 2
# Ch 4 --> John 1
# Ch 5 --> John 2
# Ch 6 --> Paul 1
# Ch 7 --> Paul 2

chLabel = {'Cherry':0, 'LG':1, 'George1':2, 'George2':3, 'John1':4, 'John2':5, 'Paul1':6, 'Paul2':7}
runnumbers = [run for run in range(299, 305)]+[306,307,308] #calibrazione range 0.5
datapath = r'C:\Users\aselmi\Desktop\Dottorato\data\dataOREO_0823\HDF5\run680'
h5keys = [ 'xpos','digiPH', 'digiTime']
energies = ['1','2','3','4','5','6', '10', '12', '15']
energies_num = [1,2,3,4,5,6,10,12,15]


distancesfromT1 = [500]
zcalo = 500+25.5
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]
divsup = 0.002
divinf = -0.002
timethsup = 210
timethinf = 170
thcherry = 200

fitrangecalibinf = [0,1000,3000, 4000, 5000, 6000, 2000,3000,4000]
fitrangecalibsup = [2000, 3000, 5000, 6000, 8000, 9000, 4000, 5000,6000]


Colors = [ 'darkred', 'orangered',  'lime', 'turquoise','hotpink',  'indigo']

mucalib = {}
muerrcalib = {}
cutdiv = {}
cuttime = {}
cuttot = {}
data = {}
cutcherry = {}
sigmadivx_ene = []
errsigmadivx_ene = []


for i, nrun in enumerate(runnumbers):
    #carico dati
    data[nrun] = opendata(nrun, datapath, h5keys)
    #divergenza
    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(data[nrun][0], 2, distancesfromT1,
                                                            p0divx, p0divy,
                                                            Rangex=[[-0.02,0.02]],
                                                            Rangey=[[-0.02,0.02]])
    
    
    datashift, shift = aligntracker(data[nrun][0], 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                                    distancesfromT1,
                                                                                    p0divx, p0divy,
                                                                                    Rangex=[[-0.004,0.004]],
                                                                                    Rangey=[[-0.004,0.004]], 
                                                                                    Color='navy', 
                                                                                    Figsize = (8,6))
    
    sigmadivx_ene.append(sigmaalign)
    errsigmadivx_ene.append(errsigmaalign)
    #tagli in divergenza
    cutdiv[nrun] = ((divexalign >divinf) & (divexalign<divsup) & (diveyalign>divinf) &(diveyalign<divsup))
    ax[0].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=divsup, color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=divsup, color="black", linestyle = '--', lw = 2)    
    ax[0].set_title(f'Lead Glass, {energies[i]} GeV ')
    #plot PH tempo
    import copy 
    import matplotlib as mpl
    my_cmap = copy.copy(mpl.cm.jet)
    my_cmap.set_bad(my_cmap(0))
    fig, ax = plt.subplots(1, 1, figsize=(8, 6))
    zhist2d, xhist2d, yhist2d, mapable  = ax.hist2d(data[nrun][1][:,chLabel['LG']],data[nrun][2][:,chLabel['LG']] ,
                bins =[100,100], cmap = my_cmap, range = [[0, 12000], [100, 300]], norm=colors.LogNorm())
    #taglio in tempo
    cuttime[nrun] = ((data[nrun][2][:,chLabel['LG']]<timethsup) & (data[nrun][2][:,chLabel['LG']]>timethinf))
    ax.axhline(y=timethinf, color="red", linestyle = '--', label = f'Threshold \n {energies[i]} GeV', lw = 2)
    ax.axhline(y=timethsup, color="red", linestyle = '--', lw = 2)
    ax.grid()
    ax.legend()
    ax.set_xlabel('PH [a.u]')
    ax.set_ylabel('time [ns]')
    fig.colorbar(mapable,ax=ax)
    #ax.set_title(f'Lead Glass {energies[i]} GeV')
    plt.tight_layout()
    #if energies[i] == '6': 
        #plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/t9/PHvsTime_6GeV.pdf')
    plt.show()
    
    #ricostruzione su faccia del LG
    xcalo = datashift[:, 0] + zcalo*np.tan(divexalign)
    ycalo = datashift[:, 1] + zcalo*np.tan(diveyalign)
    
    fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xcalo[0], ycalo[0],
                                                    bins=[100, 100], cmap=plt.cm.jet, range=[[0, 10], [0, 10]])
    
    ax.set_xlabel(f'x [cm], {energies[i]} GeV')
    ax.set_ylabel(f'y [cm], {energies[i]} GeV')
    fig.colorbar(mapable,ax=ax)

    # profilo cherenkov
    cutcherry[nrun] = ((data[nrun][1][:,chLabel['Cherry']]>thcherry))
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    
    ax.hist((data[nrun][1][:,chLabel['Cherry']]),
                 bins = 100, histtype ='step', color = 'teal', 
                 alpha = 1, lw = 3 ,label = f'Cherenkov \n {energies[i]}  GeV', range = [0, 6000])
    ax.axvline(x=thcherry, color="black", linestyle = '--', label = 'threshold', lw = 3)
    ax.grid()
    ax.legend()
    ax.set_xlabel(' PH [a.u]')
    ax.set_ylabel('Counts')
    ax.set_yscale('log')
    plt.tight_layout()
    #if energies[i] == '6': 
        #plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/t9/cherenkov_6GeV.pdf')
    plt.show()
    #taglio totale divergenza e tempo
    cuttot[nrun] = (cutdiv[nrun][0] & cuttime[nrun] & cutcherry[nrun])

#plot divergenza in funzione dell'energia
fig, ax = plt.subplots(1, 1, figsize=(8, 6)) 
sigmax_lst = []
sigmay_lst = []

for j in range(9):
    sigmax_lst.append(sigmadivx_ene[j][0]*10**(3))
    sigmay_lst.append(sigmadivx_ene[j][1]*10**(3))
    
ax.plot(energies_num, sigmax_lst, 
            marker='o',  linestyle = 'dotted', color = 'navy',
            markersize = 8, label = 'x ')
ax.plot(energies_num, sigmay_lst,  
            marker='D',  linestyle = 'dotted', color = 'darkgreen',
            markersize = 8, label = 'y')
   
ax.legend()
ax.grid()
ax.set_xlabel('Energies [GeV]')

ax.set_ylabel('Divergence [mrad]')
#plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/t9/T9divergence.pdf')
     
    
plt.tight_layout()

#plot di picchi fittati con una gaussiana per la calibrazione
figcal, axcal = plt.subplots(1, 1, figsize=(8, 6))   
for i, nrun in enumerate(runnumbers[:-3]):#########################################################################################à   

    hph, binsph, _ = axcal.hist(data[nrun][1][:,chLabel['LG']][cuttot[nrun]], bins = 200, histtype ='stepfilled',
                          color = Colors[i],edgecolor = Colors[i], alpha = 0.4, 
                          lw =3, 
                          label = f'{energies[i]} GeV', density = True, range = [0, 10000])

    xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
    phcondition = ((xdata > fitrangecalibinf[i]) & (xdata < fitrangecalibsup[i]))
    paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
    resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                        weights=np.sqrt(hph[phcondition]))
    to_plot = np.linspace(fitrangecalibinf[i], fitrangecalibsup[i], 10000)
    y_eval = model.eval(resultx.params, x=to_plot)
    #ax[i].plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = 'black')
    mucalib[energies[i]] = resultx.params['center'].value
    muerrcalib[energies[i]] = resultx.params['center'].stderr
    axcal.plot(to_plot, y_eval, linewidth=2, color='black', label=f'$\mu$ = {round(mucalib[energies[i]],2)} $\pm$ {round(muerrcalib[energies[i]],2)}')
    
axcal.grid()
axcal.set_xlabel('PH [a.u]')
axcal.set_ylabel('Normalized counts')
axcal.legend(fontsize = 14)
plt.tight_layout()
axcal.set_ylim(0, 0.0036)
axcal.set_ylim(0, 0.010)
#plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/t9/T9calibrationPeaks.pdf')
plt.show()



#########################################################################
#########################################################################ààà
############################################################################
#plot di picchi fittati con una gaussiana per la calibrazione
figcal, axcal = plt.subplots(1, 1, figsize=(12, 10))   
for i, nrun in enumerate(runnumbers[-3:]):#########################################################################################à   
    print(i)
    hph, binsph, _ = axcal.hist(data[nrun][1][:,chLabel['LG']][cuttot[nrun]], bins = 200, histtype ='stepfilled',
                          color = Colors[i+3],edgecolor = Colors[i+3], alpha = 0.4, 
                          lw =3, 
                          label = f'{energies[i+6]} GeV', density = True, range = [0, 10000])

    xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
    phcondition = ((xdata > fitrangecalibinf[i+6]) & (xdata < fitrangecalibsup[i+6]))
    paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
    resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                        weights=np.sqrt(hph[phcondition]))
    to_plot = np.linspace(fitrangecalibinf[i+6], fitrangecalibsup[i+6], 10000)
    y_eval = model.eval(resultx.params, x=to_plot)
    #ax[i].plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = 'black')
    mucalib[energies[i+6]] = resultx.params['center'].value
    muerrcalib[energies[i+6]] = resultx.params['center'].stderr
    axcal.plot(to_plot, y_eval, linewidth=2, color='black', label=f'$\mu$ = {round(mucalib[energies[i+6]],2)} $\pm$ {round(muerrcalib[energies[i+6]],2)}')
    
axcal.grid()
axcal.set_xlabel('PH [a.u]')
axcal.set_ylabel('Counts')
axcal.legend(fontsize = 18)
figcal.set_tight_layout('tight')
plt.show()


# Plot retta di calibrazione per energie range 0.5 V 

mucaliblist = []   
errmucaliblist = []

for energy in energies[:-3]: 

    mucaliblist.append(mucalib[energy])
    errmucaliblist.append(muerrcalib[energy])
    
    
figline, axline = plt.subplots(1,1, figsize=(8,8))
from lmfit.models import LinearModel
linearmodel = LinearModel()     
linearparams = linearmodel.guess(mucaliblist, x = energies_num[:-3])
linearresult = linearmodel.fit(mucaliblist, linearparams, x= energies_num[:-3], weights = errmucaliblist)
to_plot = np.linspace(0, 7, 10000)
y_eval = linearmodel.eval(linearresult.params, x=to_plot)
axline.errorbar(energies_num[:-3], mucaliblist, yerr= errmucaliblist, 
            marker='D',  linestyle = 'none', color = 'navy',
            markersize = 9)

axline.plot(to_plot,y_eval, color = 'red', lw = 2,
        label = f' a.u. = ({round(linearresult.params["slope"].value,2)} $\pm$ {round(linearresult.params["slope"].stderr,2)})$\cdot$ GeV +  ({round(linearresult.params["intercept"].value,2)}  $\pm$ {round(linearresult.params["intercept"].stderr,2)})')
axline.legend(fontsize = 15)
axline.set_ylabel('PH [a.u.]')
axline.set_xlabel('GeV')
slopeCalib23=(linearresult.params['slope'].value)
errslopeCalib23 = (linearresult.params['slope'].stderr)
interceptCalib23= (linearresult.params['intercept'].value)
errinterceptCalib23 = (linearresult.params['intercept'].stderr)
print(f'Chi-square = {linearresult.chisqr:.4f}, Reduced Chi-square = {linearresult.redchi:.4f}')

#calcolo dei residui
residual_lowE = 100* ( mucaliblist-linearresult.best_fit)/(linearresult.best_fit)


calib = [slopeCalib23, errslopeCalib23, interceptCalib23, errinterceptCalib23]

with open('LG_CalibrationOREO23_05V.dat','w') as out_file:
        np.savetxt(out_file, calib)
        
## attenzione che hanno gli stessi nomi-- qui faccio retta per i tre ad alta energia con range 2V

############### I NOMI SONO A CASOOOOOOOO TI ODIOOOOOOOOOOOOOOOOOOOOOOO########        







#  retta di calibrazione per energie con range 2V 

mucaliblist5V = []   
errmucaliblist5V = []

for energy in energies[-3:]: 

    mucaliblist5V.append(mucalib[energy])
    errmucaliblist5V.append(muerrcalib[energy])
    
    
from lmfit.models import LinearModel
linearmodel = LinearModel()     
linearparams = linearmodel.guess(mucaliblist5V, x = energies_num[-3:])
linearresult = linearmodel.fit(mucaliblist5V, linearparams, x= energies_num[-3:], weights = errmucaliblist5V)
to_plot = np.linspace(0, 15, 10000)
y_eval = linearmodel.eval(linearresult.params, x=to_plot)
axline.errorbar(energies_num[-3:], mucaliblist5V, yerr= errmucaliblist5V, 
            marker='D',  linestyle = 'none', color = 'darkgreen',
            markersize = 9)
axline.plot(to_plot,y_eval, color = 'red', lw = 2, ls = '--',
        label = f' a.u. = ({round(linearresult.params["slope"].value,2)} $\pm$ {round(linearresult.params["slope"].stderr,2)})$\cdot$ GeV +  ({round(linearresult.params["intercept"].value,2)}  $\pm$ {round(linearresult.params["intercept"].stderr,4)})')
axline.legend(fontsize = 15)
axline.set_ylabel('PH [a.u.]')
axline.set_xlabel('GeV')
axline.grid()
slopeCalib23=(linearresult.params['slope'].value)
errslopeCalib23 = (linearresult.params['slope'].stderr)
interceptCalib23= (linearresult.params['intercept'].value)
errinterceptCalib23 = (linearresult.params['intercept'].stderr)
print(f'Chi-square = {linearresult.chisqr:.4f}, Reduced Chi-square = {linearresult.redchi:.4f}')
plt.tight_layout()
#plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/t9/T9calibration_line.pdf')
plt.show()

#calcolo dei residui
residual_5V = 100* (mucaliblist5V-linearresult.best_fit)/(linearresult.best_fit)

calib5V= [slopeCalib23, errslopeCalib23, interceptCalib23, errinterceptCalib23]

with open('LG_CalibrationOREO23_2V.dat','w') as out_file:
        np.savetxt(out_file, calib5V)
        
        

fig, ax = plt.subplots(1,1, figsize=(10,4))
ax.plot(energies_num[-3:]
        , residual_5V, 'D', markersize = 8, color = 'darkgreen', label = 'Dynamic range 0.5 V')
ax.axhline(y= 0, ls = ':', c = 'k', lw = 3)

ax.set_xlabel('GeV')
ax.set_ylabel('Residual $\%$')
ax.set_ylim([-0.5, 0.5])
ax.grid()

plt.tight_layout()
#plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/t9/T9residual1.pdf')
plt.show()


fig, ax = plt.subplots(1,1, figsize=(10,4))
ax.plot(energies_num[:-3]
        , residual_lowE, 'D', markersize = 8, color = 'navy', label = 'Dynamic range 2 V')
ax.axhline(y= 0, ls = ':', c = 'k', lw = 3)

ax.set_xlabel('GeV')
ax.set_ylabel('Residual $\%$')
ax.set_ylim([-1.5, 1.5])
ax.grid()

plt.tight_layout()
#plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/t9/T9residual2.pdf')
plt.show()

#%%
### risoluzione energetica LG ---> 
##sono stupida e quindi ho fatto questo casino (stavo calcolando la std dell'errore non la std per fare la risoluzione)
LG_calib05V = np.loadtxt(r'C:\Users\aselmi\Desktop\Dottorato\analysis\OREO_0823\LG_CalibrationOREO23_05V.dat')
LG_calib2V = np.loadtxt(r'C:\Users\aselmi\Desktop\Dottorato\analysis\OREO_0823\LG_CalibrationOREO23_2V.dat')

muE = []
muEerr = []
sigmaE = []
sigmaEerr = []
fitrangecalibinf = [0.5, 1.0, 2.0, 3.0, 4.0, 5.0,8.0, 10.0, 13.0 ]
fitrangecalibsup = [2.0, 3.0, 4.0, 5.0,9.0, 11.0, 14.0, 16, 18, 20 ]

#picchi in energia
figcal, axcal = plt.subplots(1, 1, figsize=(12, 10))   
for i, nrun in enumerate(runnumbers):#########################################################################################à   
    print(i)
    if i>5 : 
        LGcalib_fact = LG_calib2V
    else : 
        LGcalib_fact = LG_calib05V
    hph, binsph, _ = axcal.hist((data[nrun][1][:,chLabel['LG']][cuttot[nrun]]-LGcalib_fact[2])/LGcalib_fact[0], bins = 200, histtype ='stepfilled',
                         alpha = 0.4, 
                          lw =3, 
                          label = f'{energies[i]} GeV', density = True)
    xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
    phcondition = ((xdata > fitrangecalibinf[i]) & (xdata < fitrangecalibsup[i]))
    paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
    resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                            weights=np.sqrt(hph[phcondition]))
    to_plot = np.linspace(fitrangecalibinf[i], fitrangecalibsup[i], 10000)
    y_eval = model.eval(resultx.params, x=to_plot)
    #ax[i].plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = 'black')
    muE.append(resultx.params['center'].value)
    muEerr.append(resultx.params['center'].stderr)
    sigmaE.append(resultx.params['sigma'].value)
    sigmaEerr.append(resultx.params['sigma'].stderr)
    axcal.plot(to_plot, y_eval, linewidth=2, color='black', label=f'$\mu$ = {round(resultx.params["center"].value,2)} $\pm$ {round(resultx.params["sigma"].value,2)}') 
    

axcal.grid()
axcal.set_xlabel('PH [a.u]')
axcal.set_ylabel('Counts')
axcal.legend(fontsize = 18)
figcal.set_tight_layout('tight')
plt.show()

#%%  calcoliamo la risoluzione  energetica del LG 
plt.rcParams['font.size'] = '20'
energy_resolution = []
energy_resolutionerr = []
for k in range(len(muE)): 
    energy_resolution.append((sigmaE[k]/muE[k])*100)
    energy_resolutionerr.append((np.sqrt((sigmaEerr[k]/sigmaE[k])**2+(muEerr[k]/muE[k])**2)*energy_resolution[k]))


def energy_resolution_function(E, a, b, c):
    return np.sqrt( (a**2 / E) + (b / E)**2+ c**2)

from lmfit import Model
fig, ax = plt.subplots(1,1, figsize=(8,6))
model = Model(energy_resolution_function)
params = model.make_params(a=1.0, b=1.0, c=1.0)
result = model.fit(energy_resolution, params, E=energies_num, weights=energy_resolutionerr)
a_fit = result.params['a'].value
b_fit = result.params['b'].value
c_fit = result.params['c'].value

sigma_a = result.params['a'].stderr
sigma_b = result.params['b'].stderr
sigma_c = result.params['c'].stderr
to_plot = np.linspace(min(energies_num), max(energies_num), 1000)
fit_energy_resolution = energy_resolution_function(to_plot, a_fit, b_fit, c_fit)

ax.errorbar(energies_num, energy_resolution,yerr=energy_resolutionerr, marker = 'D', markersize = 10
            , color = 'navy',
            linestyle= 'none', label = '$\sigma$(E)/E')


ax.plot(to_plot, fit_energy_resolution, label = '$\sigma$(E)/E = $\sqrt{(a^{2} / E) + (b / E)^2+ c^2)}$' , color = 'red')
    
ax.set_xlabel('GeV')
ax.set_ylabel('Energy resolution [%]')
ax.legend(fontsize = 18)
ax.grid()
plt.tight_layout()
plt.savefig(r'C:\Users\aselmi\Desktop\Dottorato\Technical Note Oreo2023\t9\T9_LGenergyResolution.pdf')
plt.show()


print(f'a = {a_fit}, b = {b_fit}, c = {c_fit}  ')
print(f'a = {sigma_a}, b = {sigma_b}, c = {sigma_c}  ')
# %%
