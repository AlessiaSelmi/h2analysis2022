#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 13 10:17:12 2023

@author: ale
"""
##EQUALIZZAZIONE E CALIBRAZIONE

# facciamo questo lavoro solo sulla prima matrice di ogni canale. Vogliamo equalizzare tutte e tre i canali; 
# prodiamo in due step: il primo è trovare tre q da togliere  alla Ph di tutti e tre: fitto con ladau 
# i tre picchi sia con run front sia con fascio che arriva dal lato. Faccio una retta PH spessore attraversato vs 
# energia depositata (PH). Una volta trovare le tre q prendo di nuovo la run front e tolgo a 
# ciascuna PH la sua q. Fitto queste landau (PHi-qi). Con i most probable value calcolo i fattori di equalizzazione
#Una volta equalizzati posso sommare (PHi-qi)*eqfact. 
# Per calibrare: so quanto rilascia una Mip nei due spessori; quindi faccio retta con energia teorica depositata 
# nei due spessori e le PH appena calcolate. 
#

from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
import uproot
from lmfit.models import LinearModel

plt.rcParams['font.size'] = '15'
#triggere APC + Cherry
# Ch 0 -->  XCET 48 (Cherry 2)
# Ch 1 --> LG1 (Door / T10 side)
# Ch 2 --> George 1
# Ch 3 --> George 2
# Ch 4 --> John 1
# Ch 5 --> John 2
# Ch 6 --> Paul 1
# Ch 7 --> Paul 2

datapath = r'C:\Users\aselmi\Desktop\Dottorato\data\dataOREO_0823/run680'
h5keys = [ 'xpos','digiPH', 'digiTime']

chLabel = {'Cherry':0, 'LG':1, 'George1':2, 'George2':3, 'John1':4, 'John2':5, 'Paul1':6, 'Paul2':7}
SiPMlabel = ['George1','John1', 'Paul1']
allSiPMLabel = ['George','John','Paul']
runnumber = [244, 245]

distancesfromT1 = [500]
zcryst = 500+25
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]

### varie threshold per i tagli 
thcherryinf = 0
thcherrysup = 200
divsup = 0.0015
divinf = -0.0015

timethsup = [240, 240, 240]
timethinf = [200, 200, 200]
phth = [400, 400, 400]

crystlenght = 2
 # per il cut fiduciale. La vera lunghezza è 2.5

xcrystinf = -3
xcrystsup = xcrystinf+crystlenght
ycrystinf= -1
ycrystsup = ycrystinf+crystlenght

Colors = ['lime', 'hotpink','orangered']

mpv = []
mpvstd = []
# landau limiti per fit per le due run per i tre canali 
limInf = [[0, 100, 100], [0, 0, 0]]
limSup = [[400, 400, 300], [300, 300,300]]
fitPar = {244:[[150, 150,100]]*3, 245: [[350, 100,100]]*3}
RangeLan = {244: [100, 400], 245:[50,400]}

data = {}
cutdiv = {}
cuttime = {}
cutph = {}
cutcherry = {}
mpv_simulation = []
mpvstd_simulation = []
#%%
#apertura dati + profilo camere 
#############################################
for k, nrun in enumerate(runnumber):
    #apertura dati + plot camere 
    data[nrun] = opendata(nrun, datapath, h5keys)
    xpos = data[nrun][0]
    #camere 
    xlabels = ['BC1',' BC1', 'BC2', 'BC2']
    xlim = [[-0,10]]*4
    histpos1d(xpos, 2, xlabels = xlabels, xlim = xlim, Range = [0,10])
    histpos2d(xpos, numberoftrackers = 2)
    #correlazioni camere 
    fig, ax = plt.subplots(1,2 , figsize=(8,8))
    ax = ax.flat
    ax[0].hist2d(xpos[:,0], xpos[:,2], bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet)
    ax[0].set_ylabel('x BC2 [cm]')
    ax[0].set_xlabel('x BC1 [cm]')
    
    ax[1].hist2d(xpos[:,1],xpos[:,3] , bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet,)
    ax[1].set_xlabel('y BC1 [cm]')
    ax[1].set_ylabel('x BC2 [cm]')
    plt.tight_layout()
    plt.show()
    #%%
    #divergenze
    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(xpos, 2, distancesfromT1,
                                                           p0divx, p0divy,
                                                           Rangex=[[-0.02,0.02]],
                                                           Rangey=[[-0.02,0.02]])
    
    
    datashift, shift = aligntracker(xpos, 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                                   distancesfromT1,
                                                                                    p0divx, p0divy,
                                                                                    Rangex=[[-0.004,0.004]],
                                                                                    Rangey=[[-0.004,0.004]], 
                                                                                    Color='navy', 
                                                                                    Figsize = (8,6))
    cutdiv[nrun] = ((divexalign >divinf) & (divexalign<divsup) & (diveyalign>divinf) &(diveyalign<divsup))
    ax[0].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=divsup, color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=divsup, color="black", linestyle = '--', lw = 2)   
    
    #%%
    # profilo cherenkov prendo i muoni 
    cutcherry[nrun] = ((data[nrun][1][:,chLabel['Cherry']]>thcherryinf)&(data[nrun][1][:,chLabel['Cherry']]<thcherrysup))
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    
    ax.hist((data[nrun][1][:,chLabel['Cherry']]),
                 bins = 100, histtype ='stepfilled', color = 'turquoise', edgecolor = 'turquoise', 
                 alpha = 0.5, lw = 3 ,label = 'Cherry', range = [0, 6000])
    ax.axvline(x=thcherryinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax.axvline(x=thcherrysup, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax.grid()
    ax.set_xlabel('Cherenkov PH [a.u]')
    ax.set_ylabel('Entries [ns]')
    ax.set_yscale('log')
    fig.set_tight_layout('tight')
    
    plt.show()
    #%%
    # plot PH e tempo + tagli su tempi e PH dei tre cristalli 
    fig, ax = plt.subplots(1, 3, figsize=(10, 6))
    ax = ax.flat
    for i in range(3):
        ax[i].hist2d(data[nrun][1][:,chLabel[SiPMlabel[i]]],data[nrun][2][:,chLabel[SiPMlabel[i]]] ,
                     bins =[100,100], cmap = plt.cm.jet, range = [[0, 8000], [150, 300]], norm=colors.LogNorm())
        ax[i].axhline(y=timethinf[i], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[i].axhline(y=timethsup[i], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[i].axvline(x=phth[i], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[i].set_ylabel('Entries')
        ax[i].set_xlabel('PH [a.u]')
        ax[i].set_title(f'{SiPMlabel[i]}')
        ax[i].grid()
    plt.tight_layout()
    plt.show()
    #%% faccimao un po' di tagli  su tempo e PH 
    cuttime[nrun] = [((data[nrun][2][:,chLabel['George1']]>timethinf[0]) & (data[nrun][2][:,chLabel['George1']]<timethsup[0])),
                 ((data[nrun][2][:,chLabel['John1']]>timethinf[1]) & (data[nrun][2][:,chLabel['John1']]<timethsup[1])),
                 ((data[nrun][2][:,chLabel['Paul1']]>timethinf[2]) & (data[nrun][2][:,chLabel['Paul1']]<timethsup[2]))]
    # I cristalli li vedi andando a fare i tagli sulla PH dei vari SiPM, se chiedi solo il taglio su George vedi lui 
    cutph[nrun] = ((data[nrun][1][:,chLabel['George1']]>phth[0])&
             (data[nrun][1][:,chLabel['John1']]>phth[1]) &
             (data[nrun][1][:,chLabel['Paul1']]>phth[2]) )
    
    
    #%%
    #fit picco landau dei tre canali per entrambe le run 
    from lmfit.models import ExpressionModel
    #uso l'approssimazione di Moyal 
    modLandau = ExpressionModel('A * exp(-0.5 * ((x-mpv)/width + exp(-(x-mpv)/width)))')

    fig, ax = plt.subplots(1, 3, figsize=(15, 6))
    ax = ax.flat
    
    for j, Label in enumerate(allSiPMLabel) :
    
        h, bins, _ = ax[j].hist(data[nrun][1][:,chLabel[f'{Label}1']][cuttime[nrun][j]&cutcherry[nrun]],
                    bins = 50, histtype ='stepfilled', color = Colors[j], edgecolor = Colors[j], 
                    alpha = 0.5, lw = 3 ,label = f'{Label}', range = RangeLan[nrun])
        err = h
        xdata = np.array([(bins[k+1]+bins[k])/2 for k in range(len(bins)-1)])
        phcondition = ((xdata > limInf[k][j]) & (xdata < limSup[k][j]))
        params = modLandau.make_params(A = fitPar[nrun][j][0], mpv = fitPar[nrun][j][1], width = fitPar[nrun][j][2] )
        result = modLandau.fit(h[phcondition], params, x=xdata[phcondition])
        print(f'Chi-square = {result.chisqr:.4f}, Reduced Chi-square = {result.redchi:.4f}')
        #print(result.fit_report())
        mpv.append(result.params['mpv'].value)
        mpvstd.append(result.params['mpv'].stderr)
        to_plot = np.linspace(-1000,3000,10000)
        ax[j].plot(xdata[phcondition], result.best_fit, label=f'mpv = {round(mpv[j],2)} $\pm$ {round(result.params["mpv"].stderr,2)}', color = 'black', lw = 2)
    
        ax[j].legend(fontsize = 12)
        ax[j].set_ylabel('Entries')
        ax[j].set_xlabel('PH [a.u]')
        ax[j].grid()
    plt.tight_layout()
    plt.show()



#%%
# retta spessore attraversato dal fascio ed Ph depositata nei due casi 
thick = [4.5,2.5]
intercept_eq1 = []
errintercept_eq1 = []
fig, axline = plt.subplots(3, 1, figsize=(10, 8))
axline = axline.flat
for i in range(3):      
    mpvcryst = []     
    mpvcryststd = []           
    mpvcryst.append(mpv[i])
    mpvcryst.append(mpv[i+3])
    mpvcryststd.append(mpvstd[i])
    mpvcryststd.append(mpvstd[i+3])
    
    linearmodel = LinearModel()     
    linearparams = linearmodel.guess(mpvcryst, x=thick)
    linearresult = linearmodel.fit(mpvcryst, linearparams, x = thick , weights = mpvcryststd)
    to_plot = np.linspace(0, 8, 10000)
    y_eval = linearmodel.eval(linearresult.params, x=to_plot)
    axline[i].errorbar(thick, mpvcryst, yerr = mpvcryststd, 
                marker='D',  linestyle = 'none', color = Colors[i],
                markersize = 8)
    axline[i].plot(to_plot,y_eval, color = 'black', lw = 2, ls = '--',
            label = f' m = ({round(linearresult.params["slope"].value,4)} $\pm$ {round(linearresult.params["slope"].stderr,4)}) \n q = ({round(linearresult.params["intercept"].value,4)}  $\pm$ {round(linearresult.params["intercept"].stderr,4)})')
    axline[i].legend(fontsize = 10)
    axline[i].set_ylabel('PH [a.u]')
    axline[i].set_xlabel(f'{allSiPMLabel[i]} thickness')
    slope_eq1=(linearresult.params['slope'].value)
    errslope_eq1 = (linearresult.params['slope'].stderr)
    intercept_eq1.append((linearresult.params['intercept'].value))
    errintercept_eq1.append((linearresult.params['intercept'].stderr))
    print(f'Chi-square = {linearresult.chisqr:.4f}, Reduced Chi-square = {linearresult.redchi:.4f}')

    



    ax[i].set_ylabel('PH [a.u]')
    ax[i].set_xlabel(f'{allSiPMLabel[i]} thickness')
    ax[i].grid()
plt.tight_layout()
plt.show()


with open('T9_SiPM_intercept1.dat','w') as out_file:
        np.savetxt(out_file, intercept_eq1)
#%%
## ora torniamo alla run front e plotto le tre PH a cui toglio il q appena calcolato 
## poi su questi spettri calcolo i fattori di equalizzazione 
mpv2 = []
mpv2std = []

limInf = [0, 0, 0]
limSup = [400, 400, 400]
fitPar = [[250, 100,100], [250, 100,100], [150, 20, 100]]


fig, ax = plt.subplots(1, 3, figsize=(15, 6))
ax = ax.flat

for j, Label in enumerate(allSiPMLabel) :

    h, bins, _ = ax[j].hist((data[244][1][:,chLabel[f'{Label}1']][cuttime[244][j]&cutcherry[244]])-intercept_eq1[j],
                bins = 50, histtype ='stepfilled', color = Colors[j], edgecolor = Colors[j], 
                alpha = 0.5, lw = 3 ,label = f'{Label}', range = [40, 400])
    err = h
    xdata = np.array([(bins[k+1]+bins[k])/2 for k in range(len(bins)-1)])
    phcondition = ((xdata > limInf[j]) & (xdata < limSup[j]))
    params = modLandau.make_params(A = fitPar[j][0], mpv = fitPar[j][1], width = fitPar[j][2] )
    result = modLandau.fit(h[phcondition], params, x=xdata[phcondition])
    print(f'Chi-square = {result.chisqr:.4f}, Reduced Chi-square = {result.redchi:.4f}')
    print(result.fit_report())
    mpv2.append(result.params['mpv'].value)
    mpv2std.append(result.params['mpv'].stderr)
    to_plot = np.linspace(-1000,3000,10000)
    ax[j].plot(xdata[phcondition], result.best_fit, label=f'mpv = {round(mpv[j],2)} $\pm$ {round(result.params["mpv"].stderr,2)}', color = 'black', lw = 2)

    ax[j].legend(fontsize = 12)
    ax[j].set_ylabel('Entries')
    ax[j].set_xlabel('PH [a.u]')
    ax[j].grid()
plt.tight_layout()
plt.show()
#%% calcolo eq factor
SiPM_eqfactor = []
for i in range(3): 
    SiPM_eqfactor.append(mpv2[0]/mpv2[i])

with open('T9_SiPM_eqfactor.dat','w') as out_file:
        np.savetxt(out_file, SiPM_eqfactor)
        
        
#%%
###############        CALIBRIAMOOOOOOOOOOOOOOOOOOOOOO       ##################
#Energy deposit by a MIP in PWO
# For 4.5 cm  the energy deposit would be about 46 MeV.
# For 7.5 cm , the energy deposit would be about 70 MeV.  da PDG

thicknessCalib = [4.5, 7.5]

ph_SiPM244 = {}
ph_SiPM245 = {}
for j, Label in enumerate(allSiPMLabel) :
   ph_SiPM244[Label] =  (data[244][1][:,chLabel[f'{Label}1']])-intercept_eq1[j]
for j, Label in enumerate(allSiPMLabel) :
   ph_SiPM245[Label] =  (data[245][1][:,chLabel[f'{Label}1']])-intercept_eq1[j]

#%%     sommo i tre canali dopo averli equalizzati  (ph-q) * eqfact sia per run front che per run a 90 gradi 

PH_tot244 = ph_SiPM244['George']*SiPM_eqfactor[0]+ph_SiPM244['John']*SiPM_eqfactor[1]+ph_SiPM244['Paul']*SiPM_eqfactor[2]
PH_tot245 = ph_SiPM245['George']*SiPM_eqfactor[0]+ph_SiPM245['John']*SiPM_eqfactor[1]+ph_SiPM245['Paul']*SiPM_eqfactor[2]
PH_tot = [PH_tot244, PH_tot245]

mpv_tot= []
mpvstd_tot = []

limInf = [90, 100]
limSup = [400, 500]
fitPar = [[400, 150,100], [200, 200,150]]

Colors_tot = ['green', 'steelblue']

fig, ax = plt.subplots(1, 2, figsize=(15, 6))
ax = ax.flat

for j in range(2):

    h, bins, _ = ax[j].hist(PH_tot[j],
                bins = 50, histtype ='stepfilled', color = Colors_tot[j], edgecolor = Colors_tot[j], 
                alpha = 1, lw = 3 , range = [50, 500])
    err = h
    xdata = np.array([(bins[k+1]+bins[k])/2 for k in range(len(bins)-1)])
    phcondition = ((xdata > limInf[j]) & (xdata < limSup[j]))
    params = modLandau.make_params(A = fitPar[j][0], mpv = fitPar[j][1], width = fitPar[j][2] )
    result = modLandau.fit(h[phcondition], params, x=xdata[phcondition])
    print(f'Chi-square = {result.chisqr:.4f}, Reduced Chi-square = {result.redchi:.4f}')
    print(result.params)
    mpv_tot.append(result.params['mpv'].value)
    mpvstd_tot.append(result.params['width'].value)
    to_plot = np.linspace(-1000,3000,10000)
    ax[j].plot(xdata[phcondition], result.best_fit, label=f'mpv = {round(mpv_tot[j],2)} $\pm$ {round(result.params["mpv"].stderr,2)}', color = 'black', lw = 4)

    ax[j].legend(fontsize = 20)
    ax[j].set_ylabel('Entries')
    ax[j].set_xlabel(f'OREO total PH [a.u], {thicknessCalib[j]} mm')
    ax[j].grid()
plt.tight_layout()
plt.show()


#%%
##################### simulazione##########################################àà
datapath = '/home/ale/Desktop/Dottorato/datiOreoSimulation/'
label_cryst_thickness = [44, 75]
for cryst_thick in label_cryst_thickness:
    dataAmo = uproot.open(datapath +f'T9_muons10GeV_{cryst_thick}cm_amo.root')['outData']

    simulation_xposT0 = np.array(dataAmo['Tracker_X_0'])
    simulation_yposT0 = np.array(dataAmo['Tracker_Y_0'])
    simulation_xposT1 = np.array(dataAmo['Tracker_X_1'])
    simulation_yposT1 = np.array(dataAmo['Tracker_Y_1'])
    simulation_telePos = [simulation_xposT0, simulation_yposT0, simulation_xposT1, simulation_yposT1]
    simulation_amo_crystA = np.array(dataAmo['CrystalA_EDep'])
    simulation_amo_crystB = np.array(dataAmo['CrystaB_EDep'])
    simulation_amo_crystC = np.array(dataAmo['CrystalC_EDep'])

#%%
    tele_label = ['T1, x [cm]', 'T1, y [cm]', 'T2, x [cm]', 'T2, y [cm]']
    fig, ax = plt.subplots(2, 2, figsize=(8, 8))
    ax = ax.flat
    for i in range(4):
        ax[i].hist(simulation_telePos[i], bins = 100, histtype ='step', color = 'hotpink',  
                alpha = 1, lw = 3, range = [-5,5], label = 'Simulation')
        ax[i].set_xlabel(f'{tele_label[i]}')
        ax[i].set_ylabel('Entries')
        ax[i].legend(fontsize = 10)
        ax[i].grid()
    plt.tight_layout()
    plt.show()
                     
  

#%%
    simulation_divx = (np.arctan((simulation_telePos[2]- simulation_telePos[0])/(distancesfromT1[0])))
    simulation_divy = (np.arctan((simulation_telePos[3]- simulation_telePos[1])/(distancesfromT1[0])))
    fig, ax = plt.subplots(1, 2, figsize=(10, 10))
    ax = ax.flat
    ax[0].hist(simulation_divx,  bins = 100, histtype ='step', color = 'navy',  
            alpha = 1, lw = 3, label = 'Simulation', range = [-0.004,0.004])
    ax[0].set_xlabel('x divergence')
    ax[0].set_ylabel('Entries')
    ax[0].legend()
    ax[0].grid()
    
    
    ax[1].hist(simulation_divy,  bins = 100, histtype ='step', color = 'navy',  
            alpha = 1, lw = 3, label = 'Simulation', range = [-0.004,0.004])
    ax[1].set_xlabel('y divergence')
    ax[1].set_ylabel('Entries')
    ax[1].legend()
    ax[1].grid()
#%%

    simulation_xcryst = simulation_telePos[0] + zcryst*np.tan(simulation_divx)
    simulation_ycryst = simulation_telePos[1] + zcryst*np.tan(simulation_divy)
    fig, ax = plt.subplots(1,1, figsize=(8,8))
    zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(simulation_xcryst, simulation_ycryst,
                                                   cmap=plt.cm.jet, bins=[60, 60], range=[[-5,5], [-5,5]])
    plt.colorbar(mapable, ax=ax)
    rect = patches.Rectangle((xcrystinf, ycrystinf), crystlenght*3, crystlenght, linewidth=4, edgecolor='darkred', facecolor='none')
    ax.add_patch(rect)
    ax.set_xlabel('x [cm]')
    ax.set_xlabel('y [cm]')
    ax.set_title('Proiezione su piano del cristallo, simulazione')
    plt.tight_layout()
    plt.show()
#%%

    fig, ax = plt.subplots(1, 3, figsize=(10, 6))
    ax = ax.flat
    ax[0].hist(simulation_amo_crystA, bins = 100, histtype ='step', color = 'teal',  
            alpha = 1, lw = 3, label = 'Random, crystal A', range = [0,0.2])
    ax[0].set_yscale('log')
    ax[0].legend()
    ax[0].grid()      
    ax[0].axvline(x=0.04, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].hist(simulation_amo_crystB, bins = 100, histtype ='step', color = 'indigo',  
            alpha = 1, lw = 3, label = 'Random,crystal B',  range = [0,0.5])
    ax[1].set_yscale('log')
    ax[1].legend()
    ax[1].grid()      
    ax[1].axvline(x=0.04, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[2].hist(simulation_amo_crystC, bins = 100, histtype ='step', color = 'orangered',  
            alpha = 1, lw = 3, label = 'Random, crystal C',  range = [0,0.2])
    ax[2].set_yscale('log')
    ax[2].legend()
    ax[2].grid()   
    ax[2].axvline(x=0.04, color="black", linestyle = '--', label = 'cut', lw = 2) 
    
    simulation_cutph = ((simulation_amo_crystA>0.04)|(simulation_amo_crystB>0.04)|(simulation_amo_crystC>0.04))
#%%
    fig, ax = plt.subplots(1,1, figsize=(8,8))
    zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(simulation_xcryst[simulation_cutph], 
                                                   simulation_ycryst[simulation_cutph], 
                                                   cmap=plt.cm.jet, bins=[60, 60], range=[[-5,5], [-5,5]])
    plt.colorbar(mapable, ax=ax)
    ax.set_xlabel('x [cm]')
    ax.set_xlabel('y [cm]')
    ax.set_title('Proiezione su piano del cristallo, simulazione')
    rect = patches.Rectangle((xcrystinf, ycrystinf), crystlenght*3, crystlenght, linewidth=4, edgecolor='darkred', facecolor='none')
    ax.add_patch(rect)
    plt.tight_layout()
    plt.show()
    simulation_cutpos = ((simulation_xcryst>xcrystinf) & (simulation_xcryst<(xcrystinf+crystlenght*3))
                         &(simulation_ycryst>ycrystinf) & (simulation_ycryst<(ycrystinf+crystlenght*3)))

#%%

    fig, ax = plt.subplots(1, 3, figsize=(10, 6))
    ax = ax.flat
    ax[0].hist(simulation_amo_crystA[simulation_cutpos&simulation_cutph], bins = 100, histtype ='step', color = 'teal',  
            alpha = 1, lw = 3, label = 'Random, crystal A', range = [0,0.2])
    ax[0].set_yscale('log')
    ax[0].legend()
    ax[0].grid()      
    
    
    ax[1].hist(simulation_amo_crystB[simulation_cutpos&simulation_cutph], bins = 100, histtype ='step', color = 'indigo',  
            alpha = 1, lw = 3, label = 'Random,crystal B',  range = [0,0.5])
    ax[1].set_yscale('log')
    ax[1].legend()
    ax[1].grid()      
    
    
    ax[2].hist(simulation_amo_crystC[simulation_cutpos&simulation_cutph], bins = 100, histtype ='step', color = 'orangered',  
            alpha = 1, lw = 3, label = 'Random, crystal C',  range = [0,0.2])
    ax[2].set_yscale('log')
    ax[2].legend()
    ax[2].grid()   
#%%

    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    simulation_h, simulation_bins, _ = ax.hist(simulation_amo_crystA[simulation_cutpos&simulation_cutph]+simulation_amo_crystB[simulation_cutpos&simulation_cutph]+simulation_amo_crystC[simulation_cutpos&simulation_cutph],
            bins = 100, histtype ='stepfilled', color = 'teal',  
            alpha = 0.5, edgecolor = 'teal', lw = 3, label = 'Simulation Random, crystal A+B+C', range = [0,0.15])
    
    
    
    err = simulation_h
    xdata = np.array([(simulation_bins[k+1]+simulation_bins[k])/2 for k in range(len(simulation_bins)-1)])
    phcondition = ((xdata > 0.02) & (xdata < 0.14))
    params = modLandau.make_params(A = 3500, mpv = 0.05, width = 0.04)
    result = modLandau.fit(simulation_h[phcondition], params, x=xdata[phcondition])
    print(f'Chi-square = {result.chisqr:.4f}, Reduced Chi-square = {result.redchi:.4f}')
    print(result.params)
    mpv_simulation.append(result.params['mpv'].value)
    mpvstd_simulation.append(result.params['width'].value)
    to_plot = np.linspace(-1000,3000,100000)
    ax.plot(xdata[phcondition], result.best_fit, label=f'mpv = {round(result.params["mpv"].value,4)} $\pm$ {round(result.params["mpv"].stderr,2)}', color = 'black', lw = 4)
    ax.legend(fontsize = 15)
    ax.set_ylabel('Entries')
    ax.set_xlabel(f'Energy [GeV]')
    ax.grid()
    plt.tight_layout()
    plt.show()
    
    
#%% per calibrare so quanto rilascia una mip in un dato spessore -- calcolo la retta di calibrazione
slope_SiPM_calib = []
errslope_SiPM_calib = []
intercept_SiPM_calib = []
errintercept_SiPM_calib = []

energyMIP = mpv_simulation #GeV
fig, axline = plt.subplots(1, 1, figsize=(10, 8))



linearmodel = LinearModel()     
linearparams = linearmodel.guess(mpv_tot, x=energyMIP)

linearresult = linearmodel.fit(mpv_tot, linearparams, x = energyMIP , weights =  mpvstd_tot)
to_plot = np.linspace(0, 0.2, 10000)
y_eval = linearmodel.eval(linearresult.params, x=to_plot)
axline.errorbar(energyMIP, mpv_tot, yerr = mpvstd_tot, 
            marker='D',  linestyle = 'none', color = 'navy',
            markersize = 8)
axline.plot(to_plot,y_eval, color = 'black', lw = 2, ls = '--',
        label = f' ADC = ({round(linearresult.params["slope"].value,4)} $\pm$ {round(linearresult.params["slope"].stderr,4)})$\cdot$ GeV +  ({round(linearresult.params["intercept"].value,4)}  $\pm$ {round(linearresult.params["intercept"].stderr,4)})')
axline.legend(fontsize = 15)
axline.set_ylabel('OREO total PH [a.u]')
axline.set_xlabel('MIP energy deposit [GeV]')
slope_SiPM_calib.append((linearresult.params['slope'].value))
errslope_SiPM_calib.append((linearresult.params['slope'].stderr))
intercept_SiPM_calib.append((linearresult.params['intercept'].value))
errintercept_SiPM_calib.append((linearresult.params['intercept'].stderr))
print(f'Chi-square = {linearresult.chisqr:.4f}, Reduced Chi-square = {linearresult.redchi:.4f}')
axline.grid()
plt.tight_layout()
plt.show()

SiPM_calib = [slope_SiPM_calib, errslope_SiPM_calib, intercept_SiPM_calib, errintercept_SiPM_calib]
with open('T9SiPM_Calibration_OREO0823.dat','w') as out_file:
        np.savetxt(out_file, SiPM_calib)
        
#%%
# SiPM_calib = np.loadtxt('SiPM_Calibration_OREO0823.dat')
# SiPM_eqfact = np.loadtxt('SiPM_eqfactor.dat')
# SiPM_intercept1 = np.loadtxt('SiPM_intercept1.dat')
# fig, ax = plt.subplots(1, 1, figsize=(10, 6))
# ax.hist((data[nrun][1][:,chLabel['George1']]-SiPM_intercept1[0])*SiPM_eqfact[0], 
#         bins = 50, histtype ='stepfilled', color = 'red', edgecolor = 'red', 
#         alpha = 0.5, lw = 3, range = [100, 500])
# ax.hist( ((data[nrun][1][:,chLabel['John1']]-SiPM_intercept1[1])*SiPM_eqfact[1]), 
#         bins = 50, histtype ='stepfilled', color ='green', edgecolor = 'green', 
#         alpha = 0.5, lw = 3, range = [100, 500])
# ax.hist((data[nrun][1][:,chLabel['Paul1']]-SiPM_intercept1[2])*SiPM_eqfact[2], 
#         bins = 50, histtype ='stepfilled', color = 'blue', edgecolor = 'blue', 
#         alpha = 0.5, lw = 3, range = [100, 500])