#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 13 10:17:12 2023

@author: ale
"""

#Run di elettroni a 6 GeV asse e amorfo--< per ora solo amorfo che sto provando a vedere se torna 
#con la simulazione 


from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
totOreoCalib = {}

plt.rcParams['font.size'] = '20'
#triggere APC + Cherry
# Ch 0 -->  XCET 48 (Cherry 2)
# Ch 1 --> LG1 (Door / T10 side)
# Ch 2 --> George 1
# Ch 3 --> George 2
# Ch 4 --> John 1
# Ch 5 --> John 2
# Ch 6 --> Paul 1
# Ch 7 --> Paul 2

datapath = r'C:\Users\aselmi\Desktop\Dottorato\data\dataOREO_0823/HDF5/run680'
h5keys = [ 'xpos','digiPH', 'digiTime']

chLabel = {'Cherry':0, 'LG':1, 'George1':2, 'George2':3, 'John1':4, 'John2':5, 'Paul1':6, 'Paul2':7}
SiPMlabel = ['George1','George2', 'John1','John2','Paul1','Paul2']
allSiPMlabel = ['George', 'John', 'Paul']
runnumbers = [242, 241] #asse e amorfo
#runnumbers = [242] #amorfo
distancesfromT1 = [500]
zcryst = 500+25
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]

### varie threshold per i tagli 
thcherry = 200

divsup = 0.001
divinf = -0.001
timethinf = [180]*6
timethsup = [210]*6
phth = [1700]*6
## dizionari
data = {}
cutdiv = {}
cutcherry = {}
cuttime = {}
cutph = {}
ph_totOREO = {}
cuttot = {}
cutpos = {}
cutpos_Paul = {}
cutpos_George= {}
cutpos_John = {}
crystlenght = 2 # per il cut fiduciale. La vera lunghezza è 2.5

xcrystinf = 0.5
xcrystsup = xcrystinf+crystlenght
ycrystinf= 4.7
ycrystsup = ycrystinf+crystlenght


 # per il cut fiduciale. La vera lunghezza è 2.5

simulation_xcrystinf = -3.4
simulation_xcrystsup = xcrystinf+crystlenght
simulation_ycrystinf= -1
simulation_ycrystsup = simulation_ycrystinf+crystlenght


xinf_george = 0.8
yinf_george= 5.2
cubo_lenght = 1.2
xsup_george = xinf_george + cubo_lenght
ysup_george = yinf_george + cubo_lenght
#%% carico i dati calcolati in EqSiPMs_OREO_0823OnlyoneSipm
SiPM_calib = np.loadtxt('T9_AllSiPM_Calibration_OREO0823.dat')

SiPM_eqfact = np.loadtxt('T9_AllSiPM_eqfactor.dat')
SiPM_intercept1 = np.loadtxt('T9_AllSiPM_intercept1.dat')
LG_calib05 = np.loadtxt('LG_CalibrationOREO23_05V.dat')
#%%
#############################################
#carico dati + plot posizioni camere
for i, nrun in enumerate(runnumbers):
    print(i)
    data[nrun] = opendata(runnumbers[i], datapath, h5keys)
    xpos = data[nrun][0]
    #camere 
    xlabels = ['BC1',' BC1', 'BC2', 'BC2']
    xlim = [[-0,10]]*4
    histpos1d(xpos, 2, xlabels = xlabels, xlim = xlim, Range = [0,10])
    histpos2d(xpos, numberoftrackers = 2)
    #correlazioni camere 
    fig, ax = plt.subplots(1,2 , figsize=(8,8))
    ax = ax.flat
    ax[0].hist2d(xpos[:,0], xpos[:,2], bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet)
    ax[0].set_ylabel('x BC2 [cm]')
    ax[0].set_xlabel('x BC1 [cm]')
    
    ax[1].hist2d(xpos[:,1],xpos[:,3] , bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet,)
    ax[1].set_xlabel('y BC1 [cm]')
    ax[1].set_ylabel('x BC2 [cm]')
    plt.tight_layout()
    plt.show()
#%%
    #divergenze
    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(xpos, 2, distancesfromT1,
                                                           p0divx, p0divy,
                                                           Rangex=[[-0.02,0.02]],
                                                           Rangey=[[-0.02,0.02]])
    
    
    datashift, shift = aligntracker(xpos, 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                                   distancesfromT1,
                                                                                    p0divx, p0divy,
                                                                                    Rangex=[[-0.004,0.004]],
                                                                                    Rangey=[[-0.004,0.004]], 
                                                                                    Color='navy', 
                                                                                    Figsize = (8,6))
    cutdiv[nrun] = ((divexalign >divinf) & (divexalign<divsup) & (diveyalign>divinf) &(diveyalign<divsup))
    ax[0].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=divsup, color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=divsup, color="black", linestyle = '--', lw = 2)   

# #%%

 #%%
     # profilo cherenkov selezioni gli elettroni
    cutcherry[nrun] = ((data[nrun][1][:,chLabel['Cherry']]>thcherry))
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    
    ax.hist((data[nrun][1][:,chLabel['Cherry']]),
                 bins = 100, histtype ='stepfilled', color = 'turquoise', edgecolor = 'turquoise', 
                 alpha = 0.5, lw = 3 ,label = 'Cherry', range = [0, 6000])
    ax.axvline(x=thcherry, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax.grid()
    ax.set_xlabel('Cherenkov PH [a.u]')
    ax.set_ylabel('Counts [ns]')
    ax.set_yscale('log')
    fig.set_tight_layout('tight')
     
    plt.show()
#%%
    # plot PH e tempo + tagli su tempi e PH dei tre cristalli 
    fig, ax = plt.subplots(2, 3, figsize=(10, 6))
    ax = ax.flat
    for j in range(len(SiPMlabel)):
        ax[j].hist2d(data[nrun][1][:,chLabel[SiPMlabel[j]]],data[nrun][2][:,chLabel[SiPMlabel[j]]] ,
                     bins =[100,100], cmap = plt.cm.jet, range = [[0, 8000], [150, 300]], norm=colors.LogNorm())
        ax[j].axhline(y=timethinf[j], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[j].axhline(y=timethsup[j], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[j].axvline(x=phth[j], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[j].set_ylabel('Counts')
        ax[j].set_xlabel('PH [a.u]')
        ax[j].set_title(f'{SiPMlabel[j]}')
        ax[j].grid()
    plt.tight_layout()
    plt.show()
    #%% faccimao un po' di tagli  su tempo e PH 
    ## solo una matrice
    # cuttime[nrun] =(((data[nrun][2][:,chLabel['George1']]>timethinf[0]) & (data[nrun][2][:,chLabel['George1']]<timethsup[0]))|
    #              ((data[nrun][2][:,chLabel['John1']]>timethinf[1]) & (data[nrun][2][:,chLabel['John1']]<timethsup[1]))|
    #              ((data[nrun][2][:,chLabel['Paul1']]>timethinf[2]) & (data[nrun][2][:,chLabel['Paul1']]<timethsup[2])))
    # # I cristalli li vedi andando a fare i tagli sulla PH dei vari SiPM, se chiedi solo il taglio su George vedi lui 
    # cutph[nrun] = ((data[nrun][1][:,chLabel['George1']]>phth[0])|
    #          (data[nrun][1][:,chLabel['John1']]>phth[1]) |
    #          (data[nrun][1][:,chLabel['Paul1']]>phth[2]) )
    
    # cuttot[nrun] = cutdiv[nrun][0] & cuttime[nrun] &cutph[nrun]
    #tutte e due le matrici
    cuttime[nrun] = (((data[nrun][2][:,chLabel['George1']]>timethinf[0]) & (data[nrun][2][:,chLabel['George1']]<timethsup[0])&(data[nrun][2][:,chLabel['George2']]>timethinf[0]) & (data[nrun][2][:,chLabel['George2']]<timethsup[0]))|
                 ((data[nrun][2][:,chLabel['John1']]>timethinf[1]) & (data[nrun][2][:,chLabel['John1']]<timethsup[1])&(data[nrun][2][:,chLabel['John2']]>timethinf[1]) & (data[nrun][2][:,chLabel['John2']]<timethsup[1]))|
                 ((data[nrun][2][:,chLabel['Paul1']]>timethinf[2]) & (data[nrun][2][:,chLabel['Paul1']]<timethsup[2])& (data[nrun][2][:,chLabel['Paul2']]>timethinf[2]) & (data[nrun][2][:,chLabel['Paul2']]<timethsup[2])))
    # I cristalli li vedi andando a fare i tagli sulla PH dei vari SiPM, se chiedi solo il taglio su George vedi lui 
    cutph[nrun] = ((data[nrun][1][:,chLabel['George1']]>phth[0])|(data[nrun][1][:,chLabel['George2']]>phth[0])|
             (data[nrun][1][:,chLabel['John1']]>phth[1]) | (data[nrun][1][:,chLabel['John2']]>phth[0])|
             (data[nrun][1][:,chLabel['Paul1']]>phth[2])|(data[nrun][1][:,chLabel['Paul2']]>phth[0]))
    cuttot[nrun] = cuttime[nrun]& cutcherry[nrun] & cutdiv[nrun][0] & cutph[nrun]
            
#%% efficienza cristallo 
    xcryst = xpos[:,0] + zcryst*np.tan(divexalign)
    ycryst = xpos[:,1] + zcryst*np.tan(diveyalign)
    fig, ax = plt.subplots(1,1, figsize=(6,6))
    zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xcryst[0], ycryst[0], cmap=plt.cm.jet, bins=[80, 80], range=[[0,10], [0,10]])
    plt.colorbar(mapable, ax=ax)
    ax.set_xlabel('x [cm]')
    ax.set_xlabel('y [cm]')
    ax.set_title('Proiezione su piano del cristallo')
    plt.tight_layout()
    plt.show()

   
    fig, ax = plt.subplots(1,1, figsize=(8,8))
    zhist2dcut, xhist2dcut , yhist2dcut , mapable = ax.hist2d(xcryst[0][cutph[nrun] & cuttime[nrun]], ycryst[0][cutph[nrun] & cuttime[nrun]], 
                                                                  cmap=plt.cm.jet, bins = [80,80],
                                                                  range = [[0,10], [0,10]])


    plt.colorbar(mapable, ax=ax)
    rect = patches.Rectangle((xcrystinf, ycrystinf), crystlenght*3, crystlenght, linewidth=4, edgecolor='darkred', facecolor='none')
    ax.add_patch(rect)
    ax.set_ylabel('x [cm]')
    ax.set_xlabel('y [cm]')

    plt.tight_layout()
    plt.show()
    cutpos[nrun] = ((xcryst[0]>xcrystinf) & (xcryst[0]<xcrystsup )& (ycryst[0]>ycrystinf) &( ycryst[0]<ycrystsup))
    fig, ax = plt.subplots(1,1, figsize=(6,6))
    np.seterr(divide='ignore', invalid = 'ignore')
    eff = np.divide(zhist2dcut, zhist2d)
    eff[np.isnan(eff)]=0

    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.1)
    im = ax.imshow(eff.T, cmap=plt.cm.jet, extent=[0,10, 0, 10], origin = "lower")
    fig.colorbar(im, cax=cax, orientation='vertical')
    rectG = patches.Rectangle((xinf_george, yinf_george), cubo_lenght, cubo_lenght, linewidth=4, edgecolor='black', facecolor='none', label= 'Fiducial area')
    rectJ = patches.Rectangle((xinf_george+2.5, yinf_george), cubo_lenght, cubo_lenght, linewidth=4, edgecolor='black', facecolor='none')
    rectP = patches.Rectangle((xinf_george+2.5+2.5, yinf_george), cubo_lenght, cubo_lenght, linewidth=4, edgecolor='black', facecolor='none')
    ax.add_patch(rectG)
    ax.add_patch(rectJ)
    ax.add_patch(rectP)

    ax.set_xlim([0,10])
    ax.set_ylim([0,10])
    ax.set_xlabel('x [cm]')
    ax.set_ylabel('y [cm]')
    ax.legend(fontsize=15)
    plt.tight_layout()
    #plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/t9/T9_6GeVeff.pdf')
    ## attenzione, anche qui come at ottobre le x sono invertite. quindi nella mappa di eff Paul è il primo e george l'ultimo
    cutpos_Paul[nrun] = ((xcryst[0]>xinf_george) & (xcryst[0]<xsup_george ) &(ycryst[0]>yinf_george) &( ycryst[0]<ysup_george))
    cutpos_John[nrun] = ((xcryst[0]>xinf_george+2.5) & (xcryst[0]<xinf_george+2.5+cubo_lenght ) & (ycryst[0]>yinf_george) &( ycryst[0]<yinf_george+cubo_lenght))
    cutpos_George[nrun] = ((xcryst[0]>xinf_george+2.5+2.5) & (xcryst[0]<xinf_george+2.5+2.5+cubo_lenght ) & (ycryst[0]>yinf_george) &( ycryst[0]<yinf_george+cubo_lenght))
    


    #qui sono le m e le q di calibrazione dei singoli cristalli con la retta di calibrazione fatta con i muoni 
    totOreoCalib[nrun] = (((data[nrun][1][:,chLabel['George1']]+data[nrun][1][:,chLabel['George2']])-25)/5659)+(((data[nrun][1][:,chLabel['John1']]+data[nrun][1][:,chLabel['John2']])-33)/4024)+(((data[nrun][1][:,chLabel['Paul1']]+data[nrun][1][:,chLabel['Paul2']])-4)/7276)
#%%
#non sono perfettamente equalizzate 
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    ax.hist(((data[nrun][1][:,chLabel['George1']]+data[nrun][1][:,chLabel['George2']])[cuttot[nrun] & cutpos_George[nrun]]-SiPM_intercept1[0])*SiPM_eqfact[0], 
            bins = 100, histtype ='step', color = 'orangered', 
            alpha = 0.5, lw = 3, range = [1000, 12000], density = True, label = 'George')
    ax.hist(((data[nrun][1][:,chLabel['John1']]+data[nrun][1][:,chLabel['John2']])[cuttot[nrun] & cutpos_John[nrun]]-SiPM_intercept1[1])*SiPM_eqfact[1], 
            bins = 100, histtype ='step', color ='hotpink', 
            alpha = 0.5, lw = 3, range = [1000, 12000], density = True, label = 'John')
    ax.hist(((data[nrun][1][:,chLabel['Paul1']]+data[nrun][1][:,chLabel['Paul2']])[cuttot[nrun] & cutpos_Paul[nrun]]-SiPM_intercept1[2])*SiPM_eqfact[2], 
            bins = 100, histtype ='step', color = 'lime', 
            alpha = 0.5, lw = 3, range = [1000, 12000], density = True, label = 'Paul')
    ax.legend()
    ax.grid()
    ax.set_xlabel('(PH-q)*eqfact')
    ax.set_ylabel('Counts')
    plt.tight_layout()

    
ax_amolabel = ['Random', 'Axial']
ColorA = ['navy', 'hotpink']
dataAmo = []
dataAxial = []
ph_George = {}
ph_John = {}
ph_Ringo = {}
ph_Paul = {}

for k,  nrun in enumerate(runnumbers): 
    ## equalizzando 
    # ph_George[nrun] = ((data[nrun][1][:,chLabel['George1']]+data[nrun][1][:,chLabel['George2']])-SiPM_intercept1[0])*SiPM_eqfact[0]
    # ph_John[nrun] = ((data[nrun][1][:,chLabel['John1']]+data[nrun][1][:,chLabel['John2']])-SiPM_intercept1[1])*SiPM_eqfact[1]
    # ph_Paul[nrun] =((data[nrun][1][:,chLabel['Paul1']]+data[nrun][1][:,chLabel['Paul2']])-SiPM_intercept1[2])*SiPM_eqfact[2]
   ## calibrando 
    ph_George[nrun] = (((data[nrun][1][:,chLabel['George1']]+data[nrun][1][:,chLabel['George2']])-25.35)/5659.03)
    ph_John[nrun] = (((data[nrun][1][:,chLabel['John1']]+data[nrun][1][:,chLabel['John2']])-33.66)/4024.12)
    ph_Paul[nrun] = (((data[nrun][1][:,chLabel['Paul1']]+data[nrun][1][:,chLabel['Paul2']])-4.42)/7276.75)

   
#%%
    ph_totOREO[nrun] = ((((data[nrun][1][:,chLabel['George1']]+data[nrun][1][:,chLabel['George2']])-SiPM_intercept1[0])*SiPM_eqfact[0])+
                    (((data[nrun][1][:,chLabel['John1']]+data[nrun][1][:,chLabel['John2']])-SiPM_intercept1[1])*SiPM_eqfact[1])+
                    (((data[nrun][1][:,chLabel['Paul1']]+data[nrun][1][:,chLabel['Paul2']])-SiPM_intercept1[2])*SiPM_eqfact[2]))
    


model = GaussianModel()    
listPH_OREO = [ph_George, ph_John, ph_Paul]
listCUTPH_OREO = [cutpos_George, cutpos_John, cutpos_Paul]


fitrangeinf = [1000]*3
fitrangesup = [15000]*3
rangefit = [[2000, 15000],  [200, 15000], [700, 15000]]
fig, ax = plt.subplots(1,3, figsize=(15, 6))
ax = ax.flat
colorFit = ['darkred', 'black']
lstylefit = ['--', '-']
muPH_SiPM = {}
errmuPH_SiPM = {}
meanPH_SiPM = {}
errmeanPH_SiPM = {}
for j in range(len(listPH_OREO)): 
    for k, nrun in enumerate(runnumbers): 
        hph, binsph, _  = ax[j].hist(listPH_OREO[j][nrun][listCUTPH_OREO[j][nrun]], histtype ='step', 
                   bins = 60, alpha = 1, lw = 3 ,label = f'{ax_amolabel[k]} ',
                   range = [0,4], density = True, color = ColorA[k])
        # xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
        # phcondition = ((xdata > fitrangeinf[j]) & (xdata < fitrangesup[j]))
        # paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
        # resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
        #                     weights=np.sqrt(hph[phcondition]))
        # to_plot = np.linspace(fitrangeinf[j], fitrangesup[j], 10000)
        # y_eval = model.eval(resultx.params, x=to_plot)
        #ax[i].plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = 'black')
      
        # muPH_SiPM[nrun] = resultx.params["center"].value
        # errmuPH_SiPM[nrun] =  resultx.params["center"].stderr
        meanPH_SiPM[nrun] = np.mean(listPH_OREO[j][nrun][listCUTPH_OREO[j][nrun]])
        errmeanPH_SiPM[nrun] = np.std(listPH_OREO[j][nrun][listCUTPH_OREO[j][nrun]]) / np.sqrt(len(listPH_OREO[j][nrun][listCUTPH_OREO[j][nrun]]))
       # ax[j].plot(to_plot, y_eval, linewidth=2, color= colorFit[k], linestyle = lstylefit[k], label=f'$\mu$ = {round(resultx.params["center"].value,2)} $\pm$ {round(resultx.params["center"].stderr,2)}')
        print(f'media {allSiPMlabel[j]}  = {meanPH_SiPM[nrun]} \pm {errmeanPH_SiPM[nrun]}')
        
    ax[j].set_ylabel('Normalized counts')
    ax[j].set_xlabel(f'PH [a.u] {allSiPMlabel[j]}')
    ax[j].legend(fontsize = 10)
    ax[j].grid()
   # errratio_SiPM = np.sqrt((errmuPH_SiPM[runnumbers[0]]/muPH_SiPM[runnumbers[0]])**2 + (errmuPH_SiPM[runnumbers[1]]/muPH_SiPM[runnumbers[1]])**2)
    errratio_meanSiPM = np.sqrt((errmeanPH_SiPM[runnumbers[0]]/meanPH_SiPM[runnumbers[0]])**2 + (errmeanPH_SiPM[runnumbers[1]]/meanPH_SiPM[runnumbers[1]])**2)
    #print(f'ratio SiPM = {muPH_SiPM[runnumbers[0]]/muPH_SiPM[runnumbers[1]]} $\pm$ {errratio_SiPM*muPH_SiPM[runnumbers[1]]/muPH_SiPM[runnumbers[0]]} ')
   
    print(f'ratio media {allSiPMlabel[j]} = {meanPH_SiPM[runnumbers[1]]/meanPH_SiPM[runnumbers[0]]} $\pm$ {errratio_meanSiPM}')
plt.tight_layout()
plt.show()



#%% 


fig, ax = plt.subplots(1, 1, figsize=(8, 6))
mu_OREO = {}
errmu_OREO = {}
mean_OREO = {}
errmean_OREO = {}
fitrangeTOTinf = [1.2, 0.8]
fitrangeTOTsup = [4, 4]
for j, nrun in enumerate(runnumbers): 
    #equalizzando e poi calibrando
    # mean_OREO[nrun] = np.mean(((ph_totOREO[nrun][[cuttot[nrun]]])-SiPM_calib[2])/SiPM_calib[0])
    # errmean_OREO[nrun] = np.std(((ph_totOREO[nrun][[cutpos[nrun]]])-SiPM_calib[2])/SiPM_calib[0])/np.sqrt(len(((ph_totOREO[nrun][[cutpos[nrun]]])-SiPM_calib[2])/SiPM_calib[0]))
    #calibrando direttamente i cristalli 
    mean_OREO[nrun] = np.mean(totOreoCalib[nrun][[cutpos[nrun] & cutdiv[nrun][0]]])
    errmean_OREO[nrun]  = np.std(totOreoCalib[nrun][[cutpos[nrun] & cutdiv[nrun][0]]]) / np.sqrt(len(totOreoCalib[nrun][cutpos[nrun] & cutdiv[nrun][0]]))
    # hph, binsph, _  = ax.hist(((ph_totOREO[nrun][[cutpos[nrun] & cutdiv[nrun][0]]])-SiPM_calib[2])/SiPM_calib[0], histtype ='step', 
    #           bins = 80, alpha = 1, lw = 3 ,label = f'{ax_amolabel[j]}, mean = {round(mean_OREO[nrun], 3)}, $\pm$ {round(errmean_OREO[nrun], 3)}',
    #             range = [0, 4], density = True, color = ColorA[j])
    hph, binsph, _  = ax.hist(totOreoCalib[nrun][cuttot[nrun]], histtype ='step', 
                bins = 80, alpha = 1, lw = 3 ,label = f'{ax_amolabel[j]}, mean = {round(mean_OREO[nrun], 3)} $\pm$ {round(errmean_OREO[nrun], 3)}',
                range = [0, 4], density = True, color = ColorA[j])
    
    xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
    phcondition = ((xdata > fitrangeTOTinf[j]) & (xdata < fitrangeTOTsup[j]))
    paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
    resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                        weights=np.sqrt(hph[phcondition]))
    to_plot = np.linspace(fitrangeTOTinf[j], fitrangeTOTsup[j], 10000)
    y_eval = model.eval(resultx.params, x=to_plot)
    #ax[i].plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = 'black')
  
    mu_OREO[nrun] = resultx.params["center"].value
    errmu_OREO[nrun] = resultx.params["center"].stderr
   
    #ax.plot(to_plot, y_eval, linewidth=3, color= colorFit[j], linestyle = '-', label=f'$\mu$ = {round(resultx.params["center"].value,4)} $\pm$ {round(resultx.params["center"].stderr,4)}')

ax.set_ylabel('Normalized counts')
ax.set_xlabel('OREO PH [GeV] ')
ax.legend(fontsize = 15)
ax.set_ylim([0,1.5])
ax.grid()
plt.tight_layout()
#plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/t9/T9_ratioAxAmo.pdf')
errratio = np.sqrt((errmu_OREO[runnumbers[0]]/mu_OREO[runnumbers[0]])**2 + (errmu_OREO[runnumbers[1]]/mu_OREO[runnumbers[1]])**2)
errmeanratio = np.sqrt((errmean_OREO[runnumbers[0]]/mean_OREO[runnumbers[0]])**2 + (errmean_OREO[runnumbers[1]]/mean_OREO[runnumbers[1]])**2)
#print(f'ratio = {mu_OREO[runnumbers[0]]/mu_OREO[runnumbers[1]]} $\pm$ {errratio*mu_OREO[runnumbers[1]]/mu_OREO[runnumbers[0]]} ')
print(f'ratio media oreo = {mean_OREO[runnumbers[1]]/mean_OREO[runnumbers[0]]} $\pm$ {errmeanratio*mean_OREO[runnumbers[0]]/mean_OREO[runnumbers[1]]} ')
plt.tight_layout()
# plt.show()

# # # #%%############################################################################
# # # ###############################################################################
# # # ########################        Simulation           ##########################
# # # ###############################################################################
# # # ###############################################################################

# import uproot

# datapath = '/home/ale/Desktop/Dottorato/datiOreoSimulation/'
# #dataAxial = uproot.open(datapath +'oreo0823_axial_6GeV.root')['outData']
# dataAmo = uproot.open(datapath + 'T9_electrons_6GeV_amo.root')['outData']


# amo_LG = np.array(dataAmo['GammaCal_EDep_00'])

# simulation_amo_crystA = np.array(dataAmo['CrystalA_EDep'])
# simulation_amo_crystB = np.array(dataAmo['CrystaB_EDep'])
# simulation_amo_crystC = np.array(dataAmo['CrystalC_EDep'])
# simulation_xposT0 = np.array(dataAmo['Tracker_X_0'])
# simulation_yposT0 = np.array(dataAmo['Tracker_Y_0'])
# simulation_xposT1 = np.array(dataAmo['Tracker_X_1'])
# simulation_yposT1 = np.array(dataAmo['Tracker_Y_1'])
# simulation_telePos = [simulation_xposT0, simulation_yposT0, simulation_xposT1, simulation_yposT1]

# #%%
# tele_label = ['T1, x [cm]', 'T1, y [cm]', 'T2, x [cm]', 'T2, y [cm]']
# fig, ax = plt.subplots(2, 2, figsize=(8, 8))
# ax = ax.flat
# for i in range(4):
#     ax[i].hist(simulation_telePos[i], bins = 100, histtype ='step', color = 'hotpink',  
#             alpha = 1, lw = 3, range = [-5,5], label = 'Simulation')
#     ax[i].set_xlabel(f'{tele_label[i]}')
#     ax[i].set_ylabel('Counts')
#     ax[i].legend(fontsize = 10)
#     ax[i].grid()
# plt.tight_layout()
# plt.show()


# #%%
# simulation_divx = (np.arctan((simulation_telePos[2]- simulation_telePos[0])/(distancesfromT1[0])))
# simulation_divy = (np.arctan((simulation_telePos[3]- simulation_telePos[1])/(distancesfromT1[0])))
# fig, ax = plt.subplots(1, 2, figsize=(10, 10))
# ax = ax.flat
# ax[0].hist(simulation_divx,  bins = 100, histtype ='step', color = 'navy',  
#         alpha = 1, lw = 3, label = 'Simulation', range = [-0.004,0.004])
# ax[0].set_xlabel('x divergence')
# ax[0].set_ylabel('Counts')
# ax[0].legend()
# ax[0].grid()


# ax[1].hist(simulation_divy,  bins = 100, histtype ='step', color = 'navy',  
#         alpha = 1, lw = 3, label = 'Simulation', range = [-0.004,0.004])
# ax[1].set_xlabel('y divergence')
# ax[1].set_ylabel('Counts')
# ax[1].legend()
# ax[1].grid()
# #%%

# simulation_xcryst = simulation_telePos[0] + zcryst*np.tan(simulation_divx)
# simulation_ycryst = simulation_telePos[1] + zcryst*np.tan(simulation_divy)
# fig, ax = plt.subplots(1,1, figsize=(8,8))
# zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(simulation_xcryst, simulation_ycryst,
#                                                 cmap=plt.cm.jet, bins=[60, 60], range=[[-5,5], [-5,5]])
# plt.colorbar(mapable, ax=ax)
# rect = patches.Rectangle((xcrystinf, ycrystinf), crystlenght*3, crystlenght, linewidth=4, edgecolor='darkred', facecolor='none')
# ax.add_patch(rect)
# ax.set_xlabel('x [cm]')
# ax.set_xlabel('y [cm]')
# ax.set_title('Proiezione su piano del cristallo, simulazione')
# plt.tight_layout()
# plt.show()
# #%%
# simulation_phth = 0.1
# fig, ax = plt.subplots(1, 3, figsize=(16, 8))
# ax = ax.flat
# ax[0].hist(simulation_amo_crystA, bins = 100, histtype ='step', color = 'teal',  
#         alpha = 1, lw = 3, label = 'Random simulation,\n crystal A', range = [0,3])
# ax[0].set_yscale('log')
# ax[0].legend()
# ax[0].grid()      
# ax[0].axvline(x=simulation_phth, color="black", linestyle = '--', label = 'cut', lw = 2)
# ax[1].hist(simulation_amo_crystB, bins = 100, histtype ='step', color = 'indigo',  
#         alpha = 1, lw = 3, label = 'Random simulation,\n crystal B',  range = [0,3])
# ax[1].set_yscale('log')
# ax[1].legend()
# ax[1].grid()      
# ax[1].axvline(x=simulation_phth, color="black", linestyle = '--', label = 'cut', lw = 2)
# ax[2].hist(simulation_amo_crystC, bins = 100, histtype ='step', color = 'orangered',  
#         alpha = 1, lw = 3, label = 'Random simulation,\n crystal C',  range = [0,3])
# ax[2].set_yscale('log')
# ax[2].legend()
# ax[2].grid()   
# ax[2].axvline(x=simulation_phth, color="black", linestyle = '--', label = 'cut', lw = 2) 
# for i in range(3): 
#     ax[i].set_xlabel('PH [GeV]')
#     ax[i].set_ylabel('Counts')
# plt.tight_layout()
# simulation_cutph = ((simulation_amo_crystA>simulation_phth)|(simulation_amo_crystB>simulation_phth)|(simulation_amo_crystC>simulation_phth))
# #%%
# fig, ax = plt.subplots(1,1, figsize=(8,8))
# zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(simulation_xcryst[simulation_cutph], 
#                                                 simulation_ycryst[simulation_cutph], 
#                                                 cmap=plt.cm.jet, bins=[60, 60], range=[[-5,5], [-5,5]])
# plt.colorbar(mapable, ax=ax)
# ax.set_xlabel('x [cm]')
# ax.set_xlabel('y [cm]')
# ax.set_title('Proiezione su piano del cristallo, simulazione')
# rect = patches.Rectangle((simulation_xcrystinf, simulation_ycrystinf), crystlenght*3, crystlenght, linewidth=4, edgecolor='darkred', facecolor='none')
# ax.add_patch(rect)
# plt.tight_layout()
# plt.show()
# simulation_cutpos = ((simulation_xcryst>simulation_xcrystinf)& (simulation_xcryst<(simulation_xcrystinf+crystlenght*3))
#                       &(simulation_ycryst>simulation_ycrystinf) & (simulation_ycryst<(simulation_ycrystinf+crystlenght*3)))


# #%%

# fig, ax = plt.subplots(1, 3, figsize=(12, 8))
# ax = ax.flat
# ax[0].hist(simulation_amo_crystA[simulation_cutph], bins = 100, histtype ='step', color = 'teal',  
#         alpha = 1, lw = 3, label = 'Random, crystal A', range = [0,3], density = True)
# ax[0].hist((((data[242][1][:,chLabel['George1']]+data[242][1][:,chLabel['George2']])[cuttot[242]]-SiPM_intercept1[0])*SiPM_eqfact[0]-SiPM_calib[2])/SiPM_calib[0], bins = 100, histtype ='step', color = 'pink',  
#         alpha = 1, lw = 3, label = 'George', range = [0,3], density= True)
# ax[0].set_yscale('log')
# ax[0].legend()
# ax[0].grid()      


# ax[1].hist(simulation_amo_crystB[simulation_cutph], bins = 100, histtype ='step', color = 'indigo',  
#         alpha = 1, lw = 3, label = 'Random,crystal B',  range = [0,3], density= True)
# ax[1].hist((((data[242][1][:,chLabel['John1']]+data[242][1][:,chLabel['John2']])[cuttot[242]]-SiPM_intercept1[1])*SiPM_eqfact[1]-SiPM_calib[2])/SiPM_calib[0], bins = 100, histtype ='step', color = 'lightblue',  
#         alpha = 1, lw = 3, label = 'John',  range = [0,3], density= True)
# ax[1].set_yscale('log')
# ax[1].legend()
# ax[1].grid()      


# ax[2].hist(simulation_amo_crystC[simulation_cutph], bins = 100, histtype ='step', color = 'orangered',  
#         alpha = 1, lw = 3, label = 'Random, crystal C',  range = [0,3],density= True)
# ax[2].hist((((data[242][1][:,chLabel['Paul1']]+data[242][1][:,chLabel['Paul2']])[cuttot[242]]-SiPM_intercept1[2])*SiPM_eqfact[2]-SiPM_calib[2])/SiPM_calib[0], bins = 100, histtype ='step', color = 'brown',  
#         alpha = 1, lw = 3, label = 'Paul',  range = [0,3],density= True)
# ax[2].set_yscale('log')
# ax[2].legend()
# ax[2].grid()   



# # #%%


# fig, ax = plt.subplots(1, 1, figsize=(8, 6))

# ax.hist(((ph_totOREO[runnumbers[0]][cutpos[runnumbers[0]] & cutdiv[runnumbers[0]][0]]-SiPM_calib[2])/SiPM_calib[0]), 
#             bins = 80, histtype ='step', color = 'navy',
#             alpha = 1, lw = 3, label = 'Random, first method', density = True, range = [0,3]) 
# ax.hist(totOreoCalib[runnumbers[0]][[cutpos[runnumbers[0]] & cutdiv[runnumbers[0]][0]]], 
#             bins = 80, histtype ='step', color = 'darkred',
#             alpha = 1, lw = 3, label = 'Random, second method ', density = True, range = [0,3])
# # ax.hist(((simulation_amo_crystA+simulation_amo_crystB+simulation_amo_crystC)[simulation_cutph]),
# #         bins = 100, histtype ='step', color = 'darkred',
# #             alpha = 1, lw = 3, label = 'Random, simulation', density = True)



# ax.legend(fontsize = 15)
# ax.set_xlim([0,4])
# ax.grid()
# ax.set_ylabel('Counts')
# ax.set_xlabel('OREO energy deposit [GeV]')
# plt.tight_layout()
# #plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/t9/T9random2methods.pdf')



# # #%%
# # ## guardo il LG
# # fig, ax = plt.subplots(1, 1, figsize=(10, 6))

# # ax.hist(((data[242][1][:,chLabel['LG']]-LG_calib05[2])/LG_calib05[0]), 
# #             bins = 50, histtype ='step', color = 'hotpink',
# #             alpha = 1, lw = 3, label = 'Amorphous experimental data', density = True) 
# # ax.hist(amo_LG,
# #         bins = 100, histtype ='step', color = 'navy',
# #             alpha = 1, lw = 3, label = 'Amorphous simulation, lead glass', density = True)



# # ax.legend(fontsize = 10)
# # ax.legend(fontsize = 10)

# # ax.grid()
# # ax.set_ylabel('Counts')
# # ax.set_xlabel('Energy [GeV]')


# # ### andiamo a vedere il lead glass



