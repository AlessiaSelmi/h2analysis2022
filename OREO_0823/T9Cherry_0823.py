from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches

plt.rcParams['font.size'] = '20'


datapath = r'C:\Users\aselmi\Desktop\Dottorato\data\dataOREO_0823/HDF5/run680'
h5keys = [ 'xpos','digiPH', 'digiTime']
runnumbers = [run for run in range(299, 305)]+[306,307,308]
energies = [1,2,3,4,5,6, 10,12,15]
chLabel = {'Cherry':0, 'LG':1}
thcherry = [300]*6+[240]*3
thLG = [700, 1500, 2600, 3700, 4300, 4600, 2500, 2800, 4100]
distancesfromT1 = [500]
zcalo = 500+25.5
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]
divsup = 0.002
divinf = -0.002
timethsup = 210
timethinf = 170


data = {}
cutdiv = {}
cuttime = {}
cond_cherry = {}
cond_LG = {}
ineff = []
notcond_cherry = {}
purity = []
for i, nrun in enumerate(runnumbers):
    #carico dati
    data[nrun] = opendata(nrun, datapath, h5keys)
    fig, ax = plt.subplots(1, 1, figsize=(8, 6))
    #plot PH tempo
    import copy 
    import matplotlib as mpl
    my_cmap = copy.copy(mpl.cm.jet)
    my_cmap.set_bad(my_cmap(0))
    zhist2d, xhist2d, yhist2d, mapable  = ax.hist2d(data[nrun][1][:,chLabel['LG']],data[nrun][2][:,chLabel['LG']] ,
                bins =[100,100], cmap = my_cmap, range = [[0, 12000], [100, 300]], norm=colors.LogNorm())
    #taglio in tempo
    cuttime[nrun] = ((data[nrun][2][:,chLabel['LG']]<timethsup) & (data[nrun][2][:,chLabel['LG']]>timethinf))
    ax.axhline(y=timethinf, color="red", linestyle = '--', label = f'Threshold \n {energies[i]} GeV', lw = 2)
    ax.axhline(y=timethsup, color="red", linestyle = '--', lw = 2)
    ax.grid()
    ax.legend()
    ax.set_xlabel('PH [a.u]')
    ax.set_ylabel('time [ns]')
    fig.colorbar(mapable,ax=ax)
    #ax.set_title(f'Lead Glass {energies[i]} GeV')
    plt.tight_layout()
    fig, ax = plt.subplots(1, 1, figsize=(8, 6))
    ax.hist((data[nrun][1][:,chLabel['Cherry']][cuttime[nrun]]),
                 bins = 100, histtype ='step', color = 'navy', 
                 alpha = 1, lw = 3 ,label = f'Cherenkov \n {energies[i]}  GeV')
    ax.axvline(x=thcherry[i], color="red", linestyle = '--', label = 'Threshold cherry', lw = 3)
    ax.hist((data[nrun][1][:,chLabel['LG']][cuttime[nrun]]),
                 bins = 100, histtype ='step', color = 'hotpink', 
                 alpha = 1, lw = 3 ,label = f'LG \n {energies[i]}  GeV')
    ax.axvline(x=thLG[i], color="black", linestyle = '--', label = 'Threshold LG', lw = 3)
    
    ax.grid()
    ax.legend(fontsize = 10)
    ax.set_xlabel(' PH [a.u]')
    ax.set_ylabel('Counts')
    ax.set_yscale('log')
    plt.show()
    notcond_cherry[nrun] = (data[nrun][1][:,chLabel['Cherry']][cuttime[nrun]]<thcherry[i])
    cond_cherry[nrun] = (data[nrun][1][:,chLabel['Cherry']][cuttime[nrun]]>thcherry[i])
    cond_LG[nrun] = (data[nrun][1][:,chLabel['LG']][cuttime[nrun]]>thLG[i])
    
    
    ineff.append(((cond_LG[nrun]& notcond_cherry[nrun]).sum()/cond_LG[nrun].sum())*100)
    purity.append(((cond_LG[nrun]& cond_cherry[nrun]).sum()/cond_LG[nrun].sum())*100)
    

fig, ax = plt.subplots(1, 1, figsize=(8, 6))
ax.plot(energies, ineff, marker='D',  linestyle = 'none', color = 'darkgreen',
            markersize = 8, label = 'inefficiency')
ax.set_xlabel('GeV')
ax.set_ylabel('XCET 48 inefficiency [$\%$]')
ax.grid()
    
fig, ax = plt.subplots(1, 1, figsize=(8, 6))
ax.plot(energies, purity,  marker='D',  linestyle = 'none', color = 'darkred',
            markersize = 8, label = 'purity')

ax.set_xlabel('GeV')
ax.set_ylabel('XCET 48 purity [$\%$]')
ax.grid()
    