#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 15 11:56:19 2023

@author: ale
"""

from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches

#triggere APC + Cherry
# Ch 0 -->  XCET 48 (Cherry 2)
# Ch 1 --> LG1 (Door / T10 side)
# Ch 2 --> George 1
# Ch 3 --> George 2
# Ch 4 --> John 1
# Ch 5 --> John 2
# Ch 6 --> Paul 1
# Ch 7 --> Paul 2

datapath = r'C:\Users\aselmi\Desktop\Dottorato\data\dataOREO_0823/HDF5/run680'
h5keys = [ 'xpos','digiPH', 'digiTime']

chLabel = {'Cherry':0, 'LG':1, 'George1':2, 'George2':3, 'John1':4, 'John2':5, 'Paul1':6, 'Paul2':7}
SiPMlabel = ['George1','George2', 'John1','John2','Paul1','Paul2']
allSiPMlabel = ['George', 'John', 'Paul']
distancesfromT1 = [500]
zcryst = 500+25
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]


runnumbers = [242, 241 ,314, 313, 315, 316] #Amorfo e Axial --> centrati su john
Energies = [6, 6, 10, 10, 15, 15]
energy = [6, 10, 15]
### varie threshold per i tagli 
thcherry = 200

divsup = 0.01
divinf = -0.01
timethinf = [180]*6
timethsup = [210]*6
phth = [1700]*6
thcherry = 200
LG_calib05V = np.loadtxt(r'C:\Users\aselmi\Desktop\Dottorato\analysis\OREO_0823\LG_CalibrationOREO23_05V.dat')
LG_calib2V = np.loadtxt(r'C:\Users\aselmi\Desktop\Dottorato\analysis\OREO_0823\LG_CalibrationOREO23_2V.dat')


data = {}
cutdiv = {}
cutcherry = {}
cuttime = {}
cutph = {}
PH_toteq = {}
cuttot = {}
cutpos = {}
OREO_PHtot = {}
LGphGeV = {}
LGph = {}

meanOREO_random = []
meanOREO_axial = []
errmeanOREO_random = []
errmeanOREO_axial = []


meanLG_random = []
meanLG_axial = []
errmeanLG_random = []
errmeanLG_axial = []

ratioOREO = []
errratioOREO = []


meanOREO_plusLG_random = []
errmeanOREO_plusLG_random = []
meanOREO_plusLG_axial = []
errmeanOREO_plusLG_axial = []

mean_missingEnergy_random = []
mean_missingEnergy_axial = []

#%%
for i, nrun in enumerate(runnumbers):
    print(i)
    data[nrun] = opendata(runnumbers[i], datapath, h5keys)
    LGph[nrun] = data[nrun][1][:,chLabel['LG']]
    xpos = data[nrun][0]
    #camere 
    xlabels = ['BC1',' BC1', 'BC2', 'BC2']
    xlim = [[-0,10]]*4
    histpos1d(xpos, 2, xlabels = xlabels, xlim = xlim, Range = [0,10])
    histpos2d(xpos, numberoftrackers = 2)
    #correlazioni camere 
    fig, ax = plt.subplots(1,2 , figsize=(8,8))
    ax = ax.flat
    ax[0].hist2d(xpos[:,0], xpos[:,2], bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet)
    ax[0].set_ylabel('x BC2 [cm]')
    ax[0].set_xlabel('x BC1 [cm]')
    
    ax[1].hist2d(xpos[:,1],xpos[:,3] , bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet,)
    ax[1].set_xlabel('y BC1 [cm]')
    ax[1].set_ylabel('x BC2 [cm]')
    plt.tight_layout()
    plt.show()

    #divergenze
    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(xpos, 2, distancesfromT1,
                                                           p0divx, p0divy,
                                                           Rangex=[[-0.02,0.02]],
                                                           Rangey=[[-0.02,0.02]])
    
    
    datashift, shift = aligntracker(xpos, 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                                   distancesfromT1,
                                                                                    p0divx, p0divy,
                                                                                    Rangex=[[-0.04,0.04]],
                                                                                    Rangey=[[-0.04,0.04]], 
                                                                                    Color='navy', 
                                                                                    Figsize = (8,6))
    cutdiv[nrun] = ((divexalign >divinf) & (divexalign<divsup) & (diveyalign>divinf) &(diveyalign<divsup))
    ax[0].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=divsup, color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=divsup, color="black", linestyle = '--', lw = 2)   




   # profilo cherenkov selezioni gli elettroni
    cutcherry[nrun] = ((data[nrun][1][:,chLabel['Cherry']]>thcherry))
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
 
    ax.hist((data[nrun][1][:,chLabel['Cherry']]),
                 bins = 100, histtype ='stepfilled', color = 'turquoise', edgecolor = 'turquoise', 
                 alpha = 0.5, lw = 3 ,label = 'Cherry', range = [0, 6000])
    ax.axvline(x=thcherry, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax.grid()
    ax.set_xlabel('Cherenkov PH [a.u]')
    ax.set_ylabel('Counts [ns]')
    ax.set_yscale('log')
    fig.set_tight_layout('tight')
     
    plt.show()

    # plot PH e tempo + tagli su tempi e PH dei tre cristalli 
    fig, ax = plt.subplots(2, 3, figsize=(10, 6))
    ax = ax.flat
    for j in range(len(SiPMlabel)):
        ax[j].hist2d(data[nrun][1][:,chLabel[SiPMlabel[j]]],data[nrun][2][:,chLabel[SiPMlabel[j]]] ,
                     bins =[100,100], cmap = plt.cm.jet, range = [[0, 20000], [150, 300]], norm=colors.LogNorm())
        ax[j].axhline(y=timethinf[j], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[j].axhline(y=timethsup[j], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[j].axvline(x=phth[j], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[j].set_ylabel('Counts')
        ax[j].set_xlabel('PH [a.u]')
        ax[j].set_title(f'{SiPMlabel[j]}')
        ax[j].grid()
    plt.tight_layout()
    plt.show()
    
    cuttime[nrun] = (((data[nrun][2][:,chLabel['George1']]>timethinf[0]) & (data[nrun][2][:,chLabel['George1']]<timethsup[0])&(data[nrun][2][:,chLabel['George2']]>timethinf[0]) & (data[nrun][2][:,chLabel['George2']]<timethsup[0]))|
                 ((data[nrun][2][:,chLabel['John1']]>timethinf[1]) & (data[nrun][2][:,chLabel['John1']]<timethsup[1])&(data[nrun][2][:,chLabel['John2']]>timethinf[1]) & (data[nrun][2][:,chLabel['John2']]<timethsup[1]))|
                 ((data[nrun][2][:,chLabel['Paul1']]>timethinf[2]) & (data[nrun][2][:,chLabel['Paul1']]<timethsup[2])& (data[nrun][2][:,chLabel['Paul2']]>timethinf[2]) & (data[nrun][2][:,chLabel['Paul2']]<timethsup[2])))
    # I cristalli li vedi andando a fare i tagli sulla PH dei vari SiPM, se chiedi solo il taglio su George vedi lui 
    cutph[nrun] = ((data[nrun][1][:,chLabel['George1']]>phth[0])|(data[nrun][1][:,chLabel['George2']]>phth[0])|
             (data[nrun][1][:,chLabel['John1']]>phth[1]) | (data[nrun][1][:,chLabel['John2']]>phth[0])|
             (data[nrun][1][:,chLabel['Paul1']]>phth[2])|(data[nrun][1][:,chLabel['Paul2']]>phth[0]))
    cuttot[nrun] = cuttime[nrun] & cutdiv[nrun][0] & cutph[nrun] & cutcherry[nrun]
    
    
    
    
 
    xcryst = xpos[:,0] + zcryst*np.tan(divexalign)
    ycryst = xpos[:,1] + zcryst*np.tan(diveyalign)
    fig, ax = plt.subplots(1,1, figsize=(6,6))
    zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xcryst[0], ycryst[0], cmap=plt.cm.jet, bins=[80, 80], range=[[0,10], [0,10]])
    plt.colorbar(mapable, ax=ax)
    ax.set_xlabel('x [cm]')
    ax.set_xlabel('y [cm]')
    ax.set_title('Proiezione su piano del cristallo')
    plt.tight_layout()
    plt.show()

   
    fig, ax = plt.subplots(1,1, figsize=(6,6))
    zhist2dcut, xhist2dcut , yhist2dcut , mapable = ax.hist2d(xcryst[0][cuttime[nrun] & cutph[nrun]& cutdiv[nrun][0]], ycryst[0][cuttime[nrun] & cutph[nrun]& cutdiv[nrun][0]], 
                                                                  cmap=plt.cm.jet, bins = [80,80],
                                                                  range = [[0,10], [0,10]])


    plt.colorbar(mapable, ax=ax)

    ax.set_ylabel('x [cm]')
    ax.set_xlabel('y [cm]')

    plt.tight_layout()
    plt.show()

    ## efficienza
    fig, ax = plt.subplots(1,1, figsize=(6,6))
    np.seterr(divide='ignore', invalid = 'ignore')
    eff = np.divide(zhist2dcut, zhist2d)
    eff[np.isnan(eff)]=0

    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.1)
    im = ax.imshow(eff.T, cmap=plt.cm.jet, extent=[0,10, 0, 10], origin = "lower")
    fig.colorbar(im, cax=cax, orientation='vertical')
  
    ax.set_xlim([0,10])
    ax.set_ylim([0,10])
    ax.set_xlabel('x [cm]')
    ax.set_ylabel('y [cm]')
    ax.legend(fontsize= 15)
    plt.tight_layout()
    

    OREO_PHtot[nrun] = ((((data[nrun][1][:,chLabel['George1']]+data[nrun][1][:,chLabel['George2']])-25.35)/5659.03)+
                        (((data[nrun][1][:,chLabel['John1']]+data[nrun][1][:,chLabel['John2']])-33.66)/4024.12)+
                        ((data[nrun][1][:,chLabel['Paul1']]+data[nrun][1][:,chLabel['Paul2']])-4.42)/7276.75)
    
    
    
    ## calibrando 
    if  Energies[i] == 6: 
        
      LGphGeV[nrun] = (((LGph[nrun])-LG_calib05V[2])/LG_calib05V[0])
    else: 
        LGphGeV[nrun] = (((LGph[nrun])-LG_calib2V[2])/LG_calib2V[0])

        
    if i%2==0: 
           
        meanOREO_random.append(np.mean(OREO_PHtot[nrun][cuttot[nrun]])) ############ questa è la media di oreo calcolata con somma dei tre cristalli calibrati singolarmente
        errmeanOREO_random.append(np.std(OREO_PHtot[nrun][cuttot[nrun]]) / np.sqrt(len(OREO_PHtot[nrun][cuttot[nrun]])))
        
        meanLG_random.append(np.mean(LGphGeV[nrun][cuttot[nrun]]))
        errmeanLG_random.append(np.std(LGphGeV[nrun][cuttot[nrun]]) / np.sqrt(len(LGphGeV[nrun][cuttot[nrun]])))
        
        meanOREO_plusLG_random.append(np.mean(OREO_PHtot[nrun][cuttot[nrun]] + LGphGeV[nrun][cuttot[nrun]]))
        
        #mean_missingEnergy_random.append(120- meanOREO_plusLG_random)
        
        fig, ax = plt.subplots(1, 1, figsize=(10, 6))
        ax.hist(OREO_PHtot[nrun][cuttot[nrun]], 
                    bins = 100, histtype ='step', color = 'navy',
                    alpha = 1, lw = 3, label = f'Random, energy = {Energies[i]}',  range = [0,20]) 
        ax.legend(fontsize = 15)

        ax.grid()
        ax.set_ylabel('Counts')
        ax.set_xlabel('OREO energy deposited')
        
        fig, ax = plt.subplots(1, 1, figsize=(10, 6))
        ax.hist(LGphGeV[nrun][cuttot[nrun]], 
                    bins = 100, histtype ='step', color = 'teal',
                    alpha = 1, lw = 3, label = f'Random, energy = {Energies[i]}') 
        ax.legend(fontsize = 15)

        ax.grid()
        ax.set_ylabel('Counts')
        ax.set_xlabel('LG energy deposited [GeV]')
        
    else : 
            
         meanOREO_axial.append(np.mean(OREO_PHtot[nrun][cuttot[nrun]])) ############ questa è la media di oreo calcolata con somma dei tre cristalli calibrati singolarmente
         errmeanOREO_axial.append(np.std(OREO_PHtot[nrun][cuttot[nrun]]) / np.sqrt(len(OREO_PHtot[nrun][cuttot[nrun]])))
         
         meanLG_axial.append(np.mean(LGphGeV[nrun][cuttot[nrun]]))
         errmeanLG_axial.append(np.std(LGphGeV[nrun][cuttot[nrun]]) / np.sqrt(len(LGphGeV[nrun][cuttot[nrun]])))
         
         meanOREO_plusLG_axial.append(np.mean(OREO_PHtot[nrun][cuttot[nrun]]+ LGphGeV[nrun][cuttot[nrun]]))
         #mean_missingEnergy_axial.append(120- meanOREO_plusLG_axial)
         fig, ax = plt.subplots(1, 1, figsize=(10, 6))
         ax.hist(OREO_PHtot[nrun][cuttot[nrun]], 
                     bins = 100, histtype ='step', color = 'hotpink',
                     alpha = 1, lw = 3, label = f'Axial, energy = {Energies[i]}', range = [0,20]) 
         ax.legend(fontsize = 15)

         ax.grid()
         ax.set_ylabel('Counts')
         ax.set_xlabel('OREO energy deposited [GeV]')
         
         fig, ax = plt.subplots(1, 1, figsize=(10, 6))
         ax.hist(LGphGeV[nrun][cuttot[nrun]], 
                     bins = 100, histtype ='step', color = 'darkred',
                     alpha = 1, lw = 3, label = f'Axial, energy = {Energies[i]}') 
         ax.legend(fontsize = 15)

         ax.grid()
         ax.set_ylabel('Counts')
         ax.set_xlabel('LG energy deposited [GeV]')
         
#%%
#%%
for k, ene in enumerate(energy): 
    mean_missingEnergy_random.append(ene- meanOREO_plusLG_random[k])
    mean_missingEnergy_axial.append(ene- meanOREO_plusLG_axial[k])
    ratioOREO.append(meanOREO_axial[k]/meanOREO_random[k])
#%%  PLot deposito in oreo vs energia + ratio     
#%%  PLot deposito in oreo vs energia + ratio     

fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(nrows=5, sharex=True, figsize =(13, 13)) 
ax1.errorbar(energy, meanOREO_random, yerr =errmeanOREO_random, marker = 'd', linestyle = '', markersize = '10', color = 'navy', label = 'Random')
ax1.errorbar(energy, meanOREO_axial, yerr =errmeanOREO_axial, marker = 'o', linestyle = '', markersize = '10' , color = 'hotpink', label = 'Axial')


ax1.legend(fontsize = 20)

ax1.grid()
ax1.set_ylabel('OREO \n $E_{dep}$ [GeV]')

   


#    valori medi energia depositata nel LG 

ax2.errorbar(energy, meanLG_random, yerr =errmeanLG_random, marker = 'd', linestyle = '', markersize = '10', color = 'navy', label = 'Random')
ax2.errorbar(energy, meanLG_axial, yerr =errmeanLG_axial, marker = 'o', linestyle = '', markersize = '10' , color = 'hotpink', label = 'Axial')


ax2.grid()
ax2.set_ylabel('LG \n $E_{dep}$ [GeV]')


   ## LG+ OREO 
ax3.errorbar(energy, meanOREO_plusLG_random, yerr =errmeanLG_random, marker = 'd', linestyle = '', markersize = '10', color = 'navy', label = 'Random')
ax3.errorbar(energy, meanOREO_plusLG_axial, yerr =errmeanLG_axial, marker = 'o', linestyle = '', markersize = '10' , color = 'hotpink', label = 'Axial')


ax3.grid()
ax3.set_ylabel('LG + OREO \n $E_{dep}$ [GeV]')




#   missing energy) 
ax4.errorbar(energy,mean_missingEnergy_random, yerr =errmeanLG_random, marker = 'd', linestyle = '', markersize = '10', color = 'navy', label = 'Random')
ax4.errorbar(energy, mean_missingEnergy_axial, yerr =errmeanLG_axial, marker = 'o', linestyle = '', markersize = '10' , color = 'hotpink', label = 'Axial')

ax4.grid()
ax4.set_ylabel('Missing energy \n [GeV]')


ax5.errorbar(energy, ratioOREO, yerr =errmeanOREO_random, marker = '*', linestyle = '', markersize = '10', color = 'teal', label = 'Ratio')
ax5.grid()
ax5.set_ylabel('OREO \n Axial/Random')
ax5.legend(fontsize = 20)
ax5.set_xlabel('Beam energy [GeV]')
plt.tight_layout()

#%% vediamo se la risoluzione energetica del calo cambia 
model = GaussianModel()
muE = []
muEerr = []
sigmaE = []
sigmaEerr = []
fitrangecalibinf = [0, 0, 5, 5, 7.5,7.5]
fitrangecalibsup = [7.5, 7.5, 12.5, 12.5,17.5, 17.5 ]

for i, nrun in enumerate(runnumbers):
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    hph, binsph, _ = ax.hist(OREO_PHtot[nrun][cuttot[nrun]]+LGphGeV[nrun][cuttot[nrun]], 
                     bins = 100, histtype ='step', color = 'hotpink',
                     alpha = 1, lw = 3, label = f'energy = {Energies[i]}', range = [0,20]) 
    xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
    phcondition = ((xdata > fitrangecalibinf[i]) & (xdata < fitrangecalibsup[i]))
    paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
    resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                            weights=np.sqrt(hph[phcondition]))
    to_plot = np.linspace(fitrangecalibinf[i], fitrangecalibsup[i], 10000)
    y_eval = model.eval(resultx.params, x=to_plot)
    #ax[i].plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = 'black')
    muE.append(resultx.params['center'].value)
    muEerr.append(resultx.params['center'].stderr)
    sigmaE.append(resultx.params['sigma'].value)
    sigmaEerr.append(resultx.params['sigma'].stderr)
    ax.plot(to_plot, y_eval, linewidth=2, color='black', label=f'$\mu$ = {round(resultx.params["center"].value,2)} $\pm$ {round(resultx.params["sigma"].value,2)}') 
    ax.legend(fontsize = 15)

    ax.grid()
    ax.set_ylabel('Counts')
    ax.set_xlabel('OREO + LG energy deposited [GeV]')
####  

#%%      
energy_resolutionRandom = []
energy_resolutionerrRandom = []

energy_resolutionAxial = []
energy_resolutionerrAxial = []
j = 0
f = 0
for k in range(len(muE)): 
    print(k)
    if k%2 == 0: 
        energy_resolutionRandom.append((sigmaE[k]/muE[k])*100)
        energy_resolutionerrRandom.append((np.sqrt((sigmaEerr[k]/sigmaE[k])**2+(muEerr[k]/muE[k])**2)*energy_resolutionRandom[j]))
        j = j+1
    else: 
         energy_resolutionAxial.append((sigmaE[k]/muE[k])*100)
         energy_resolutionerrAxial.append((np.sqrt((sigmaEerr[k]/sigmaE[k])**2+(muEerr[k]/muE[k])**2)*energy_resolutionAxial[f]))
         f = f+1

def energy_resolution_function(E, a, b, c):
    return np.sqrt( (a**2 / E) + (b / E)**2+ c**2)

from lmfit import Model
fig, ax = plt.subplots(1,1, figsize=(8,6))
model = Model(energy_resolution_function)
params = model.make_params(a=10, b=10, c=5)
result = model.fit(energy_resolutionRandom, params, E=energy, weights=energy_resolutionerrRandom)
a_fit = result.params['a'].value
b_fit = result.params['b'].value
c_fit = result.params['c'].value

sigma_a = result.params['a'].stderr
sigma_b = result.params['b'].stderr
sigma_c = result.params['c'].stderr
to_plot = np.linspace(min(energy), max(energy), 1000)
fit_energy_resolution = energy_resolution_function(to_plot, a_fit, b_fit, c_fit)

ax.errorbar(energy, energy_resolutionRandom,yerr=energy_resolutionerrRandom, marker = 'D', markersize = 10
            , color = 'navy',
            linestyle= 'none', label = '$\sigma$(E)/E')


ax.plot(to_plot, fit_energy_resolution, label = '$\sigma$(E)/E = $\sqrt{(a^{2} / E) + (b / E)^2+ c^2)}$' , color = 'red')
    
ax.set_xlabel('GeV')
ax.set_ylabel('Energy resolution [%]')
ax.legend(fontsize = 18)
ax.grid()
plt.tight_layout()
#plt.savefig(r'C:\Users\aselmi\Desktop\Dottorato\Technical Note Oreo2023\t9\T9_LGenergyResolution.pdf')
plt.show()


print(f' random a = {a_fit}, b = {b_fit}, c = {c_fit}  ')
print(f' random a = {sigma_a}, b = {sigma_b}, c = {sigma_c}  ')  


fig, ax = plt.subplots(1,1, figsize=(8,6))
model = Model(energy_resolution_function)
params = model.make_params(a=10, b=10, c=5)
result = model.fit(energy_resolutionAxial, params, E=energy, weights=energy_resolutionerrAxial)
a_fitAsse = result.params['a'].value
b_fitAsse = result.params['b'].value
c_fitAsse = result.params['c'].value

sigma_aAsse = result.params['a'].stderr
sigma_bAsse = result.params['b'].stderr
sigma_cAsse = result.params['c'].stderr
to_plot = np.linspace(min(energy), max(energy), 1000)
fit_energy_resolution = energy_resolution_function(to_plot, a_fit, b_fit, c_fit)

ax.errorbar(energy, energy_resolutionAxial,yerr=energy_resolutionerrAxial, marker = 'D', markersize = 10
            , color = 'navy',
            linestyle= 'none', label = '$\sigma$(E)/E')


ax.plot(to_plot, fit_energy_resolution, label = '$\sigma$(E)/E = $\sqrt{(a^{2} / E) + (b / E)^2+ c^2)}$' , color = 'red')
    
ax.set_xlabel('GeV')
ax.set_ylabel('Energy resolution [%]')
ax.legend(fontsize = 18)
ax.grid()
plt.tight_layout()
#plt.savefig(r'C:\Users\aselmi\Desktop\Dottorato\Technical Note Oreo2023\t9\T9_LGenergyResolution.pdf')
plt.show()


print(f' Axial a = {a_fitAsse}, b = {b_fitAsse}, c = {c_fitAsse}  ')
print(f' Axial a = {sigma_aAsse}, b = {sigma_bAsse}, c = {sigma_cAsse}  ')  
# %%
