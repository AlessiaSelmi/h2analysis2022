from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches

plt.rcParams['font.size'] = '20'

## proviamo a vedere le run a 10 GeV 
datapath = r'C:\Users\aselmi\Desktop\Dottorato\data\dataOREO_0823/HDF5/run680'
runnumbers = [315, 316] ## 10 GeV
energy = 15
orientation = ['Random', 'Axial']
h5keys = [ 'xpos','digiPH', 'digiTime']

chLabel = {'Cherry':0, 'LG':1, 'George1':2, 'George2':3, 'John1':4, 'John2':5, 'Paul1':6, 'Paul2':7}
SiPMlabel = ['George1','George2', 'John1','John2','Paul1','Paul2']
allSiPMlabel = ['George', 'John', 'Paul']
distancesfromT1 = [500]
zcryst = 500+25
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]

divsup = 0.01
divinf = -0.01
timethinf = [180]*6
timethsup = [210]*6

LG_calib2V = np.loadtxt(r'C:\Users\aselmi\Desktop\Dottorato\analysis\OREO_0823\LG_CalibrationOREO23_2V.dat')
data = {}
cutdiv = {}
cutcherry = {}
cuttime = {}
cutph = {}
PH_toteq = {}
cuttot = {}
cutpos = {}
OREO_PHtot = {}
LGphGeV = {}
LGph = {}

for i, nrun in enumerate(runnumbers):
    print(i)
    data[nrun] = opendata(runnumbers[i], datapath, h5keys)
    LGph[nrun] = data[nrun][1][:,chLabel['LG']]
    xpos = data[nrun][0]
    #camere 
    xlabels = ['BC1',' BC1', 'BC2', 'BC2']
    xlim = [[-0,10]]*4
    histpos1d(xpos, 2, xlabels = xlabels, xlim = xlim, Range = [0,10])
    histpos2d(xpos, numberoftrackers = 2)
    #correlazioni camere 
    fig, ax = plt.subplots(1,2 , figsize=(8,8))
    ax = ax.flat
    ax[0].hist2d(xpos[:,0], xpos[:,2], bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet)
    ax[0].set_ylabel('x BC2 [cm]')
    ax[0].set_xlabel('x BC1 [cm]')
    
    ax[1].hist2d(xpos[:,1],xpos[:,3] , bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet,)
    ax[1].set_xlabel('y BC1 [cm]')
    ax[1].set_ylabel('x BC2 [cm]')
    plt.tight_layout()
    plt.show()

    #divergenze
    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(xpos, 2, distancesfromT1,
                                                           p0divx, p0divy,
                                                           Rangex=[[-0.02,0.02]],
                                                           Rangey=[[-0.02,0.02]])
    
    
    datashift, shift = aligntracker(xpos, 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                                   distancesfromT1,
                                                                                    p0divx, p0divy,
                                                                                    Rangex=[[-0.04,0.04]],
                                                                                    Rangey=[[-0.04,0.04]], 
                                                                                    Color='navy', 
                                                                                    Figsize = (8,6))
    cutdiv[nrun] = ((divexalign >divinf) & (divexalign<divsup) & (diveyalign>divinf) &(diveyalign<divsup))
    ax[0].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=divsup, color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=divsup, color="black", linestyle = '--', lw = 2)   


    # plot PH e tempo + tagli su tempi e PH dei tre cristalli 
    fig, ax = plt.subplots(2, 3, figsize=(10, 6))
    ax = ax.flat
    for j in range(len(SiPMlabel)):
        ax[j].hist2d(data[nrun][1][:,chLabel[SiPMlabel[j]]],data[nrun][2][:,chLabel[SiPMlabel[j]]] ,
                     bins =[100,100], cmap = plt.cm.jet, range = [[0, 20000], [150, 300]], norm=colors.LogNorm())
        ax[j].axhline(y=timethinf[j], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[j].axhline(y=timethsup[j], color="black", linestyle = '--', label = 'cut', lw = 2)

        ax[j].set_ylabel('Counts')
        ax[j].set_xlabel('PH [a.u]')
        ax[j].set_title(f'{SiPMlabel[j]}')
        ax[j].grid()
    plt.tight_layout()
    plt.show()
    
    cuttime[nrun] = (((data[nrun][2][:,chLabel['George1']]>timethinf[0]) & (data[nrun][2][:,chLabel['George1']]<timethsup[0])&(data[nrun][2][:,chLabel['George2']]>timethinf[0]) & (data[nrun][2][:,chLabel['George2']]<timethsup[0]))|
                 ((data[nrun][2][:,chLabel['John1']]>timethinf[1]) & (data[nrun][2][:,chLabel['John1']]<timethsup[1])&(data[nrun][2][:,chLabel['John2']]>timethinf[1]) & (data[nrun][2][:,chLabel['John2']]<timethsup[1]))|
                 ((data[nrun][2][:,chLabel['Paul1']]>timethinf[2]) & (data[nrun][2][:,chLabel['Paul1']]<timethsup[2])& (data[nrun][2][:,chLabel['Paul2']]>timethinf[2]) & (data[nrun][2][:,chLabel['Paul2']]<timethsup[2])))
    # I cristalli li vedi andando a fare i tagli sulla PH dei vari SiPM, se chiedi solo il taglio su George vedi lui 
    """ cutph[nrun] = ((data[nrun][1][:,chLabel['George1']]>phth[0])|(data[nrun][1][:,chLabel['George2']]>phth[0])|
             (data[nrun][1][:,chLabel['John1']]>phth[1]) | (data[nrun][1][:,chLabel['John2']]>phth[0])|
             (data[nrun][1][:,chLabel['Paul1']]>phth[2])|(data[nrun][1][:,chLabel['Paul2']]>phth[0])) """
    cuttot[nrun] = cuttime[nrun] & cutdiv[nrun][0] 
    
    
    
    
 
    xcryst = xpos[:,0] + zcryst*np.tan(divexalign)
    ycryst = xpos[:,1] + zcryst*np.tan(diveyalign)
    fig, ax = plt.subplots(1,1, figsize=(6,6))
    zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xcryst[0], ycryst[0], cmap=plt.cm.jet, bins=[80, 80], range=[[0,10], [0,10]])
    plt.colorbar(mapable, ax=ax)
    ax.set_xlabel('x [cm]')
    ax.set_xlabel('y [cm]')
    ax.set_title('Proiezione su piano del cristallo')
    plt.tight_layout()
    plt.show()

   
    fig, ax = plt.subplots(1,1, figsize=(6,6))
    zhist2dcut, xhist2dcut , yhist2dcut , mapable = ax.hist2d(xcryst[0][cuttot[nrun]], ycryst[0][cuttot[nrun]], 
                                                                  cmap=plt.cm.jet, bins = [80,80],
                                                                  range = [[0,10], [0,10]])


    plt.colorbar(mapable, ax=ax)

    ax.set_ylabel('x [cm]')
    ax.set_xlabel('y [cm]')

    plt.tight_layout()
    plt.show()

    ## efficienza
    fig, ax = plt.subplots(1,1, figsize=(6,6))
    np.seterr(divide='ignore', invalid = 'ignore')
    eff = np.divide(zhist2dcut, zhist2d)
    eff[np.isnan(eff)]=0

    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.1)
    im = ax.imshow(eff.T, cmap=plt.cm.jet, extent=[0,10, 0, 10], origin = "lower")
    fig.colorbar(im, cax=cax, orientation='vertical')
  
    ax.set_xlim([0,10])
    ax.set_ylim([0,10])
    ax.set_xlabel('x [cm]')
    ax.set_ylabel('y [cm]')
    ax.legend(fontsize= 15)
    plt.tight_layout()
    

    OREO_PHtot[nrun] = ((((data[nrun][1][:,chLabel['George1']]+data[nrun][1][:,chLabel['George2']])-25.35)/5659.03)+
                        (((data[nrun][1][:,chLabel['John1']]+data[nrun][1][:,chLabel['John2']])-33.66)/4024.12)+
                        ((data[nrun][1][:,chLabel['Paul1']]+data[nrun][1][:,chLabel['Paul2']])-4.42)/7276.75)
    
    LGphGeV[nrun] = (((LGph[nrun])-LG_calib2V[2])/LG_calib2V[0])
    ## plot energy deposit in OREO with cut in time and div
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    ax.hist(OREO_PHtot[nrun][cuttot[nrun]], 
                    bins = 100, histtype ='step', color = 'navy',
                    alpha = 1, lw = 3, label = f'{orientation[i]}, energy = {energy} GeV',  range = [0,20]) 
    ax.legend(fontsize = 15)
    ax.grid()
    ax.set_ylabel('Counts')
    ax.set_xlabel('OREO energy deposited')
    
     ## plot energy deposit in LG with cut in time and div
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    ax.hist(LGphGeV[nrun][cuttot[nrun]], 
                    bins = 100, histtype ='step', color = 'teal',
                    alpha = 1, lw = 3, label = f'{orientation[i]}, energy = {energy} GeV') 
    ax.legend(fontsize = 15)
    ax.grid()
    ax.set_ylabel('Counts')
    ax.set_xlabel('LG energy deposited [GeV]')
#%%    
import copy 
import matplotlib as mpl
my_cmap = copy.copy(mpl.cm.jet)
my_cmap.set_bad(my_cmap(0))    
fig, ax = plt.subplots(1,2, figsize=(15, 6))
for i, nrun in enumerate(runnumbers):
    ## plot 2d correlation energy deposit in OREO and LG with cut in time and div
    
    ax[i].hist2d(LGphGeV[nrun][cuttot[nrun]], OREO_PHtot[nrun][cuttot[nrun]],
                 cmap=my_cmap, bins = [80,80], norm=colors.LogNorm())


    plt.colorbar(mapable, ax=ax[i])

    ax[i].set_xlabel('LG [GeV')
    ax[i].set_ylabel('OREO [GeV]')

    plt.tight_layout()
plt.show()
