#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  8 15:55:26 2023

@author: ale
"""

from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable

plt.rcParams['font.size'] = '15'

# Ch 0 -->  XCET 48 (Cherry 2)
# Ch 1 --> LG1 (Door / T10 side)
# Ch 2 --> George 1
# Ch 3 --> George 2
# Ch 4 --> John 1
# Ch 5 --> John 2
# Ch 6 --> Paul 1
# Ch 7 --> Paul 2

chLabel = ['cherry 2', 'LG', 'APC ', 'APC']


plt.rcParams['font.size'] = '20'
runnumber = 513

datapath = r'C:\Users\aselmi\Desktop\Dottorato\data\dataOREO_0823/run680'
#%%
h5keys = ['digiPH', 'xpos', 'digiTime']
data = opendata(runnumber, datapath, h5keys)
xpos = data[1]

#camere 
xlabels = ['BC1',' BC1', 'BC2', 'BC2']
xlim = [[-0,10]]*4
histpos1d(xpos, 2, xlabels = xlabels, xlim = xlim, Range = [0,10])
histpos2d(xpos, numberoftrackers = 2)
fig, ax = plt.subplots(1,2 , figsize=(8,8))
ax = ax.flat
ax[0].hist2d(xpos[:,0], xpos[:,2], bins = [100,100], range = [[0,10], [0,10]])
ax[1].hist2d(xpos[:,1],xpos[:,3] , bins = [100,100], range = [[0,10], [0,10]])
plt.show()

#%%
#%%
#divergenze
distancesfromT1 = [268]
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]

divex, divey, sigma, errsigma, mu, _ = divgaussianfit(xpos, 2, distancesfromT1,
                                                       p0divx, p0divy,
                                                       Rangex=[[-0.02,0.02]],
                                                       Rangey=[[-0.02,0.02]])


datashift, shift = aligntracker(xpos, 2, mu, distancesfromT1)
divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                               distancesfromT1,
                                                                                p0divx, p0divy,
                                                                                Rangex=[[-0.02,0.02]],
                                                                                Rangey=[[-0.02,0.02]], 
                                                                                Color='navy', 
                                                                                Figsize = (8,6))
#%%
fig, ax = plt.subplots(2,2, figsize=(8,8))
ax = ax.flat
for i in range(4): 
    ax[i].hist(data[0][:,i], bins = 100, 
               histtype = 'stepfilled', color = 'navy', edgecolor = 'navy',
               alpha = 0.6,lw =3, label = f'{chLabel[i]}')
    ax[i].legend()
    ax[i].set_xlabel('PH [a.u]')
    ax[i].set_ylabel('Entries')
    ax[i].grid()
    ax[i].set_yscale('log')
plt.show()

#%%
fig, ax = plt.subplots(2,2, figsize=(8,8))
ax = ax.flat
for i in range(4): 
    ax[i].hist(data[2][:,0]-data[2][:,i], bins = 100, 
               histtype = 'stepfilled', color = 'navy', edgecolor = 'navy',
               alpha = 0.6,lw =3, label = f'{chLabel[i]}')
    ax[i].legend()
    ax[i].set_xlabel('Time')
    ax[i].set_ylabel('Entries')
    ax[i].grid()
    ax[i].set_yscale('log')
plt.tight_layout()
plt.show()


#%%
diff = data[2][:,0]-data[2][:,1]
cuttime = ((diff>50) &(diff<150))
fig, ax = plt.subplots(2,2, figsize=(8,8))
ax = ax.flat
for j in range(4): 
    ax[j].hist2d(data[0][:,j],(data[2][:,0]-data[2][:,j]), bins= [100,100],
                 norm=colors.LogNorm(), range = [[0,5000],[-300, 300]])
    ax[j].axhline(y=10, color="black", linestyle = '--')
    ax[j].axhline(y=200, color="black", linestyle = '--')
    ax[j].set_xlabel(f'PH {chLabel[j]}')
    ax[j].set_ylabel(' Time')
    
    ax[j].grid()
plt.tight_layout()
plt.show()
cutdec = ((cuttime) | (data[0][:,1]>2000)  | (data[0][:,0]>2300))


# #%% #############################################################################à
# mucalib = []
# muerrcalib= []
# calibrun = [542,543,544]
# model = GaussianModel()
# for i, run in enumerate(calibrun): 
#     data = opendata(run, datapath, h5keys)
#     fig, ax = plt.subplots(1,1, figsize=(8,8))
    
#     hph, binsph, _ = ax.hist(data[0][:,1], bins = 100, 
#             histtype = 'stepfilled', color = 'navy', edgecolor = 'navy',
#             alpha = 0.6,lw =3, label = 'LG')
    
#     xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
#     phcondition = ((xdata > 2000) & (xdata < 5000))
#     paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
#     resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
#                         weights=np.sqrt(hph[phcondition]))
#     to_plot = np.linspace(2000, 5000, 10000)
#     y_eval = model.eval(resultx.params, x=to_plot)
#     #ax[i].plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = 'red')
#     mucalib.append(resultx.params['center'].value)
#     muerrcalib.append( resultx.params['center'].stderr)
#     ax.plot(to_plot, y_eval, linewidth=2, color='black')
#     ax.legend()
#     ax.set_xlabel('PH [a.u]')
#     ax.set_ylabel('Entries')
#     ax.grid()

# plt.show()




#zdec = 90+85+666
zdec = 90 + distancesfromT1[0]
xdec = xpos[:,0] + zdec*np.tan(divex)
ydec = xpos[:,1] + zdec*np.tan(divey)
fig, ax = plt.subplots(1,1, figsize=(8,8))
zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xdec[0], ydec[0], cmap=plt.cm.jet, bins=[50, 50], range=[[0,10], [0,10]])
plt.colorbar(mapable, ax=ax)
ax.set_xlabel('x [cm]')
ax.set_xlabel('y [cm]')
ax.set_title('Proiezione su piano del cristallo')
plt.tight_layout()
plt.show()


fig, ax = plt.subplots(1,1, figsize=(8,8))
zhist2dcut, xhist2dcut , yhist2dcut , mapable = ax.hist2d(xdec[0][cutdec], ydec[0][cutdec], 
                                                              cmap=plt.cm.jet, bins = [50,50],
                                                              range = [[0,10], [0,10]], weights = (data[0][:,1][cutdec]))


plt.rcParams['font.size'] = '20'
plt.colorbar(mapable, ax=ax)
# perchè non si vede la x? 
ax.set_ylabel('x [cm]')
ax.set_xlabel('y [cm]')
ax.set_title('proiezione cristalli run 513')
plt.tight_layout()
plt.show()





fig, ax = plt.subplots(1, 1, figsize=(8,8))
np.seterr(divide='ignore', invalid = 'ignore')
eff = np.divide(zhist2dcut, zhist2d)
eff[np.isnan(eff)]=0
divider = make_axes_locatable(ax)
cax = divider.append_axes('right', size='5%', pad=0.1)
im = ax.imshow(np.flip((np.transpose(eff))), cmap=plt.cm.jet, extent=[0,10, 0, 10])
fig.colorbar(im, cax=cax, orientation='vertical')
ax.set_xlim([0,10])
ax.set_ylim([0,10])
ax.set_xlabel('x [cm]')
ax.set_ylabel('y [cm]')
ax.set_title(f'Efficency run {runnumber}')
plt.tight_layout()
plt.show()

#%%

