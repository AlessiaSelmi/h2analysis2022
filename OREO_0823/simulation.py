#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 16 20:55:11 2023

@author: ale
"""

from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
import uproot

datapath = '/home/ale/Desktop/Dottorato/datiOreoSimulation/'
dataAmo = uproot.open(datapath +'H2oreo_amorphous120GeV.root')['outData']
dataAxial = uproot.open(datapath +'H2oreo_axial120GeV.root')['outData']


# nevent = np.array(data['NEvent'])
amo_LG = np.array(dataAmo['GammaCal_EDep_00'])
axial_LG = np.array(dataAxial['GammaCal_EDep_00'])
amo_crystA = np.array(dataAmo['CrystalA_EDep'])


amo_crystB = np.array(dataAmo['CrystaB_EDep'])
amo_crystC = np.array(dataAmo['CrystalC_EDep'])


ax_crystA= np.array(dataAxial['CrystalA_EDep'])
ax_crystB= np.array(dataAxial['CrystaB_EDep'])
ax_crystC= np.array(dataAxial['CrystalC_EDep'])


fig, ax = plt.subplots(1, 1, figsize=(10, 6))
ax.hist((ax_crystA + ax_crystB + ax_crystC), bins = 100, histtype ='step', color = 'orangered', 
        alpha = 1, lw = 3, label = 'axial')

ax.hist((amo_crystA  + amo_crystB  + amo_crystC), bins = 100, histtype ='step', color = 'lime',  
        alpha = 1, lw = 3, label = 'Random')
ax.set_yscale('log')
ax.legend()
ax.grid()
#%%
fig, ax = plt.subplots(1, 1, figsize=(10, 6))
ax.hist(amo_LG, bins = 100, histtype ='step', color = 'lime', 
        alpha = 1, lw = 3, label = 'Random')

ax.hist(axial_LG , bins = 100, histtype ='step', color = 'orangered',  
        alpha = 1, lw = 3, label = 'Axial')
# ax.set_yscale('log')
ax.legend()
ax.grid()
ax.set_xlim(70,115)
# fig, ax = plt.subplots(1, 1, figsize=(10, 6))
# ax.hist(LG, bins = 100, histtype ='stepfilled', color = 'turquoise', edgecolor = 'turquoise', 
# alpha = 0.5, lw = 3 )
