from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
from sklearn.metrics import confusion_matrix
plt.rcParams['font.size'] = '20'


datapath = r'C:\Users\aselmi\Desktop\Dottorato\data\dataOREO_1023\HDF5\run720'
run2merge = [120, 122, 124, 125] #amorfo
runnumbers = [666,127] #asse la 666 non esiste, ma per me da oggi sarà quella in random dove ho unito più run
h5keys = [ 'xpos','digiPH', 'digiTime']
LG_calib= np.loadtxt('1023LG_CalibrationOREO_05V.dat')
q_LG = LG_calib[2]
m_LG = LG_calib[0]
chLabel = {'LG':0, 'APC':1, 'Cherry44':2, 'Cherry48':3, 'George':4, 'John':5, 'Paul':6, 'Ringo':7}
SiPMlabel = ['George','John', 'Paul', 'Ringo']
SiPMcalib_intercept = np.loadtxt(r'C:\Users\aselmi\Desktop\Dottorato\analysis\OREO_1023\T9oct_OREOintercept.dat')
SiPMcalib_slope = np.loadtxt(r'C:\Users\aselmi\Desktop\Dottorato\analysis\OREO_1023\T9oct_OREOslope.dat')

distancesfromT1 = [500]
zcryst = 549
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]
divsup = 0.001
divinf = -0.001
timethinf = [190]*4
timethsup = [230]*4
timethinf_LG = 200
timethsup_LG = 210
thcherry44 = 100
thcherry48 = 100
phth = [100]*4
cubo_lenght = 1.3
thLG = 5
thOREO = [0.1, 0.2, 0.5, 0.8, 1, 1.3, 1.5, 1.8, 2, 2.2, 2.5, 2.8,3]

data = {}
partialData = []
outdata = []
dataAmo = []
dataAxial = []


cutdiv = {}
cutcherry = {}
cuttime_OREO= {}
cuttime_George = {}
cuttime_John = {}
cuttime_Paul = {}
cuttime_Ringo = {}
cutph_OREO = {}
cutph_George = {}
cutph_Paul = {}
cutph_John = {}
cutph_Ringo = {}
cuttot_OREO = {}
cutpos_OREO = {}
cutpos_Paul = {}
cutpos_George = {}
cutpos_John = {}
cutpos_Ringo = {}
cutTimeLG = {}

ph_GeorgeGeV = {}
ph_JohnGeV = {}
ph_PaulGeV = {}
ph_RingoGeV = {}
ph_OREOGeV = {}
LG_PHGeV = {}

TrueValues = {}
OREO_electrons = {}
sensitivity= {}
precision = {}
accuracy = {}

xinf_paul = 2.3
yinf_paul = 4.2
cubo_lenght = 1.2
crystlenght = 2 # per il cut fiduciale. La vera lunghezza è 2.5
xsup_paul = xinf_paul + cubo_lenght
ysup_paul = yinf_paul + cubo_lenght


for i, nrun in enumerate(runnumbers):
    if nrun == 666: 
        for j in h5keys: 
            outdata = []
            for k, nrun2merge in enumerate(run2merge): 
                with h5py.File(datapath +f'{run2merge[k]}.h5', 'r', libver='latest', swmr=True) as hf: 
                    print("opening %s" % run2merge[k])
                    print(j)
                    outdata.append(np.array(hf[j]))
            partialData.append(np.vstack((outdata[0],outdata[1], outdata[2], outdata[3])))
        data[nrun] = partialData
    else: 
        data[nrun] = opendata(runnumbers[i], datapath, h5keys)
    xpos = data[nrun][0]
    ## le y dovrebbero essere invertite 
    xpos[:,1] = 10-xpos[:,1]
    xpos[:,3] = 10-xpos[:,3]

    #camere 
    xlabels = ['BC1',' BC1', 'BC2', 'BC2']
    xlim = [[-0,10]]*4
    histpos1d(xpos, 2, xlabels = xlabels, xlim = xlim, Range = [0,10])
    histpos2d(xpos, numberoftrackers = 2)
    #correlazioni camere 

    fig, ax = plt.subplots(1,2 , figsize=(8,8))
    ax = ax.flat
    ax[0].hist2d(xpos[:,0], xpos[:,2], bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet)
    ax[0].set_ylabel('x BC2 [cm]')
    ax[0].set_xlabel('x BC1 [cm]')
    
    ax[1].hist2d(xpos[:,1],xpos[:,3] , bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet,)
    ax[1].set_xlabel('y BC1 [cm]')
    ax[1].set_ylabel('x BC2 [cm]')
    plt.tight_layout()
    plt.show()

    #divergenze
    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(xpos, 2, distancesfromT1,
                                                           p0divx, p0divy,
                                                           Rangex=[[-0.02,0.02]],
                                                           Rangey=[[-0.02,0.02]])
    
    
    datashift, shift = aligntracker(xpos, 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                                   distancesfromT1,
                                                                                    p0divx, p0divy,
                                                                                    Rangex=[[-0.004,0.004]],
                                                                                    Rangey=[[-0.004,0.004]], 
                                                                                    Color='navy', 
                                                                                    Figsize = (8,6))
    cutdiv[nrun] = ((divexalign >divinf) & (divexalign<divsup) & (diveyalign>divinf) &(diveyalign<divsup))
    ax[0].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=divsup, color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=divsup, color="black", linestyle = '--', lw = 2)   
    # plot tempo vs ph oreo
    fig, ax = plt.subplots(2, 2, figsize=(10, 6))
    ax = ax.flat
    for j, BeatName in enumerate(SiPMlabel):
        ax[j].hist2d(data[nrun][1][:,chLabel[SiPMlabel[j]]],data[nrun][2][:,chLabel[SiPMlabel[j]]] ,
                     bins =[100,100], cmap = plt.cm.jet, norm=colors.LogNorm())
        ax[j].axhline(y=timethinf[j], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[j].axhline(y=timethsup[j], color="black", linestyle = '--', label = 'cut', lw = 2)
        # ax[j].axvline(x=phth[j], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[j].set_ylabel('Entries')
        ax[j].set_xlabel('PH [a.u]')
        ax[j].set_title(f'{SiPMlabel[j]}')
        ax[j].grid()
        exec(f'cuttime_{BeatName}[nrun] = (data[nrun][2][:,chLabel["{BeatName}"]]>timethinf[{j}]) & (data[nrun][2][:,chLabel["{BeatName}"]]<timethsup[j])&(data[nrun][2][:,chLabel["{BeatName}"]]>timethinf[j]) & (data[nrun][2][:,chLabel["{BeatName}"]]<timethsup[j])')
        exec(f'cutph_{BeatName}[nrun] = data[nrun][1][:,chLabel["{BeatName}"]]>phth[{j}]')
    plt.tight_layout()
    plt.show()
    cuttime_OREO[nrun] = (cuttime_George[nrun] | cuttime_John[nrun] | cuttime_Paul[nrun] | cuttime_Ringo[nrun])
    cutph_OREO[nrun] = (cutph_George[nrun] | cutph_John[nrun] | cutph_Paul[nrun] | cutph_Ringo[nrun])
    
    # tempo vs ph LG
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    ax.hist2d(data[nrun][1][:,chLabel['LG']],data[nrun][2][:,chLabel['LG']] ,
                     bins =[100,100], cmap = plt.cm.jet, norm=colors.LogNorm())
    ax.axhline(y=timethinf_LG, color="red", linestyle = '--', label = 'cut', lw = 2)
    ax.axhline(y=timethsup_LG, color="red", linestyle = '--', label = 'cut', lw = 2)
    ax.set_ylabel('Entries')
    ax.set_xlabel(' LG PH [a.u]')
    ax.grid()
    cutTimeLG[nrun] = ((data[nrun][2][:,chLabel['LG']]>timethinf_LG) & (data[nrun][2][:,chLabel['LG']]<timethsup_LG))
    
    
    # profilo cherenkov selezioni gli elettroni
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    
    ax.hist((data[nrun][1][:,chLabel['Cherry44']]),
                 bins = 100, histtype ='step', color = 'turquoise',
                 alpha = 1, lw = 3 ,label = 'XCET 44', range = [0, 4000])
    ax.axvline(x=thcherry44, color="red", linestyle = '--', label = 'Threshold XCET 44', lw = 2)

    ax.hist((data[nrun][1][:,chLabel['Cherry48']]),
                 bins = 100, histtype ='step', color = 'forestgreen',
                 alpha = 1, lw = 3 ,label = 'XCET 48', range = [0, 4000])
    ax.axvline(x=thcherry48, color="black", linestyle = '--', label = 'Threshold XCET 48', lw = 2)
    ax.grid()
    ax.set_xlabel('Cherenkov PH [a.u]')
    ax.set_ylabel('Entries [ns]')
    ax.set_yscale('log')
    ax.legend()
    fig.set_tight_layout('tight')
    plt.show()
    cutcherry[nrun] = (((data[nrun][1][:,chLabel['Cherry44']]>thcherry44) & (data[nrun][1][:,chLabel['Cherry48']]>thcherry48)))
    ## profilo OREO 
    xcryst = xpos[:,0] + zcryst*np.tan(divexalign)
    ycryst = xpos[:,1] + zcryst*np.tan(diveyalign)
    fig, ax = plt.subplots(1,1, figsize=(6,6))
    zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xcryst[0], ycryst[0], cmap=plt.cm.jet, bins=[80, 80], range=[[0,10], [0,10]])
    plt.colorbar(mapable, ax=ax)
    ax.set_xlabel('x [cm]')
    ax.set_xlabel('y [cm]')
    ax.set_title('Proiezione su piano del cristallo')
    plt.tight_layout()
    plt.show()

    
    # fig, ax = plt.subplots(1,1, figsize=(8,8))
    zhist2dcut, xhist2dcut , yhist2dcut , mapable = ax.hist2d(xcryst[0][cuttime_OREO[nrun] & cutph_OREO[nrun]], ycryst[0][cuttime_OREO[nrun] & cutph_OREO[nrun]], 
                                                                  cmap=plt.cm.jet, bins = [80,80],
                                                                  range = [[0,10], [0,10]])


    # plt.colorbar(mapable, ax=ax)
    # # rect = patches.Rectangle((xcrystinf, ycrystinf), crystlenght*3, crystlenght, linewidth=4, edgecolor='darkred', facecolor='none')
    # # ax.add_patch(rect)
    # ax.set_ylabel('x [cm]')
    # ax.set_xlabel('y [cm]')
    

    # plt.tight_layout()
    # plt.show()
    fig, ax = plt.subplots(1,1, figsize=(8,8))
    np.seterr(divide='ignore', invalid = 'ignore')
    eff = np.divide(zhist2dcut, zhist2d)
    eff[np.isnan(eff)]=0

    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.1)
    im = ax.imshow(eff.T, cmap=plt.cm.jet, extent=[0,10, 0, 10], origin = "lower")
    fig.colorbar(im, cax=cax, orientation='vertical')
    rectG = patches.Rectangle((xinf_paul, yinf_paul+2.5), cubo_lenght, cubo_lenght, linewidth=3, edgecolor='black', facecolor='none', label= 'Fiducial area')
    rectR = patches.Rectangle((xinf_paul+2.5, yinf_paul), cubo_lenght, cubo_lenght, linewidth=3, edgecolor='black', facecolor='none')
    rectP = patches.Rectangle((xinf_paul, yinf_paul), cubo_lenght, cubo_lenght, linewidth=3, edgecolor='black', facecolor='none')
    rectJ = patches.Rectangle((xinf_paul+2.5,yinf_paul+2.5), cubo_lenght, cubo_lenght, linewidth=3, edgecolor='black', facecolor='none')
    ax.add_patch(rectP)
    ax.add_patch(rectR)
    ax.add_patch(rectG)
    ax.add_patch(rectJ)
    ax.set_xlim([0,10])
    ax.set_ylim([0,10])
    ax.set_xlabel('x [cm]')
    ax.set_ylabel('y [cm]')
    ax.legend()
    #ax.set_title(f'Run 7200{nrun} ')
    plt.tight_layout()
    
    ## siccome le y sono al contrario la mappa di eff in y è sbagliata--> qui ora cambio i nomi, a fer la cosa giusta dovresti invertire le y 
    ### QUI I SOMI SONO CAMBIATI
    
    cutpos_George[nrun] = ((xcryst[0]>xinf_paul) & (xcryst[0]<xinf_paul+cubo_lenght ) &(ycryst[0]>yinf_paul+2.5) &( ycryst[0]<yinf_paul+2.5+cubo_lenght))
    cutpos_John[nrun] = ((xcryst[0]>xinf_paul+2.5) & (xcryst[0]<xinf_paul+2.5+cubo_lenght ) & (ycryst[0]>yinf_paul+2.5) &( ycryst[0]<yinf_paul+2.5+cubo_lenght))
    cutpos_Paul[nrun] = ((xcryst[0]>xinf_paul) & (xcryst[0]<xinf_paul+cubo_lenght)&(ycryst[0]> yinf_paul) &( ycryst[0]< yinf_paul+cubo_lenght))
    cutpos_Ringo[nrun] = ((xcryst[0]>xinf_paul+2.5) & (xcryst[0]<xinf_paul+2.5+cubo_lenght )&(ycryst[0]>yinf_paul) &( ycryst[0]<yinf_paul + cubo_lenght))

    cutpos_OREO[nrun] = (cutpos_George[nrun] | cutpos_John[nrun] | cutpos_Paul[nrun] | cutpos_Ringo[nrun])
    
    

# #%%
# fig, ax = plt.subplots(1,1, figsize=(8,8))
# zhist2dcut, xhist2dcut , yhist2dcut , mapable = ax.hist2d(xcryst[0][cutpos_OREO[nrun]], ycryst[0][cutpos_OREO[nrun]], 
#                                                                   cmap=plt.cm.jet, bins = [80,80],
#                                                                   range = [[0,10], [0,10]])
# #%%
#%%
for i,  nrun in enumerate(runnumbers): 
    for j , BeatName in enumerate(SiPMlabel):
        exec(f'ph_{BeatName}GeV[nrun] = ((data[nrun][1][:,chLabel["{BeatName}"]])-SiPMcalib_intercept[{j}])/SiPMcalib_slope[{j}]')
    ph_OREOGeV[nrun] = ph_GeorgeGeV[nrun] + ph_JohnGeV[nrun]  + ph_PaulGeV[nrun] + ph_RingoGeV[nrun] 
 
    LG_PHGeV[nrun] = (data[nrun][1][:,chLabel['LG']] -q_LG)/m_LG
#%%
   
fig, ax = plt.subplots(1,1, figsize=(8,8))
ax.hist(ph_OREOGeV[runnumbers[0]][cutpos_OREO[runnumbers[0]]],
                 bins = 100, histtype ='step', 
                 alpha = 1, lw = 3 ,label = 'Random',  range = [0, 4])
ax.hist(ph_OREOGeV[runnumbers[1]][cutpos_OREO[runnumbers[1]]],
                 bins = 100, histtype ='step', 
                 alpha = 1, lw = 3 ,label = 'Axial', range = [0, 4])
   
ax.grid()
ax.set_xlabel('Oreo PH [a.u]')
ax.set_ylabel('Entries [ns]')

fig.set_tight_layout('tight')
ax.legend()
plt.show()
#%%
fig, ax = plt.subplots(1,1, figsize=(8,8))
ax.hist(LG_PHGeV[runnumbers[0]],
                 bins = 100, histtype ='step', 
                 alpha = 1, lw = 3 ,label = 'Random')
ax.hist(LG_PHGeV[runnumbers[1]],
                 bins = 100, histtype ='step', 
                 alpha = 1, lw = 3 ,label = 'Axial')
   
ax.grid()
# ax.set_yscale('log')
ax.set_xlabel('LG PH [a.u]')
ax.set_ylabel('Entries')
fig.set_tight_layout('tight')
ax.legend()
plt.show()

#%% 
fig, ax = plt.subplots(1,1, figsize=(8,8))
ax.hist(ph_OREOGeV[runnumbers[1]],
                 bins = 100, histtype ='step', 
                 alpha = 1, lw = 3 ,label = 'cherry ')
ax.hist(ph_OREOGeV[runnumbers[1]],
                 bins = 100, histtype ='step', 
                 alpha = 1, lw = 3 ,label = 'notcherry')
   
ax.grid()
ax.set_yscale('log')
ax.set_xlabel('LG PH [a.u]')
ax.set_ylabel('Entries')
fig.set_tight_layout('tight')
ax.legend()
plt.show()

# %%
import copy 
import matplotlib as mpl
my_cmap = copy.copy(mpl.cm.jet)
my_cmap.set_bad(my_cmap(0))

# #%% No cuts
ax_amolabel = ['Random', 'Axial']
# fig, ax = plt.subplots(1, 2, figsize=(10, 6))
# for j, nrun in enumerate(runnumbers):   
#     zhist, xhist , yhist , mapable  = ax[j].hist2d(
#         LG_PHGeV[nrun],ph_OREOGeV[nrun], 
#         bins =[80,80], 
#         cmap = my_cmap, 
#         # range = [[0.8,15], [0.8,5]], 
#         norm=colors.LogNorm())
#     for k in range(len(thOREO)): 
#         ax[j].axhline(y = thOREO[k], color="red", linestyle = '--', lw = 2)
#     plt.colorbar(mapable, ax = ax[j])
     
#     ax[j].set_xlabel('LG PH [GeV]')
#     ax[j].set_ylabel('OREO PH [GeV]')
#     # ax[j].legend(fontsize = 15)
#     ax[j].set_title(f'{ax_amolabel[j]} no cuts',)
#     plt.tight_layout()

#  #%% cut on cherry 

# fig, ax = plt.subplots(1, 2, figsize=(10, 6))
# for j, nrun in enumerate(runnumbers):   
#     zhist, xhist , yhist , mapable  = ax[j].hist2d(
#         LG_PHGeV[nrun][cutcherry[nrun]],ph_OREOGeV[nrun][cutcherry[nrun]], 
#         bins =[80,80], 
#         cmap = my_cmap, 
#         # range = [[0.8,15], [0.8,5]], 
#         norm=colors.LogNorm())
#     plt.colorbar(mapable, ax = ax[j])
     
#     ax[j].set_xlabel('LG PH [GeV]')
#     ax[j].set_ylabel('OREO PH [GeV]')
#     # ax[j].legend(fontsize = 15)
#     ax[j].set_title(f'{ax_amolabel[j]}, cutcherry')
#     plt.tight_layout()
    
# %%
#  #%% cut time, cut div e cut pos

# fig, ax = plt.subplots(1, 2, figsize=(10, 6))
# for j, nrun in enumerate(runnumbers):   
#     zhist, xhist , yhist , mapable  = ax[j].hist2d(
#         LG_PHGeV[nrun][cuttime_OREO[nrun] & cutdiv[nrun][0] & cutpos_OREO[nrun] ],ph_OREOGeV[nrun][cuttime_OREO[nrun] & cutdiv[nrun][0] & cutpos_OREO[nrun] ], 
#         bins =[80,80], 
#         cmap = my_cmap, 
#         # range = [[0.8,15], [0.8,5]], 
#         norm=colors.LogNorm())
#     plt.colorbar(mapable, ax = ax[j])
     
#     ax[j].set_xlabel('LG PH [GeV]')
#     ax[j].set_ylabel('OREO PH [GeV]')
#     #ax[j].legend(fontsize = 15)
#     ax[j].set_title(f'{ax_amolabel[j]} cut time, div e pos')
#     plt.tight_layout()
# #%% solo cut pos e cut cherry

# fig, ax = plt.subplots(1, 2, figsize=(10, 6))
# for j, nrun in enumerate(runnumbers):   
#     zhist, xhist , yhist , mapable  = ax[j].hist2d(
#         LG_PHGeV[nrun][cutpos_OREO[nrun] & cutcherry[nrun]],ph_OREOGeV[nrun][cutpos_OREO[nrun]  & cutcherry[nrun]], 
#         bins =[80,80], 
#         cmap = my_cmap, 
#         # range = [[0.8,15], [0.8,5]], 
#         norm=colors.LogNorm())
#     plt.colorbar(mapable, ax = ax[j])
     
#     ax[j].set_xlabel('LG PH [GeV]')
#     ax[j].set_ylabel('OREO PH [GeV]')
#     #ax[j].legend(fontsize = 15)
#     ax[j].set_title(f'{ax_amolabel[j]} cutpos + cut cherry')
#     plt.tight_layout()
################################################################################################################
################################################################################################################
##############################            PLOT TRUE VALUE         #############################################
################################################################################################################
################################################################################################################ 
 #%% cut time, cut div, cutpos e cut cherry

fig, ax = plt.subplots(1, 2, figsize=(10, 6))
for j, nrun in enumerate(runnumbers):   
    zhist, xhist , yhist , mapable  = ax[j].hist2d(
        LG_PHGeV[nrun][cuttime_OREO[nrun] & cutdiv[nrun][0] & cutpos_OREO[nrun]  & cutcherry[nrun]],ph_OREOGeV[nrun][cuttime_OREO[nrun] & cutdiv[nrun][0] & cutpos_OREO[nrun] & cutcherry[nrun]], 
        bins =[80,80], 
        cmap = my_cmap, 
        # range = [[0.8,15], [0.8,5]], 
        norm=colors.LogNorm())
    plt.colorbar(mapable, ax = ax[j])
     
    ax[j].set_xlabel('LG PH [GeV]')
    ax[j].set_ylabel('OREO PH [GeV]')
    #ax[j].legend(fontsize = 15)
    ax[j].set_title(f'{ax_amolabel[j]} True Value')
    plt.tight_layout()

fig, ax = plt.subplots(1, 2, figsize=(10, 6))
for j, nrun in enumerate(runnumbers):   
    zhist, xhist , yhist , mapable  = ax[j].hist2d(
        LG_PHGeV[nrun][cutpos_OREO[nrun]& cuttime_OREO[nrun]],ph_OREOGeV[nrun][cutpos_OREO[nrun] & cuttime_OREO[nrun]], 
        bins =[80,80], 
        cmap = my_cmap, 
        # range = [[0.8,15], [0.8,5]], 
        norm=colors.LogNorm())
    for k in range(len(thOREO)):
        ax[j].axhline(y = thOREO[k], color="red", linestyle = '--', lw = 2)
    plt.colorbar(mapable, ax = ax[j])
     
    ax[j].set_xlabel('LG PH [GeV]')
    ax[j].set_ylabel('OREO PH [GeV]')
    # ax[j].legend(fontsize = 15)
    ax[j].set_title(f'{ax_amolabel[j]} cutpos + cuttimeoreo & cuttimeLG')
    plt.tight_layout()
    
    
    
#%% calcolo della confusion matrix
sensitivity_list = []
precision_list = []
accuracy_list = []
for j, nrun in enumerate(runnumbers):

    TrueValues[nrun] = cuttime_OREO[nrun] & cutdiv[nrun][0] & cutpos_OREO[nrun] & cutcherry[nrun]
    sensitivity_list = []
    precision_list = []
    accuracy_list = []
    for nth in range(len(thOREO)): 
        OREO_electrons[nrun] =  (ph_OREOGeV[nrun]>thOREO[nth]) & cuttime_OREO[nrun]& cutpos_OREO[nrun]
        tn_oreo, fp_oreo, fn_oreo, tp_oreo = confusion_matrix(TrueValues[nrun],OREO_electrons[nrun]).ravel()
        sensitivity_list.append((tp_oreo/(tp_oreo+fn_oreo))*100)
        precision_list.append((tp_oreo/(tp_oreo+fp_oreo))*100)
        accuracy_list.append((tp_oreo+tn_oreo)/((tp_oreo+tn_oreo+fp_oreo+fn_oreo))*100)
    sensitivity[nrun] = sensitivity_list
    precision[nrun] = precision_list
    accuracy[nrun] = accuracy_list
    #considero come eventi totali solo quelli che impattano su oreo, è giusta sta roba???
    print(f'total events, {ax_amolabel[j]} = {len(TrueValues[nrun][cutpos_OREO[nrun]])}')
    print(f'total electron, {ax_amolabel[j]} = {(TrueValues[nrun][cutpos_OREO[nrun]]).sum()}')
    
    

    # print(f"Confusion matrix threshold on Oreo, {ax_amolabel[j]}")
    # print(f'Sensitivity = {(tp_oreo/(tp_oreo+fn_oreo))*100} %')
    # print(f'Precision = {(tp_oreo/(tp_oreo+fp_oreo))*100} %')
    #print(f'Accuracy = {(tp_oreo+tn_oreo)/((tp_oreo+tn_oreo+fp_oreo+fn_oreo))*100} %')


# %%
fig, ax = plt.subplots(1, 1, figsize=(10, 6))
for j, nrun in enumerate(runnumbers):
    ax.plot(sensitivity[nrun], precision[nrun], '.', markersize = 10, label = f'{ax_amolabel[j]}' )
ax.set_xlabel('Sensitivity [%]')
ax.set_ylabel('Precision [%]')
ax.legend(fontsize = 15)
ax.grid()
plt.tight_layout()
plt.show()

fig, ax = plt.subplots(1, 1, figsize=(10, 6))
for j, nrun in enumerate(runnumbers):
    ax.plot(thOREO, sensitivity[nrun], '.', markersize = 15, label = f'{ax_amolabel[j]}')
ax.set_xlabel('Oreo PH threshold[%]')
ax.set_ylabel(' Sensitivity [%]')
ax.legend(fontsize = 15)
ax.grid()
plt.tight_layout()
plt.show()

fig, ax = plt.subplots(1, 1, figsize=(10, 6))
for j, nrun in enumerate(runnumbers):
    ax.plot(thOREO, precision[nrun], '.', markersize = 15, label = f'{ax_amolabel[j]}')
ax.set_xlabel('Oreo PH threshold[%]')
ax.set_ylabel(' Precision [%]')
ax.legend(fontsize = 15)
ax.grid()
plt.tight_layout()
plt.show()

fig, ax = plt.subplots(1, 1, figsize=(10, 6))
for j, nrun in enumerate(runnumbers):
    ax.plot(thOREO, accuracy[nrun], '.', markersize = 15, label = f'{ax_amolabel[j]}')
ax.set_xlabel('Oreo PH threshold[%]')
ax.set_ylabel(' Accuracy [%]')
ax.legend(fontsize = 15)
ax.grid()
plt.tight_layout()
plt.show()
# %%
