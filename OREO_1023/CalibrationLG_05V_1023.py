#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 13 10:29:27 2023

@author: ale
"""
### calibrazione unico LG con tagli in divergenza e tempo 
## c'è un po' di casino per avere le due rette sia a 0.5 di range che a 2
from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
model = GaussianModel()
plt.rcParams['font.size'] = '20'

# Calibrazione con Range 0.5V 
chLabel = {'LG':0, 'APC':1, 'Cherry44':2, 'Cherry48':3, 'George':4, 'John':5, 'Paul':6, 'Ringo':7}
 #calibrazione range 0.5
datapath = r'C:\Users\aselmi\Desktop\Dottorato\data\dataOREO_1023\HDF5\run7200'
runnumbers = [93, 94, 95, 96, 97, 98]
h5keys = [ 'xpos','digiPH', 'digiTime']
energies = ['6', '5', '4', '3', '2', '1']
energies_num = [6,5,4,3,2,1]


distancesfromT1 = [500]
zcalo = 500+54
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]
divsup = 0.002
divinf = -0.002
timethsup = 240
timethinf = 200
thcherry = 200



Colors = [ 'darkred', 'orangered',  'lime', 'turquoise','hotpink',  'indigo']

mucalib = {}
muerrcalib = {}
sigmacaliberr = {}
sigmacalib = {}
cutdiv = {}
cuttime = {}
cuttot = {}
data = {}
cutcherry = {}
sigmadivx_ene = []
errsigmadivx_ene = []


for i, nrun in enumerate(runnumbers):
    #carico dati
    data[nrun] = opendata(nrun, datapath, h5keys)
    #divergenza
    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(data[nrun][0], 2, distancesfromT1,
                                                            p0divx, p0divy,
                                                            Rangex=[[-0.02,0.02]],
                                                            Rangey=[[-0.02,0.02]])
    
    
    datashift, shift = aligntracker(data[nrun][0], 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                                    distancesfromT1,
                                                                                    p0divx, p0divy,
                                                                                    Rangex=[[-0.004,0.004]],
                                                                                    Rangey=[[-0.004,0.004]], 
                                                                                    Color='navy', 
                                                                                    Figsize = (8,6))
    
    sigmadivx_ene.append(sigmaalign)
    errsigmadivx_ene.append(errsigmaalign)
    #tagli in divergenza
    cutdiv[nrun] = ((divexalign >divinf) & (divexalign<divsup) & (diveyalign>divinf) &(diveyalign<divsup))
    ax[0].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=divsup, color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=divsup, color="black", linestyle = '--', lw = 2)    
    ax[0].set_title(f'Lead Glass, {energies[i]} GeV ')
    #plot PH tempo
    import copy 
    import matplotlib as mpl
    my_cmap = copy.copy(mpl.cm.jet)
    my_cmap.set_bad(my_cmap(0))
    fig, ax = plt.subplots(1, 1, figsize=(8, 6))
    zhist2d, xhist2d, yhist2d, mapable  = ax.hist2d(data[nrun][1][:,chLabel['LG']],data[nrun][2][:,chLabel['LG']] ,
                bins =[100,100], cmap = my_cmap, range = [[0, 12000], [100, 300]], norm=colors.LogNorm())
    #taglio in tempo
    cuttime[nrun] = ((data[nrun][2][:,chLabel['LG']]<timethsup) & (data[nrun][2][:,chLabel['LG']]>timethinf))
    ax.axhline(y=timethinf, color="red", linestyle = '--', label = f'Threshold \n {energies[i]} GeV', lw = 2)
    ax.axhline(y=timethsup, color="red", linestyle = '--', lw = 2)
    ax.grid()
    ax.legend()
    ax.set_xlabel('PH [a.u]')
    ax.set_ylabel('time [ns]')
    fig.colorbar(mapable,ax=ax)
    #ax.set_title(f'Lead Glass {energies[i]} GeV')
    plt.tight_layout()
    #if energies[i] == '6': 
        #plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/t9/PHvsTime_6GeV.pdf')
    plt.show()
    
    #ricostruzione su faccia del LG
    xcalo = datashift[:, 0] + zcalo*np.tan(divexalign)
    ycalo = datashift[:, 1] + zcalo*np.tan(diveyalign)
    
    fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xcalo[0], ycalo[0],
                                                    bins=[100, 100], cmap=plt.cm.jet, range=[[0, 10], [0, 10]])
    
    ax.set_xlabel(f'x [cm], {energies[i]} GeV')
    ax.set_ylabel(f'y [cm], {energies[i]} GeV')
    fig.colorbar(mapable,ax=ax)

    # profilo cherenkov
    cutcherry[nrun] = ((data[nrun][1][:,chLabel['Cherry44']]>thcherry))
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    
    ax.hist((data[nrun][1][:,chLabel['Cherry44']]),
                 bins = 100, histtype ='step', color = 'teal', 
                 alpha = 1, lw = 3 ,label = f'Cherenkov \n {energies[i]}  GeV', range = [0, 6000])
    ax.axvline(x=thcherry, color="black", linestyle = '--', label = 'threshold', lw = 3)
    ax.grid()
    ax.legend()
    ax.set_xlabel(' PH [a.u]')
    ax.set_ylabel('Counts')
    ax.set_yscale('log')
    plt.tight_layout()
    #if energies[i] == '6': 
        #plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/t9/cherenkov_6GeV.pdf')
    plt.show()
    #taglio totale divergenza e tempo
    cuttot[nrun] = (cutdiv[nrun][0] & cuttime[nrun] & cutcherry[nrun])

#plot divergenza in funzione dell'energia
fig, ax = plt.subplots(1, 1, figsize=(8, 6)) 
sigmax_lst = []
sigmay_lst = []

for j in range(len(energies)):
    sigmax_lst.append(sigmadivx_ene[j][0]*10**(3))
    sigmay_lst.append(sigmadivx_ene[j][1]*10**(3))
    
ax.plot(energies_num, sigmax_lst, 
            marker='o',  linestyle = 'dotted', color = 'navy',
            markersize = 8, label = 'x ')
ax.plot(energies_num, sigmay_lst,  
            marker='D',  linestyle = 'dotted', color = 'darkgreen',
            markersize = 8, label = 'y')
   
ax.legend()
ax.grid()
ax.set_xlabel('Energies [GeV]')

ax.set_ylabel('Divergence [mrad]')
plt.savefig(r'C:\Users\aselmi\Desktop\Dottorato\Technical Note Oreo2023\T9_ottobre\T9Oct_beamDivercence.pdf')
     
    
plt.tight_layout()


#%%

fitrangecalibinf = [7000, 5500, 4000, 2500, 1000, 0]
fitrangecalibsup = [10000, 8000, 6000, 4500, 3000, 2000]
#plot di picchi fittati con una gaussiana per la calibrazione
figcal, axcal = plt.subplots(1, 1, figsize=(8, 6))   
for i, nrun in enumerate(runnumbers):#########################################################################################à   

    hph, binsph, _ = axcal.hist(data[nrun][1][:,chLabel['LG']][cuttot[nrun]], bins = 200, histtype ='stepfilled',
                          color = Colors[i],edgecolor = Colors[i], alpha = 0.4, 
                          lw =3, 
                          label = f'{energies[i]} GeV', density = True, range = [0, 10000])

    xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
    phcondition = ((xdata > fitrangecalibinf[i]) & (xdata < fitrangecalibsup[i]))
    paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
    resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                        weights=np.sqrt(hph[phcondition]))
    to_plot = np.linspace(fitrangecalibinf[i], fitrangecalibsup[i], 10000)
    y_eval = model.eval(resultx.params, x=to_plot)
    #ax[i].plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = 'black')
    mucalib[energies[i]] = resultx.params['center'].value
    muerrcalib[energies[i]] = resultx.params['center'].stderr
    sigmacalib[energies[i]] = resultx.params['sigma'].value
    sigmacaliberr[energies[i]] = resultx.params['sigma'].stderr
    axcal.plot(to_plot, y_eval, linewidth=2, color='black', label=f'$\mu$ = {round(mucalib[energies[i]],2)} $\pm$ {round(muerrcalib[energies[i]],2)}')
    
axcal.grid()
axcal.set_xlabel('PH [a.u]')
axcal.set_ylabel('Normalized counts')
axcal.legend(fontsize = 14)
plt.tight_layout()
axcal.set_ylim(0, 0.0036)
axcal.set_ylim(0, 0.010)
#plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/t9/T9calibrationPeaks.pdf')
plt.show()



#########################################################################
#########################################################################ààà
############################################################################


# Plot retta di calibrazione per energie range 0.5 V 

mucaliblist = []   
errmucaliblist = []
sigmacaliblist =[]
errsigmacaliblist = []

for energy in energies: 

    mucaliblist.append(mucalib[energy])
    errmucaliblist.append(muerrcalib[energy])
    sigmacaliblist.append(sigmacalib[energy])
    errsigmacaliblist.append(sigmacaliberr[energy])
    
    
figline, axline = plt.subplots(1,1, figsize=(8,8))
from lmfit.models import LinearModel
linearmodel = LinearModel()     
linearparams = linearmodel.guess(mucaliblist, x = energies_num)
linearresult = linearmodel.fit(mucaliblist, linearparams, x= energies_num, weights = errmucaliblist)
to_plot = np.linspace(0, max(energies_num), 10000)
y_eval = linearmodel.eval(linearresult.params, x=to_plot)
axline.errorbar(energies_num, mucaliblist, yerr= errmucaliblist, 
            marker='D',  linestyle = 'none', color = 'navy',
            markersize = 9)

axline.plot(to_plot,y_eval, color = 'red', lw = 2,
        label = f' a.u. = ({round(linearresult.params["slope"].value,2)} $\pm$ {round(linearresult.params["slope"].stderr,2)})$\cdot$ GeV +  ({round(linearresult.params["intercept"].value,2)}  $\pm$ {round(linearresult.params["intercept"].stderr,2)})')
axline.legend(fontsize = 15)
axline.set_ylabel('PH [a.u.]')
axline.set_xlabel('GeV')
slopeCalib23=(linearresult.params['slope'].value)
errslopeCalib23 = (linearresult.params['slope'].stderr)
interceptCalib23= (linearresult.params['intercept'].value)
errinterceptCalib23 = (linearresult.params['intercept'].stderr)
print(f'Chi-square = {linearresult.chisqr:.4f}, Reduced Chi-square = {linearresult.redchi:.4f}')
axline.legend(fontsize = 15)
axline.set_ylabel('PH [a.u.]')
axline.set_xlabel('GeV')
axline.grid()
axline.set_ylim(0, 9000)
plt.tight_layout()
plt.savefig(r'C:\Users\aselmi\Desktop\Dottorato\Technical Note Oreo2023\T9_ottobre\T9Oct_LGCalibLine_05V.pdf')
plt.show()


# calibParam2V = np.loadtxt(r'C:\Users\aselmi\Desktop\Dottorato\analysis\OREO_1023\T9oct_calibLineParameter2V.dat')
# mucaliblist = calibParam2V[0]
# errmucaliblist = calibParam2V[1] 
# sigmacaliblist = calibParam2V[2]
# errsigmacaliblist = calibParam2V[3]
# energies_num = [1,3,6,10]

# linearparams = linearmodel.guess(mucaliblist, x = energies_num)
# linearresult = linearmodel.fit(mucaliblist, linearparams, x= energies_num, weights = errmucaliblist)
# to_plot = np.linspace(0, max(energies_num), 10000)
# y_eval = linearmodel.eval(linearresult.params, x=to_plot)
# axline.errorbar(energies_num, mucaliblist, yerr= errmucaliblist, 
#             marker='D',  linestyle = 'none', color = 'darkgreen',
#             markersize = 9)

# axline.plot(to_plot,y_eval, color = 'red', lw = 2,linestyle = 'dotted', 
#         label = f' a.u. = ({round(linearresult.params["slope"].value,2)} $\pm$ {round(linearresult.params["slope"].stderr,2)})$\cdot$ GeV +  ({round(linearresult.params["intercept"].value,2)}  $\pm$ {round(linearresult.params["intercept"].stderr,2)})')
# axline.legend(fontsize = 15)
# axline.set_ylabel('PH [a.u.]')
# axline.set_xlabel('GeV')
# slopeCalib23=(linearresult.params['slope'].value)
# errslopeCalib23 = (linearresult.params['slope'].stderr)
# interceptCalib23= (linearresult.params['intercept'].value)
# errinterceptCalib23 = (linearresult.params['intercept'].stderr)
# print(f'Chi-square = {linearresult.chisqr:.4f}, Reduced Chi-square = {linearresult.redchi:.4f}')
# axline.legend(fontsize = 15)
# axline.set_ylabel('PH [a.u.]')
# axline.set_xlabel('GeV')
# axline.grid()
# axline.set_ylim(0, 9000)
# plt.tight_layout()
# plt.savefig(r'C:\Users\aselmi\Desktop\Dottorato\Technical Note Oreo2023\T9_ottobre\T9Oct_LGCalibLine_2V&05V.pdf')
# plt.show()

#calcolo dei residui
#%%
residual_lowE = 100* ( mucaliblist-linearresult.best_fit)/(linearresult.best_fit)
fig, ax = plt.subplots(1,1, figsize=(10,4))
ax.plot(energies_num
        , residual_lowE, 'D', markersize = 8, color = 'navy', label = 'Dynamic range 0.5 V')
ax.axhline(y= 0, ls = ':', c = 'k', lw = 3)

ax.set_xlabel('GeV')
ax.set_ylabel('Residual $\%$')
#ax.set_ylim([-0.5, 0.5])
ax.grid()

plt.tight_layout()
plt.savefig(r'C:\Users\aselmi\Desktop\Dottorato\Technical Note Oreo2023\T9_ottobre\T9Oct_LGResidual05V.pdf')
plt.show()

calib = [slopeCalib23, errslopeCalib23, interceptCalib23, errinterceptCalib23]

with open('1023LG_CalibrationOREO_05V.dat','w') as out_file:
        np.savetxt(out_file, calib)
        
## attenzione che hanno gli stessi nomi-- qui faccio retta per i tre ad alta energia con range 2V

############### I NOMI SONO A CASOOOOOOOO TI ODIOOOOOOOOOOOOOOOOOOOOOOO########        

# %%
#%%  calcoliamo la risoluzione  energetica del LG 



plt.rcParams['font.size'] = '20'
energy_resolution = []
energy_resolutionerr = []
for k in range(len(mucaliblist)): 
    energy_resolution.append((sigmacaliblist[k]/mucaliblist[k])*100)
    energy_resolutionerr.append((np.sqrt((errsigmacaliblist[k]/sigmacaliblist[k])**2+(errmucaliblist[k]/mucaliblist[k])**2)*energy_resolution[k]))


def energy_resolution_function(E, a, b, c):
    return np.sqrt( (a**2 / E) + (b / E)**2+ c**2)

from lmfit import Model
fig, ax = plt.subplots(1,1, figsize=(8,6))
model = Model(energy_resolution_function)
params = model.make_params(a=1.0, b=1.0, c=1.0)
result = model.fit(energy_resolution, params, E=energies_num, weights=energy_resolutionerr)
a_fit = result.params['a'].value
b_fit = result.params['b'].value
c_fit = result.params['c'].value

sigma_a = result.params['a'].stderr
sigma_b = result.params['b'].stderr
sigma_c = result.params['c'].stderr
to_plot = np.linspace(min(energies_num), max(energies_num), 1000)
fit_energy_resolution = energy_resolution_function(to_plot, a_fit, b_fit, c_fit)

ax.errorbar(energies_num, energy_resolution,yerr=energy_resolutionerr, marker = 'D', markersize = 10
            , color = 'navy',
            linestyle= 'none', label = '$\sigma$(E)/E')


ax.plot(to_plot, fit_energy_resolution, label = '$\sigma$(E)/E = $\sqrt{(a^{2} / E) + (b / E)^2+ c^2)}$' , color = 'red')
    
ax.set_xlabel('GeV')
ax.set_ylabel('Energy resolution [%]')
ax.legend(fontsize = 18)
ax.grid()
plt.tight_layout()
plt.savefig(r'C:\Users\aselmi\Desktop\Dottorato\Technical Note Oreo2023\T9_ottobre\T9oct_LGenergyRes05V.pdf')
plt.show()


print(f'a = {a_fit}, b = {b_fit}, c = {c_fit}  ')
print(f'a = {sigma_a}, b = {sigma_b}, c = {sigma_c}  ')
# %%
