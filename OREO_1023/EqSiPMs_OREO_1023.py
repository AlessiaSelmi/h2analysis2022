#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 13 10:17:12 2023

@author: ale
"""

# Prova a fare la cosa dell'efficienza con MIP. poi devi equalizzare la risposta dei SiPM e fari la stessa cosa con gli ettroni
# da PDG --> una MIP fa 10.20 MeV cm 
#
#### vedere relazione divergenza posizione 
from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
import uproot
from lmfit.models import LinearModel

plt.rcParams['font.size'] = '15'
#triggere APC + Cherry
# Ch 0 -->  XCET 48 (Cherry 2)
# Ch 1 --> LG1 (Door / T10 side)
# Ch 2 --> George 1
# Ch 3 --> George 2
# Ch 4 --> John 1
# Ch 5 --> John 2
# Ch 6 --> Paul 1
# Ch 7 --> Paul 2


datapath = r'C:\Users\aselmi\Desktop\Dottorato\data\dataOREO_1023\HDF5\run720'
h5keys = [ 'xpos','digiPH', 'digiTime']

chLabel = {'LG':0, 'APC':1, 'Cherry44':2, 'Cherry48':3, 'George':4, 'John':5, 'Paul':6, 'Ringo':7}
SiPMlabel = ['George','John', 'Paul', 'Ringo']
runnumber = [117, 119] # 0°, 90°
#runnumbers = [242] #amorfo
distancesfromT1 = [500]
zcryst = 549
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]
### varie threshold per i tagli 
thcherryinf = 0
thcherrysup = 200
divsup = 0.0010
divinf = -0.0010

timethsup = [240]*6
timethinf = [200]*6
phth = [400]*6

crystlenght = 2.3# per il cut fiduciale. La vera lunghezza è 2.5

xcrystinf = 0.5
xcrystsup = xcrystinf+crystlenght
ycrystinf= 4.5
ycrystsup = ycrystinf+crystlenght


crystlenght_simulation = 2
 # per il cut fiduciale. La vera lunghezza è 2.5

xcrystinf_simulation = -3
xcrystsup_simulation = xcrystinf_simulation+crystlenght_simulation
ycrystinf_simulation = -1
ycrystsup_simulation = ycrystinf_simulation+crystlenght_simulation

Colors = ['lime', 'hotpink','orangered', 'navy']
mpv = []
mpvstd = []
limInf = [[110, 100, 150, 150], [80, 70, 100, 100]]
limSup = [[250, 500, 800, 400], [200, 200, 250, 250]]
fitPar = [[[160, 300,200], [200, 300,200], [160, 400,250], [100, 200,150]],[[160, 80,100], [100, 90,100], [100, 120,100], [100, 110,100]]]

rangecrystlandau = [[[80,400], [70, 300], [100, 500], [100, 500]], [[60, 300], [53, 400], [60, 300], [60,300]]]
data = {}
cutdiv = {}
cuttime = {}
cutph = {}
cutcherry = {}
cuttot = {}
mpv_simulation = []
mpvstd_simulation = []
#%%
#apertura dati + profilo camere 
#############################################
for k, nrun in enumerate(runnumber): 
    data[nrun] = opendata(nrun, datapath, h5keys)
    xpos = data[nrun][0]
    #camere 
    xlabels = ['BC1',' BC1', 'BC2', 'BC2']
    xlim = [[-0,10]]*4
    histpos1d(xpos, 2, xlabels = xlabels, xlim = xlim, Range = [0,10])
    histpos2d(xpos, numberoftrackers = 2)
    #correlazioni camere 
    fig, ax = plt.subplots(1,2 , figsize=(8,8))
    ax = ax.flat
    ax[0].hist2d(xpos[:,0], xpos[:,2], bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet)
    ax[0].set_ylabel('x BC2 [cm]')
    ax[0].set_xlabel('x BC1 [cm]')
    
    ax[1].hist2d(xpos[:,1],xpos[:,3] , bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet,)
    ax[1].set_xlabel('y BC1 [cm]')
    ax[1].set_ylabel('x BC2 [cm]')
    plt.tight_layout()
    plt.show()
    
    #divergenze


    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(xpos, 2, distancesfromT1,
                                                           p0divx, p0divy,
                                                           Rangex=[[-0.02,0.02]],
                                                           Rangey=[[-0.02,0.02]])
    
    
    datashift, shift = aligntracker(xpos, 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                                   distancesfromT1,
                                                                                    p0divx, p0divy,
                                                                                    Rangex=[[-0.004,0.004]],
                                                                                    Rangey=[[-0.004,0.004]], 
                                                                                    Color='navy', 
                                                                                    Figsize = (8,6))
    cutdiv[nrun] = ((divexalign >divinf) & (divexalign<divsup) & (diveyalign>divinf) &(diveyalign<divsup))
    ax[0].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=divsup, color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=divsup, color="black", linestyle = '--', lw = 2)   
    
   
    # profilo cherenkov
    cutcherry[nrun] = ((data[nrun][1][:,chLabel['Cherry44']]>thcherryinf)&(data[nrun][1][:,chLabel['Cherry44']]<thcherrysup))
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    
    ax.hist((data[nrun][1][:,chLabel['Cherry44']]),
                 bins = 100, histtype ='stepfilled', color = 'turquoise', edgecolor = 'turquoise', 
                 alpha = 0.5, lw = 3 ,label = 'Cherry', range = [0, 6000])
    ax.axvline(x=thcherryinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax.axvline(x=thcherrysup, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax.grid()
    ax.set_xlabel('Cherenkov PH [a.u]')
    ax.set_ylabel('Entries [ns]')
    ax.set_yscale('log')
    fig.set_tight_layout('tight')
    
    plt.show()
   
    # plot PH e tempo + tagli su tempi e PH dei tre cristalli 
    fig, ax = plt.subplots(2, 2, figsize=(10, 6))
    ax = ax.flat
    for i in range(len(SiPMlabel)):
        ax[i].hist2d(data[nrun][1][:,chLabel[SiPMlabel[i]]],data[nrun][2][:,chLabel[SiPMlabel[i]]] ,
                     bins =[100,100], cmap = plt.cm.jet, range = [[0, 8000], [150, 300]], norm=colors.LogNorm())
        ax[i].axhline(y=timethinf[i], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[i].axhline(y=timethsup[i], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[i].axvline(x=phth[i], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[i].set_ylabel('Entries')
        ax[i].set_xlabel('PH [a.u]')
        ax[i].set_title(f'{SiPMlabel[i]}')
        ax[i].grid()
    plt.tight_layout()
    plt.show()
    # faccimao un po' di tagli 
    cuttime[nrun] = (((data[nrun][2][:,chLabel['George']]>timethinf[0]) & (data[nrun][2][:,chLabel['George']]<timethsup[0])&(data[nrun][2][:,chLabel['George']]>timethinf[0]) & (data[nrun][2][:,chLabel['George']]<timethsup[0]))|
                 ((data[nrun][2][:,chLabel['John']]>timethinf[1]) & (data[nrun][2][:,chLabel['John']]<timethsup[1])&(data[nrun][2][:,chLabel['John']]>timethinf[1]) & (data[nrun][2][:,chLabel['John']]<timethsup[1]))|
                 ((data[nrun][2][:,chLabel['Paul']]>timethinf[2]) & (data[nrun][2][:,chLabel['Paul']]<timethsup[2])& (data[nrun][2][:,chLabel['Paul']]>timethinf[2]) & (data[nrun][2][:,chLabel['Paul']]<timethsup[2]))|
                 ((data[nrun][2][:,chLabel['Ringo']]>timethinf[3]) & (data[nrun][2][:,chLabel['Ringo']]<timethsup[3])&(data[nrun][2][:,chLabel['Ringo']]>timethinf[3]) & (data[nrun][2][:,chLabel['Ringo']]<timethsup[3])))
    # I cristalli li vedi andando a fare i tagli sulla PH dei vari SiPM, se chiedi solo il taglio su George vedi lui 
    cutph[nrun] = ((data[nrun][1][:,chLabel['George']]>phth[0])|
             (data[nrun][1][:,chLabel['John']]>phth[1])|
             (data[nrun][1][:,chLabel['Paul']]>phth[2])|(data[nrun][1][:,chLabel['Ringo']]>phth[2]))
    
    
    cuttot[nrun] = cuttime[nrun]
    # ricostruzione traccia 
    xcryst = xpos[:,0] + zcryst*np.tan(divexalign)
    ycryst = xpos[:,1] + zcryst*np.tan(diveyalign)
    fig, ax = plt.subplots(1,1, figsize=(8,8))
    zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xcryst[0], ycryst[0], cmap=plt.cm.jet, bins=[80, 80], range=[[0,10], [0,10]])
    plt.colorbar(mapable, ax=ax)
    ax.set_xlabel('x [cm]')
    ax.set_xlabel('y [cm]')
    ax.set_title('Proiezione su piano del cristallo')
    plt.tight_layout()
    plt.show()


    fig, ax = plt.subplots(1,1, figsize=(8,8))
    zhist2dcut, xhist2dcut , yhist2dcut , mapable = ax.hist2d(xcryst[0][cuttot[nrun]], ycryst[0][cuttot[nrun]], 
                                                                  cmap=plt.cm.jet, bins = [80,80],
                                                                  range = [[0,10], [0,10]])


    plt.colorbar(mapable, ax=ax)

    ax.set_ylabel('x [cm]')
    ax.set_xlabel('y [cm]')

    plt.tight_layout()
    plt.show()



    
    fig, ax = plt.subplots(1,1, figsize=(8,8))
    np.seterr(divide='ignore', invalid = 'ignore')
    eff = np.divide(zhist2dcut, zhist2d)
    eff[np.isnan(eff)]=0

    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.1)
    im = ax.imshow((np.transpose((eff))), cmap=plt.cm.jet, extent=[0,10, 0, 10])
    fig.colorbar(im, cax=cax, orientation='vertical')
    #rect = patches.Rectangle((3.2, 1.4), 3.1, 3.1, linewidth=2, edgecolor='darkred', facecolor='none')
    #ax1[i].add_patch(rect)
    ax.set_xlim([0,10])
    ax.set_ylim([0,10])
    ax.set_xlabel('x [cm]')
    ax.set_ylabel('y [cm]')
    
    
    from lmfit.models import ExpressionModel
    #uso l'approssimazione di Moyal 
    modLandau = ExpressionModel('A * exp(-0.5 * ((x-mpv)/width + exp(-(x-mpv)/width)))')

    fig, ax = plt.subplots(2, 2, figsize=(15, 6))
    ax = ax.flat
    
    for j, Label in enumerate(SiPMlabel) :
    
        h, bins, _ = ax[j].hist(data[nrun][1][:,chLabel[f'{Label}']][cutdiv[nrun][0]],
                    bins = 80, histtype ='stepfilled', color = Colors[j], edgecolor = Colors[j], 
                    alpha = 0.5, lw = 3 ,label = f'{Label}', range = rangecrystlandau[k][j])
        err = h
        xdata = np.array([(bins[k+1]+bins[k])/2 for k in range(len(bins)-1)])
        phcondition = ((xdata > limInf[k][j]) & (xdata < limSup[k][j]))
        params = modLandau.make_params(A = fitPar[k][j][0], mpv = fitPar[k][j][1], width = fitPar[k][j][2] )
        result = modLandau.fit(h[phcondition], params, x=xdata[phcondition])
        print(f'Chi-square = {result.chisqr:.4f}, Reduced Chi-square = {result.redchi:.4f}')
        #print(result.fit_report())
        mpv.append(result.params['mpv'].value)
        mpvstd.append(result.params['mpv'].stderr)
        to_plot = np.linspace(-1000,3000,10000)
        ax[j].plot(xdata[phcondition], result.best_fit, label=f'mpv = {round(result.params["mpv"].value,2)} $\pm$ {round(result.params["mpv"].stderr,2)}', color = 'black', lw = 2)

        ax[j].legend(fontsize = 12)
        ax[j].set_ylabel('Entries')
        ax[j].set_xlabel('PH [a.u]')
        ax[j].grid()
    plt.tight_layout()
    plt.show()



#%% calcolo q da sottrarre
thick = [4.5,2.5]
intercept_eq1 = []
errintercept_eq1 = []
fig, axline = plt.subplots(2, 2, figsize=(10, 8))
axline = axline.flat
for i in range(4):      
    mpvcryst = []     
    mpvcryststd = []           
    mpvcryst.append(mpv[i])
    mpvcryst.append(mpv[i+4])
    mpvcryststd.append(mpvstd[i])
    mpvcryststd.append(mpvstd[i+4])
    
    linearmodel = LinearModel()     
    linearparams = linearmodel.guess(mpvcryst, x=thick)
    linearresult = linearmodel.fit(mpvcryst, linearparams, x = thick , weights = mpvcryststd)
    to_plot = np.linspace(0, 8, 10000)
    y_eval = linearmodel.eval(linearresult.params, x=to_plot)
    axline[i].errorbar(thick, mpvcryst, yerr = mpvcryststd, 
                marker='D',  linestyle = 'none', color = Colors[i],
                markersize = 8)
    axline[i].plot(to_plot,y_eval, color = 'black', lw = 2, ls = '--',
            label = f' m = ({round(linearresult.params["slope"].value,4)}) \n q = ({round(linearresult.params["intercept"].value,4)})')
    axline[i].legend(fontsize = 15)
    axline[i].set_ylabel('PH [a.u]')
    axline[i].set_xlabel(f'{SiPMlabel[i]} thickness')
    slope_eq1=(linearresult.params['slope'].value)
    errslope_eq1 = (linearresult.params['slope'].stderr)
    intercept_eq1.append((linearresult.params['intercept'].value))
    errintercept_eq1.append((linearresult.params['intercept'].stderr))
    axline[i].grid()
    print(f'Chi-square = {linearresult.chisqr:.4f}, Reduced Chi-square = {linearresult.redchi:.4f}')

plt.tight_layout()
plt.show()


with open('1023T9_AllSiPM_intercept1.dat','w') as out_file:
        np.savetxt(out_file, intercept_eq1)
        
        
#%% 
#prova calibrazione dei tre cristalli
energyMIP  = [10.20*4.5*10**(-3), 10.20*2.5*10**(-3)]
OREO_mcalib = []
OREO_qcalib = []
fig, axline = plt.subplots(2, 2, figsize=(10, 8))
axline = axline.flat
for i in range(4):      
    mpvcryst = []     
    mpvcryststd = []           
    mpvcryst.append(mpv[i])
    mpvcryst.append(mpv[i+4])
    mpvcryststd.append(mpvstd[i])
    mpvcryststd.append(mpvstd[i+4])
    
    linearmodel = LinearModel()     
    linearparams = linearmodel.guess(mpvcryst, x=energyMIP )
    linearresult = linearmodel.fit(mpvcryst, linearparams, x = energyMIP  , weights = mpvcryststd)
    to_plot = np.linspace(0.02, 0.05, 10000)
    y_eval = linearmodel.eval(linearresult.params, x=to_plot)
    axline[i].errorbar(energyMIP , mpvcryst, yerr = mpvcryststd, 
                marker='D',  linestyle = 'none', color = Colors[i],
                markersize = 8)
    axline[i].plot(to_plot,y_eval, color = 'black', lw = 2, ls = '--',
            label = f' m = {round(linearresult.params["slope"].value,4)} \n q = {round(linearresult.params["intercept"].value,4)}')
    axline[i].legend(fontsize = 13)
    axline[i].set_ylabel(f'{SiPMlabel[i]} PH [a.u]')
    axline[i].set_xlabel('MIPs energy deposited [GeV]')
    axline[i].grid()
    OREO_mcalib.append(linearresult.params["slope"].value)
    OREO_qcalib.append(linearresult.params["intercept"])
plt.tight_layout()
with open('T9oct_OREOslope.dat','w') as out_file:
        np.savetxt(out_file, OREO_mcalib)
with open('T9oct_OREOintercept.dat','w') as out_file:
        np.savetxt(out_file, OREO_qcalib)
plt.savefig(r'C:\Users\aselmi\Desktop\Dottorato\Technical Note Oreo2023\T9_ottobre\T9Oct_CalibLine_allSiPMs.pdf')
plt.show()
#%% sommo e a faccio (PH-q)

mpv2 = []
mpv2std = []

limInf = [85, 90, 140, 140]
limSup = [300, 300, 350, 350]
fitPar = [[250, 200,150], [250, 150,150], [150, 150, 200],  [150, 150, 200]]


fig, ax = plt.subplots(2, 2, figsize=(15, 6))
ax = ax.flat

for j, Label in enumerate(SiPMlabel) :

    h, bins, _ = ax[j].hist((data[117][1][:,chLabel[f'{Label}']])-intercept_eq1[j],
                bins = 150, histtype ='stepfilled', color = Colors[j], edgecolor = Colors[j], 
                alpha = 0.5, lw = 3 ,label = f'{Label}', range = [60, 600])
    err = h
    xdata = np.array([(bins[k+1]+bins[k])/2 for k in range(len(bins)-1)])
    phcondition = ((xdata > limInf[j]) & (xdata < limSup[j]))
    params = modLandau.make_params(A = fitPar[j][0], mpv = fitPar[j][1], width = fitPar[j][2] )
    result = modLandau.fit(h[phcondition], params, x=xdata[phcondition])
    print(f'Chi-square = {result.chisqr:.4f}, Reduced Chi-square = {result.redchi:.4f}')
    print(result.fit_report())
    mpv2.append(result.params['mpv'].value)
    mpv2std.append(result.params['mpv'].stderr)
    to_plot = np.linspace(-1000,3000,10000)
    ax[j].plot(xdata[phcondition], result.best_fit, label=f'mpv = {round(mpv[j],2)} $\pm$ {round(result.params["mpv"].stderr,2)}', color = 'black', lw = 2)

    ax[j].legend(fontsize = 12)
    ax[j].set_ylabel('Entries')
    ax[j].set_xlabel('(PH - q) [a.u]')
    ax[j].grid()
plt.tight_layout()
plt.show()
#%%
SiPM_eqfactor = []
for i in range(4): 
    SiPM_eqfactor.append(mpv2[0]/mpv2[i])

with open('1023T9_AllSiPM_eqfactor.dat','w') as out_file:
        np.savetxt(out_file, SiPM_eqfactor)
## vediamo se sono equalizzate        

for nrun in runnumber: 
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    ax.hist(((data[nrun][1][:,chLabel['George']])[cuttot[nrun]]-intercept_eq1[0])*SiPM_eqfactor[0], 
            bins = 100, histtype ='step', color = 'orangered', 
            alpha = 0.5, lw = 3, range = [0, 400], density = True, label = 'George')
    ax.hist(((data[nrun][1][:,chLabel['John']])[cuttot[nrun]]-intercept_eq1[1])*SiPM_eqfactor[1], 
            bins = 100, histtype ='step', color ='hotpink', 
            alpha = 0.5, lw = 3, range = [0, 400], density = True, label = 'John')
    ax.hist(((data[nrun][1][:,chLabel['Paul']])[cuttot[nrun]]-intercept_eq1[2])*SiPM_eqfactor[2], 
            bins = 100, histtype ='step', color = 'lime', 
            alpha = 0.5, lw = 3, range = [0, 400], density = True, label = 'Paul')
    ax.hist(((data[nrun][1][:,chLabel['Ringo']])[cuttot[nrun]]-intercept_eq1[3])*SiPM_eqfactor[3], 
            bins = 100, histtype ='step', color = 'navy', 
            alpha = 0.5, lw = 3, range = [0, 400], density = True, label = 'Ringo')
    ax.legend()
    ax.grid()
    ax.set_xlabel(f'(PH-q)*eqfact, run {nrun}')
    ax.set_ylabel('entries')
    plt.tight_layout()


# PH_tot[nrun] = ((((data[nrun][1][:,chLabel['George1']]+data[nrun][1][:,chLabel['George2']])-SiPM_intercept1[0])*SiPM_eqfact[0])+
#                  (((data[nrun][1][:,chLabel['John1']]+data[nrun][1][:,chLabel['John2']])-SiPM_intercept1[1])*SiPM_eqfact[1])+
#                  (((data[nrun][1][:,chLabel['Paul1']]+data[nrun][1][:,chLabel['Paul2']])-SiPM_intercept1[2])*SiPM_eqfact[2]))
         
#%%
###############        CALIBRIAMOOOOOOOOOOOOOOOOOOOOOO       ##################
#Energy deposit by a MIP in PWO

thicknessCalib = [4.5, 5]

ph_SiPMfront = {}
ph_SiPMlateral = {}
for j, Label in enumerate(SiPMlabel) :
   ph_SiPMfront[Label] =  ((data[117][1][:,chLabel[f'{Label}']])-intercept_eq1[j])*SiPM_eqfactor[j]
for j, Label in enumerate(SiPMlabel) :
   ph_SiPMlateral[Label] =  ((data[119][1][:,chLabel[f'{Label}']])-intercept_eq1[j])*SiPM_eqfactor[j]

#%%    
#PH_totfront = ((data[runnumber[0]][1][:,chLabel['George']])[cuttot[nrun]]-intercept_eq1[0])*SiPM_eqfactor[0] + ((data[runnumber[0]][1][:,chLabel['John']])[cuttot[nrun]]-intercept_eq1[1])*SiPM_eqfactor[1] + ((data[runnumber[0]][1][:,chLabel['Paul']])[cuttot[nrun]]-intercept_eq1[2])*SiPM_eqfactor[2] + ((data[runnumber[0]][1][:,chLabel['Ringo']])[cuttot[nrun]]-intercept_eq1[3])*SiPM_eqfactor[3]
PH_totfront = ph_SiPMfront['George']+ph_SiPMfront['John']+ph_SiPMfront['Paul']+ph_SiPMfront['Ringo']              
#PH_totlateral = ((data[runnumber[]][1][:,chLabel['George']])[cuttot[nrun]]-intercept_eq1[0])*SiPM_eqfactor[0] + ((data[runnumber[0]][1][:,chLabel['John']])[cuttot[nrun]]-intercept_eq1[1])*SiPM_eqfactor[1] + ((data[runnumber[0]][1][:,chLabel['Paul']])[cuttot[nrun]]-intercept_eq1[2])*SiPM_eqfactor[2] + ((data[runnumber[0]][1][:,chLabel['Ringo']])[cuttot[nrun]]-intercept_eq1[3])*SiPM_eqfactor[3]
PH_totlateral = ph_SiPMlateral['George']+ph_SiPMlateral['John']+ph_SiPMlateral['Paul']+ph_SiPMlateral['Ringo']


PH_tot = [PH_totfront, PH_totlateral]

mpv_tot= []
mpvstd_tot = []

limInf = [100, 120]
limSup = [500, 400]
fitPar = [[200, 100,200], [200, 150, 300]]

Colors_tot = ['green', 'steelblue']

fig, ax = plt.subplots(1, 2, figsize=(15, 6))
ax = ax.flat

for j, nrun in enumerate(runnumber):

    h, bins, _ = ax[j].hist(PH_tot[j][cuttot[nrun]],
                bins = 300, histtype ='stepfilled', color = Colors_tot[j], edgecolor = Colors_tot[j], 
                alpha = 1, lw = 3 , range = [0, 500])
    err = h
    xdata = np.array([(bins[k+1]+bins[k])/2 for k in range(len(bins)-1)])
    phcondition = ((xdata > limInf[j]) & (xdata < limSup[j]))
    params = modLandau.make_params(A = fitPar[j][0], mpv = fitPar[j][1], width = fitPar[j][2] )
    result = modLandau.fit(h[phcondition], params, x=xdata[phcondition])
    print(f'Chi-square = {result.chisqr:.4f}, Reduced Chi-square = {result.redchi:.4f}')
    print(result.fit_report())
    mpv_tot.append(result.params['mpv'].value)
    mpvstd_tot.append(result.params['mpv'].stderr)
    to_plot = np.linspace(-1000,3000,10000)
    ax[j].plot(xdata[phcondition], result.best_fit, label=f'mpv = {round(mpv_tot[j],2)} $\pm$ {round(result.params["mpv"].stderr,2)}', color = 'black', lw = 4)

    ax[j].legend(fontsize = 12)
    ax[j].set_ylabel('Entries')
    ax[j].set_xlabel(f'OREO total PH [a.u], {thicknessCalib[j]} mm')
    ax[j].grid()
plt.tight_layout()
plt.show()

#%%
#%%%


    
    
#%% per calibrare so quanto rilascia una mip in un dato spessore -- calcolo la retta di calibrazione
slope_SiPM_calib = []
errslope_SiPM_calib = []
intercept_SiPM_calib = []
errintercept_SiPM_calib = []

#energyMIP = mpv_simulation #GeV
energyMIP  = [10.20*thicknessCalib[0]*10**(-3), 10.20*thicknessCalib[1]*10**(-3)]
fig, axline = plt.subplots(1, 1, figsize=(6, 6))



linearmodel = LinearModel()     
linearparams = linearmodel.guess(mpv_tot, x=energyMIP)

linearresult = linearmodel.fit(mpv_tot, linearparams, x = energyMIP , weights =  mpvstd_tot)
to_plot = np.linspace(0.04, 0.06, 10000)
y_eval = linearmodel.eval(linearresult.params, x=to_plot)
axline.errorbar(energyMIP, mpv_tot, yerr = mpvstd_tot, 
            marker='D',  linestyle = 'none', color = 'navy',
            markersize = 8)
axline.plot(to_plot,y_eval, color = 'black', lw = 2, ls = '--',
        label = f' a.u. = ({round(linearresult.params["slope"].value,4)}$\cdot$ GeV +  ({round(linearresult.params["intercept"].value,4)}')
axline.legend(fontsize = 15)
axline.set_ylabel('OREO total PH [a.u]')
axline.set_xlabel('MIPs energy deposit [GeV]')
slope_SiPM_calib.append((linearresult.params['slope'].value))
errslope_SiPM_calib.append((linearresult.params['slope'].stderr))

intercept_SiPM_calib.append((linearresult.params['intercept'].value))
errintercept_SiPM_calib.append((linearresult.params['intercept'].stderr))
print(f'Chi-square = {linearresult.chisqr:.4f}, Reduced Chi-square = {linearresult.redchi:.4f}')
axline.grid()
plt.tight_layout()
plt.savefig(r'C:\Users\aselmi\Desktop\Dottorato\Technical Note Oreo2023\T9_ottobre\T9Oct_CalibLine_OREO.pdf')
plt.show()

SiPM_calib = [slope_SiPM_calib, errslope_SiPM_calib, intercept_SiPM_calib, errintercept_SiPM_calib]
with open('1023T9_AllSiPM_Calibration_OREO.dat','w') as out_file:
        np.savetxt(out_file, SiPM_calib)
        
#%%
# SiPM_calib = np.loadtxt('SiPM_Calibration_OREO0823.dat')
# SiPM_eqfact = np.loadtxt('SiPM_eqfactor.dat')
# SiPM_intercept1 = np.loadtxt('SiPM_intercept1.dat')
# fig, ax = plt.subplots(1, 1, figsize=(10, 6))
# ax.hist((data[nrun][1][:,chLabel['George1']]-SiPM_intercept1[0])*SiPM_eqfact[0], 
#         bins = 50, histtype ='stepfilled', color = 'red', edgecolor = 'red', 
#         alpha = 0.5, lw = 3, range = [100, 500])
# ax.hist( ((data[nrun][1][:,chLabel['John1']]-SiPM_intercept1[1])*SiPM_eqfact[1]), 
#         bins = 50, histtype ='stepfilled', color ='green', edgecolor = 'green', 
#         alpha = 0.5, lw = 3, range = [100, 500])
# ax.hist((data[nrun][1][:,chLabel['Paul1']]-SiPM_intercept1[2])*SiPM_eqfact[2], 
#         bins = 50, histtype ='stepfilled', color = 'blue', edgecolor = 'blue', 
#         alpha = 0.5, lw = 3, range = [100, 500])
#%%

# energyMIP = [0.9, 1.4] #MeV
# fig, axline = plt.subplots(1, 1, figsize=(10, 8))


# linearmodel = LinearModel()     
# linearparams = linearmodel.guess(mpv_tot, x=energyMIP)
# linearresult = linearmodel.fit(mpv_tot, linearparams, x = energyMIP , weights =  mpvstd_tot)
# to_plot = np.linspace(0, 2, 10000)
# y_eval = linearmodel.eval(linearresult.params, x=to_plot)
# axline.errorbar(energyMIP, mpv_tot, yerr = mpvstd_tot, 
#             marker='D',  linestyle = 'none', color = 'navy',
#             markersize = 8)
# axline.plot(to_plot,y_eval, color = 'black', lw = 2, ls = '--',
#         label = f' ADC = ({round(linearresult.params["slope"].value,4)} $\pm$ {round(linearresult.params["slope"].stderr,4)})$\cdot$ MeV +  ({round(linearresult.params["intercept"].value,4)}  $\pm$ {round(linearresult.params["intercept"].stderr,4)})')
# axline.legend(fontsize = 15)
# axline.set_ylabel('OREO total PH [a.u]')
# axline.set_xlabel('MIP energy deposit [MeV]')
# slope_eq1=(linearresult.params['slope'].value)
# errslope_eq1 = (linearresult.params['slope'].stderr)
# intercept_eq1.append((linearresult.params['intercept'].value))
# errintercept_eq1.append((linearresult.params['intercept'].stderr))
# print(f'Chi-square = {linearresult.chisqr:.4f}, Reduced Chi-square = {linearresult.redchi:.4f}')
# axline.grid()
# plt.tight_layout()
# plt.show()


# with open('SiPM_intercept1','w') as out_file:
#         np.savetxt(out_file, intercept_eq1)