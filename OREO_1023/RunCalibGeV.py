#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 13 10:29:27 2023

@author: ale
"""

#%%
### calibrazione unico LG con tagli in divergenza e tempo 
## c'è un po' di casino per avere le due rette sia a 0.5 di range che a 2
from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
model = GaussianModel()
plt.rcParams['font.size'] = '20'
import copy 
import matplotlib as mpl
my_cmap = copy.copy(mpl.cm.jet)
my_cmap.set_bad(my_cmap(0))
# Calibrazione con Range 0.5V 
chLabel = {'LG':0, 'APC':1, 'Cherry44':2, 'Cherry48':3, 'George':4, 'John':5, 'Paul':6, 'Ringo':7}
 #calibrazione range 0.5
datapath = r'C:\Users\aselmi\Desktop\Dottorato\data\dataOREO_1023\HDF5\run720'
runnumbers = ['125', '127']
h5keys = [ 'xpos','digiPH', 'digiTime']
energies = ['6', '6']
energies_num = [6, 6]
axRndLabel = ['Random', 'Axial']

distancesfromT1 = [500]
zcalo = 500+54
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]
divsup = 0.001
divinf = -0.001
timethsup = 240
timethinf = 200
thcherry = 200
zcryst = 549
cutpos_OREO = {}
cutpos_Paul = {}
cutpos_George = {}
cutpos_John = {}
cutpos_Ringo = {}
xinf_paul = 2.3
yinf_paul = 4.2
cubo_lenght = 1.2
crystlenght = 2 # per il cut fiduciale. La vera lunghezza è 2.5
xsup_paul = xinf_paul + cubo_lenght
ysup_paul = yinf_paul + cubo_lenght

Colors = [ 'darkred', 'orangered',  'lime', 'turquoise','hotpink',  'indigo']

mucalib = {}
muerrcalib = {}
sigmacaliberr = {}
sigmacalib = {}
cutdiv = {}
cuttime = {}
cuttot = {}
data = {}
cutcherry = {}
sigmadivx_ene = []
errsigmadivx_ene = []


for i, nrun in enumerate(runnumbers):
    #carico dati
    data[nrun] = opendata(nrun, datapath, h5keys)
    #divergenza
    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(data[nrun][0], 2, distancesfromT1,
                                                            p0divx, p0divy,
                                                            Rangex=[[-0.02,0.02]],
                                                            Rangey=[[-0.02,0.02]])
    
    
    datashift, shift = aligntracker(data[nrun][0], 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                                    distancesfromT1,
                                                                                    p0divx, p0divy,
                                                                                    Rangex=[[-0.004,0.004]],
                                                                                    Rangey=[[-0.004,0.004]], 
                                                                                    Color='navy', 
                                                                                    Figsize = (8,6))
    
    sigmadivx_ene.append(sigmaalign)
    errsigmadivx_ene.append(errsigmaalign)
    #tagli in divergenza
    cutdiv[nrun] = ((divexalign >divinf) & (divexalign<divsup) & (diveyalign>divinf) &(diveyalign<divsup))
    ax[0].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=divsup, color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=divsup, color="black", linestyle = '--', lw = 2)    
    ax[0].set_title(f'Lead Glass, {energies[i]} GeV ')
    #plot PH tempo
    import copy 
    import matplotlib as mpl
    my_cmap = copy.copy(mpl.cm.jet)
    my_cmap.set_bad(my_cmap(0))
    fig, ax = plt.subplots(1, 1, figsize=(8, 6))
    zhist2d, xhist2d, yhist2d, mapable  = ax.hist2d(data[nrun][1][:,chLabel['LG']],data[nrun][2][:,chLabel['LG']] ,
                bins =[100,100], cmap = my_cmap, range = [[0, 12000], [100, 300]], norm=colors.LogNorm())
    #taglio in tempo
    cuttime[nrun] = ((data[nrun][2][:,chLabel['LG']]<timethsup) & (data[nrun][2][:,chLabel['LG']]>timethinf))
    ax.axhline(y=timethinf, color="red", linestyle = '--', label = f'Threshold \n {energies[i]} GeV', lw = 2)
    ax.axhline(y=timethsup, color="red", linestyle = '--', lw = 2)
    ax.grid()
    ax.legend()
    ax.set_xlabel('PH [a.u]')
    ax.set_ylabel('time [ns]')
    fig.colorbar(mapable,ax=ax)
    #ax.set_title(f'Lead Glass {energies[i]} GeV')
    plt.tight_layout()
    #if energies[i] == '6': 
        #plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/t9/PHvsTime_6GeV.pdf')
    plt.show()
    
    #ricostruzione su faccia del LG
    xcalo = datashift[:, 0] + zcalo*np.tan(divexalign)
    ycalo = datashift[:, 1] + zcalo*np.tan(diveyalign)
    
    fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xcalo[0], ycalo[0],
                                                    bins=[100, 100], cmap=plt.cm.jet, range=[[0, 10], [0, 10]])
    
    
    ax.set_xlabel(f'x [cm], {energies[i]} GeV')
    ax.set_ylabel(f'y [cm], {energies[i]} GeV')
    fig.colorbar(mapable,ax=ax)

    # profilo cherenkov
    
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    
    ax.hist((data[nrun][1][:,chLabel['Cherry44']]),
                 bins = 100, histtype ='step', color = 'teal', 
                 alpha = 1, lw = 3 ,label = f'Cherenkov \n {energies[i]}  GeV', range = [0, 6000])
    ax.hist((data[nrun][1][:,chLabel['Cherry48']]),
                 bins = 100, histtype ='step', color = 'hotpink', 
                 alpha = 1, lw = 3 ,label = f'Cherenkov \n {energies[i]}  GeV', range = [0, 6000])
    ax.axvline(x=thcherry, color="black", linestyle = '--', label = 'threshold', lw = 3)
    ax.grid()
    ax.legend()
    ax.set_xlabel(' PH [a.u]')
    ax.set_ylabel('Counts')
    ax.set_yscale('log')
    plt.tight_layout()
    plt.show()
    cutcherry[nrun] = ((data[nrun][1][:,chLabel['Cherry44']]>thcherry) & (data[nrun][1][:,chLabel['Cherry48']]>thcherry))
    #taglio totale divergenza e tempo
    xpos = data[nrun][0]
    ## le y dovrebbero essere invertite 
    xpos[:,1] = 10-xpos[:,1]
    xpos[:,3] = 10-xpos[:,3]
    xcryst = xpos[:,0] + zcryst*np.tan(divexalign)
    ycryst = xpos[:,1] + zcryst*np.tan(diveyalign)
    cutpos_George[nrun] = ((xcryst[0]>xinf_paul) & (xcryst[0]<xinf_paul+cubo_lenght ) &(ycryst[0]>yinf_paul+2.5) &( ycryst[0]<yinf_paul+2.5+cubo_lenght))
    cutpos_John[nrun] = ((xcryst[0]>xinf_paul+2.5) & (xcryst[0]<xinf_paul+2.5+cubo_lenght ) & (ycryst[0]>yinf_paul+2.5) &( ycryst[0]<yinf_paul+2.5+cubo_lenght))
    cutpos_Paul[nrun] = ((xcryst[0]>xinf_paul) & (xcryst[0]<xinf_paul+cubo_lenght)&(ycryst[0]> yinf_paul) &( ycryst[0]< yinf_paul+cubo_lenght))
    cutpos_Ringo[nrun] = ((xcryst[0]>xinf_paul+2.5) & (xcryst[0]<xinf_paul+2.5+cubo_lenght )&(ycryst[0]>yinf_paul) &( ycryst[0]<yinf_paul + cubo_lenght))

    cutpos_OREO[nrun] = (cutpos_George[nrun] | cutpos_John[nrun] | cutpos_Paul[nrun] | cutpos_Ringo[nrun])
    cuttot[nrun] = (cutdiv[nrun][0] & cuttime[nrun] & cutpos_OREO[nrun])
    fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    zhist, xhist , yhist , mapable = ax.hist2d(data[nrun][1][:,chLabel['LG']], data[nrun][1][:,chLabel['Cherry44']], bins=[100, 100], cmap = my_cmap, norm=colors.LogNorm())
    
    ax.set_xlabel(f'LG')
    ax.set_ylabel(f'cherry 44')
    fig.colorbar(mapable,ax=ax)
    plt.tight_layout()
    # fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    # zhist, xhist , yhist , mapable = ax.hist2d(data[nrun][1][:,chLabel['LG']][cuttot[nrun]], data[nrun][1][:,chLabel['Cherry48']], bins=[100, 100], cmap = my_cmap, norm=colors.LogNorm())
    
    # ax.set_xlabel(f'LG')
    # ax.set_ylabel(f'cherry 48')
    # fig.colorbar(mapable,ax=ax)
    # plt.tight_layout()
    fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    fig.set_size_inches(12,5)
    
    ax.hist(data[nrun][1][:,chLabel['LG']][cuttot[nrun]],
                 bins = 100, histtype ='step', 
                 alpha = 1, lw = 1 ,label = 'no cuts')
    ax.hist(data[nrun][1][:,chLabel['LG']][cutcherry[nrun] & cuttot[nrun]],
                 bins = 100, histtype ='step', 
                 alpha = 1, lw = 1 ,label = 'cut cherry')
    ax.hist(data[nrun][1][:,chLabel['LG']][np.logical_not(cutcherry[nrun]) & cuttot[nrun]],
                 bins = 100, histtype ='step', 
                 alpha = 1, lw = 1 ,label = 'not cut cherry')
    ax.grid()
    ax.set_xlabel(f'LG, {axRndLabel[i]}  [a.u]')
    ax.set_ylabel('Entries')
    ax.set_yscale("log")

    fig.set_tight_layout('tight')
    ax.legend()
    plt.show()
    
    
    

# %%
