#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 13 10:17:12 2023

@author: ale
"""

from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches

plt.rcParams['font.size'] = '20'
datapath = r'C:\Users\aselmi\Desktop\Dottorato\data\dataOREO_1023\HDF5\run720'
h5keys = [ 'xpos','digiPH', 'digiTime']

chLabel = {'LG':0, 'APC':1, 'Cherry44':2, 'Cherry48':3, 'George':4, 'John':5, 'Paul':6, 'Ringo':7}
SiPMlabel = ['George','John', 'Paul', 'Ringo']

label_axRnd = ['Random', 'Axial']
Color = ['navy', 'hotpink']
## le q e le m si calibrazione sono in ordine G, J, P, R
SiPMcalib_intercept = np.loadtxt(r'C:\Users\aselmi\Desktop\Dottorato\analysis\OREO_1023\T9oct_OREOintercept.dat')
SiPMcalib_slope = np.loadtxt(r'C:\Users\aselmi\Desktop\Dottorato\analysis\OREO_1023\T9oct_OREOslope.dat')
run2merge = [120, 122, 124, 125] #amorfo
runnumbers = [666,127] # la 666 contiene le run amorfo mergiate
distancesfromT1 = [500]
zcryst = 549
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]

### varie threshold per i tagli 
thcherry = 130
divsup = 0.001
divinf = -0.001
timethinf = [200]*4
timethsup = [230]*4
phth = [1000]*4

#liste 
partialData = []
outdata = []

## dizionari
data = {}
cutdiv = {}
cutcherry = {}
cuttime_OREO= {}
cuttime_George = {}
cuttime_John = {}
cuttime_Paul = {}
cuttime_Ringo = {}
cutph_OREO = {}
cutph_George = {}
cutph_Paul = {}
cutph_John = {}
cutph_Ringo = {}
cutpos_OREO = {}
cuttot_OREO = {}
cutpos_Paul = {}
cutpos_George = {}
cutpos_John = {}
cutpos_Ringo = {}
ph_GeorgeGeV = {}
ph_JohnGeV = {}
ph_PaulGeV = {}
ph_RingoGeV={}
ph_OREOGeV = {}

meanPH_SiPM = {}
errmeanPH_SiPM = {}
mean_OREO = {}
errmean_OREO = {}
### iniziamo a fare tagli centrali su ogni canale; ci centriamo su 
# Paul (in basso a sx) e poi mi muovo 
xinf_paul = 2.3
yinf_paul = 4.2
cubo_lenght = 1.2
crystlenght = 2 # per il cut fiduciale. La vera lunghezza è 2.5
xsup_paul = xinf_paul + cubo_lenght
ysup_paul = yinf_paul + cubo_lenght
#%%
#############################################
#carico dati + plot posizioni camere
for i, nrun in enumerate(runnumbers):
    print(i)
    ## mergio le run in amorfo e le butto nella run 666
    if nrun == 666: 
        for j in h5keys: 
            outdata = []
            for k, nrun2merge in enumerate(run2merge): 
                with h5py.File(datapath +f'{run2merge[k]}.h5', 'r', libver='latest', swmr=True) as hf: 
                    print("opening %s" % run2merge[k])
                    print(j)
                    outdata.append(np.array(hf[j]))
            partialData.append(np.vstack((outdata[0],outdata[1], outdata[2], outdata[3])))
        data[nrun] = partialData
    else: 
        data[nrun] = opendata(runnumbers[i], datapath, h5keys)
    ## le y dovrebbero essere invertite 
    xpos = data[nrun][0]
    xpos[:,1] = 10-xpos[:,1]
    xpos[:,3] = 10-xpos[:,3]
    # xpos[:,0] = 10-xpos[:,0]
    # xpos[:,2] = 10-xpos[:,2]
    #camere 
    xlabels = ['BC1',' BC1', 'BC2', 'BC2']
    xlim = [[-0,10]]*4
    histpos1d(xpos, 2, xlabels = xlabels, xlim = xlim, Range = [0,10])
    histpos2d(xpos, numberoftrackers = 2)
    #correlazioni camere 
    fig, ax = plt.subplots(1,2 , figsize=(8,8))
    ax = ax.flat
    ax[0].hist2d(xpos[:,0], xpos[:,2], bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet)
    ax[0].set_ylabel('x BC2 [cm]')
    ax[0].set_xlabel('x BC1 [cm]')
    
    ax[1].hist2d(xpos[:,1],xpos[:,3] , bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet,)
    ax[1].set_xlabel('y BC1 [cm]')
    ax[1].set_ylabel('y BC2 [cm]')
    plt.tight_layout()
    plt.show()

    #divergenze
    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(xpos, 2, distancesfromT1,
                                                           p0divx, p0divy,
                                                           Rangex=[[-0.02,0.02]],
                                                           Rangey=[[-0.02,0.02]])
    
    
    datashift, shift = aligntracker(xpos, 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                                   distancesfromT1,
                                                                                    p0divx, p0divy,
                                                                                    Rangex=[[-0.004,0.004]],
                                                                                    Rangey=[[-0.004,0.004]], 
                                                                                    Color='navy', 
                                                                                    Figsize = (8,6))
    cutdiv[nrun] = ((divexalign >divinf) & (divexalign<divsup) & (diveyalign>divinf) &(diveyalign<divsup))
    ax[0].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=divsup, color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=divsup, color="black", linestyle = '--', lw = 2)   


 
     # profilo cherenkov selezioni gli elettroni
     ## Cherry 44
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    ax.hist((data[nrun][1][:,chLabel['Cherry44']]),
                 bins = 100, histtype ='step', color = 'turquoise',
                 alpha = 1, lw = 3 ,label = 'Cherry44', range = [0, 4000])
    ax.axvline(x=thcherry, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax.grid()
    ax.set_xlabel('Cherenkov PH [a.u]')
    ax.set_ylabel('Entries [ns]')
    ax.set_yscale('log')
    ax.legend()
    fig.set_tight_layout('tight')
    plt.show()
    ## cherry 48
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    
    ax.hist((data[nrun][1][:,chLabel['Cherry48']]),
                 bins = 100, histtype ='step', color = 'turquoise', 
                 alpha = 1, lw = 3 ,label = 'Cherry48', range = [0, 6000])
    ax.axvline(x=thcherry, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax.grid()
    ax.set_xlabel('Cherenkov PH [a.u]')
    ax.set_ylabel('Entries [ns]')
    ax.set_yscale('log')
    fig.set_tight_layout('tight')
    ax.legend()
    plt.show()
    cutcherry[nrun] = ((data[nrun][1][:,chLabel['Cherry48']]>thcherry) & (data[nrun][1][:,chLabel['Cherry44']]>thcherry))
# 


    # plot PH e tempo + tagli su tempi e PH dei tre cristalli 
    fig, ax = plt.subplots(2, 2, figsize=(10, 6))
    ax = ax.flat
    for j , BeatName in enumerate(SiPMlabel):
        ax[j].hist2d(data[nrun][1][:,chLabel[BeatName]],data[nrun][2][:,chLabel[BeatName]] ,
                     bins =[100,100], cmap = plt.cm.jet, norm=colors.LogNorm())
        ax[j].axhline(y=timethinf[j], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[j].axhline(y=timethsup[j], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[j].axvline(x=phth[j], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[j].set_ylabel('Entries')
        ax[j].set_xlabel('PH [a.u]')
        ax[j].set_title(f'{BeatName}')
        ax[j].grid()
        exec(f'cuttime_{BeatName}[nrun] = (data[nrun][2][:,chLabel["{BeatName}"]]>timethinf[{j}]) & (data[nrun][2][:,chLabel["{BeatName}"]]<timethsup[j])&(data[nrun][2][:,chLabel["{BeatName}"]]>timethinf[j]) & (data[nrun][2][:,chLabel["{BeatName}"]]<timethsup[j])')
        exec(f'cutph_{BeatName}[nrun] = data[nrun][1][:,chLabel["{BeatName}"]]>phth[{j}]')
    plt.tight_layout()
    plt.show()
    
    cuttime_OREO[nrun] = (cuttime_George[nrun] | cuttime_John[nrun] | cuttime_Paul[nrun] | cuttime_Ringo[nrun])
    cutph_OREO[nrun] = (cutph_George[nrun] | cutph_John[nrun] | cutph_Paul[nrun] | cutph_Ringo[nrun])
    
    
        
    # efficienza cristallo 
    xcryst = xpos[:,0] + zcryst*np.tan(divexalign)
    ycryst = xpos[:,1] + zcryst*np.tan(diveyalign)
    fig, ax = plt.subplots(1,1, figsize=(6,6))
    zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xcryst[0], ycryst[0], cmap=plt.cm.jet, bins=[80, 80], range=[[0,10], [0,10]])
    plt.colorbar(mapable, ax=ax)
    ax.set_xlabel('x [cm]')
    ax.set_xlabel('y [cm]')
    ax.set_title('Proiezione su piano del cristallo')
    plt.tight_layout()
    plt.show()

   
    ### istogramma con tagli 
    zhist2dcut, xhist2dcut , yhist2dcut , mapable = ax.hist2d(xcryst[0][cuttime_OREO[nrun] & cutph_OREO[nrun]], ycryst[0][cuttime_OREO[nrun] & cutph_OREO[nrun]], 
                                                                  cmap=plt.cm.jet, bins = [80,80],
                                                                  range = [[0,10], [0,10]])


    ## EFFICIENZA
    fig, ax = plt.subplots(1,1, figsize=(8,8))
    np.seterr(divide='ignore', invalid = 'ignore')
    eff = np.divide(zhist2dcut, zhist2d)
    eff[np.isnan(eff)]=0
    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.1)
    im = ax.imshow(eff.T, cmap=plt.cm.jet, extent=[0,10, 0, 10], origin = "lower")
    fig.colorbar(im, cax=cax, orientation='vertical')

    rectG = patches.Rectangle((xinf_paul, yinf_paul+2.5), cubo_lenght, cubo_lenght, linewidth=3, edgecolor='black', facecolor='none', label= 'Fiducial area')
    rectR = patches.Rectangle((xinf_paul+2.5, yinf_paul), cubo_lenght, cubo_lenght, linewidth=3, edgecolor='black', facecolor='none')
    rectP = patches.Rectangle((xinf_paul, yinf_paul), cubo_lenght, cubo_lenght, linewidth=3, edgecolor='black', facecolor='none')
    rectJ = patches.Rectangle((xinf_paul+2.5,yinf_paul+2.5), cubo_lenght, cubo_lenght, linewidth=3, edgecolor='black', facecolor='none')
    
    ax.add_patch(rectP)
    ax.add_patch(rectR)
    ax.add_patch(rectG)
    ax.add_patch(rectJ)
    ax.set_xlim([0,10])
    ax.set_ylim([0,10])
    ax.set_xlabel('x [cm]')
    ax.set_ylabel('y [cm]')
    ax.legend()
    plt.savefig(r'C:\Users\aselmi\Desktop\Dottorato\Technical Note Oreo2023\T9_ottobre\T9Oct_oreoeff.pdf')
    plt.tight_layout()
    

    cutpos_George[nrun] = ((xcryst[0]>xinf_paul) & (xcryst[0]<xinf_paul+cubo_lenght ) &(ycryst[0]>yinf_paul+2.5) &( ycryst[0]<yinf_paul+2.5+cubo_lenght))
    cutpos_John[nrun] = ((xcryst[0]>xinf_paul+2.5) & (xcryst[0]<xinf_paul+2.5+cubo_lenght ) & (ycryst[0]>yinf_paul+2.5) &( ycryst[0]<yinf_paul+2.5+cubo_lenght))
    cutpos_Paul[nrun] = ((xcryst[0]>xinf_paul) & (xcryst[0]<xinf_paul+cubo_lenght)&(ycryst[0]> yinf_paul) &( ycryst[0]< yinf_paul+cubo_lenght))
    cutpos_Ringo[nrun] = ((xcryst[0]>xinf_paul+2.5) & (xcryst[0]<xinf_paul+2.5+cubo_lenght )&(ycryst[0]>yinf_paul) &( ycryst[0]<yinf_paul + cubo_lenght))

    cutpos_OREO[nrun] = (cutpos_George[nrun] | cutpos_John[nrun] | cutpos_Paul[nrun] | cutpos_Ringo[nrun])
    cuttot_OREO[nrun] = cuttime_OREO[nrun] & cutdiv[nrun][0] & cutcherry[nrun]  & cutpos_OREO[nrun] &  cutph_OREO[nrun]
    



for i,  nrun in enumerate(runnumbers): 
    for j , BeatName in enumerate(SiPMlabel):
        exec(f'ph_{BeatName}GeV[nrun] = ((data[nrun][1][:,chLabel["{BeatName}"]])-SiPMcalib_intercept[{j}])/SiPMcalib_slope[{j}]')
    ph_OREOGeV[nrun] = ph_GeorgeGeV[nrun] + ph_JohnGeV[nrun]  + ph_PaulGeV[nrun] + ph_RingoGeV[nrun] 
 
 
#%%  
## NOTA: i tagli in posizione hanno comuqnue degli eventi a bassa PH, potrebbero essere adroni che impattano nell'area fiduciale 
#e non lasciano segnale in OREO  
fig, ax = plt.subplots(2,2, figsize=(10,10))
ax = ax.flat
for j, BeatName in enumerate(SiPMlabel): 
    for k, nrun in enumerate(runnumbers): 
         exec(f'hph, binsph, _  = ax[{j}].hist(ph_{BeatName}GeV[nrun][cutph_{BeatName}[nrun] & cuttime_{BeatName}[nrun] & cutpos_{BeatName}[nrun] & cutdiv[nrun][0] & cutcherry[nrun]], histtype ="step", bins = 60, alpha = 1, lw = 3, label = "{label_axRnd[k]}",  density = True, color = Color[{k}])')
         exec(f'meanPH_SiPM[nrun] = np.mean(ph_{BeatName}GeV[nrun][cutph_{BeatName}[nrun] & cuttime_{BeatName}[nrun] & cutpos_{BeatName}[nrun] & cutdiv[nrun][0] & cutcherry[nrun]])')
         exec(f'errmeanPH_SiPM[nrun] = np.std(ph_{BeatName}GeV[nrun][cutph_{BeatName}[nrun] & cuttime_{BeatName}[nrun] & cutpos_{BeatName}[nrun] & cutdiv[nrun][0] & cutcherry[nrun]]) / np.sqrt(len(ph_{BeatName}GeV[nrun][cutph_{BeatName}[nrun] & cuttime_{BeatName}[nrun] & cutpos_{BeatName}[nrun] & cutdiv[nrun][0] & cutcherry[nrun]]))')
         print(f'media {SiPMlabel[j]}, {label_axRnd[k]} =  {meanPH_SiPM[nrun]} pm {errmeanPH_SiPM[nrun]} ')

    ax[j].set_ylabel('Entries')
    ax[j].set_xlabel(f'PH [a.u] {SiPMlabel[j]}')
    ax[j].legend(fontsize = 10)
    ax[j].grid()
    
    errratio_meanSiPM = np.sqrt((errmeanPH_SiPM[runnumbers[0]]/meanPH_SiPM[runnumbers[0]])**2 + (errmeanPH_SiPM[runnumbers[1]]/meanPH_SiPM[runnumbers[1]])**2)

    print(f'ratio media, {SiPMlabel[j]} = {meanPH_SiPM[runnumbers[1]]/meanPH_SiPM[runnumbers[0]]} $\pm$ {errratio_meanSiPM*meanPH_SiPM[runnumbers[1]]/meanPH_SiPM[runnumbers[0]]}')
plt.tight_layout()
plt.show()


#%%

fig, ax = plt.subplots(1, 1, figsize=(8, 6))

for j, nrun in enumerate(runnumbers): 
    mean_OREO[nrun] = np.mean(ph_OREOGeV[nrun][cuttot_OREO[nrun]])
    errmean_OREO[nrun] = np.std(ph_OREOGeV[nrun][cuttot_OREO[nrun]])/np.sqrt(len(ph_OREOGeV[nrun][cuttot_OREO[nrun]]))
    hph, binsph, _  = ax.hist(ph_OREOGeV[nrun][cuttot_OREO[nrun]], histtype ='step', 
                bins = 80, alpha = 1, lw = 3 ,label = f'{label_axRnd[j]}, mean = {round(mean_OREO[nrun], 3)} $\pm$ {round(errmean_OREO[nrun], 3)} ',
                range = [0, 4], density = True, color = Color[j])
    
    print(f'media = {mean_OREO[nrun]} pm {errmean_OREO[nrun]}')

ax.set_ylabel('Normalized counts')
ax.set_xlabel('OREO energy deposit [GeV] ')
ax.legend(fontsize = 15)
ax.grid()
ax.set_ylim(0,1.4)
plt.tight_layout()
plt.savefig(r'C:\Users\aselmi\Desktop\Dottorato\Technical Note Oreo2023\T9_ottobre\T9oct_ratioAxAmo.pdf')
plt.show()

errmeanratio = np.sqrt((errmean_OREO[runnumbers[0]]/mean_OREO[runnumbers[0]])**2 + (errmean_OREO[runnumbers[1]]/mean_OREO[runnumbers[1]])**2)
print(f'ratio media  = {mean_OREO[runnumbers[1]]/mean_OREO[runnumbers[0]]} $\pm$ {errmeanratio*mean_OREO[runnumbers[1]]/mean_OREO[runnumbers[0]]} ')



# # m = 1293.163 #calibrazione LG presa da logbook
# # q = -73.761 
# # mu_LG = {}
# # fitrangeTOTinf = [2, 2]
# # fitrangeTOTsup = [5,4]
# # fig, ax = plt.subplots(1, 1, figsize=(10, 6))
# # for j, nrun in enumerate(runnumbers): 
# #     hph, binsph, _  = ax.hist(((data[nrun][1][:,chLabel['LG']][cutpos_totOREO[nrun]& cutdiv[nrun][0]])-q)/m, histtype ='step', 
# #                               bins = 80, alpha = 1, lw = 3 ,label = f'{ax_amolabel[j]} ',
# #                               range = [0.5, 10], density = True, color = ColorA[j])
# #     xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
# #     phcondition = ((xdata > fitrangeTOTinf[j]) & (xdata < fitrangeTOTsup[j]))
# #     paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
# #     resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
# #                         weights=np.sqrt(hph[phcondition]))
# #     to_plot = np.linspace(fitrangeTOTinf[j], fitrangeTOTsup[j], 10000)
# #     y_eval = model.eval(resultx.params, x=to_plot)
# #     #ax[i].plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = 'black')
  
# #     mu_LG[nrun] = resultx.params["center"].value
# #     ax.plot(to_plot, y_eval, linewidth=2, color= colorFit[j], linestyle = '-', label=f'$\mu$ = {round(resultx.params["center"].value,2)} $\pm$ {round(resultx.params["center"].stderr,2)}')
# # ax.grid()
# # ax.set_ylabel('Entries')
# # ax.set_xlabel(f'LG PH [GeV] ')
# # ax.legend(fontsize = 18)

# # ratioLG = mu_LG[runnumbers[1]]/mu_LG[runnumbers[0]]
# # print(ratioLG)


# %%
      ## LG 
#     fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    
#     ax.hist((data[nrun][1][:,chLabel['LG']]),
#                  bins = 100, histtype ='step', color = 'navy', 
#                  alpha = 0.5, lw = 3 ,label = 'Lead Glass', range = [0, 12000])

#     ax.grid()
#     ax.set_xlabel('PH [a.u]')
#     ax.set_ylabel('Entries [ns]')
#     ax.set_yscale('log')
#     fig.set_tight_layout('tight')
#     ax.legend()
#     plt.show()