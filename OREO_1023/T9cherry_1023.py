from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches

plt.rcParams['font.size'] = '20'


datapath = r'C:\Users\aselmi\Desktop\Dottorato\data\dataOREO_1023/HDF5/run7200'

h5keys = [ 'xpos','digiPH', 'digiTime']
runnumbers = [run for run in range(93, 99)]

energies = [6,5,4,3,2,1]
chLabel = {'LG':0, 'Cherry44':2, 'Cherry48':3}
thcherry48 = [100]*6
thcherry44 = [100]*6
thLG = [6500, 5100, 4000, 3000, 2000, 950]
distancesfromT1 = [500]
zcalo = 500+25.5
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]
divsup = 0.002
divinf = -0.002
timethinf = 180
timethsup = 225


data = {}
cutdiv = {}
cuttime = {}
cond_cherry48 = {}
notcond_cherry48 = {}
cond_cherry44 = {}
notcond_cherry44 = {}
cond_LG = {}

ineff48 = []
ineff44 = []
ineffAND = []
ineffOR = []


purity48 = []
purity44 = []
purityAND = []
purityOR = []
for i, nrun in enumerate(runnumbers):
    #carico dati
    data[nrun] = opendata(nrun, datapath, h5keys)
    fig, ax = plt.subplots(1, 1, figsize=(8, 6))
    #plot PH tempo
    import copy 
    import matplotlib as mpl
    my_cmap = copy.copy(mpl.cm.jet)
    my_cmap.set_bad(my_cmap(0))
    zhist2d, xhist2d, yhist2d, mapable  = ax.hist2d(data[nrun][1][:,chLabel['LG']],data[nrun][2][:,chLabel['LG']] ,
                bins =[100,100], cmap = my_cmap, range = [[0, 12000], [100, 300]], norm=colors.LogNorm())
    #taglio in tempo
    cuttime[nrun] = ((data[nrun][2][:,chLabel['LG']]<timethsup) & (data[nrun][2][:,chLabel['LG']]>timethinf))
    ax.axhline(y=timethinf, color="red", linestyle = '--', label = f'Threshold \n       {energies[i]} GeV', lw = 2)
    ax.axhline(y=timethsup, color="red", linestyle = '--', lw = 2)
    ax.grid()
    ax.legend()
    ax.set_xlabel('PH [a.u]')
    ax.set_ylabel('time [ns]')
    fig.colorbar(mapable,ax=ax)
    #ax.set_title(f'Lead Glass {energies[i]} GeV')
    plt.tight_layout()
    fig, ax = plt.subplots(1, 1, figsize=(8, 6))
    ax.hist((data[nrun][1][:,chLabel['Cherry44']][cuttime[nrun]]),
                 bins = 100, histtype ='step', color = 'navy', 
                 alpha = 1, lw = 3 ,label = f'XCET 44 \n {energies[i]}  GeV')
    ax.axvline(x=thcherry44[i], color="red", linestyle = '--', label = 'Threshold XCET 44', lw = 3)
    ax.hist((data[nrun][1][:,chLabel['Cherry48']][cuttime[nrun]]),
                 bins = 100, histtype ='step', color = 'darkgreen', 
                 alpha = 1, lw = 3 ,label = f'XCET 48 \n {energies[i]}  GeV')
    ax.axvline(x=thcherry48[i], color="darkorange", linestyle = '--', label = 'Threshold XCET 48', lw = 3)
    
    ax.hist((data[nrun][1][:,chLabel['LG']][cuttime[nrun]]),
                 bins = 100, histtype ='step', color = 'hotpink', 
                 alpha = 1, lw = 3 ,label = f'LG \n {energies[i]}  GeV')
    ax.axvline(x=thLG[i], color="black", linestyle = '--', label = 'Threshold LG', lw = 3)
    
    ax.grid()
    ax.legend(fontsize = 10)
    ax.set_xlabel(' PH [a.u]')
    ax.set_ylabel('Counts')
    ax.set_yscale('log')
    plt.show()
    notcond_cherry48[nrun] = (data[nrun][1][:,chLabel['Cherry48']][cuttime[nrun]]<thcherry48[i])
    cond_cherry48[nrun] = (data[nrun][1][:,chLabel['Cherry48']][cuttime[nrun]]>thcherry48[i])
    
    notcond_cherry44[nrun] = (data[nrun][1][:,chLabel['Cherry44']][cuttime[nrun]]<thcherry44[i])
    cond_cherry44[nrun] = (data[nrun][1][:,chLabel['Cherry44']][cuttime[nrun]]>thcherry44[i])
    cond_LG[nrun] = (data[nrun][1][:,chLabel['LG']][cuttime[nrun]]>thLG[i])
    
    
    ineff48.append(((cond_LG[nrun]& notcond_cherry48[nrun]).sum()/cond_LG[nrun].sum())*100)
    purity48.append(((cond_LG[nrun]& cond_cherry48[nrun]).sum()/cond_LG[nrun].sum())*100)
    
    
    ineff44.append(((cond_LG[nrun]& notcond_cherry44[nrun]).sum()/cond_LG[nrun].sum())*100)
    purity44.append(((cond_LG[nrun]& cond_cherry44[nrun]).sum()/cond_LG[nrun].sum())*100)
    
    ineffAND.append(((cond_LG[nrun]& (notcond_cherry48[nrun]&notcond_cherry44[nrun])).sum()/cond_LG[nrun].sum())*100)
    ineffOR.append(((cond_LG[nrun]& (notcond_cherry48[nrun]|notcond_cherry44[nrun])).sum()/cond_LG[nrun].sum())*100)
    
    purityAND.append(((cond_LG[nrun]& (cond_cherry44[nrun]&cond_cherry48[nrun])).sum()/cond_LG[nrun].sum())*100)
    purityOR.append(((cond_LG[nrun]& (cond_cherry44[nrun]|cond_cherry48[nrun])).sum()/cond_LG[nrun].sum())*100)
    
#%%
fig, ax = plt.subplots(1, 1, figsize=(8, 6))
ax.plot(energies, ineff48, marker='D',  linestyle = 'dotted', color = 'navy',
            markersize = 8, label = 'XCET 48')
ax.plot(energies, ineff44, marker='D',  linestyle = 'dotted', color = 'darkgreen',
            markersize = 8, label = 'XCET 44')
ax.plot(energies, ineffAND, marker='D',  linestyle = 'dotted', color = 'hotpink',
            markersize = 8, label = 'XCET 44 and XCET 48 ')
ax.plot(energies, ineffOR, marker='D',  linestyle = 'dotted', color = 'deepskyblue',
            markersize = 8, label = 'XCET 44 or XCET 48 ')
ax.legend(fontsize= 15)
ax.set_xlabel('GeV')
ax.set_ylabel('Inefficiency [$\%$]')
ax.grid()
ax.set_ylim([-0.5,5])
plt.tight_layout()
plt.savefig(r'C:\Users\aselmi\Desktop\Dottorato\Technical Note Oreo2023\T9_ottobre\CherryIneff_1023.pdf')
plt.show()


fig, ax = plt.subplots(1, 1, figsize=(8, 6))
ax.plot(energies, purity48,  marker='o',  linestyle = 'dotted', color = 'navy',
            markersize = 8, label = 'XCET 48')
ax.plot(energies, purity44,  marker='o',  linestyle = 'dotted', color = 'darkgreen',
            markersize = 8, label = 'XCET 44')
ax.plot(energies, purityAND,  marker='o',  linestyle = 'dotted', color = 'hotpink',
            markersize = 8, label = 'XCET 44 and XCET48')
ax.plot(energies, purityOR,  marker='o',  linestyle = 'dotted', color = 'deepskyblue',
            markersize = 8, label = 'XCET 44 or XCET48')

ax.legend(fontsize = 15)
ax.set_xlabel('GeV')
ax.set_ylabel('Purity [$\%$]')
ax.grid()
ax.set_ylim([95,100.5])
plt.tight_layout()
plt.savefig(r'C:\Users\aselmi\Desktop\Dottorato\Technical Note Oreo2023\T9_ottobre\CherryPurity_1023.pdf')
plt.show()
# %%
