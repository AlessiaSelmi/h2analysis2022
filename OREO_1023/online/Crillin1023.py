#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 13 10:17:12 2023

@author: ale
"""

# 6 GeV hadrons (secondary beam)  nome file ‘collimator closed’, 
# trigger on APC. Run is good for equalization.


from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches

Colors = ['green', 'green','green', 'green']
plt.rcParams['font.size'] = '20'

####### Le y sono al contrario!! quindi la mappa di eff è P, R, G, J e non come c'è su il log --> 
##--> devi girare le y quando le carichi 
datapath = '/home/ale/Desktop/Dottorato/dati_remoti /dataOREO_1023/run720'
h5keys = [ 'xpos','digiPH', 'digiTime']

SiPM_calib = np.loadtxt('1023T9_AllSiPM_Calibration_OREO.dat')

SiPM_eqfact = np.loadtxt('1023T9_AllSiPM_eqfactor.dat')
SiPM_intercept1 = np.loadtxt('1023T9_AllSiPM_intercept1.dat')

chLabel = {'LG':0, 'APC':1, 'Cherry44':2, 'Cherry48':3, 'George':4, 'John':5, 'Paul':6, 'Ringo':7}
SiPMlabel = ['George','John', 'Paul', 'Ringo']
runnumbers = [124, 127] #asse e amorfo
#runnumbers = [242] #amorfo
distancesfromT1 = [500]
zcryst = 549
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]

### varie threshold per i tagli 
thcherry = 130

divsup = 0.001
divinf = -0.001
timethinf = [160]*4
timethsup = [250]*4
phth = [150]*4
## dizionari
data = {}
cutdiv = {}
cutcherry = {}
cuttime = {}
cutph = {}
PH_tot = {}
cuttot = {}
cutpos_Paul = {}
cutpos_George = {}
cutpos_John = {}
cutpos_Ringo = {}
crystlenght = 2 # per il cut fiduciale. La vera lunghezza è 2.5

xcrystinf = 2
xcrystsup = xcrystinf+crystlenght*2
ycrystinf= 1.7
ycrystsup = ycrystinf+crystlenght*2


### iniziamo a fare tagli centrali su ogni canale; ci centriamo su 
# Paul (in basso a sx) e poi mi muovo 
xinf_paul = 2.2
yinf_paul = 2
cubo_lenght = 1.5
xsup_paul = xinf_paul + cubo_lenght
ysup_paul = yinf_paul + cubo_lenght
#%%
#############################################
#carico dati + plot posizioni camere
for i, nrun in enumerate(runnumbers):
    print(i)
    data[nrun] = opendata(runnumbers[i], datapath, h5keys)
    xpos = data[nrun][0]
    #camere 
    xlabels = ['BC1',' BC1', 'BC2', 'BC2']
    xlim = [[-0,10]]*4
    histpos1d(xpos, 2, xlabels = xlabels, xlim = xlim, Range = [0,10])
    histpos2d(xpos, numberoftrackers = 2)
    #correlazioni camere 
    fig, ax = plt.subplots(1,2 , figsize=(8,8))
    ax = ax.flat
    ax[0].hist2d(xpos[:,0], xpos[:,2], bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet)
    ax[0].set_ylabel('x BC2 [cm]')
    ax[0].set_xlabel('x BC1 [cm]')
    
    ax[1].hist2d(xpos[:,1],xpos[:,3] , bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet,)
    ax[1].set_xlabel('y BC1 [cm]')
    ax[1].set_ylabel('x BC2 [cm]')
    plt.tight_layout()
    plt.show()
#%%
    #divergenze
    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(xpos, 2, distancesfromT1,
                                                           p0divx, p0divy,
                                                           Rangex=[[-0.02,0.02]],
                                                           Rangey=[[-0.02,0.02]])
    
    
    datashift, shift = aligntracker(xpos, 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                                   distancesfromT1,
                                                                                    p0divx, p0divy,
                                                                                    Rangex=[[-0.004,0.004]],
                                                                                    Rangey=[[-0.004,0.004]], 
                                                                                    Color='navy', 
                                                                                    Figsize = (8,6))
    cutdiv[nrun] = ((divexalign >divinf) & (divexalign<divsup) & (diveyalign>divinf) &(diveyalign<divsup))
    ax[0].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=divsup, color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=divsup, color="black", linestyle = '--', lw = 2)   

# #%%


 #%%
     # profilo cherenkov selezioni gli elettroni
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    
    ax.hist((data[nrun][1][:,chLabel['Cherry44']]),
                 bins = 100, histtype ='step', color = 'turquoise',
                 alpha = 1, lw = 3 ,label = 'Cherry44', range = [0, 4000])
    ax.axvline(x=thcherry, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax.grid()
    ax.set_xlabel('Cherenkov PH [a.u]')
    ax.set_ylabel('Entries [ns]')
    ax.set_yscale('log')
    ax.legend()
    fig.set_tight_layout('tight')
     
    plt.show()
    #######################################################################
    #####################       CUT CHERRY     ############################
    #######################################################################
    cutcherry[nrun] = ((data[nrun][1][:,chLabel['Cherry48']]>thcherry) & (data[nrun][1][:,chLabel['Cherry44']]>thcherry))
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    
    ax.hist((data[nrun][1][:,chLabel['Cherry48']]),
                 bins = 100, histtype ='step', color = 'turquoise', 
                 alpha = 1, lw = 3 ,label = 'Cherry48', range = [0, 6000])
    ax.axvline(x=thcherry, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax.grid()
    ax.set_xlabel('Cherenkov PH [a.u]')
    ax.set_ylabel('Entries [ns]')
    ax.set_yscale('log')
    fig.set_tight_layout('tight')
    ax.legend()
    plt.show()
    
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    
    ax.hist((data[nrun][1][:,chLabel['LG']]),
                 bins = 100, histtype ='step', color = 'navy', 
                 alpha = 0.5, lw = 3 ,label = 'Lead Glass', range = [0, 12000])

    ax.grid()
    ax.set_xlabel('PH [a.u]')
    ax.set_ylabel('Entries [ns]')
    ax.set_yscale('log')
    fig.set_tight_layout('tight')
    ax.legend()
    plt.show()
#%%
    # plot PH e tempo + tagli su tempi e PH dei tre cristalli 
    fig, ax = plt.subplots(2, 2, figsize=(10, 6))
    ax = ax.flat
    for j in range(len(SiPMlabel)):
        ax[j].hist2d(data[nrun][1][:,chLabel[SiPMlabel[j]]],data[nrun][2][:,chLabel[SiPMlabel[j]]] ,
                     bins =[100,100], cmap = plt.cm.jet, norm=colors.LogNorm())
        ax[j].axhline(y=timethinf[j], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[j].axhline(y=timethsup[j], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[j].axvline(x=phth[j], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[j].set_ylabel('Entries')
        ax[j].set_xlabel('PH [a.u]')
        ax[j].set_title(f'{SiPMlabel[j]}')
        ax[j].grid()
    plt.tight_layout()
    plt.show()
    #%% faccimao un po' di tagli  su tempo e PH 
    ## solo una matrice
    # cuttime[nrun] =(((data[nrun][2][:,chLabel['George1']]>timethinf[0]) & (data[nrun][2][:,chLabel['George1']]<timethsup[0]))|
    #              ((data[nrun][2][:,chLabel['John1']]>timethinf[1]) & (data[nrun][2][:,chLabel['John1']]<timethsup[1]))|
    #              ((data[nrun][2][:,chLabel['Paul1']]>timethinf[2]) & (data[nrun][2][:,chLabel['Paul1']]<timethsup[2])))
    # # I cristalli li vedi andando a fare i tagli sulla PH dei vari SiPM, se chiedi solo il taglio su George vedi lui 
    # cutph[nrun] = ((data[nrun][1][:,chLabel['George1']]>phth[0])|
    #          (data[nrun][1][:,chLabel['John1']]>phth[1]) |
    #          (data[nrun][1][:,chLabel['Paul1']]>phth[2]) )
    
    # cuttot[nrun] = cutdiv[nrun][0] & cuttime[nrun] &cutph[nrun]
    #tutte e due le matrici
    cuttime[nrun] = (((data[nrun][2][:,chLabel['George']]>timethinf[0]) & (data[nrun][2][:,chLabel['George']]<timethsup[0])&(data[nrun][2][:,chLabel['George']]>timethinf[0]) & (data[nrun][2][:,chLabel['George']]<timethsup[0]))|
                 ((data[nrun][2][:,chLabel['John']]>timethinf[1]) & (data[nrun][2][:,chLabel['John']]<timethsup[1])&(data[nrun][2][:,chLabel['John']]>timethinf[1]) & (data[nrun][2][:,chLabel['John']]<timethsup[1]))|
                 ((data[nrun][2][:,chLabel['Paul']]>timethinf[2]) & (data[nrun][2][:,chLabel['Paul']]<timethsup[2])& (data[nrun][2][:,chLabel['Paul']]>timethinf[2]) & (data[nrun][2][:,chLabel['Paul']]<timethsup[2]))|
                 ((data[nrun][2][:,chLabel['Ringo']]>timethinf[3]) & (data[nrun][2][:,chLabel['Ringo']]<timethsup[3])&(data[nrun][2][:,chLabel['Ringo']]>timethinf[3]) & (data[nrun][2][:,chLabel['Ringo']]<timethsup[3])))
    # I cristalli li vedi andando a fare i tagli sulla PH dei vari SiPM, se chiedi solo il taglio su George vedi lui 
    cutph[nrun] = ((data[nrun][1][:,chLabel['George']]>phth[0])|
             (data[nrun][1][:,chLabel['John']]>phth[1])|
             (data[nrun][1][:,chLabel['Paul']]>phth[2])|(data[nrun][1][:,chLabel['Ringo']]>phth[2]))
    
    
    
    
    #######################################################################
    #####################       CUT TOT    ############################
    #######################################################################
    cuttot[nrun] = cuttime[nrun] & cutph[nrun]
            
#%% efficienza cristallo 
    xcryst = xpos[:,0] + zcryst*np.tan(divexalign)
    ycryst = xpos[:,1] + zcryst*np.tan(diveyalign)
    fig, ax = plt.subplots(1,1, figsize=(6,6))
    zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xcryst[0], ycryst[0], cmap=plt.cm.jet, bins=[80, 80], range=[[0,10], [0,10]])
    plt.colorbar(mapable, ax=ax)
    ax.set_xlabel('x [cm]')
    ax.set_xlabel('y [cm]')
    ax.set_title('Proiezione su piano del cristallo')
    plt.tight_layout()
    plt.show()

   
    fig, ax = plt.subplots(1,1, figsize=(8,8))
    zhist2dcut, xhist2dcut , yhist2dcut , mapable = ax.hist2d(xcryst[0][cuttot[nrun]], ycryst[0][cuttot[nrun]], 
                                                                  cmap=plt.cm.jet, bins = [80,80],
                                                                  range = [[0,10], [0,10]])


    plt.colorbar(mapable, ax=ax)
    # rect = patches.Rectangle((xcrystinf, ycrystinf), crystlenght*3, crystlenght, linewidth=4, edgecolor='darkred', facecolor='none')
    # ax.add_patch(rect)
    ax.set_ylabel('x [cm]')
    ax.set_xlabel('y [cm]')
    

    plt.tight_layout()
    plt.show()
    fig, ax = plt.subplots(1,1, figsize=(8,8))
    np.seterr(divide='ignore', invalid = 'ignore')
    eff = np.divide(zhist2dcut, zhist2d)
    eff[np.isnan(eff)]=0

    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.1)
    im = ax.imshow(eff.T, cmap=plt.cm.jet, extent=[0,10, 0, 10], origin = "lower")
    fig.colorbar(im, cax=cax, orientation='vertical')
    rectP = patches.Rectangle((xinf_paul, yinf_paul), cubo_lenght, cubo_lenght, linewidth=3, edgecolor='white', facecolor='none')
    rectR = patches.Rectangle((xinf_paul+2.5, yinf_paul), cubo_lenght, cubo_lenght, linewidth=3, edgecolor='white', facecolor='none')
    rectG = patches.Rectangle((xinf_paul, yinf_paul+2.5), cubo_lenght, cubo_lenght, linewidth=3, edgecolor='white', facecolor='none')
    rectJ = patches.Rectangle((xinf_paul+2.5, yinf_paul+2.5), cubo_lenght, cubo_lenght, linewidth=3, edgecolor='white', facecolor='none')
    ax.add_patch(rectP)
    ax.add_patch(rectR)
    ax.add_patch(rectG)
    ax.add_patch(rectJ)
    ax.set_xlim([0,10])
    ax.set_ylim([0,10])
    ax.set_xlabel('x [cm]')
    ax.set_ylabel('y [cm]')
    ax.set_title(f'Run 7200{nrun} ')
    plt.tight_layout()
    
    ## siccome le y sono al contrario la mappa di eff in y è sbagliata--> qui ora cambio i nomi, a fer la cosa giusta dovresti invertire le y 
    cutpos_George[nrun] = ((xcryst[0]>xinf_paul) & (xcryst[0]<xsup_paul ) &(ycryst[0]>yinf_paul) &( ycryst[0]<ysup_paul))
    cutpos_John[nrun] = ((xcryst[0]>xinf_paul+2.5) & (xcryst[0]<xinf_paul+2.5+cubo_lenght ) & (ycryst[0]>yinf_paul) &( ycryst[0]<yinf_paul+cubo_lenght))
    cutpos_Paul[nrun] = ((xcryst[0]>xinf_paul) & (xcryst[0]<xsup_paul +cubo_lenght)|(ycryst[0]>yinf_paul+2.5) &( ycryst[0]<yinf_paul+2.5+cubo_lenght))
    cutpos_Ringo[nrun] = ((xcryst[0]>xinf_paul+2.5) & (xcryst[0]< xinf_paul+2.5+cubo_lenght )&(ycryst[0]>yinf_paul+2.5) &( ycryst[0]<ysup_paul+2.5+cubo_lenght))


#%%
ColorsA = ['orangred', 'orangered', 'orangered', 'orangered']

dataAmo = []
dataAxial = []
for nrun in runnumbers: 
    fig, ax = plt.subplots(1,1, figsize=(8,8))
    ax.hist(((data[nrun][1][:,chLabel['George']][cutpos_George[nrun]& cutdiv[nrun][0]])-SiPM_intercept1[0])*SiPM_eqfact[0],  histtype ='step', 
                bins = 80, alpha = 1, lw = 1 ,label = 'George ', range = [500, 10000], density = True)
    ax.hist(((data[nrun][1][:,chLabel['John']][cutpos_John[nrun] & cutdiv[nrun][0]])-SiPM_intercept1[1])*SiPM_eqfact[1],  histtype ='step', 
                bins = 80, alpha = 1, lw = 1 ,label = 'John ', range = [500, 10000], density = True)
    
    ax.hist(((data[nrun][1][:,chLabel['Paul']][cutpos_Paul[nrun]& cutdiv[nrun][0]])-SiPM_intercept1[2])*SiPM_eqfact[2],  histtype ='step', 
                bins = 80, alpha = 1, lw = 1 ,label = 'Paul ',range = [500, 10000], density = True)
    
    ax.hist(((data[nrun][1][:,chLabel['Ringo']][cutpos_Ringo[nrun] & cutdiv[nrun][0]])-SiPM_intercept1[3])*SiPM_eqfact[3],  histtype ='step', 
                bins = 80, alpha = 1, lw = 1 ,label = 'Ringo ', range = [500, 10000], density = True)
    
    ax.set_ylabel('Entries')
    ax.set_xlabel('PH [a.u]')
    ax.grid()
    ax.legend()

plt.tight_layout()
plt.show()

# SiPM_amo = {}
# SiPM_ax = {}
# for j, Label in enumerate(SiPMlabel) :
#     SiPM_amo[Label] =  ((runnumbers[0][1][:,chLabel[f'{Label}']])-intercept_eq1[j])*SiPM_eqfactor[j]
# for j, Label in enumerate(SiPMlabel) :
#     SiPM_ax[Label] =  ((runnumbers[1][1][:,chLabel[f'{Label}']])-intercept_eq1[j])*SiPM_eqfactor[j]

# #%%    
# #PH_totfront = ((data[runnumber[0]][1][:,chLabel['George']])[cuttot[nrun]]-intercept_eq1[0])*SiPM_eqfactor[0] + ((data[runnumber[0]][1][:,chLabel['John']])[cuttot[nrun]]-intercept_eq1[1])*SiPM_eqfactor[1] + ((data[runnumber[0]][1][:,chLabel['Paul']])[cuttot[nrun]]-intercept_eq1[2])*SiPM_eqfactor[2] + ((data[runnumber[0]][1][:,chLabel['Ringo']])[cuttot[nrun]]-intercept_eq1[3])*SiPM_eqfactor[3]
# Oreo_amo = SiPM_amo['George']+SiPM_amo['John']+SiPM_amo['Paul']+SiPM_amo['Ringo']              
# #PH_totlateral = ((data[runnumber[]][1][:,chLabel['George']])[cuttot[nrun]]-intercept_eq1[0])*SiPM_eqfactor[0] + ((data[runnumber[0]][1][:,chLabel['John']])[cuttot[nrun]]-intercept_eq1[1])*SiPM_eqfactor[1] + ((data[runnumber[0]][1][:,chLabel['Paul']])[cuttot[nrun]]-intercept_eq1[2])*SiPM_eqfactor[2] + ((data[runnumber[0]][1][:,chLabel['Ringo']])[cuttot[nrun]]-intercept_eq1[3])*SiPM_eqfactor[3]
# Oreo_ax = ph_SiPMlateral['George']+ph_SiPMlateral['John']+ph_SiPMlateral['Paul']+ph_SiPMlateral['Ringo']


# for nsipm in range(len(SiPMlabel)): 
# #     print(nrun)
# #     # ax[nsipm].hist(data[85][1][:,chLabel[SiPMlabel[nsipm]]][cuttot[85]& cutpos[85]], histtype ='step', 
# #     #            bins = 100, alpha = 1, lw = 1 ,label = 'Run 85', range = [0, 10000], density = True,)
# #     # ax[nsipm].hist(data[89][1][:,chLabel[SiPMlabel[nsipm]]][cuttot[89]& cutpos[89]], histtype ='step', 
# #     #            bins = 100, alpha = 1, lw = 1 ,label = 'Run 89', range = [0, 10000], density = True)
# #     # ax[nsipm].hist(data[90][1][:,chLabel[SiPMlabel[nsipm]]][cuttot[90]& cutpos[90]], histtype ='step',
# #     #            bins = 100, alpha = 1, lw = 1 ,label = 'Run 90', range = [0, 10000], density = True)
#     ax[nsipm].hist(data[runnumbers[0]][1][:,chLabel[SiPMlabel[nsipm]]][cuttot[runnumbers[0]]& cutpos[runnumbers[0]]], histtype ='step',
#                 bins = 100, alpha = 1, lw = 1 ,label = 'Run 102', range = [0, 10000], density = True)
#     ax[nsipm].hist(data[runnumbers[1]][1][:,chLabel[SiPMlabel[nsipm]]][cuttot[runnumbers[0]]& cutpos[runnumbers[0]]], histtype ='step',
#                 bins = 100, alpha = 1, lw = 1 ,label = 'Run 102', range = [0, 10000], density = True)
    
#     ax[nsipm].hist(data[720103][1][:,chLabel[SiPMlabel[nsipm]]][cuttot[720103]& cutpos[720103]], histtype ='step',
#                bins = 100, alpha = 1, lw = 1 ,label = 'Run 103', range = [0, 10000], density = True)
#     ax[nsipm].set_ylabel('Entries')
#     ax[nsipm].set_xlabel('PH [a.u]')
#     ax[nsipm].set_title(f'{SiPMlabel[nsipm]}')
#     ax[nsipm].grid()
#     ax[nsipm].legend()
#     ax[nsipm].set_yscale('log')
# plt.tight_layout()
# plt.show()


# fig, ax = plt.subplots(1,1, figsize=(8,8))
# # ax.hist((data[85][1][:,chLabel['LG']][cuttot[85] & cutpos[85]]),
# #               bins = 100, histtype ='step', color = 'navy', 
# #               alpha = 1, lw = 1 ,label = 'Lead Glass, run 85', range = [0, 12000], density = True)
# # ax.hist((data[89][1][:,chLabel['LG']][cuttot[89] & cutpos[89]]),
# #               bins = 100, histtype ='step', color = 'red', 
# #               alpha = 1, lw = 1 ,label = 'Lead Glass, run 89', range = [0, 12000], density = True)
# ax.hist((data[720102][1][:,chLabel['LG']][cuttot[720102] & cutpos[720102]]),
#               bins = 100, histtype ='step', color = 'hotpink', 
#               alpha = 1, lw = 2 ,label = 'Lead Glass, run 102', range = [0, 12000], density = True)
# ax.hist((data[runnumbers[0]][1][:,chLabel['LG']]),
#               bins = 50, histtype ='step', color = 'black', 
#               alpha = 1, lw = 1 ,label = 'Lead Glass, run 103', range = [0, 20000], density = True)
# #               alpha = 1, lw = 2 ,label = 'Lead Glass, run 102', range = [0, 12000], density = True)
# ax.hist((data[runnumbers[0]][1][:,chLabel['LG']][cutpos[runnumbers[0]]]),
#               bins = 50, histtype ='step', color = 'hotpink', 
#               alpha = 1, lw = 2 ,label = 'Lead Glass, run 103', range = [0, 20000], density = True)

# # ax.hist((data[82][1][:,chLabel['LG']][cuttot[82] & cutpos[82]]),
# #               bins = 100, histtype ='step', color = 'red', 
# #               alpha = 1, lw = 1 ,label = 'Lead Glass, run 82', range = [0, 12000], density = True)


# ax.grid()
# ax.set_xlabel('PH [a.u]')
# ax.set_ylabel('Entries [ns]')
# ax.set_yscale('log')
# # ax.legend(fontsize = 15)
# fig.set_tight_layout('tight')
# plt.show()

     

# #%%
# fig, ax = plt.subplots(1,1, figsize=(8,8))
# # ax.hist((data[85][1][:,chLabel['LG']][cuttot[85] & cutpos[85]]),
# #               bins = 100, histtype ='step', color = 'navy', 
# #               alpha = 1, lw = 1 ,label = 'Lead Glass, run 85', range = [0, 12000], density = True)
# # ax.hist((data[89][1][:,chLabel['LG']][cuttot[89] & cutpos[89]]),
# #               bins = 100, histtype ='step', color = 'red', 
# #               alpha = 1, lw = 1 ,label = 'Lead Glass, run 89', range = [0, 12000], density = True)
# # ax.hist((data[90][1][:,chLabel['LG']][cuttot[90] & cutpos[90]]),
# #               bins = 100, histtype ='step', color = 'green', 
# #               alpha = 1, lw = 1 ,label = 'Lead Glass, run 90', range = [0, 12000], density = True)
# # ax.hist((data[81][1][:,chLabel['LG']]),
# #               bins = 100, histtype ='step', color = 'green', 
# #               alpha = 1, lw = 1 ,label = 'Lead Glass, run 91', range = [0, 12000], density = True)

# ax.hist((data[720103][1][:,chLabel['LG']]),
#               bins = 100, histtype ='step', color = 'red', 
#               alpha = 1, lw = 1 ,label = 'Lead Glass, run 103', range = [0, 12000], density = True)

# ax.hist((data[720102][1][:,chLabel['LG']]),
#               bins = 100, histtype ='step', color = 'red', 
#               alpha = 1, lw = 1 ,label = 'Lead Glass, run 102', range = [0, 12000], density = True)

# ax.grid()
# ax.set_xlabel('PH [a.u]')
# ax.set_ylabel('Entries [ns]')
# # ax.set_yscale('log')
# ax.legend(fontsize = 15)
# fig.set_tight_layout('tight')
# ax.legend()
# plt.show()
    


# # sommaSipm102 = (data[720102][1][:,chLabel['George']][cuttot[720102]& cutpos[720102]])*0.98+(data[720102][1][:,chLabel['Paul']][cuttot[720102]& cutpos[720102]])*0.712+(data[720102][1][:,chLabel['Ringo']][cuttot[720102]& cutpos[720102]]*0.713)+data[720102][1][:,chLabel['John']][cuttot[720102]& cutpos[720102]]*1
# # sommaSipm103 = data[720103][1][:,chLabel['George']][cuttot[720103]& cutpos[720103]]*0.98+data[720103][1][:,chLabel['Paul']][cuttot[720103]& cutpos[720103]]*0.712+data[720103][1][:,chLabel['Ringo']][cuttot[720103]& cutpos[720103]]*0.713+data[720103][1][:,chLabel['John']][cuttot[720103]& cutpos[720103]]*1

# # fig, ax = plt.subplots(1,1, figsize=(8,8))
# # ax.hist(sommaSipm102,
# #               bins = 100, histtype ='step', color = 'hotpink', 
# #               alpha = 1, lw = 3, range = [0, 12000], density = True, label = 'run102')
# # ax.hist(sommaSipm103,
# #               bins = 100, histtype ='step', color = 'black', 
# #               alpha = 1,lw = 1, range = [0, 12000], density = True, label = 'run103')
# # ax.grid()
# # ax.set_xlabel('PH OREO [a.u]')
# # ax.set_ylabel('Entries [ns]')
# # # ax.set_yscale('log')
# # ax.legend(fontsize = 25)
# # fig.set_tight_layout('tight')
# # plt.show()
    