#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct  9 10:50:18 2023

@author: ale
"""


import numpy as np
import sys, os, re, glob
import uproot
from matplotlib import pyplot as plt
import matplotlib as mpl
#%%
path = "/home/ale/Desktop/Dottorato/dati_remoti /wf_OREO"

numRun = 720162

nameFile = f"run{numRun}_00009.root"
# nameFile = f"run{numRun}.root"

fileToLoad = os.path.join(path, nameFile)

#%%
with uproot.open(fileToLoad)["h2"] as f:
    f.show()
    for k in f:
        print(k)
    Nwdigi = f["Nwdigi_730"].arrays(library = "np")["Nwdigi_730"]
    Idigi = f["Idigi_730"].arrays(library = "np")["Idigi_730"]
    
    print(Nwdigi.shape, Idigi.shape)

    
# Condizione da soddisfare, (direi che è sempre vera)
conda = (Nwdigi == 4152)#32992)
print(f"Good events: {conda.sum()}\nTotal: {conda.shape}")

#Nwdigi_742a = Nwdigi_742a[conda]
Idigi = Idigi[conda]
print(Nwdigi.shape, Idigi.shape)

#%%
# Parametri caratteristici che ho appena descritto sopra

nPtsDigi = 512
nWordSingleChannel = 5+nPtsDigi+2
nChannels = 8 # 32


def getWaveform(matrice, canale, evento):
    """
    matrice: matrice dei dati
    canale: numero di canale, contando da 0
    evento: numero di evento (riga), contando da 0
    """
    i = evento
    j = canale
    return matrice[i, (5 + nWordSingleChannel*j) : (5 + nWordSingleChannel*j + nPtsDigi)]


#%%
xVect = np.arange(0, nPtsDigi) * 2 #ns, sampling 500 MHz

chanToPlot = 0

for i in range(10):
    fig, ax = plt.subplots()
    fig.set_size_inches(12, 5)
    wf = getWaveform(Idigi, chanToPlot, i)
    
    ax.plot(xVect, wf, ls = ":", c = "tab:green")
    
    ax.grid()
    ax.set_xlabel("Time [ns]", fontsize = 14)
    ax.set_ylabel("[ADC]", fontsize = 14)
    
    ax.set_title(f"Chan. {chanToPlot} -- Ev. {i}", fontsize = 16)
    
    plt.show()
    
#%%
Idigipack = Idigi.reshape(Idigi.shape[0], nChannels, nWordSingleChannel)[:,:,5:5+nPtsDigi ]

#%%
Idigipack = Idigi.reshape(Idigi.shape[0], nChannels, nWordSingleChannel)[:,:,5:5+nPtsDigi ]
iChan = 2

fig, ax = plt.subplots(3,3)
fig.set_size_inches(60, 40)
ax = ax.flatten()

for i in range(8):
    iChan = i

    # Ripeto 0..512 per il numero di eventi (Idigipack.shape[0])
    hh = ax[i].hist2d(np.tile(np.arange(nPtsDigi),Idigipack.shape[0]), 
                      Idigipack[:,iChan,:].flatten(), 
                      bins=(nPtsDigi,50), norm=mpl.colors.LogNorm(), cmap = "jet")
    ax[i].set_title(f"Chan {iChan}", fontsize = 40)
    fig.colorbar(hh[3], ax = ax[i])
plt.show()
