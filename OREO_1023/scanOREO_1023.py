#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 18 17:15:40 2023

@author: ale
"""

from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches

plt.rcParams['font.size'] = '20'
datapath = r'C:\Users\aselmi\Desktop\Dottorato\data\dataOREO_1023\HDF5\run720'
h5keys = [ 'xpos','digiPH', 'digiTime', 'xinfo']

chLabel = {'LG':0, 'APC':1, 'Cherry44':2, 'Cherry48':3, 'George':4, 'John':5, 'Paul':6, 'Ringo':7}
SiPMlabel = ['George','John', 'Paul', 'Ringo']

label_axRnd = ['Random', 'Axial']
Color = ['navy', 'hotpink']
## le q e le m si calibrazione sono in ordine G, J, P, R
SiPMcalib_intercept = np.loadtxt(r'C:\Users\aselmi\Desktop\Dottorato\analysis\OREO_1023\T9oct_OREOintercept.dat')
SiPMcalib_slope = np.loadtxt(r'C:\Users\aselmi\Desktop\Dottorato\analysis\OREO_1023\T9oct_OREOslope.dat')

runnumbers = [113]
distancesfromT1 = [500]
zcryst = 549
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]

### varie threshold per i tagli 
thcherry = 130
divsup = 0.001
divinf = -0.001
timethinf = [200]*4
timethsup = [230]*4
phth = [1000]*4

#liste 
partialData = []
outdata = []

## dizionari
data = {}
cutdiv = {}
cutcherry = {}
cuttime_OREO= {}
cuttime_George = {}
cuttime_John = {}
cuttime_Paul = {}
cuttime_Ringo = {}
cutph_OREO = {}
cutph_George = {}
cutph_Paul = {}
cutph_John = {}
cutph_Ringo = {}
cutpos_OREO = {}
cuttot_OREO = {}
cutpos_Paul = {}
cutpos_George = {}
cutpos_John = {}
cutpos_Ringo = {}
ph_GeorgeGeV = {}
ph_JohnGeV = {}
ph_PaulGeV = {}
ph_RingoGeV={}
ph_OREOGeV = {}
xmeanPH = {}
ymeanPH = {}

xinf_paul = 2.3
yinf_paul = 4.2
cubo_lenght = 1.2
crystlenght = 2 # per il cut fiduciale. La vera lunghezza è 2.5
xsup_paul = xinf_paul + cubo_lenght
ysup_paul = yinf_paul + cubo_lenght
#%%
data = opendata(runnumbers[0], datapath, h5keys)
xpos = data[0]
xpos[:,1] = 10-xpos[:,1]
xpos[:,3] = 10-xpos[:,3]
# xpos[:,0] = 10-xpos[:,0]
# xpos[:,2] = 10-xpos[:,2]
#camere 
xlabels = ['BC1',' BC1', 'BC2', 'BC2']
xlim = [[-0,10]]*4
histpos1d(xpos, 2, xlabels = xlabels, xlim = xlim, Range = [0,10])
histpos2d(xpos, numberoftrackers = 2)
#correlazioni camere 
fig, ax = plt.subplots(1,2 , figsize=(8,8))
ax = ax.flat
ax[0].hist2d(xpos[:,0], xpos[:,2], bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet)
ax[0].set_ylabel('x BC2 [cm]')
ax[0].set_xlabel('x BC1 [cm]')

ax[1].hist2d(xpos[:,1],xpos[:,3] , bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet,)
ax[1].set_xlabel('y BC1 [cm]')
ax[1].set_ylabel('y BC2 [cm]')
plt.tight_layout()
plt.show()

#divergenze
divex, divey, sigma, errsigma, mu, _ = divgaussianfit(xpos, 2, distancesfromT1,
																		p0divx, p0divy,
																		Rangex=[[-0.02,0.02]],
																		Rangey=[[-0.02,0.02]])


datashift, shift = aligntracker(xpos, 2, mu, distancesfromT1)
divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
																										distancesfromT1,
																										p0divx, p0divy,
																										Rangex=[[-0.004,0.004]],
																										Rangey=[[-0.004,0.004]], 
																										Color='navy', 
																										Figsize = (8,6))
cutdiv = ((divexalign >divinf) & (divexalign<divsup) & (diveyalign>divinf) &(diveyalign<divsup))
ax[0].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
ax[0].axvline(x=divsup, color="black", linestyle = '--', lw = 2)
ax[1].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
ax[1].axvline(x=divsup, color="black", linestyle = '--', lw = 2)   



# profilo cherenkov selezioni gli elettroni
## Cherry 44
fig, ax = plt.subplots(1, 1, figsize=(10, 6))
ax.hist((data[1][:,chLabel['Cherry44']]),
				bins = 100, histtype ='step', color = 'turquoise',
				alpha = 1, lw = 3 ,label = 'Cherry44', range = [0, 4000])
ax.axvline(x=thcherry, color="black", linestyle = '--', label = 'cut', lw = 2)
ax.grid()
ax.set_xlabel('Cherenkov PH [a.u]')
ax.set_ylabel('Entries [ns]')
ax.set_yscale('log')
ax.legend()
fig.set_tight_layout('tight')
plt.show()
## cherry 48
fig, ax = plt.subplots(1, 1, figsize=(10, 6))

ax.hist((data[1][:,chLabel['Cherry48']]),
				bins = 100, histtype ='step', color = 'turquoise', 
				alpha = 1, lw = 3 ,label = 'Cherry48', range = [0, 6000])
ax.axvline(x=thcherry, color="black", linestyle = '--', label = 'cut', lw = 2)
ax.grid()
ax.set_xlabel('Cherenkov PH [a.u]')
ax.set_ylabel('Entries [ns]')
ax.set_yscale('log')
fig.set_tight_layout('tight')
ax.legend()
plt.show()
cutcherry = ((data[1][:,chLabel['Cherry48']]>thcherry) & (data[1][:,chLabel['Cherry44']]>thcherry))


# plot PH e tempo + tagli su tempi e PH dei tre cristalli 
fig, ax = plt.subplots(2, 2, figsize=(10, 6))
ax = ax.flat
for j , BeatName in enumerate(SiPMlabel):
	ax[j].hist2d(data[1][:,chLabel[BeatName]],data[2][:,chLabel[BeatName]] ,
					bins =[100,100], cmap = plt.cm.jet, norm=colors.LogNorm(), range = [[0, 10000], [0,500]])
	ax[j].axhline(y=timethinf[j], color="black", linestyle = '--', label = 'cut', lw = 2)
	ax[j].axhline(y=timethsup[j], color="black", linestyle = '--', label = 'cut', lw = 2)
	ax[j].axvline(x=phth[j], color="black", linestyle = '--', label = 'cut', lw = 2)
	ax[j].set_ylabel('Entries')
	ax[j].set_xlabel('PH [a.u]')
	ax[j].set_title(f'{BeatName}')
	ax[j].grid()
	exec(f'cuttime_{BeatName} = (data[2][:,chLabel["{BeatName}"]]>timethinf[{j}]) & (data[2][:,chLabel["{BeatName}"]]<timethsup[j])&(data[2][:,chLabel["{BeatName}"]]>timethinf[j]) & (data[2][:,chLabel["{BeatName}"]]<timethsup[j])')
	exec(f'cutph_{BeatName} = data[1][:,chLabel["{BeatName}"]]>phth[{j}]')
plt.tight_layout()
plt.show()

cuttime_OREO = (cuttime_George | cuttime_John | cuttime_Paul | cuttime_Ringo)
cutph_OREO = (cutph_George | cutph_John | cutph_Paul | cutph_Ringo)
# efficienza cristallo 
xcryst = xpos[:,0] + zcryst*np.tan(divexalign)
ycryst = xpos[:,1] + zcryst*np.tan(diveyalign)
fig, ax = plt.subplots(1,1, figsize=(6,6))
zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xcryst[0], ycryst[0], cmap=plt.cm.jet, bins=[80, 80], range=[[0,10], [0,10]])
plt.colorbar(mapable, ax=ax)
ax.set_xlabel('x [cm]')
ax.set_xlabel('y [cm]')
ax.set_title('Proiezione su piano del cristallo')
plt.tight_layout()
plt.show()


### istogramma con tagli 
zhist2dcut, xhist2dcut , yhist2dcut , mapable = ax.hist2d(xcryst[0][cuttime_OREO & cutph_OREO], ycryst[0][cuttime_OREO & cutph_OREO], 
																				cmap=plt.cm.jet, bins = [80,80],
																				range = [[0,10], [0,10]])


## EFFICIENZA
fig, ax = plt.subplots(1,1, figsize=(8,8))
np.seterr(divide='ignore', invalid = 'ignore')
eff = np.divide(zhist2dcut, zhist2d)
eff[np.isnan(eff)]=0
divider = make_axes_locatable(ax)
cax = divider.append_axes('right', size='5%', pad=0.1)
im = ax.imshow(eff.T, cmap=plt.cm.jet, extent=[0,10, 0, 10], origin = "lower")
fig.colorbar(im, cax=cax, orientation='vertical')

rectG = patches.Rectangle((xinf_paul, yinf_paul+2.5), cubo_lenght, cubo_lenght, linewidth=3, edgecolor='black', facecolor='none', label= 'Fiducial area')
rectR = patches.Rectangle((xinf_paul+2.5, yinf_paul), cubo_lenght, cubo_lenght, linewidth=3, edgecolor='black', facecolor='none')
rectP = patches.Rectangle((xinf_paul, yinf_paul), cubo_lenght, cubo_lenght, linewidth=3, edgecolor='black', facecolor='none')
rectJ = patches.Rectangle((xinf_paul+2.5,yinf_paul+2.5), cubo_lenght, cubo_lenght, linewidth=3, edgecolor='black', facecolor='none')

ax.add_patch(rectP)
ax.add_patch(rectR)
ax.add_patch(rectG)
ax.add_patch(rectJ)
ax.set_xlim([0,10])
ax.set_ylim([0,10])
ax.set_xlabel('x [cm]')
ax.set_ylabel('y [cm]')
ax.legend()
plt.savefig(r'C:\Users\aselmi\Desktop\Dottorato\Technical Note Oreo2023\T9_ottobre\T9Oct_oreoeff.pdf')
plt.tight_layout()


cutpos_George = ((xcryst[0]>xinf_paul) & (xcryst[0]<xinf_paul+cubo_lenght ) &(ycryst[0]>yinf_paul+2.5) &( ycryst[0]<yinf_paul+2.5+cubo_lenght))
cutpos_John = ((xcryst[0]>xinf_paul+2.5) & (xcryst[0]<xinf_paul+2.5+cubo_lenght ) & (ycryst[0]>yinf_paul+2.5) &( ycryst[0]<yinf_paul+2.5+cubo_lenght))
cutpos_Paul = ((xcryst[0]>xinf_paul) & (xcryst[0]<xinf_paul+cubo_lenght)&(ycryst[0]> yinf_paul) &( ycryst[0]< yinf_paul+cubo_lenght))
cutpos_Ringo = ((xcryst[0]>xinf_paul+2.5) & (xcryst[0]<xinf_paul+2.5+cubo_lenght )&(ycryst[0]>yinf_paul) &( ycryst[0]<yinf_paul + cubo_lenght))

#%%

gonio= data[3][:,0]
cradle = data[3][:,1]
for j , BeatName in enumerate(SiPMlabel):
   exec(f'ph_{BeatName}GeV = ((data[1][:,chLabel["{BeatName}"]])-SiPMcalib_intercept[{j}])/SiPMcalib_slope[{j}]')
   fig, ax = plt.subplots(figsize=(12,10))
   exec(f'isto2d = plt.hist2d(gonio[cuttime_{BeatName} & cutph_{BeatName} & cutpos_{BeatName} & cutcherry], ph_{BeatName}GeV[cuttime_{BeatName} & cutph_{BeatName} & cutpos_{BeatName} & cutcherry] , bins = [12,100], cmap = "jet", range = [[25000, 45000],[0, 5]])')
   ax.set_ylabel(f'{BeatName} energy deposited [GeV]')
   ax.set_xlabel('$\u03F4_{x,mis}$ [$\mu$ rad]'	)
   plt.colorbar()
   x1,y1,_ = hist2dToProfile(isto2d)
   xmeanPH[BeatName] = x1
   ymeanPH[BeatName] = y1
#     # Proietto l'istogramma su un asse


#%%
Colors = ['limegreen', 'deeppink','orangered', 'navy']
markerts = ["o",  "v", "s", "*" ]
fig, ax = plt.subplots(figsize=(10,6))
for i, label in enumerate(SiPMlabel):
    ax.plot(xmeanPH[label],ymeanPH[label], marker = markerts[i], markersize = 12, ls='--', lw = 2, label=f'{label}', color = Colors[i])
plt.grid(linewidth=0.5)
ax.set_xlim(25000, 45000)
ax.axvline(x=32552, color="black", linestyle = '--', label = 'Axis', lw = 2)
ax.set_ylim(1, 3)
ax.set_xlabel('$\u03F4_{x}$ [$\mu$ rad]', fontsize = 24)
ax.set_ylabel('Energy deposited [GeV]', fontsize = 24)
ax.legend(fontsize = 18)
plt.tight_layout()
plt.savefig(r'C:\Users\aselmi\Desktop\Dottorato\Technical Note Oreo2023\T9_ottobre\T9oct_angularScanInteralign.pdf')
plt.show()
# # # plt.show()
# # #  #%%   
# # # import matplotlib.colors as colors
# # # import matplotlib.cm as cm


# # # plt.rcParams['font.size'] = '18'


# # # xpos = xstereo[1]
# # # ypos = [cradle[111][1]]*len(xstereo[1])
# # # zpos = 0  # z coordinates of each bar
# # # dx = [0.0001]  # Width of each bar
# # # dy = [0.0001]# Depth of each bar
# # # dz = np.array(ystereo[1])

# # # offset = dz + np.abs(dz.min())
# # # fracs = offset.astype(float)/offset.max()
# # # norm = colors.Normalize(fracs.min(), fracs.max())
# # # colors = cm.jet(norm(fracs))
# # # cmap = 'jet'





# # # area = dz*10**-2

# # # fig, ax = plt.subplots(figsize=(8,6), constrained_layout=True)
# # # plt.scatter(xpos, ypos, s=area, c= colors,cmap = cm.jet)
# # # plt.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap))

# # # # ax.set_xlim([1.642*10**6, 1.654*10**6])
# # # # ax.set_ylim([22000, 28000])
# # # # ax.set_xlim([1.64649*10**6, 1.64751*10**6])
# # # # ax.set_ylim([24500, 26800])
# # # # ax.plot( 1647000,25384, '*', c= 'black', markersize = 20, label = 'Axis')
# # # ax.set_ylabel('$\u03B8_{crad}$ [rad]', fontsize = 22)
# # # ax.set_xlabel('$\u03B8_{ang}$ [rad]', fontsize = 22)
# # # ax.grid()
# # # ax.legend(fontsize = 26)


# # # fig.show()  
    

# %%
