#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 13 10:29:27 2023

@author: ale
"""
### calibrazione unico LG con tagli in divergenza e tempo 
## c'è un po' di casino per avere le due rette sia a 0.5 di range che a 5
from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
model = GaussianModel()
plt.rcParams['font.size'] = '20'
#triggere APC + Cherry
# Ch 0 -->  XCET 48 (Cherry 2)
# Ch 1 --> LG1 (Door / T10 side)
# Ch 2 --> George 1
# Ch 3 --> George 2
# Ch 4 --> John 1
# Ch 5 --> John 2
# Ch 6 --> Paul 1
# Ch 7 --> Paul 2

chLabel = {'LG_05V':0, 'LG_2V':1, 'George1':2, 'George2':3, 'John1':4, 'John2':5, 'Paul1':6, 'Paul2':7}
plt.rcParams['font.size'] = '20'
runnumbers = [run for run in range(95, 100)]
datapath = r'C:\Users\aselmi\Desktop\Dottorato\data\h2dataOREO_0823\HDF5\run7100'
h5keys = [ 'xpos','digiPH', 'digiTime']
energies = ['120', '80', '60', '40', '20']
energies_num = [120, 80, 60, 40, 20]
ene_correction = [120*0.9, 80*0.9, 60*0.9, 40*0.9, 20*0.9]

distancesfromT1 = [1548]
zcalo = 1548+60+3
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]
divsup = 0.0002
divinf = -0.0002
timethsup = 260
timethinf = 240




Colors = [ 'deeppink',  'cyan', 'chartreuse','gold',  'maroon']

mucalib = {}
muerrcalib = {}
cutdiv = {}
cuttime = {}
cuttot = {}
data = {}
sigmadivx_ene = []
errsigmadivx_ene = []
#%%

for i, nrun in enumerate(runnumbers):
    #carico dati
    data[nrun] = opendata(nrun, datapath, h5keys)
    #divergenza
    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(data[nrun][0], 2, distancesfromT1,
                                                            p0divx, p0divy,
                                                            Rangex=[[-0.002,0.002]],
                                                            Rangey=[[-0.002,0.002]])
    
    
    datashift, shift = aligntracker(data[nrun][0], 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                                    distancesfromT1,
                                                                                    p0divx, p0divy,
                                                                                    Rangex=[[-0.0004,0.0004]],
                                                                                    Rangey=[[-0.0004,0.0004]], 
                                                                                    Color='navy', 
                                                                                    Figsize = (8,6))
    
    sigmadivx_ene.append(sigmaalign)
    errsigmadivx_ene.append(errsigmaalign)
    #tagli in divergenza
    cutdiv[nrun] = ((divexalign >divinf) & (divexalign<divsup) & (diveyalign>divinf) &(diveyalign<divsup))
    ax[0].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=divsup, color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=divsup, color="black", linestyle = '--', lw = 2)    
    ax[0].set_title(f'Lead Glass, {energies[i]} GeV ')
    #plot PH tempo
    import copy 
    import matplotlib as mpl
    my_cmap = copy.copy(mpl.cm.jet)
    my_cmap.set_bad(my_cmap(0))
    fig, ax = plt.subplots(1, 1, figsize=(8, 6))
    zhist2d, xhist2d, yhist2d, mapable =  ax.hist2d(data[nrun][1][:,chLabel['LG_2V']],data[nrun][2][:,chLabel['LG_2V']] ,
                bins =[100,100], cmap = my_cmap, range = [[0, 20000], [200, 300]], norm=colors.LogNorm())
    #taglio in tempo
    cuttime[nrun] = ((data[nrun][2][:,chLabel['LG_2V']]<timethsup) & (data[nrun][2][:,chLabel['LG_2V']]>timethinf))
    ax.axhline(y=timethinf, color="red", linestyle = '--', lw = 2, label = f'Threshold \n  {energies[i]} GeV ')
    ax.axhline(y=timethsup, color="red", linestyle = '--', lw = 2)
    ax.grid()
    ax.set_xlabel('PH [a.u]')
    ax.set_ylabel('time [ns]')
    ax.legend()
    fig.colorbar(mapable,ax=ax)
    #ax.set_title(f'Lead Glass {energies[i]} GeV')
    plt.tight_layout()
    if energies[i] == '120': 
        print('ciao')
        #plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/h2/H2_PHvsTime.pdf')
    plt.show()
    
    #ricostruzione su faccia del LG
    xcalo = datashift[:, 0] + zcalo*np.tan(divexalign)
    ycalo = datashift[:, 1] + zcalo*np.tan(diveyalign)
    
    fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xcalo[0], ycalo[0],
                                                    bins=[100, 100], cmap=plt.cm.jet, range=[[0, 10], [0, 10]])
    
    ax.set_xlabel(f'x [cm], {energies[i]} GeV')
    ax.set_ylabel(f'y [cm], {energies[i]} GeV')
    fig.colorbar(mapable,ax=ax)
    
    #taglio totale divergenza e tempo
    cuttot[nrun] = (cutdiv[nrun][0] & cuttime[nrun])
#%%
#plot divergenza in funzione dell'energia
fig, ax = plt.subplots(1, 1, figsize=(8, 6)) 
sigmax_lst = []
sigmay_lst = []

for j in range(5):
    sigmax_lst.append(sigmadivx_ene[j][0]*10**(6))
    sigmay_lst.append(sigmadivx_ene[j][1]*10**(6))
    
ax.plot(energies_num, sigmax_lst, 
            marker='o',  linestyle = 'dotted', color = 'indigo',
            markersize = 8, label = 'x ')
ax.plot(energies_num, sigmay_lst,  
            marker='D',  linestyle = 'dotted', color = 'orangered',
            markersize = 8, label = 'y')
   
ax.legend()
ax.grid()
ax.set_xlabel('Energies [GeV]')

ax.set_ylabel('Divergence [$\mu$rad]')   
    

#plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/h2/H2_divergence.pdf')
plt.tight_layout()
#%%
# #plot di picchi fittati con una gaussiana per la calibrazione
# figcal, axcal = plt.subplots(1, 1, figsize=(12, 10))   
# for i, nrun in enumerate(runnumbers[:-3]):#########################################################################################à   

#     hph, binsph, _ = axcal.hist(data[nrun][1][:,chLabel['LG_2V']][cuttot[nrun]], bins = 200, histtype ='stepfilled',
#                           color = Colors[i],edgecolor = Colors[i], alpha = 0.4, 
#                           lw =3, 
#                           label = f'{energies[i]} GeV', density = True, range = [0, 10000])

#     xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
#     phcondition = ((xdata > fitrangecalibinf[i]) & (xdata < fitrangecalibsup[i]))
#     paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
#     resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
#                         weights=np.sqrt(hph[phcondition]))
#     to_plot = np.linspace(fitrangecalibinf[i], fitrangecalibsup[i], 10000)
#     y_eval = model.eval(resultx.params, x=to_plot)
#     #ax[i].plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = 'black')
#     mucalib[energies[i]] = resultx.params['center'].value
#     muerrcalib[energies[i]] = resultx.params['center'].stderr
#     axcal.plot(to_plot, y_eval, linewidth=2, color='black', label=f'$\mu$ = {round(mucalib[energies[i]],2)} $\pm$ {round(muerrcalib[energies[i]],2)}')
    
# axcal.grid()
# axcal.set_xlabel('PH [a.u]')
# axcal.set_ylabel('Counts')
# axcal.legend(fontsize = 18)
# figcal.set_tight_layout('tight')
# plt.show()





#%%#########################################################################
#########################################################################ààà
############################################################################
#plot di picchi fittati con una gaussiana per la calibrazione range 2V
sigmacalib = {}
sigmacaliberr = {}
fitrangecalibinf = [9000,6300,4700, 3000, 1250]
fitrangecalibsup = [10000, 7800, 5500, 3400, 1800]
Range = [[9000, 13000], [5000, 8000], [4500, 6000], [3000, 4000],[1000, 3000] ]
figcal, axcal = plt.subplots(1, 1, figsize=(8, 6))
for i, nrun in enumerate(runnumbers):#########################################################################################à   
    print(i)
      
    hph, binsph, _ = axcal.hist(data[nrun][1][:,chLabel['LG_2V']][cuttot[nrun]], bins = 150, histtype ='stepfilled',
                          color = Colors[i],edgecolor = Colors[i], alpha = 0.4, 
                          lw =3, 
                          label = f'{energies[i]} GeV', range = Range[i], density = True)

    xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
    phcondition = ((xdata > fitrangecalibinf[i]) & (xdata < fitrangecalibsup[i]))
    paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
    resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                        weights=np.sqrt(hph[phcondition]))
    to_plot = np.linspace(fitrangecalibinf[i], fitrangecalibsup[i], 10000)
    y_eval = model.eval(resultx.params, x=to_plot)
    #ax[i].plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = 'black')
    mucalib[energies[i]] = resultx.params['center'].value
    muerrcalib[energies[i]] = resultx.params['center'].stderr
    ##################################################################
    ##################################################################
    ##################################################################
    sigmacalib[energies[i]] = resultx.params['sigma'].value
    sigmacaliberr[energies[i]] = resultx.params['sigma'].stderr
    
    axcal.plot(to_plot, y_eval, linewidth=2, color='black', label=f'$\mu$ = {round(mucalib[energies[i]],2)} $\pm$ {round(muerrcalib[energies[i]],2)}')
    
axcal.grid()
axcal.set_xlabel('PH [a.u]')
axcal.set_ylabel('Normalized counts')
axcal.legend(fontsize = 14)
# axcal.set_ylim(0,0.01)
plt.tight_layout()
#plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/h2/H2_calibrationPeaks.pdf')
plt.show()

#%% picchi gaussiana range 05V
fitrangecalibinf = [10000,0]
fitrangecalibsup = [15000, 14000]

mucalib05 = {}
muerrcalib05 = {}
figcal, axcal = plt.subplots(1, 1, figsize=(10, 8))   
for i, nrun in enumerate(runnumbers[-2:]):#########################################################################################à   
    print(i)
    hph, binsph, _ = axcal.hist(data[nrun][1][:,chLabel['LG_05V']], bins = 200, histtype ='stepfilled',
                          color = Colors[i],edgecolor = Colors[i], alpha = 0.4, 
                          lw =3, 
                          label = f'{energies[i+3]} GeV', density = True, range = [0, 25000])

    xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
    phcondition = ((xdata > fitrangecalibinf[i]) & (xdata < fitrangecalibsup[i]))
    paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
    resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                        weights=np.sqrt(hph[phcondition]))
    to_plot = np.linspace(fitrangecalibinf[i], fitrangecalibsup[i], 10000)
    y_eval = model.eval(resultx.params, x=to_plot)
    #ax[i].plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = 'black')
    mucalib05[energies[i+3]] = resultx.params['center'].value
    muerrcalib05[energies[i+3]] = resultx.params['center'].stderr
    axcal.plot(to_plot, y_eval, linewidth=2, color='black', label=f'$\mu$ = {round(mucalib[energies[i+3]],2)} $\pm$ {round(muerrcalib[energies[i+3]],2)}')
    
axcal.grid()
axcal.set_xlabel('PH [a.u]')
axcal.set_ylabel('Counts')
axcal.legend(fontsize = 20)
figcal.set_tight_layout('tight')

plt.show()

#%%
# Plot retta di calibrazione per energie range 2 V 
mucaliblist = []   
errmucaliblist = []
sigmacaliblist = []
errsigmacaliblist = []


for energy in energies: 

    mucaliblist.append(mucalib[energy])
    errmucaliblist.append(muerrcalib[energy])
    errsigmacaliblist.append(sigmacaliberr[energy])
    sigmacaliblist.append(sigmacalib[energy])
 
mucaliblist05 = []   
errmucaliblist05 = []

for energy in energies[-2:]: 

    mucaliblist05.append(mucalib05[energy])
    errmucaliblist05.append(muerrcalib05[energy])
    
figline, axline = plt.subplots(1,1, figsize=(8,8))
from lmfit.models import LinearModel
linearmodel = LinearModel()     
linearparams = linearmodel.guess(mucaliblist, x =energies_num)
linearresult = linearmodel.fit(mucaliblist, linearparams, x= energies_num, weights = errmucaliblist)
to_plot = np.linspace(0, 125, 10000)
y_eval = linearmodel.eval(linearresult.params, x=to_plot)
axline.errorbar(energies_num, mucaliblist,  yerr= errmucaliblist, 
            marker='D',  linestyle = 'none', color = 'darkgreen',
            markersize = 9)

axline.plot(to_plot,y_eval, color = 'red', lw = 2, ls = '--',
        label = f' a.u. = ({round(linearresult.params["slope"].value,2)} $\pm$ {round(linearresult.params["slope"].stderr,2)})$\cdot$ GeV +  ({round(linearresult.params["intercept"].value,2)}  $\pm$ {round(linearresult.params["intercept"].stderr,2)})')
axline.legend(fontsize = 15)
axline.set_ylabel('PH [a.u.]')
axline.set_xlabel('GeV')
axline.grid()
plt.tight_layout()
plt.savefig(r'C:\Users\aselmi\Desktop\Dottorato\Technical Note Oreo2023\h2/H2_calibline.pdf')
slopeCalib23=(linearresult.params['slope'].value)
errslopeCalib23 = (linearresult.params['slope'].stderr)
interceptCalib23= (linearresult.params['intercept'].value)
errinterceptCalib23 = (linearresult.params['intercept'].stderr)
print(f'Chi-square = {linearresult.chisqr:.4f}, Reduced Chi-square = {linearresult.redchi:.4f}')

#calcolo dei residui
residual_lowE = 100* (mucaliblist-linearresult.best_fit)/(linearresult.best_fit)


calib = [slopeCalib23, errslopeCalib23, interceptCalib23, errinterceptCalib23]

with open('H2_LG_CalibrationOREO23_2V.dat','w') as out_file:
        np.savetxt(out_file, calib)
        
#%%
figline, axline = plt.subplots(1,1, figsize=(8,8))
linearmodel = LinearModel()     
linearparams = linearmodel.guess(mucaliblist, x = ene_correction)
linearresult_corr = linearmodel.fit(mucaliblist, linearparams, x = ene_correction, weights = errmucaliblist)
to_plot = np.linspace(0, 125, 10000)
y_eval = linearmodel.eval(linearresult_corr.params, x=to_plot)
axline.errorbar(ene_correction, mucaliblist,  yerr= errmucaliblist, 
            marker='D',  linestyle = 'none', color = 'hotpink',
            markersize = 6)

axline.plot(to_plot,y_eval, color = 'black', lw = 2, ls = '--',
        label = f' a.u. = ({round(linearresult_corr.params["slope"].value,2)} $\pm$ {round(linearresult_corr.params["slope"].stderr,2)})$\cdot$ GeV +  ({round(linearresult_corr.params["intercept"].value,2)}  $\pm$ {round(linearresult_corr.params["intercept"].stderr,2)})')
axline.legend(fontsize = 10)
axline.set_ylabel('PH [a.u.]')
axline.set_xlabel('GeV')
slopeCalib23=(linearresult_corr.params['slope'].value)
errslopeCalib23 = (linearresult_corr.params['slope'].stderr)
interceptCalib23= (linearresult_corr.params['intercept'].value)
errinterceptCalib23 = (linearresult_corr.params['intercept'].stderr)
print(f'Chi-square = {linearresult_corr.chisqr:.4f}, Reduced Chi-square = {linearresult_corr.redchi:.4f}')
axline.grid()
#calcolo dei residui
#residual_lowE = 100* (mucaliblist-linearresult_corr.best_fit)/(linearresult_corr.best_fit)


calib = [slopeCalib23, errslopeCalib23, interceptCalib23, errinterceptCalib23]

with open('H2_LG_CalibrationOREO23_2Vcorrection.dat','w') as out_file:
        np.savetxt(out_file, calib)
        
        
# ## attenzione che hanno gli stessi nomi-- qui faccio retta per i tre ad alta energia con range 0.5V

        
# linearmodel = LinearModel()     
# linearparams = linearmodel.guess(energies_num[-2:], x=mucaliblist05)
# linearresult = linearmodel.fit(energies_num[-2:], linearparams, x= mucaliblist05, weights = errmucaliblist05)
# to_plot = np.linspace(50, 15000, 10000)
# y_eval = linearmodel.eval(linearresult.params, x=to_plot)
# axline.errorbar(mucaliblist05, energies_num[-2:], xerr= errmucaliblist05, 
#             marker='D',  linestyle = 'none', color = 'midnightblue',
#             markersize = 6)

# axline.plot(to_plot,y_eval, color = 'red', lw = 2,
#         label = f' GeV = ({round(linearresult.params["slope"].value,4)} $\pm$ {round(linearresult.params["slope"].stderr,5)})$\cdot$ ADC +  ({round(linearresult.params["intercept"].value,4)}  $\pm$ {round(linearresult.params["intercept"].stderr,5)})')
# axline.legend(fontsize = 10)
# axline.set_ylabel('GeV')
# axline.set_xlabel('PH [a.u.]')
# slopeCalib23=(linearresult.params['slope'].value)
# errslopeCalib23 = (linearresult.params['slope'].stderr)
# interceptCalib23= (linearresult.params['intercept'].value)
# errinterceptCalib23 = (linearresult.params['intercept'].stderr)
# print(f'Chi-square = {linearresult.chisqr:.4f}, Reduced Chi-square = {linearresult.redchi:.4f}')
# axline.grid()
# #calcolo dei residui
# residual_lowE05 = 100* (energies_num[-2:]-linearresult.best_fit)/(linearresult.best_fit)


# calib = [slopeCalib23, errslopeCalib23, interceptCalib23, errinterceptCalib23]

# with open('H2_LG_CalibrationOREO23_05V.dat','w') as out_file:
#         np.savetxt(out_file, calib)
        










#%%  
# fig, ax = plt.subplots(1,1, figsize=(10,4))
# ax.plot(mucaliblist05
#         , residual_lowE05, 'D', markersize = 8, color = 'midnightblue', label = 'Dynamic range 0.5 V')
# ax.axhline(y= 0, ls = ':', c = 'k', lw = 3)

# ax.set_xlabel('PH [a.u.]')
# ax.set_ylabel('Residual $\%$')
# ax.set_ylim([-0.1, 0.1])
# ax.grid()
# plt.tight_layout()

# plt.show()


fig, ax = plt.subplots(1,1, figsize=(10,4))
ax.plot(energies_num
        , residual_lowE, 'D', markersize = 8, color = 'darkgreen', label = 'Dynamic range 2 V')
ax.axhline(y= 0, ls = ':', c = 'k', lw = 3)

ax.set_xlabel('GeV')
ax.set_ylabel('Residual $\%$')
ax.set_ylim([-5, 5])
ax.grid()
plt.tight_layout()


#plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/h2/H2_residual2V.pdf')

plt.show()
#%%

#%%  calcoliamo la risoluzione  energetica del LG 

plt.rcParams['font.size'] = '20'
energy_resolution = []
energy_resolutionerr = []
for k in range(len(mucaliblist)): 
    energy_resolution.append((sigmacaliblist[k]/mucaliblist[k])*100)
    energy_resolutionerr.append((np.sqrt((errsigmacaliblist[k]/sigmacaliblist[k])**2+(errmucaliblist[k]/mucaliblist[k])**2)*energy_resolution[k]))


def energy_resolution_function(E, a, b, c):
    return np.sqrt( (a**2 / E) + (b / E)**2+ c**2)

from lmfit import Model
fig, ax = plt.subplots(1,1, figsize=(8,6))
model = Model(energy_resolution_function)
params = model.make_params(a=1.0, b=1.0, c=1.0)
result = model.fit(energy_resolution, params, E=energies_num, weights=energy_resolutionerr)
a_fit = result.params['a'].value
b_fit = result.params['b'].value
c_fit = result.params['c'].value

sigma_a = result.params['a'].stderr
sigma_b = result.params['b'].stderr
sigma_c = result.params['c'].stderr
to_plot = np.linspace(min(energies_num), max(energies_num), 1000)
fit_energy_resolution = energy_resolution_function(to_plot, a_fit, b_fit, c_fit)

ax.errorbar(energies_num, energy_resolution,yerr=energy_resolutionerr, marker = 'D', markersize = 10
            , color = 'navy',
            linestyle= 'none', label = '$\sigma$(E)/E')


ax.plot(to_plot, fit_energy_resolution, label = '$\sigma$(E)/E = $\sqrt{(a^{2} / E) + (b / E)^2+ c^2)}$' , color = 'red')
    
ax.set_xlabel('GeV')
ax.set_ylabel('Energy resolution [%]')
ax.legend(fontsize = 18)
ax.grid()
plt.tight_layout()
plt.savefig(r'C:\Users\aselmi\Desktop\Dottorato\Technical Note Oreo2023\h2\H2_LGenergyresolution.pdf')
plt.show()


print(f'a = {a_fit}, b = {b_fit}, c = {c_fit}  ')
print(f'a = {sigma_a}, b = {sigma_b}, c = {sigma_c}  ')
# %%
