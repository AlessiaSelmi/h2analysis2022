#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Dec 12 12:29:58 2023

@author: ale
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 13 10:17:12 2023

@author: ale
"""

#facciamo i plot in funzione dell'energia, 
## - deposito in oreo asse e amorfo, 
# - rapporto in OREO asse e amorfo
# - LG energy deposited asse e amorfo
#- missing energy (120-Edep Oreo- ELG )
#- somma oreo + LG 


from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches

plt.rcParams['font.size'] = '20'
#triggere APC + Cherry
# Ch 0 -->  XCET 48 (Cherry 2)
# Ch 1 --> LG1 (Door / T10 side)
# Ch 2 --> George 1
# Ch 3 --> George 2
# Ch 4 --> John 1
# Ch 5 --> John 2
# Ch 6 --> Paul 1
# Ch 7 --> Paul 2

datapath = r'C:\Users\aselmi\Desktop\Dottorato\data\h2dataOREO_0823/HDF5/run'
h5keys = [ 'xpos','digiPH', 'digiTime']

chLabel = {'LG05':0, 'LG2':1, 'George1':2, 'George2':3, 'John1':4, 'John2':5, 'Paul1':6, 'Paul2':7}
SiPMlabel = ['George1','George2', 'John1', 'John2','Paul1', 'Paul2']
allSiPMLabel = ['George','John','Paul']

runnumbers = [710113, 710114, 710118, 710117, 710120, 710121, 710125, 710124, 710093, 710090, 710126, 710127] #Amorfo e Axial --> centrati su john
Energies = [40, 40,  60, 60, 80,  80, 100, 100,  120, 120, 150,  150]
energy = [40, 60, 80,  100,  120,  150]
distancesfromT1 = [1548]
zcryst = 1548+60+200
zLG = zcryst+30
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]

### varie threshold per i tagli 
thcherry = 500
divsup = 0.00015
divinf = -0.00015
timethinf = [230]*6
timethsup = [280]*6
phth = [2000]*6
## dizionari
## dizionari
data = {}
cutdiv = {}
cutcherry = {}
cuttime = {}
cutph = {}
PH_toteq = {}
cuttot = {}
cutpos = {}
crystlenght = 1.5# per il cut fiduciale. La vera lunghezza è 2.5
LGph = {}
xcrystinf = [4.3, 4.3, 4.6, 5, 4.8, 4.7, 4.9, 4.7, 5, 5, 5, 5.2]

ycrystinf= [5, 5.2, 4.1, 4.2, 4.5, 4.3, 3.3, 3.4, 3, 3.3, 3.4, 3.5]
#ycrystsup = ycrystinf+crystlenght

 

OREO_PHtot = {}
LGphGeV = {}


meanOREO_random = []
meanOREO_axial = []
errmeanOREO_random = []
errmeanOREO_axial = []


meanLG_random = []
meanLG_axial = []
errmeanLG_random = []
errmeanLG_axial = []

ratioOREO = []
errratioOREO = []


meanOREO_plusLG_random = []
errmeanOREO_plusLG_random = []
meanOREO_plusLG_axial = []
errmeanOREO_plusLG_axial = []

mean_missingEnergy_random = []
mean_missingEnergy_axial = []

# meanOREO_plusLG_correction = {}
# meanLG_correction = {}
# LGphGeV_correction = {}




#%%
SiPM_calib = np.loadtxt(r'C:\Users\aselmi\Desktop\Dottorato\analysis\Oreo_0823H2\H2_AllSiPM_Calibration_OREO0823.dat')
SiPM_eqfact = np.loadtxt(r'C:\Users\aselmi\Desktop\Dottorato\analysis\Oreo_0823H2\H2_AllSiPM_eqfactor.dat')
SiPM_intercept1 = np.loadtxt(r'C:\Users\aselmi\Desktop\Dottorato\analysis\Oreo_0823H2\H2_AllSiPM_intercept1.dat')
LG_calib = np.loadtxt(r'C:\Users\aselmi\Desktop\Dottorato\analysis\Oreo_0823H2\H2_LG_CalibrationOREO23_2V.dat')
LG_calib_correction = np.loadtxt(r'C:\Users\aselmi\Desktop\Dottorato\analysis\Oreo_0823H2\H2_LG_CalibrationOREO23_2Vcorrection.dat')
#%%
#############################################

for i, nrun in enumerate(runnumbers):
    print(i)
    data[nrun] = opendata(runnumbers[i], datapath, h5keys)
    LGph[nrun] = data[nrun][1][:,chLabel['LG2']]
    xpos = data[nrun][0]
    #camere 
    xlabels = ['BC1',' BC1', 'BC2', 'BC2']
    xlim = [[-0,10]]*4
    histpos1d(xpos, 2, xlabels = xlabels, xlim = xlim, Range = [0,10])
    histpos2d(xpos, numberoftrackers = 2)
    #correlazioni camere 
    fig, ax = plt.subplots(1,2 , figsize=(8,8))
    ax = ax.flat
    ax[0].hist2d(xpos[:,0], xpos[:,2], bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet)
    ax[0].set_ylabel('x BC2 [cm]')
    ax[0].set_xlabel('x BC1 [cm]')
    
    ax[1].hist2d(xpos[:,1],xpos[:,3] , bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet,)
    ax[1].set_xlabel('y BC1 [cm]')
    ax[1].set_ylabel('x BC2 [cm]')
    plt.tight_layout()
    plt.show()

    #divergenze
    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(xpos, 2, distancesfromT1,
                                                           p0divx, p0divy,
                                                           Rangex=[[-0.002,0.002]],
                                                           Rangey=[[-0.002,0.002]])
    
    
    datashift, shift = aligntracker(xpos, 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                                   distancesfromT1,
                                                                                    p0divx, p0divy,
                                                                                    Rangex=[[-0.0004,0.0004]],
                                                                                    Rangey=[[-0.0004,0.0004]], 
                                                                                    Color='navy', 
                                                                                    Figsize = (8,6))
    cutdiv[nrun] = ((divexalign >divinf) & (divexalign<divsup) & (diveyalign>divinf) &(diveyalign<divsup))
    ax[0].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=divsup, color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=divsup, color="black", linestyle = '--', lw = 2)   





    # plot PH e tempo + tagli su tempi e PH dei tre cristalli 
    fig, ax = plt.subplots(2, 3, figsize=(10, 6))
    ax = ax.flat
    for j in range(len(SiPMlabel)):
        ax[j].hist2d(data[nrun][1][:,chLabel[SiPMlabel[j]]],data[nrun][2][:,chLabel[SiPMlabel[j]]] ,
                     bins =[100,100], cmap = plt.cm.jet, range = [[0, 20000], [150, 300]], norm=colors.LogNorm())
        ax[j].axhline(y=timethinf[j], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[j].axhline(y=timethsup[j], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[j].axvline(x=phth[j], color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[j].set_ylabel('Counts')
        ax[j].set_xlabel('PH [a.u]')
        ax[j].set_title(f'{SiPMlabel[j]}')
        ax[j].grid()
    plt.tight_layout()
    plt.show()
    # faccimao un po' di tagli  su tempo e PH 
    ## solo una matrice
    # cuttime[nrun] =(((data[nrun][2][:,chLabel['George1']]>timethinf[0]) & (data[nrun][2][:,chLabel['George1']]<timethsup[0]))|
    #              ((data[nrun][2][:,chLabel['John1']]>timethinf[1]) & (data[nrun][2][:,chLabel['John1']]<timethsup[1]))|
    #              ((data[nrun][2][:,chLabel['Paul1']]>timethinf[2]) & (data[nrun][2][:,chLabel['Paul1']]<timethsup[2])))
    # # I cristalli li vedi andando a fare i tagli sulla PH dei vari SiPM, se chiedi solo il taglio su George vedi lui 
    # cutph[nrun] = ((data[nrun][1][:,chLabel['George1']]>phth[0])|
    #          (data[nrun][1][:,chLabel['John1']]>phth[1]) |
    #          (data[nrun][1][:,chLabel['Paul1']]>phth[2]) )
    
    # cuttot[nrun] = cutdiv[nrun][0] & cuttime[nrun] &cutph[nrun]
    #tutte e due le matrici
    cuttime[nrun] = (((data[nrun][2][:,chLabel['George1']]>timethinf[0]) & (data[nrun][2][:,chLabel['George1']]<timethsup[0])&(data[nrun][2][:,chLabel['George2']]>timethinf[0]) & (data[nrun][2][:,chLabel['George2']]<timethsup[0]))|
                 ((data[nrun][2][:,chLabel['John1']]>timethinf[1]) & (data[nrun][2][:,chLabel['John1']]<timethsup[1])&(data[nrun][2][:,chLabel['John2']]>timethinf[1]) & (data[nrun][2][:,chLabel['John2']]<timethsup[1]))|
                 ((data[nrun][2][:,chLabel['Paul1']]>timethinf[2]) & (data[nrun][2][:,chLabel['Paul1']]<timethsup[2])& (data[nrun][2][:,chLabel['Paul2']]>timethinf[2]) & (data[nrun][2][:,chLabel['Paul2']]<timethsup[2])))
    # I cristalli li vedi andando a fare i tagli sulla PH dei vari SiPM, se chiedi solo il taglio su George vedi lui 
    cutph[nrun] = ((data[nrun][1][:,chLabel['George1']]>phth[0])|(data[nrun][1][:,chLabel['George2']]>phth[0])|
             (data[nrun][1][:,chLabel['John1']]>phth[1]) | (data[nrun][1][:,chLabel['John2']]>phth[0])|
             (data[nrun][1][:,chLabel['Paul1']]>phth[2])|(data[nrun][1][:,chLabel['Paul2']]>phth[0]))
    
            
#    efficienza cristallo 
    xcryst = xpos[:,0] + zcryst*np.tan(divexalign)
    ycryst = xpos[:,1] + zcryst*np.tan(diveyalign)
    fig, ax = plt.subplots(1,1, figsize=(6,6))
    zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xcryst[0], ycryst[0], cmap=plt.cm.jet, bins=[80, 80], range=[[0,10], [0,10]])
    plt.colorbar(mapable, ax=ax)
    ax.set_xlabel('x [cm]')
    ax.set_xlabel('y [cm]')
    ax.set_title('Proiezione su piano del cristallo')
    plt.tight_layout()
    plt.show()

   
    fig, ax = plt.subplots(1,1, figsize=(6,6))
    zhist2dcut, xhist2dcut , yhist2dcut , mapable = ax.hist2d(xcryst[0][cuttime[nrun] & cutph[nrun]& cutdiv[nrun][0]], ycryst[0][cuttime[nrun] & cutph[nrun]& cutdiv[nrun][0]], 
                                                                  cmap=plt.cm.jet, bins = [80,80],
                                                                  range = [[0,10], [0,10]])


    plt.colorbar(mapable, ax=ax)
    rect = patches.Rectangle((xcrystinf[i], ycrystinf[i]), crystlenght, crystlenght, linewidth=4, edgecolor='darkred', facecolor='none')
    #ax.add_patch(rect)
    ax.set_ylabel('x [cm]')
    ax.set_xlabel('y [cm]')

    plt.tight_layout()
    plt.show()
   
    ## efficienza
    
    
    fig, ax = plt.subplots(1,1, figsize=(6,6))
    np.seterr(divide='ignore', invalid = 'ignore')
    eff = np.divide(zhist2dcut, zhist2d)
    eff[np.isnan(eff)]=0

    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.1)
    im = ax.imshow(eff.T, cmap=plt.cm.jet, extent=[0,10, 0, 10], origin = "lower")
    fig.colorbar(im, cax=cax, orientation='vertical')
    rect = patches.Rectangle((xcrystinf[i], ycrystinf[i]), crystlenght, crystlenght, linewidth=4, edgecolor='black', facecolor='none', label = f'Fiducial area, {i} ')
    ax.add_patch(rect)
    ax.set_xlim([0,10])
    ax.set_ylim([0,10])
    ax.set_xlabel('x [cm]')
    ax.set_ylabel('y [cm]')
    ax.legend(fontsize= 15)
    plt.tight_layout()
    
    cutpos[nrun] = ((xcryst[0]>xcrystinf[i]) & (xcryst[0]<(xcrystinf[i]+crystlenght) )& (ycryst[0]>ycrystinf[i]) &( ycryst[0]<(ycrystinf[i]+crystlenght)))
    OREO_PHtot[nrun] = ((((data[nrun][1][:,chLabel['George1']]+data[nrun][1][:,chLabel['George2']])-8.76)/2066.90)+
                        (((data[nrun][1][:,chLabel['John1']]+data[nrun][1][:,chLabel['John2']])-13.67)/1532.20)+
                        (((data[nrun][1][:,chLabel['Paul1']]+data[nrun][1][:,chLabel['Paul2']])-13.99)/1883.81))
    LGphGeV[nrun] = (((LGph[nrun])-LG_calib[2])/LG_calib[0])
    
    cuttot[nrun] = cuttime[nrun] & cutdiv[nrun][0] & cutph[nrun] & cutpos[nrun]
    
    if i%2==0: 
           
        meanOREO_random.append(np.mean(OREO_PHtot[nrun][cuttot[nrun]])) ############ questa è la media di oreo calcolata con somma dei tre cristalli calibrati singolarmente
        errmeanOREO_random.append(np.std(OREO_PHtot[nrun][cuttot[nrun]]) / np.sqrt(len(OREO_PHtot[nrun][cuttot[nrun]])))
        
        meanLG_random.append(np.mean(LGphGeV[nrun][cuttot[nrun]]))
        errmeanLG_random.append(np.std(LGphGeV[nrun][cuttot[nrun]]) / np.sqrt(len(LGphGeV[nrun][cuttot[nrun]])))
        
        meanOREO_plusLG_random.append(np.mean(OREO_PHtot[nrun][cuttot[nrun]]+ LGphGeV[nrun][cuttot[nrun]]))
        
        #mean_missingEnergy_random.append(120- meanOREO_plusLG_random)
        fig, ax = plt.subplots(1, 1, figsize=(10, 6))
        ax.hist(OREO_PHtot[nrun][cuttot[nrun]], 
                    bins = 100, histtype ='step', color = 'navy',
                    alpha = 1, lw = 3, label = f'Random, energy = {Energies[i]}',  range = [0,20]) 
        ax.legend(fontsize = 15)

        ax.grid()
        ax.set_ylabel('Counts')
        ax.set_xlabel('OREO energy deposited')
        
        fig, ax = plt.subplots(1, 1, figsize=(10, 6))
        ax.hist(LGphGeV[nrun][cuttot[nrun]], 
                    bins = 100, histtype ='step', color = 'teal',
                    alpha = 1, lw = 3, label = f'Random, energy = {Energies[i]}') 
        ax.legend(fontsize = 15)

        ax.grid()
        ax.set_ylabel('Counts')
        ax.set_xlabel('LG energy deposited [GeV]')
        
        
        
        
    else : 
           
        meanOREO_axial.append(np.mean(OREO_PHtot[nrun][cuttot[nrun]])) ############ questa è la media di oreo calcolata con somma dei tre cristalli calibrati singolarmente
        errmeanOREO_axial.append(np.std(OREO_PHtot[nrun][cuttot[nrun]]) / np.sqrt(len(OREO_PHtot[nrun][cuttot[nrun]])))
        
        meanLG_axial.append(np.mean(LGphGeV[nrun][cuttot[nrun]]))
        errmeanLG_axial.append(np.std(LGphGeV[nrun][cuttot[nrun]]) / np.sqrt(len(LGphGeV[nrun][cuttot[nrun]])))
        
        meanOREO_plusLG_axial.append(np.mean(OREO_PHtot[nrun][cuttot[nrun]]+ LGphGeV[nrun][cuttot[nrun]]))
        #mean_missingEnergy_axial.append(120- meanOREO_plusLG_axial)
        fig, ax = plt.subplots(1, 1, figsize=(10, 6))
        ax.hist(OREO_PHtot[nrun][cuttot[nrun]], 
                    bins = 100, histtype ='step', color = 'hotpink',
                    alpha = 1, lw = 3, label = f'Axial, energy = {Energies[i]}', range = [0,20]) 
        ax.legend(fontsize = 15)

        ax.grid()
        ax.set_ylabel('Counts')
        ax.set_xlabel('OREO energy deposited [GeV]')
        
        fig, ax = plt.subplots(1, 1, figsize=(10, 6))
        ax.hist(LGphGeV[nrun][cuttot[nrun]], 
                    bins = 100, histtype ='step', color = 'darkred',
                    alpha = 1, lw = 3, label = f'Axial, energy = {Energies[i]}') 
        ax.legend(fontsize = 15)

        ax.grid()
        ax.set_ylabel('Counts')
        ax.set_xlabel('LG energy deposited [GeV]')
 
        
#%%
for k, ene in enumerate(energy): 
    mean_missingEnergy_random.append(ene- meanOREO_plusLG_random[k])
    mean_missingEnergy_axial.append(ene- meanOREO_plusLG_axial[k])
    ratioOREO.append(meanOREO_axial[k]/meanOREO_random[k])
#   PLot deposito in oreo vs energia + ratio     
# fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(nrows=5, sharex=True, figsize =(13, 13)) 
# ax1.errorbar(energy, meanOREO_random, yerr =errmeanOREO_random, marker = 'd', linestyle = '', markersize = '8', color = 'navy', label = 'Random')
# ax1.errorbar(energy, meanOREO_axial, yerr =errmeanOREO_axial, marker = 'o', linestyle = '', markersize = '8' , color = 'hotpink', label = 'Axial')


# ax1.legend(fontsize = 20)

# ax1.grid()
# ax1.set_ylabel('OREO \n $E_{dep}$ [GeV]')

   


# #    valori medi energia depositata nel LG 

# ax2.errorbar(energy, meanLG_random, yerr =errmeanLG_random, marker = 'd', linestyle = '', markersize = '8', color = 'navy', label = 'Random')
# ax2.errorbar(energy, meanLG_axial, yerr =errmeanLG_axial, marker = 'o', linestyle = '', markersize = '8' , color = 'hotpink', label = 'Axial')


# ax2.grid()
# ax2.set_ylabel('LG \n $E_{dep}$ [GeV]')


#    ## LG+ OREO 
# ax3.errorbar(energy, meanOREO_plusLG_random, yerr =errmeanLG_random, marker = 'd', linestyle = '', markersize = '8', color = 'navy', label = 'Random')
# ax3.errorbar(energy, meanOREO_plusLG_axial, yerr =errmeanLG_axial, marker = 'o', linestyle = '', markersize = '8' , color = 'hotpink', label = 'Axial')


# ax3.grid()
# ax3.set_ylabel('LG + OREO \n $E_{dep}$ [GeV]')




# #   missing energy) 
# ax4.errorbar(energy,mean_missingEnergy_random, yerr =errmeanLG_random, marker = 'd', linestyle = '', markersize = '8', color = 'navy', label = 'Random')
# ax4.errorbar(energy, mean_missingEnergy_axial, yerr =errmeanLG_axial, marker = 'o', linestyle = '', markersize = '8' , color = 'hotpink', label = 'Axial')

# ax4.grid()
# ax4.set_ylabel('Missing energy \n [GeV]')


# ax5.errorbar(energy, ratioOREO, yerr =errmeanOREO_random, marker = '*', linestyle = '', markersize = '8', color = 'teal', label = 'Ratio')
# ax5.grid()
# ax5.set_ylabel('OREO \n Axial/Random')
# ax5.legend(fontsize = 20)
# ax5.set_xlabel('Beam energy [GeV]')
# plt.tight_layout()



###
errmeanOREO_random_T9 =  [0.0025226099787370782, 0.016192009218244954, 0.02457419805229565] ## stai usando per tutti lo stesso errore, poi sistema

meanOREO_random_T9 =  [1.3650777805369068, 1.6899117706787563, 2.051672643660368]
meanOREO_axial_T9 =  [1.5545048011616074, 2.0887689765031037, 2.76808314635978]

meanLG_random_T9 = [2.9624236699793833, 6.300952507513608, 10.256292384730662]
meanLG_axial_T9 =  [2.5208371611079365, 5.512757406153498, 8.756640190268989]

meanOREO_plusLG_random_T9 =  [4.327501450516291, 7.990864278192364, 12.30796502839103]
meanOREO_plusLG_axial_T9 = [4.075341962269544, 7.601526382656602, 11.52472333662877]

mean_missingEnergy_random_T9 = [1.6724985494837092, 2.0091357218076364, 2.6920349716089707]
mean_missingEnergy_axial_T9 = [1.924658037730456, 2.398473617343398, 3.475276663371231]
ratioOREO_T9 = [1.1387664668823456, 1.23602250291691, 1.349183630689383]


#%%  PLot deposito in oreo vs energia + ratio   
plt.rcParams['font.size'] = '14'
fig, (ax1, ax2, ax3, ax4, ax5) = plt.subplots(nrows=5, sharex=True, figsize =(10, 13)) 
ax1.errorbar(energy +[6, 10, 15], meanOREO_random + meanOREO_random_T9 , yerr =errmeanOREO_random +errmeanOREO_random_T9 , marker = 's', linestyle = '', markersize = '8', color = 'navy', label = 'Random')
ax1.errorbar(energy +[6, 10, 15], meanOREO_axial + meanOREO_axial_T9 , yerr = errmeanOREO_axial+ errmeanOREO_random_T9 , marker = 'H', linestyle = '', markersize = '8' , color = 'hotpink', label = 'Axial')


ax1.legend(fontsize = 20)

ax1.grid()
ax1.set_ylabel('OREO \n $E_{dep}$ [GeV]', fontsize = 20)

   
ax2.errorbar(energy +[6, 10, 15], ratioOREO+ratioOREO_T9 , yerr =errmeanOREO_random+ errmeanOREO_random_T9, marker = '*', linestyle = '', markersize = '10', color = 'teal', label = 'Ratio')
ax2.grid()
ax2.set_ylabel('OREO \n Axial/Random', fontsize = 20)
ax2.legend(fontsize = 20)

#    valori medi energia depositata nel LG 

ax3.errorbar(energy +[6, 10, 15], meanLG_random + meanLG_random_T9, yerr = errmeanLG_random+ errmeanOREO_random_T9 , marker = 's', linestyle = '', markersize = '8', color = 'navy', label = 'Random')
ax3.errorbar(energy +[6, 10, 15], meanLG_axial+meanLG_axial_T9, yerr =errmeanLG_axial+errmeanOREO_random_T9 , marker = 'H', linestyle = '', markersize = '8' , color = 'hotpink', label = 'Axial')


ax3.grid()
ax3.set_ylabel('LG \n $E_{dep}$ [GeV]', fontsize = 20)
ax3.legend(fontsize = 20)

    ## LG+ OREO 
ax4.errorbar(energy +[6, 10, 15], meanOREO_plusLG_random+meanOREO_plusLG_random_T9, yerr =errmeanLG_random+ errmeanOREO_random_T9 , marker = 's', linestyle = '', markersize = '8', color = 'navy', label = 'Random')
ax4.errorbar(energy +[6, 10, 15], meanOREO_plusLG_axial+meanOREO_plusLG_axial_T9, yerr =errmeanLG_axial+ errmeanOREO_random_T9 , marker = 'H', linestyle = '', markersize = '8' , color = 'hotpink', label = 'Axial')


ax4.grid()
ax4.set_ylabel('LG + OREO \n $E_{dep}$ [GeV]', fontsize = 20)

ax4.legend(fontsize = 20)


#   missing energy) 
ax5.errorbar(energy +[6, 10, 15] ,mean_missingEnergy_random+mean_missingEnergy_random_T9, yerr =errmeanLG_random+ errmeanOREO_random_T9, marker = 's', linestyle = '', markersize = '8', color = 'navy', label = 'Random')
ax5.errorbar(energy +[6, 10, 15], mean_missingEnergy_axial+mean_missingEnergy_axial_T9, yerr =errmeanLG_axial+ errmeanOREO_random_T9, marker = 'H', linestyle = '', markersize = '8' , color = 'hotpink', label = 'Axial')

ax5.grid()
ax5.set_ylabel('Missing energy \n [GeV]', fontsize = 20)
ax5.legend(fontsize = 20)


#ax5.set_xlabel('Beam energy [GeV]', fontsize = 20)

plt.tight_layout()
# plt.text(125,157,'(a)', fontsize = 30)
# plt.text(125,120,'(b)', fontsize = 30)
# plt.text(125,83,'(c)', fontsize = 30)
# plt.text(125,45,'(d)', fontsize = 30)
# plt.text(125,1.2,'(e)', fontsize = 30)
##plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/H2+T9energydepositedVSbeamenergy.pdf')

tot_missing_random = mean_missingEnergy_random+mean_missingEnergy_random_T9
tot_missing_axial= mean_missingEnergy_axial+mean_missingEnergy_axial_T9
totE = energy +[6, 10, 15]
missingfraction_random = []
missingfraction_axial = []
for k in range(len(tot_missing_random)): 
    missingfraction_random.append((tot_missing_random[k]/totE[k])*100)
    missingfraction_axial.append((tot_missing_axial[k]/totE[k])*100)
    
fig, ax = plt.subplots(1, 1, figsize=(10, 6))
ax.plot(energy +[6, 10, 15],missingfraction_random, 's', markersize = '8', color = 'navy', label = 'Random')
ax.plot(energy +[6, 10, 15],missingfraction_axial, 'H', markersize = '8', color = 'hotpink', label = 'Axial')
ax.set_xlabel('Beam energy [GeV]')
ax.set_ylabel('%')
ax.grid()
#

##################################################################
##################################################################
#######################   Risoluzione   ###########################
###################################################################
###################################################################
#%% vediamo se la risoluzione energetica del calo cambia 
model = GaussianModel()
muE = []
muEerr = []
sigmaE = []
sigmaEerr = []
fitrangecalibinf = [10, 10,  20, 20, 40, 40, 60, 60, 80, 70, 120, 80]
fitrangecalibsup = [60, 60, 80,  80, 100, 100, 120, 120,  140, 140, 180, 180]

for i, nrun in enumerate(runnumbers):
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))
    hph, binsph, _ = ax.hist(OREO_PHtot[nrun][cuttot[nrun]]+LGphGeV[nrun][cuttot[nrun]], 
                     bins = 100, histtype ='step', color = 'hotpink',
                     alpha = 1, lw = 3, label = f'energy = {Energies[i]}', range = [0,200]) 
    xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
    phcondition = ((xdata > fitrangecalibinf[i]) & (xdata < fitrangecalibsup[i]))
    paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
    resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                            weights=np.sqrt(hph[phcondition]))
    to_plot = np.linspace(fitrangecalibinf[i], fitrangecalibsup[i], 10000)
    y_eval = model.eval(resultx.params, x=to_plot)
    #ax[i].plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = 'black')
    muE.append(resultx.params['center'].value)
    muEerr.append(resultx.params['center'].stderr)
    sigmaE.append(resultx.params['sigma'].value)
    sigmaEerr.append(resultx.params['sigma'].stderr)
    ax.plot(to_plot, y_eval, linewidth=2, color='black', label=f'$\mu$ = {round(resultx.params["center"].value,2)} $\pm$ {round(resultx.params["sigma"].value,2)}') 
    ax.legend(fontsize = 15)

    ax.grid()
    ax.set_ylabel('Counts')
    ax.set_xlabel('OREO + LG energy deposited [GeV]')
####  

#%%      
energy_resolutionRandom = []
energy_resolutionerrRandom = []

energy_resolutionAxial = []
energy_resolutionerrAxial = []
j = 0
f = 0
for k in range(len(muE)): 
    print(k)
    if k%2 == 0: 
        energy_resolutionRandom.append((sigmaE[k]/muE[k])*100)
        energy_resolutionerrRandom.append((np.sqrt((sigmaEerr[k]/sigmaE[k])**2+(muEerr[k]/muE[k])**2)*energy_resolutionRandom[j]))
        j = j+1
    else: 
         energy_resolutionAxial.append((sigmaE[k]/muE[k])*100)
         energy_resolutionerrAxial.append((np.sqrt((sigmaEerr[k]/sigmaE[k])**2+(muEerr[k]/muE[k])**2)*energy_resolutionAxial[f]))
         f = f+1

def energy_resolution_function(E, a, b, c):
    return np.sqrt( (a**2 / E) + (b / E)**2+ c**2)

from lmfit import Model
fig, ax = plt.subplots(1,1, figsize=(8,6))
model = Model(energy_resolution_function)
params = model.make_params(a=10, b=10, c=5)
result = model.fit(energy_resolutionRandom, params, E=energy, weights=energy_resolutionerrRandom)
a_fit = result.params['a'].value
b_fit = result.params['b'].value
c_fit = result.params['c'].value

sigma_a = result.params['a'].stderr
sigma_b = result.params['b'].stderr
sigma_c = result.params['c'].stderr
to_plot = np.linspace(min(energy), max(energy), 1000)
fit_energy_resolution = energy_resolution_function(to_plot, a_fit, b_fit, c_fit)

ax.errorbar(energy, energy_resolutionRandom,yerr=energy_resolutionerrRandom, marker = 'D', markersize = 10
            , color = 'navy',
            linestyle= 'none', label = '$\sigma$(E)/E')


ax.plot(to_plot, fit_energy_resolution, label = '$\sigma$(E)/E = $\sqrt{(a^{2} / E) + (b / E)^2+ c^2)}$' , color = 'red')
    
ax.set_xlabel('GeV')
ax.set_ylabel('Energy resolution [%]')
ax.legend(fontsize = 18)
ax.grid()
plt.tight_layout()
#plt.savefig(r'C:\Users\aselmi\Desktop\Dottorato\Technical Note Oreo2023\t9\T9_LGenergyResolution.pdf')
plt.show()


print(f' random a = {a_fit}, b = {b_fit}, c = {c_fit}  ')
print(f' random a = {sigma_a}, b = {sigma_b}, c = {sigma_c}  ')  

##

fig, ax = plt.subplots(1,1, figsize=(8,6))
model = Model(energy_resolution_function)
params = model.make_params(a=10, b=10, c=5)
result = model.fit(energy_resolutionAxial, params, E=energy, weights=energy_resolutionerrAxial)
a_fitAsse = result.params['a'].value
b_fitAsse = result.params['b'].value
c_fitAsse = result.params['c'].value

sigma_aAsse = result.params['a'].stderr
sigma_bAsse = result.params['b'].stderr
sigma_cAsse = result.params['c'].stderr
to_plot = np.linspace(min(energy), max(energy), 1000)
fit_energy_resolution = energy_resolution_function(to_plot, a_fit, b_fit, c_fit)

ax.errorbar(energy, energy_resolutionAxial,yerr=energy_resolutionerrAxial, marker = 'D', markersize = 10
            , color = 'navy',
            linestyle= 'none', label = '$\sigma$(E)/E')


ax.plot(to_plot, fit_energy_resolution, label = '$\sigma$(E)/E = $\sqrt{(a^{2} / E) + (b / E)^2+ c^2)}$' , color = 'red')
    
ax.set_xlabel('GeV')
ax.set_ylabel('Energy resolution [%]')
ax.legend(fontsize = 18)
ax.grid()
plt.tight_layout()
#plt.savefig(r'C:\Users\aselmi\Desktop\Dottorato\Technical Note Oreo2023\t9\T9_LGenergyResolution.pdf')
plt.show()


print(f' Axial a = {a_fitAsse}, b = {b_fitAsse}, c = {c_fitAsse}  ')
print(f' Axial a = {sigma_aAsse}, b = {sigma_bAsse}, c = {sigma_cAsse}  ')  
# %%
