#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 13 10:17:12 2023

@author: ale
"""

# Prova a fare la cosa dell'efficienza con MIP. poi devi equalizzare la risposta dei SiPM e fari la stessa cosa con gli ettroni


from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable

plt.rcParams['font.size'] = '15'
#triggere APC + Cherry
# Ch 0 -->  XCET 48 (Cherry 2)
# Ch 1 --> LG1 (Door / T10 side)
# Ch 2 --> George 1
# Ch 3 --> George 2
# Ch 4 --> John 1
# Ch 5 --> John 2
# Ch 6 --> Paul 1
# Ch 7 --> Paul 2

datapath = '/home/ale/Desktop/Dottorato/dati_remoti/h2dataOREO_0823/HDF5/'
h5keys = [ 'xpos','digiPH', 'digiTime']

chLabel = {'Cherry':0, 'LG':1, 'George1':2, 'George2':3, 'John1':4, 'John2':5, 'Paul1':6, 'Paul2':7}
SiPMlabel = ['George1', 'John1','Paul1','George2', 'John2', 'Paul2']
runnumber = 710043

distancesfromT1 = [500]
zcryst = 500+25
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]

### varie threshold per i tagli 
thcherry = 0
divsup = 0.0015
divinf = -0.0015
timethinf = 200
timethsup = 250
#%%
#apertura dati + profilo camere 
#############################################
data = opendata(runnumber, datapath, h5keys)
xpos = data[0]
#camere 
xlabels = ['BC1',' BC1', 'BC2', 'BC2']
xlim = [[-0,10]]*4
histpos1d(xpos, 2, xlabels = xlabels, xlim = xlim, Range = [0,10])
histpos2d(xpos, numberoftrackers = 2)
#correlazioni camere 
fig, ax = plt.subplots(1,2 , figsize=(8,8))
ax = ax.flat
ax[0].hist2d(xpos[:,0], xpos[:,2], bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet)
ax[0].set_ylabel('x BC2 [cm]')
ax[0].set_xlabel('x BC1 [cm]')

ax[1].hist2d(xpos[:,1],xpos[:,3] , bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet,)
ax[1].set_xlabel('y BC1 [cm]')
ax[1].set_ylabel('x BC2 [cm]')
plt.tight_layout()
plt.show()
#%%
#divergenze


divex, divey, sigma, errsigma, mu, _ = divgaussianfit(xpos, 2, distancesfromT1,
                                                       p0divx, p0divy,
                                                       Rangex=[[-0.02,0.02]],
                                                       Rangey=[[-0.02,0.02]])


datashift, shift = aligntracker(xpos, 2, mu, distancesfromT1)
divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                               distancesfromT1,
                                                                                p0divx, p0divy,
                                                                                Rangex=[[-0.004,0.004]],
                                                                                Rangey=[[-0.004,0.004]], 
                                                                                Color='navy', 
                                                                                Figsize = (8,6))
cutdiv = ((divexalign >divinf) & (divexalign<divsup) & (diveyalign>divinf) &(diveyalign<divsup))
ax[0].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
ax[0].axvline(x=divsup, color="black", linestyle = '--', lw = 2)
ax[1].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
ax[1].axvline(x=divsup, color="black", linestyle = '--', lw = 2)   

#%%
# profilo cherenkov
cutcherry = (data[1][:,chLabel['Cherry']]>thcherry)
fig, ax = plt.subplots(1, 1, figsize=(10, 6))

ax.hist((data[1][:,chLabel['Cherry']]),
             bins = 100, histtype ='stepfilled', color = 'turquoise', edgecolor = 'turquoise', 
             alpha = 0.5, lw = 3 ,label = 'Cherry', range = [0, 6000])
ax.axvline(x=thcherry, color="black", linestyle = '--', label = 'cut', lw = 2)
ax.grid()
ax.set_xlabel('Cherenkov PH [a.u]')
ax.set_ylabel('Entries [ns]')
ax.set_yscale('log')
fig.set_tight_layout('tight')

plt.show()
#%%
# plot PH e tempo + tagli su tempi e PH dei tre cristalli 
fig, ax = plt.subplots(2, 3, figsize=(8, 8))
ax = ax.flat
for i in range(6):
    ax[i].hist2d(data[1][:,chLabel[SiPMlabel[i]]],data[2][:,chLabel[SiPMlabel[i]]] ,
                 bins =[100,100], cmap = plt.cm.jet, range = [[0, 8000], [175, 275]], norm=colors.LogNorm())
    ax[i].axhline(y=timethinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[i].axhline(y=timethsup, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[i].set_ylabel('Entries')
    ax[i].set_xlabel('PH [a.u]')
    ax[i].set_title(f'{SiPMlabel[i]}')
    ax[i].grid()
plt.tight_layout()
plt.show()
cuttime = ((data[2][:,chLabel['George1']]>timethinf) & (data[2][:,chLabel['George1']]<timethsup)|
            #(data[2][:,chLabel['George2']]>timethinf) & (data[2][:,chLabel['George2']]<timethsup)|
             (data[2][:,chLabel['John1']]>timethinf) & (data[2][:,chLabel['John1']]<timethsup)|
             #(data[2][:,chLabel['John2']]>timethinf) & (data[2][:,chLabel['John2']]<timethsup)|
             (data[2][:,chLabel['Paul1']]>timethinf) & (data[2][:,chLabel['Paul1']]<timethsup))
             #(data[2][:,chLabel['Paul2']]>timethinf) & (data[2][:,chLabel['Paul2']]<timethsup))
             
# I cristalli li vedi andando a fare i tagli sulla PH dei vari SiPM, se chiedi solo il taglio su George vedi lui 
cutph = (
    (data[1][:,chLabel['George1']]>100)|
              #(data[1][:,chLabel['George2']]>200) |
               (data[1][:,chLabel['John1']]>100) |
               #(data[1][:,chLabel['John2']]>100) |
              (data[1][:,chLabel['Paul1']]>100) )
              #(data[1][:,chLabel['Paul2']]>200))


#%%

cuttot = cutdiv[0] & cuttime & cutph

xcryst = xpos[:,0] + zcryst*np.tan(divexalign)
ycryst = xpos[:,1] + zcryst*np.tan(diveyalign)
fig, ax = plt.subplots(1,1, figsize=(8,8))
zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xcryst[0], ycryst[0], cmap=plt.cm.jet, bins=[60, 60], range=[[0,10], [0,10]])
plt.colorbar(mapable, ax=ax)
ax.set_xlabel('x [cm]')
ax.set_xlabel('y [cm]')
ax.set_title('Proiezione su piano del cristallo')
plt.tight_layout()
plt.show()


fig, ax = plt.subplots(1,1, figsize=(8,8))
zhist2dcut, xhist2dcut , yhist2dcut , mapable = ax.hist2d(xcryst[0][cuttot], ycryst[0][cuttot], 
                                                              cmap=plt.cm.jet, bins = [60,60],
                                                              range = [[0,10], [0,10]])


plt.colorbar(mapable, ax=ax)

ax.set_ylabel('x [cm]')
ax.set_xlabel('y [cm]')

plt.tight_layout()
plt.show()



#%%
fig, ax = plt.subplots(1,1, figsize=(8,8))
np.seterr(divide='ignore', invalid = 'ignore')
eff = np.divide(zhist2dcut, zhist2d)
eff[np.isnan(eff)]=0

divider = make_axes_locatable(ax)
cax = divider.append_axes('right', size='5%', pad=0.1)
im = ax.imshow((np.transpose((eff))), cmap=plt.cm.jet, extent=[0,10, 0, 10])
fig.colorbar(im, cax=cax, orientation='vertical')
#rect = patches.Rectangle((3.2, 1.4), 3.1, 3.1, linewidth=2, edgecolor='darkred', facecolor='none')
#ax1[i].add_patch(rect)
ax.set_xlim([0,10])
ax.set_ylim([0,10])
ax.set_xlabel('x [cm]')
ax.set_ylabel('y [cm]')
#%%



fig, ax = plt.subplots(1, 3, figsize=(10, 6))
ax = ax.flat

ax[0].hist((data[1][:,chLabel['George1']][cuttot]),
             bins = 100, histtype ='stepfilled', color = 'lime', edgecolor = 'lime', 
             alpha = 0.2, lw = 3 ,label = 'George ch1',density = True, range = [0, 10000])

ax[0].hist(data[1][:,chLabel['George2']][cuttot],
              bins = 100, histtype ='stepfilled', color = 'turquoise', edgecolor = 'turquoise', 
              alpha = 0.2, lw = 3, label = 'George ch2',density = True, range = [0, 10000]) 
ax[1].hist(data[1][:,chLabel['John1']][cuttot],
              bins = 100,  histtype ='stepfilled', color = 'hotpink', edgecolor = 'hotpink', 
              alpha = 0.2, lw = 3, label = 'John ch1', density = True, range = [0, 10000])
ax[1].hist(data[1][:,chLabel['John2']][cuttot],
              bins = 100, histtype ='stepfilled', color = 'indigo', edgecolor = 'indigo', 
              alpha = 0.2, lw = 3, label = 'John ch2', density = True, range = [0, 10000])
ax[2].hist(data[1][:,chLabel['Paul1']][cuttot],
              bins = 100,  histtype ='stepfilled', color = 'darkred', edgecolor = 'darkred', 
              alpha = 0.2, lw = 3, label = 'Paul ch1', density = True, range = [0, 10000])
ax[2].hist(data[1][:,chLabel['Paul2']][cuttot],
              bins = 100, histtype ='stepfilled', color = 'orangered', edgecolor = 'orangered', 
              alpha = 0.2, lw = 3, label = 'Paul ch2', density = True, range = [0, 10000])

for i in range(3): 
    ax[i].set_ylabel('Entries')
    ax[i].set_xlabel('PH [a.u]')
    ax[i].grid()
    ax[i].legend(fontsize = 10)
    ax[i].set_yscale('log')
plt.tight_layout()
plt.show()

#%%




