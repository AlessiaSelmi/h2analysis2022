#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 13 10:17:12 2023

@author: ale
"""

# Prova a fare la cosa dell'efficienza con MIP. poi devi equalizzare la risposta dei SiPM e fari la stessa cosa con gli ettroni


from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches

plt.rcParams['font.size'] = '20'
#triggere APC + Cherry
# Ch 0 -->  XCET 48 (Cherry 2)
# Ch 1 --> LG1 (Door / T10 side)
# Ch 2 --> George 1
# Ch 3 --> George 2
# Ch 4 --> John 1
# Ch 5 --> John 2
# Ch 6 --> Paul 1
# Ch 7 --> Paul 2

datapath = r'C:\Users\aselmi\Desktop\Dottorato\data\h2dataOREO_0823/HDF5/run7100'
h5keys = [ 'xpos','digiPH', 'digiTime']

chLabel = {'LG05':0, 'LG2':1, 'George1':2, 'George2':3, 'John1':4, 'John2':5, 'Paul1':6, 'Paul2':7}
SiPMlabel = ['George1','George2', 'John1', 'John2','Paul1', 'Paul2']
allSiPMLabel = ['George','John','Paul']
#runnumbers = [94, 89] #Amorfo e Axial --> George 
#runnumbers = [93, 90] #Amorfo e Axial --> John
runnumbers = [92, 91] #Amorfo e Axial --> Paul

distancesfromT1 = [1548]
zcryst = 1548+60
zLG = zcryst+30
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]

### varie threshold per i tagli 
thcherry = 500
divsup = 0.00015
divinf = -0.00015
timethinf = [230]*6
timethsup = [280]*6
phth = [2000]*6
## dizionari
## dizionari
data = {}
cutdiv = {}
cutcherry = {}
cuttime = {}
cutph = {}
PH_toteq = {}
OREO_PHtot = {}
cuttot = {}
cutpos = {}
crystlenght = 1.5# per il cut fiduciale. La vera lunghezza è 2.5
LGph = {}
xcrystinf = 5
xcrystsup = xcrystinf+crystlenght
ycrystinf= 3
ycrystsup = ycrystinf+crystlenght
SiPMphGeV = {}
LGphGeV = {}
meanSiPM = {}
meanLG = {}
meanOREO = {}
errmeanOREO = {}
meanOREO_plusLG = {}
meanOREO_plusLG_correction = {}
meanLG_correction = {}
LGphGeV_correction = {}
mean120_OREO = {}
errmeanPH_SiPM = {}

ratioSiPM = {}
errratio_meanSiPM = {}

#%%
SiPM_calib = np.loadtxt('H2_AllSiPM_Calibration_OREO0823.dat')
SiPM_eqfact = np.loadtxt('H2_AllSiPM_eqfactor.dat')
SiPM_intercept1 = np.loadtxt('H2_AllSiPM_intercept1.dat')
LG_calib = np.loadtxt('H2_LG_CalibrationOREO23_2V.dat')
LG_calib_correction = np.loadtxt('H2_LG_CalibrationOREO23_2Vcorrection.dat')
#%%
#############################################
for k, name in enumerate(allSiPMLabel):
    ## cambi run per vedere interallineamento, quando sei centrata su john fai ancha la somma con quelli a lato
    if name == 'George': 
        runnumbers = [94, 89] #Amorfo e Axial --> George 
    if name == 'John': 
        runnumbers = [93, 90] #Amorfo e Axial --> John
    if name == 'Paul': 
        runnumbers = [92, 91] #Amorfo e Axial --> Paul
    #carico dati + plot posizioni camere
    for i, nrun in enumerate(runnumbers):
        print(i)
        data[nrun] = opendata(runnumbers[i], datapath, h5keys)
        LGph[nrun] = data[nrun][1][:,chLabel['LG2']]
        xpos = data[nrun][0]
        #camere 
        xlabels = ['BC1',' BC1', 'BC2', 'BC2']
        xlim = [[-0,10]]*4
        histpos1d(xpos, 2, xlabels = xlabels, xlim = xlim, Range = [0,10])
        histpos2d(xpos, numberoftrackers = 2)
        #correlazioni camere 
        fig, ax = plt.subplots(1,2 , figsize=(8,8))
        ax = ax.flat
        ax[0].hist2d(xpos[:,0], xpos[:,2], bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet)
        ax[0].set_ylabel('x BC2 [cm]')
        ax[0].set_xlabel('x BC1 [cm]')
        
        ax[1].hist2d(xpos[:,1],xpos[:,3] , bins = [100,100], range = [[0,10], [0,10]], cmap=plt.cm.jet,)
        ax[1].set_xlabel('y BC1 [cm]')
        ax[1].set_ylabel('x BC2 [cm]')
        plt.tight_layout()
        plt.show()
    #%%
        #divergenze
        divex, divey, sigma, errsigma, mu, _ = divgaussianfit(xpos, 2, distancesfromT1,
                                                               p0divx, p0divy,
                                                               Rangex=[[-0.002,0.002]],
                                                               Rangey=[[-0.002,0.002]])
        
        
        datashift, shift = aligntracker(xpos, 2, mu, distancesfromT1)
        divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                                       distancesfromT1,
                                                                                        p0divx, p0divy,
                                                                                        Rangex=[[-0.0004,0.0004]],
                                                                                        Rangey=[[-0.0004,0.0004]], 
                                                                                        Color='navy', 
                                                                                        Figsize = (8,6))
        cutdiv[nrun] = ((divexalign >divinf) & (divexalign<divsup) & (diveyalign>divinf) &(diveyalign<divsup))
        ax[0].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[0].axvline(x=divsup, color="black", linestyle = '--', lw = 2)
        ax[1].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
        ax[1].axvline(x=divsup, color="black", linestyle = '--', lw = 2)   
    
    # #%%
    
    
    #%%
        # plot PH e tempo + tagli su tempi e PH dei tre cristalli 
        fig, ax = plt.subplots(2, 3, figsize=(10, 6))
        ax = ax.flat
        for j in range(len(SiPMlabel)):
            ax[j].hist2d(data[nrun][1][:,chLabel[SiPMlabel[j]]],data[nrun][2][:,chLabel[SiPMlabel[j]]] ,
                         bins =[100,100], cmap = plt.cm.jet, range = [[0, 20000], [150, 300]], norm=colors.LogNorm())
            ax[j].axhline(y=timethinf[j], color="black", linestyle = '--', label = 'cut', lw = 2)
            ax[j].axhline(y=timethsup[j], color="black", linestyle = '--', label = 'cut', lw = 2)
            ax[j].axvline(x=phth[j], color="black", linestyle = '--', label = 'cut', lw = 2)
            ax[j].set_ylabel('Counts')
            ax[j].set_xlabel('PH [a.u]')
            ax[j].set_title(f'{SiPMlabel[j]}')
            ax[j].grid()
        plt.tight_layout()
        plt.show()
        #%% faccimao un po' di tagli  su tempo e PH 
        ## solo una matrice
        # cuttime[nrun] =(((data[nrun][2][:,chLabel['George1']]>timethinf[0]) & (data[nrun][2][:,chLabel['George1']]<timethsup[0]))|
        #              ((data[nrun][2][:,chLabel['John1']]>timethinf[1]) & (data[nrun][2][:,chLabel['John1']]<timethsup[1]))|
        #              ((data[nrun][2][:,chLabel['Paul1']]>timethinf[2]) & (data[nrun][2][:,chLabel['Paul1']]<timethsup[2])))
        # # I cristalli li vedi andando a fare i tagli sulla PH dei vari SiPM, se chiedi solo il taglio su George vedi lui 
        # cutph[nrun] = ((data[nrun][1][:,chLabel['George1']]>phth[0])|
        #          (data[nrun][1][:,chLabel['John1']]>phth[1]) |
        #          (data[nrun][1][:,chLabel['Paul1']]>phth[2]) )
        
        # cuttot[nrun] = cutdiv[nrun][0] & cuttime[nrun] &cutph[nrun]
        #tutte e due le matrici
        cuttime[nrun] = (((data[nrun][2][:,chLabel['George1']]>timethinf[0]) & (data[nrun][2][:,chLabel['George1']]<timethsup[0])&(data[nrun][2][:,chLabel['George2']]>timethinf[0]) & (data[nrun][2][:,chLabel['George2']]<timethsup[0]))|
                     ((data[nrun][2][:,chLabel['John1']]>timethinf[1]) & (data[nrun][2][:,chLabel['John1']]<timethsup[1])&(data[nrun][2][:,chLabel['John2']]>timethinf[1]) & (data[nrun][2][:,chLabel['John2']]<timethsup[1]))|
                     ((data[nrun][2][:,chLabel['Paul1']]>timethinf[2]) & (data[nrun][2][:,chLabel['Paul1']]<timethsup[2])& (data[nrun][2][:,chLabel['Paul2']]>timethinf[2]) & (data[nrun][2][:,chLabel['Paul2']]<timethsup[2])))
        # I cristalli li vedi andando a fare i tagli sulla PH dei vari SiPM, se chiedi solo il taglio su George vedi lui 
        cutph[nrun] = ((data[nrun][1][:,chLabel['George1']]>phth[0])|(data[nrun][1][:,chLabel['George2']]>phth[0])|
                 (data[nrun][1][:,chLabel['John1']]>phth[1]) | (data[nrun][1][:,chLabel['John2']]>phth[0])|
                 (data[nrun][1][:,chLabel['Paul1']]>phth[2])|(data[nrun][1][:,chLabel['Paul2']]>phth[0]))
        cuttot[nrun] = cuttime[nrun] & cutdiv[nrun][0] & cutph[nrun]
                
    #%% efficienza cristallo 
        xcryst = xpos[:,0] + zcryst*np.tan(divexalign)
        ycryst = xpos[:,1] + zcryst*np.tan(diveyalign)
        fig, ax = plt.subplots(1,1, figsize=(6,6))
        zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xcryst[0], ycryst[0], cmap=plt.cm.jet, bins=[80, 80], range=[[0,10], [0,10]])
        plt.colorbar(mapable, ax=ax)
        ax.set_xlabel('x [cm]')
        ax.set_xlabel('y [cm]')
        ax.set_title('Proiezione su piano del cristallo')
        plt.tight_layout()
        plt.show()
    
       
        fig, ax = plt.subplots(1,1, figsize=(6,6))
        zhist2dcut, xhist2dcut , yhist2dcut , mapable = ax.hist2d(xcryst[0][cuttime[nrun] & cutph[nrun]& cutdiv[nrun][0]], ycryst[0][cuttime[nrun] & cutph[nrun]& cutdiv[nrun][0]], 
                                                                      cmap=plt.cm.jet, bins = [80,80],
                                                                      range = [[0,10], [0,10]])
    
    
        plt.colorbar(mapable, ax=ax)
        rect = patches.Rectangle((xcrystinf, ycrystinf), crystlenght*3, crystlenght, linewidth=4, edgecolor='darkred', facecolor='none')
        #ax.add_patch(rect)
        ax.set_ylabel('x [cm]')
        ax.set_xlabel('y [cm]')
    
        plt.tight_layout()
        plt.show()
        cutpos[nrun] = ((xcryst[0]>xcrystinf) & (xcryst[0]<xcrystsup )& (ycryst[0]>ycrystinf) &( ycryst[0]<ycrystsup))
        ## efficienza
        fig, ax = plt.subplots(1,1, figsize=(6,6))
        np.seterr(divide='ignore', invalid = 'ignore')
        eff = np.divide(zhist2dcut, zhist2d)
        eff[np.isnan(eff)]=0
    
        divider = make_axes_locatable(ax)
        cax = divider.append_axes('right', size='5%', pad=0.1)
        im = ax.imshow(eff.T, cmap=plt.cm.jet, extent=[0,10, 0, 10], origin = "lower")
        fig.colorbar(im, cax=cax, orientation='vertical')
        rect = patches.Rectangle((xcrystinf, ycrystinf), crystlenght, crystlenght, linewidth=4, edgecolor='black', facecolor='none', label = 'Fiducial area')
        ax.add_patch(rect)
        ax.set_xlim([0,10])
        ax.set_ylim([0,10])
        ax.set_xlabel('x [cm]')
        ax.set_ylabel('y [cm]')
        ax.legend(fontsize= 15)
        plt.tight_layout()
        if name == 'Paul': 
            #plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/h2/H2_PaulEff_axial.pdf')
            
     
    
    
        
        cuttot[nrun] = cuttime[nrun] & cutpos[nrun]
    #%% facciamo somma sia con i fattori di eq e proviamo invece calibrando direttamente i singoli SiPM e vediamo cosa cambia 
    
        ### per interallineamento, apro la run centrata su ogni canale, poi solo su john  sommo quelli laterali per avere somma di oreo 
        if name == 'George': 
            SiPMphGeV[nrun] = (((data[nrun][1][:,chLabel['George1']]+data[nrun][1][:,chLabel['George2']])-8.76)/2066.90)
        if name == 'John': 
            # #qui calibrisingolarmente i tre cristalli  con i valori trovati in H2_EqSiPMs_OREO_0823 au = mGeV + q
            OREO_PHtot[nrun] = ((((data[nrun][1][:,chLabel['George1']]+data[nrun][1][:,chLabel['George2']])-8.76)/2066.90)+
                            (((data[nrun][1][:,chLabel['John1']]+data[nrun][1][:,chLabel['John2']])-13.67)/1532.20)+
                            (((data[nrun][1][:,chLabel['Paul1']]+data[nrun][1][:,chLabel['Paul2']])-13.99)/1883.81))
            ## solo canale john 
            SiPMphGeV[nrun] = (((data[nrun][1][:,chLabel['John1']]+data[nrun][1][:,chLabel['John2']])-13.67)/1532.20)
            
            meanOREO[nrun] = np.mean(OREO_PHtot[nrun][cuttot[nrun]]) ############ questa è la media di oreo calcolata con somma dei tre cristalli calibrati singolarmente
            errmeanOREO[nrun] =  np.std(OREO_PHtot[nrun][cuttot[nrun]]) / np.sqrt(len(OREO_PHtot[nrun][cuttot[nrun]]))
            ## qui per confronto faccio prima l'equalizzazione e poi dovrò calibrare la somma 
            # PH_toteq[nrun] = ((((data[nrun][1][:,chLabel['George1']]+data[nrun][1][:,chLabel['George2']])-SiPM_intercept1[0])*SiPM_eqfact[0])+
            #                 (((data[nrun][1][:,chLabel['John1']]+data[nrun][1][:,chLabel['John2']])-SiPM_intercept1[1])*SiPM_eqfact[1])+
            #                 (((data[nrun][1][:,chLabel['Paul1']]+data[nrun][1][:,chLabel['Paul2']])-SiPM_intercept1[2])*SiPM_eqfact[2]))
            
            
        if name == 'Paul': 
           SiPMphGeV[nrun] = (((data[nrun][1][:,chLabel['Paul1']]+data[nrun][1][:,chLabel['Paul2']])-13.99)/1883.81)
            
    
        
        LGphGeV[nrun] = (((LGph[nrun])-LG_calib[2])/LG_calib[0])
        # LGphGeV_correction[nrun] = (((LGph[nrun][cuttot[nrun]])-LG_calib_correction[2])/LG_calib_correction[0])
        # ## medie su vettori già tagliati 
        ## media di ogni SiPM sempre per allineamento 
        meanSiPM[nrun] = np.mean(SiPMphGeV[nrun][cuttot[nrun]])
        
        meanLG[nrun] = np.mean(LGphGeV[nrun])
        #meanLG_correction[nrun] = np.mean( LGphGeV_correction[nrun])

        # meanOREO_plusLG_correction[nrun] = np.mean(OREOphGeV[nrun][cuttot[nrun]]+ LGphGeV_correction[nrun][cuttot[nrun]])
        # mean120_OREO[nrun] = np.mean(120-OREOphGeV[nrun][cuttot[nrun]])
        errmeanPH_SiPM[nrun] = np.std(SiPMphGeV[nrun][cuttot[nrun]]) / np.sqrt(len(SiPMphGeV[nrun][cuttot[nrun]]))
        
        
    # serve per vedere l'inter-allineamento, plotta il canale di interesse in asse e amorfo     
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))

    ax.hist(SiPMphGeV[runnumbers[0]], 
                bins = 100, histtype ='step', color = 'navy',
                alpha = 1, lw = 3, label = f'Random, mean = {round(meanSiPM[runnumbers[0]], 2)}', density = True, range = [0.1,20]) 
    ax.hist(SiPMphGeV[runnumbers[1]], 
                bins = 100, histtype ='step', color = 'hotpink',
                alpha = 1, lw = 3, label = f'Axial,mean = {round(meanSiPM[runnumbers[1]], 2)}', density = True, range = [0.1,20]) 

    ax.legend(fontsize = 15)

    ax.grid()
    ax.set_ylabel('Counts')
    ax.set_xlabel(f'{name} energy [GeV]')
     
    ratioSiPM[name] = (meanSiPM[runnumbers[1]] / meanSiPM[runnumbers[0]])
    errratio_meanSiPM[name] = (np.sqrt((errmeanPH_SiPM[runnumbers[0]]/meanSiPM[runnumbers[0]])**2 + (errmeanPH_SiPM[runnumbers[1]]/meanSiPM[runnumbers[1]])**2))
    
    if name == 'John': 
        ratioOREO = (meanOREO[runnumbers[1]] / meanOREO[runnumbers[0]])
        errratio_meanOREO = (np.sqrt((errmeanOREO[runnumbers[0]]/meanOREO[runnumbers[0]])**2 + (errmeanOREO[runnumbers[1]]/meanOREO[runnumbers[1]])**2))
        meanOREO_plusLG[nrun]  = np.mean(OREO_PHtot[nrun][cuttot[nrun]]+ LGphGeV[nrun][cuttot[nrun]])
        fig, ax = plt.subplots(1, 1, figsize=(8, 6))

        ax.hist(OREO_PHtot[runnumbers[0]][cuttot[runnumbers[0]]], 
                    bins = 100, histtype ='step', color = 'navy',
                    alpha = 1, lw = 3, label = f'Random, mean = {round(meanOREO[runnumbers[0]], 3)}, $\pm$ {round(errmeanOREO[runnumbers[0]], 3)}' , density = True, range = [0.1,20]) 
        ax.hist(OREO_PHtot[runnumbers[1]][cuttot[runnumbers[1]]], 
                    bins = 100, histtype ='step', color = 'hotpink',
                    alpha = 1, lw = 3, label = f'Axial, mean = {round(meanOREO[runnumbers[1]], 3)} $\pm$ {round(errmeanOREO[runnumbers[0]], 3)}', density = True, range = [0.1,20]) 

        ax.legend(fontsize = 15)
        ax.set_ylim([0, 0.4])
        ax.grid()
        ax.set_ylabel('Normalized counts')
        ax.set_xlabel('OREO energy deposit [GeV]')
        plt.tight_layout()
        #plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/h2/H2OREO_ratioAxAmo.pdf')
#%%
        print(f'mean {name} = {meanSiPM[nrun]} \pm {errmeanPH_SiPM[nrun]}')
print(f'OREO axial/random = {meanSiPM[runnumbers[1]] / meanSiPM[runnumbers[0]]} $\pm$ {errratio_meanSiPM}')

#%%## questo dovresti farlo sommando tutti e tre i cristalli 
# fig, ax = plt.subplots(1, 1, figsize=(10, 6))

# ax.hist(OREOphGeV[runnumbers[0]], 
#             bins = 100, histtype ='step', color = 'navy',
#             alpha = 1, lw = 3, label = f'Random, ,mean = {round(meanOREO[runnumbers[0]], 2)}', density = True, range = [0.1,20]) 
# ax.hist(OREOphGeV[runnumbers[1]], 
#             bins = 100, histtype ='step', color = 'hotpink',
#             alpha = 1, lw = 3, label = f'Axial,mean = {round(meanOREO[runnumbers[1]], 2)}', density = True, range = [0.1,20]) 

# ax.legend(fontsize = 15)

# ax.grid()
# ax.set_ylabel('Counts')
# ax.set_xlabel('SiPM Energy [GeV]')

# #%%

# ## guardiamo il LG 
# fig, ax = plt.subplots(1, 1, figsize=(10, 6))

# ax.hist(LGphGeV[runnumbers[0]], 
#             bins = 100, histtype ='step', color = 'black',
#             alpha = 1, lw = 3, label = f'Random LG, mean = {round(meanLG[runnumbers[0]], 2)}', density = True, range = [0,150]) 
# #ax.hist(120-OREOphGeV[runnumbers[0]], 
#             # bins = 100, histtype ='step', color = 'hotpink',
#             # alpha = 1, lw = 3, label = f'Random 120-OREO, mean = {round(mean120_OREO[runnumbers[0]], 2)}', density = True, range = [0,150]) 

# ax.hist(LGphGeV[runnumbers[1]], 
#             bins = 100, histtype ='step', color = 'darkred',
#             alpha = 1, lw = 3, label = f'Axial LG,mean = {round(meanLG[runnumbers[1]], 2)}', density = True, range = [0,150])  

# #ax.hist(120-OREOphGeV[runnumbers[1]], 
#             # bins = 100, histtype ='step', color = 'orange',
#             # alpha = 1, lw = 3, label = f'Axial 120-OREO, mean = {round(mean120_OREO[runnumbers[1]], 2)}', density = True, range = [0,150]) 
# ax.legend(fontsize = 15)

# ax.grid()
# ax.set_ylabel('Counts')
# ax.set_xlabel('LG Energy [GeV]')
# #%%
# ## guardiamo il LG 
# fig, ax = plt.subplots(1, 1, figsize=(10, 6))

# ax.hist(LGphGeV_correction[runnumbers[0]], 
#             bins = 100, histtype ='step', color = 'black',
#             alpha = 1, lw = 3, label = f'Random, mean = {round(meanLG_correction[runnumbers[0]], 2)}', density = True, range = [0,150]) 
# ax.hist(LGphGeV_correction[runnumbers[1]], 
#             bins = 100, histtype ='step', color = 'darkred',
#             alpha = 1, lw = 3, label = f'Axial,mean = {round(meanLG_correction[runnumbers[1]], 2)}', density = True, range = [0,150])  
# ax.legend(fontsize = 15)

# ax.grid()
# ax.set_ylabel('Counts')
# ax.set_xlabel('LG with correction Energy [GeV]')
# #%% 

# # proviamo a fare la somma LG e OREO
# fig, ax = plt.subplots(1, 1, figsize=(10, 6))

# ax.hist(LGphGeV[runnumbers[0]]+ OREOphGeV[runnumbers[0]], 
#             bins = 100, histtype ='step', color = 'lime',
#             alpha = 1, lw = 3, label = f'Random, mean = {round(meanOREO_plusLG[runnumbers[0]], 2)}', density = True, range = [0,150])

# ax.hist(LGphGeV[runnumbers[1]]+OREOphGeV[runnumbers[1]], 
#             bins = 100, histtype ='step', color = 'orangered',
#             alpha = 1, lw = 3, label = f'Axial, mean = {round(meanOREO_plusLG[runnumbers[1]], 2)}', density = True, range = [0,150])  
# ax.legend(fontsize = 15)

# ax.grid()
# ax.set_ylabel('Counts')
# ax.set_xlabel('LG + OREO Energy [GeV] ')

# #%%

# # proviamo a fare la somma LG e OREO
# fig, ax = plt.subplots(1, 1, figsize=(10, 6))

# ax.hist(LGphGeV_correction[runnumbers[0]]+ OREOphGeV[runnumbers[0]], 
#             bins = 100, histtype ='step', color = 'lime',
#             alpha = 1, lw = 3, label = f'Random, mean = {round(meanOREO_plusLG_correction[runnumbers[0]], 2)}', density = True, range = [0,150]) 
# ax.hist(LGphGeV_correction[runnumbers[1]]+OREOphGeV[runnumbers[1]], 
#             bins = 100, histtype ='step', color = 'orangered',
#             alpha = 1, lw = 3, label = f'Axial, mean = {round(meanOREO_plusLG_correction[runnumbers[1]], 2)}', density = True, range = [0,150])  
# ax.legend(fontsize = 15)

# ax.grid()
# ax.set_ylabel('Counts')
# ax.set_xlabel('LG with correction + OREO Energy [GeV] ')

# #%%
# # print(f'OREO axial/random = {meanOREO[runnumbers[1]] / meanOREO[runnumbers[0]]}')
# # print(f'LG axial/random = {meanLG[runnumbers[1]] / meanLG[runnumbers[0]]}')
# # print(f'LG with correction axial/random = {meanLG_correction[runnumbers[1]] / meanLG_correction[runnumbers[0]]}')
# # print(f'OREO + LG  axial/random = {meanOREO_plusLG[runnumbers[1]] / meanOREO_plusLG[runnumbers[0]]}')
# # print(f'OREO + LG with correction  axial/random = {meanOREO_plusLG_correction[runnumbers[1]] / meanOREO_plusLG_correction[runnumbers[0]]}')
# # print(f'120-OREO  random = {mean120_OREO[runnumbers[1]]}')
# # print(f'120-OREO  axial = {mean120_OREO[runnumbers[0]]}')
# # print(f'120-OREO  axial/random = {mean120_OREO[runnumbers[1]]/mean120_OREO[runnumbers[0]]}')
# import uproot

# datapath = '/home/ale/Desktop/Dottorato/datiOreoSimulation/'
# #dataAxial = uproot.open(datapath +'oreo0823_axial_6GeV.root')['outData']
# dataAmo = uproot.open(datapath + 'H2oreo_amorphous120GeV.root')['outData']


# amo_LG = np.array(dataAmo['GammaCal_EDep_00'])

# simulation_amo_crystA = np.array(dataAmo['CrystalA_EDep'])
# simulation_amo_crystB = np.array(dataAmo['CrystaB_EDep'])
# simulation_amo_crystC = np.array(dataAmo['CrystalC_EDep'])
# simulation_xposT0 = np.array(dataAmo['Tracker_X_0'])
# simulation_yposT0 = np.array(dataAmo['Tracker_Y_0'])
# simulation_xposT1 = np.array(dataAmo['Tracker_X_1'])
# simulation_yposT1 = np.array(dataAmo['Tracker_Y_1'])
# simulation_telePos = [simulation_xposT0, simulation_yposT0, simulation_xposT1, simulation_yposT1]

# #%%
# tele_label = ['T1, x [cm]', 'T1, y [cm]', 'T2, x [cm]', 'T2, y [cm]']
# fig, ax = plt.subplots(2, 2, figsize=(8, 8))
# ax = ax.flat
# for i in range(4):
#     ax[i].hist(simulation_telePos[i], bins = 100, histtype ='step', color = 'hotpink',  
#             alpha = 1, lw = 3, range = [-5,5], label = 'Simulation')
#     ax[i].set_xlabel(f'{tele_label[i]}')
#     ax[i].set_ylabel('Counts')
#     ax[i].legend(fontsize = 10)
#     ax[i].grid()
# plt.tight_layout()
# plt.show()


# #%%
# simulation_divx = (np.arctan((simulation_telePos[2]- simulation_telePos[0])/(distancesfromT1[0])))
# simulation_divy = (np.arctan((simulation_telePos[3]- simulation_telePos[1])/(distancesfromT1[0])))
# fig, ax = plt.subplots(1, 2, figsize=(10, 10))
# ax = ax.flat
# ax[0].hist(simulation_divx,  bins = 100, histtype ='step', color = 'navy',  
#         alpha = 1, lw = 3, label = 'Simulation', range = [-0.004,0.004])
# ax[0].set_xlabel('x divergence')
# ax[0].set_ylabel('Counts')
# ax[0].legend()
# ax[0].grid()


# ax[1].hist(simulation_divy,  bins = 100, histtype ='step', color = 'navy',  
#         alpha = 1, lw = 3, label = 'Simulation', range = [-0.004,0.004])
# ax[1].set_xlabel('y divergence')
# ax[1].set_ylabel('Counts')
# ax[1].legend()
# ax[1].grid()
# #%%

# simulation_xcryst = simulation_telePos[0] + zcryst*np.tan(simulation_divx)
# simulation_ycryst = simulation_telePos[1] + zcryst*np.tan(simulation_divy)
# fig, ax = plt.subplots(1,1, figsize=(8,8))
# zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(simulation_xcryst, simulation_ycryst,
#                                                 cmap=plt.cm.jet, bins=[60, 60], range=[[-5,5], [-5,5]])
# plt.colorbar(mapable, ax=ax)
# rect = patches.Rectangle((xcrystinf, ycrystinf), crystlenght*3, crystlenght, linewidth=4, edgecolor='darkred', facecolor='none')
# ax.add_patch(rect)
# ax.set_xlabel('x [cm]')
# ax.set_xlabel('y [cm]')
# ax.set_title('Proiezione su piano del cristallo, simulazione')
# plt.tight_layout()
# plt.show()
# #%%
# simulation_phth = 0.1
# fig, ax = plt.subplots(1, 3, figsize=(16, 8))
# ax = ax.flat
# ax[0].hist(simulation_amo_crystA, bins = 100, histtype ='step', color = 'teal',  
#         alpha = 1, lw = 3, label = 'Random simulation,\n crystal A', range = [0,30])
# ax[0].set_yscale('log')
# ax[0].legend()
# ax[0].grid()      
# ax[0].axvline(x=simulation_phth, color="black", linestyle = '--', label = 'cut', lw = 2)
# ax[1].hist(simulation_amo_crystB, bins = 100, histtype ='step', color = 'indigo',  
#         alpha = 1, lw = 3, label = 'Random simulation,\n crystal B',  range = [0,30])
# ax[1].set_yscale('log')
# ax[1].legend()
# ax[1].grid()      
# ax[1].axvline(x=simulation_phth, color="black", linestyle = '--', label = 'cut', lw = 2)
# ax[2].hist(simulation_amo_crystC, bins = 100, histtype ='step', color = 'orangered',  
#         alpha = 1, lw = 3, label = 'Random simulation,\n crystal C',  range = [0,30])
# ax[2].set_yscale('log')
# ax[2].legend()
# ax[2].grid()   
# ax[2].axvline(x=simulation_phth, color="black", linestyle = '--', label = 'cut', lw = 2) 
# for i in range(3): 
#     ax[i].set_xlabel('PH [GeV]')
#     ax[i].set_ylabel('Counts')
# plt.tight_layout()
# simulation_cutph = ((simulation_amo_crystA>simulation_phth)|(simulation_amo_crystB>simulation_phth)|(simulation_amo_crystC>simulation_phth))
# #%%
# fig, ax = plt.subplots(1,1, figsize=(8,8))
# zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(simulation_xcryst[simulation_cutph], 
#                                                 simulation_ycryst[simulation_cutph], 
#                                                 cmap=plt.cm.jet, bins=[60, 60], range=[[-5,5], [-5,5]])
# plt.colorbar(mapable, ax=ax)
# ax.set_xlabel('x [cm]')
# ax.set_xlabel('y [cm]')
# ax.set_title('Proiezione su piano del cristallo, simulazione')
# plt.show()


# #%%

# fig, ax = plt.subplots(1, 3, figsize=(12, 8))
# ax = ax.flat
# ax[0].hist(simulation_amo_crystA[simulation_cutph], bins = 100, histtype ='step', color = 'teal',  
#         alpha = 1, lw = 3, label = 'Random, crystal A', range = [0,20], density = True)
# ax[0].hist((((data[runnumbers[0]][1][:,chLabel['George1']]+data[runnumbers[0]][1][:,chLabel['George2']])[cuttot[runnumbers[0]]]-SiPM_intercept1[0])*SiPM_eqfact[0]-SiPM_calib[2])/SiPM_calib[0], bins = 100, histtype ='step', color = 'pink',  
#         alpha = 1, lw = 3, label = 'George', range = [0,20], density= True)
# ax[0].set_yscale('log')
# ax[0].legend()
# ax[0].grid()      


# ax[1].hist(simulation_amo_crystB[simulation_cutph], bins = 100, histtype ='step', color = 'indigo',  
#         alpha = 1, lw = 3, label = 'Random,crystal B',  range = [0,20], density= True)
# ax[1].hist((((data[runnumbers[0]][1][:,chLabel['John1']]+data[runnumbers[0]][1][:,chLabel['John2']])[cuttot[runnumbers[0]]]-SiPM_intercept1[1])*SiPM_eqfact[1]-SiPM_calib[2])/SiPM_calib[0], bins = 100, histtype ='step', color = 'lightblue',  
#         alpha = 1, lw = 3, label = 'John',  range = [0,20], density= True)
# ax[1].set_yscale('log')
# ax[1].legend()
# ax[1].grid()      


# ax[2].hist(simulation_amo_crystC[simulation_cutph], bins = 100, histtype ='step', color = 'orangered',  
#         alpha = 1, lw = 3, label = 'Random, crystal C',  range = [0,20],density= True)
# ax[2].hist((((data[runnumbers[0]][1][:,chLabel['Paul1']]+data[runnumbers[0]][1][:,chLabel['Paul2']])[cuttot[runnumbers[0]]]-SiPM_intercept1[2])*SiPM_eqfact[2]-SiPM_calib[2])/SiPM_calib[0], bins = 100, histtype ='step', color = 'brown',  
#         alpha = 1, lw = 3, label = 'Paul',  range = [0,20],density= True)
# ax[2].set_yscale('log')
# ax[2].legend()
# ax[2].grid()   



# #%%


#
#%%
# ## guardo il LG
# fig, ax = plt.subplots(1, 1, figsize=(10, 6))

# ax.hist(((data[runnumbers[0]][1][:,chLabel['LG2']]-LG_calib05[2])/LG_calib05[0]), 
#             bins = 50, histtype ='step', color = 'hotpink',
#             alpha = 1, lw = 3, label = 'Amorphous experimental data', density = True) 
# ax.hist(amo_LG,
#         bins = 100, histtype ='step', color = 'navy',
#             alpha = 1, lw = 3, label = 'Amorphous simulation, lead glass', density = True)



# ax.legend(fontsize = 10)
# ax.legend(fontsize = 10)

# ax.grid()
# ax.set_ylabel('Counts')
# ax.set_xlabel('Energy [GeV]')


# ### andiamo a vedere il lead glass

# #%% non cambia praticamente nulla se equalizzo e poi calibro la somma o se calibro direttamente i tre SiPM indipenentemente 
# ##Random simulation però non va molto bene 
# fig, ax = plt.subplots(1, 1, figsize=(8, 6))

# ax.hist(((PH_toteq[93][cuttot[93]]-SiPM_calib[2])/SiPM_calib[0]), 
#             bins = 100, histtype ='step', color = 'navy',
#             alpha = 1, lw = 3, label = 'Random, first method', density = True, range = [0.2,20]) 
# ax.hist(OREO_PHtot[93][cuttot[93]], 
#             bins = 100, histtype ='step', color = 'darkred',
#             alpha = 1, lw = 3, label = 'Random, second method', density = True, range = [0.2,20]) 
# # ax.hist(((simulation_amo_crystA+simulation_amo_crystB+simulation_amo_crystC)[simulation_cutph]),
# #         bins = 100, histtype ='step', color = 'darkred',
# #             alpha = 1, lw = 3, label = 'Random simulation', density = True, range = [0.2,20])
# ax.legend(fontsize = 15)
# ax.set_xlim([0.2,20])
# ax.grid()
# ax.set_ylabel('Counts')
# ax.set_xlabel('OREO energy deposit [GeV]')
# plt.tight_layout()
# #plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/h2/H2random2methods.pdf')