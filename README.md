# Title
This repository contains codes for the analysis of data taken  either during  beam test or with cosmic rays in the INSULAB laboratory.  

che sì ardito intrò per questo regno.
Sol si ritorni per la folle strada: 
pruovi, se sa

A word of caution is in order: if you are seeking clear and useful code for the analysis of HEP datasets, then this is not the place for you. However, if you wish to take a journey through the profound and breathtaking valleys of chaos, then I suggest you to go into these codes. They are a reflection of my mind: a part from beer, kittens, flowers and polenta uncia, you will find only a magnificent mess.

