#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 24 17:26:28 2023

@author: ale
"""


# Dimensioni cristallo → 3.1x3.1 x4.1 cm
##############################################################################
#####################          RUN 735            #######################

# lateral→ George + Paul digi ch 1-2
# lateral → John + Ringo digi ch 3-4
# Sipm bias 30.99 V
############################################################################
############################################################################
from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import copy
import matplotlib as mpl
import matplotlib.colors as colors
import matplotlib.patches as patches
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.optimize import curve_fit

nrun = 754

plt.rcParams['font.size'] = '15'
xlabels = ['BC1', 'BC1', 'BC2', 'BC2']
xlim = [[-0,10]]*4
zcryst = 18
Colors = ['limegreen', 'limegreen', 'deepskyblue', 'deepskyblue']
sipmlabels = ['George, lateral', 'Paul, lateral', 'John, lateral', 'Ringo, lateral']
#apro i dati, ci interessa solo PH1 cioè il primo digi che contiene canedese + i 4 sipm
datapath = '/home/ale/Desktop/datilabo2022/run360'
h5keys = ['PH1', 'Time1', 'xpos']
data = opendata(nrun, datapath, h5keys)
#camere 
histpos1d(data[2], 2, xlabels = xlabels, xlim = xlim, Range = [0,10])
histpos2d(data[2], numberoftrackers = 2)

#estraggo ph e tempi
phsipm = data[0][:,1:5]
timesipm = data[1][:,1:5]
mpverr = []
#%% vediamo le ph dei quattro SiPM

fig, ax = plt.subplots(2,2, figsize=(8,8))
ax = ax.flat
for i in range(4): 
    ax[i].hist(phsipm[:,i], bins=100, range = [0,1000], histtype = 'stepfilled', 
               color = Colors[i] ,edgecolor = Colors[i], alpha = 0.4, lw =2)
    ax[i].set_xlabel(f'PH {sipmlabels[i]} [a.u]')
    ax[i].grid()
    ax[i].set_yscale('log')
    ax[i].legend()
plt.tight_layout()
plt.show()

#%% PH vs tempi
fig, ax = plt.subplots(2,2, figsize=(8,8))
ax= ax.flat
mycmap = copy.copy(mpl.cm.jet)
mycmap.set_bad(mycmap(0))
phsel = [170, 240, 200, 250]
#phsel = [0]*4
timeselsup = [130, 130, 130, 130]
timeselinf = [88,88,88,85]
cuttime = []
cutph= []
for k in range(4): 
    
    timeHist, _, _, mapable = ax[k].hist2d(phsipm[:,k], timesipm[:,k], range = [[0,1000], [0,500]], cmap=mycmap,
                                           bins = [100, 100], norm=colors.LogNorm())
    
    
    ax[k].grid()
    ax[k].set_xlabel(f'PH {sipmlabels[k]}')
    ax[k].set_ylabel('time [ns]')
    fig.colorbar(mapable,ax=ax[k])
    plt.tight_layout()
    ax[k].axhline(y=timeselsup[k], color="red", label = 'threshold')
    ax[k].axhline(y = timeselinf[k], color="red")
    ax[k].axvline(x = phsel[k], color ='red')
    cuttime.append((timesipm[:,k]>timeselinf[k]) & (timesipm[:,k]<timeselsup[k]))
    cutph.append(phsipm[:,k]>phsel[k])
#%% calcolimao la divergenza 
divex, divey = divergence(data[2], numberoftrackers = 2, distancesfromT1=[5]) 
# ricostruzione traccia su cristallo 
xcryst = data[2][:,0] + zcryst*np.tan(divex)
ycryst = data[2][:,1] + zcryst*np.tan(divey)
# zbc = 5 
# divex = np.arctan((xpos[:,0]-xpos[:,2])/zbc)
# divey = np.arctan((xpos[:,1]-xpos[:,3])/zbc)
divselinf = -0.3
divselsup = 0.3
fig, ax = plt.subplots(1,2,figsize = (6,6))
ax = ax.flat
ax[0].hist(divex[0], bins = 100, range =[-1,1], color = 'navy')
ax[1].hist(divey[0], bins = 100, range = [-1,1], color = 'navy')
ax[0].axvline(x = divselinf, color = 'red', lw = 2)
ax[0].axvline(x = divselsup, color = 'red', lw = 2)
ax[1].axvline(x = divselinf, color = 'red', lw = 2)
ax[1].axvline(x = divselsup, color = 'red', lw = 2)
ax[0].grid()
ax[1].grid()
ax[0].set_xlabel('divergence x [rad]', fontsize = 14)
ax[1].set_xlabel('divergence y [rad]', fontsize = 14)
ax[0].set_ylabel('Entries')
ax[1].set_ylabel('Entries')
plt.tight_layout()  
##taglio in divergenza
cutdiv = ((divex>divselinf)& (divex<divselsup) &(divey>divselinf)& (divey<divselsup)) 
cuttop = ((cuttime[0]|cuttime[1])&cutdiv)
cutbottom = ((cuttime[2]|cuttime[3])&cutdiv)
#%%
## ph con tagli in divergenza + or ph top e or ph bottom
from lmfit.models import ExpressionModel
#uso l'approssimazione di Moyal 
modLandau = ExpressionModel('A * exp(-0.5 * ((x-mpv)/width + exp(-(x-mpv)/width)))')
mpv = []
fitPar = [[400,400,200]]*4
limInf = [200, 300, 200, 200]
limSup = [800]*4
fig, ax = plt.subplots(2,2, figsize=(10,8))
ax = ax.flat
for i in range(4): 
    if (i == 0) |(i==1):
       h, bins,_ =  ax[i].hist(phsipm[:,i][cuttop[0]&(cutph[0]|cutph[1])], bins=100, range = [0,1000], histtype = 'stepfilled', 
                               color = Colors[i] ,edgecolor = Colors[i],
                               alpha = 0.4, lw =2)
    else: 
        h, bins, _ = ax[i].hist(phsipm[:,i][cutbottom[0]&(cutph[2]|cutph[3])], bins=100, range = [0,1000], histtype = 'stepfilled', 
                                color = Colors[i] ,edgecolor = Colors[i], 
                                alpha = 0.4, lw =2)
        
        
        
    err = h
    x = np.array([(bins[k+1]+bins[k])/2 for k in range(len(bins)-1)])
    condition= ((x>limInf[i]) & (x<limSup[i]))
    params = modLandau.make_params(A = fitPar[i][0], mpv = fitPar[i][1], width = fitPar[i][2] )
    result = modLandau.fit(h[condition], params, x=x[condition])
    print(f'Chi-square = {result.chisqr:.4f}, Reduced Chi-square = {result.redchi:.4f}')
    #print(result.fit_report())
    mpv.append(result.params['mpv'].value)
    mpverr.append(result.params['mpv'].stderr)
    
    ax[i].plot(x[condition], result.best_fit, label=f'mpv = {round(mpv[i],2)} $\pm$ {round(result.params["mpv"].stderr,2)}', color = 'red', lw = 2)   
    ax[i].set_xlabel(f'PH {sipmlabels[i]} [a.u]')
    ax[i].grid()
    ax[i].legend()
plt.tight_layout()
plt.show()
eqpar = [mpv[0]/mpv[0], mpv[0]/mpv[1], mpv[0]/mpv[2], mpv[0]/mpv[3]]

with open('cosmicRay.dat','w') as out_file:
        np.savetxt(out_file, eqpar)
#%%
#rifaccio fit equalizzando 
fitPar = [[400,400,200]]*4
limInf = [50, 50, 50, 50]
limSup = [800]*4
mpv = []
fig, ax = plt.subplots(2,2, figsize=(10,8))
ax = ax.flat
for i in range(4): 
    if (i == 0) |(i==1):
       h, bins,_ =  ax[i].hist(phsipm[:,i][cuttop[0]&(cutph[0]|cutph[1])]*eqpar[i], bins=300, range = [0,1000], histtype = 'stepfilled', 
                               color = Colors[i] ,edgecolor = Colors[i],
                               alpha = 0.4, lw =2)
    else: 
        h, bins, _ = ax[i].hist(phsipm[:,i][cutbottom[0]&(cutph[2]|cutph[3])]*eqpar[i], bins=300, range = [0,1000], histtype = 'stepfilled', 
                                color = Colors[i] ,edgecolor = Colors[i], 
                                alpha = 0.4, lw =2)
        
        
        
    err = h
    x = np.array([(bins[k+1]+bins[k])/2 for k in range(len(bins)-1)])
    condition= ((x>limInf[i]) & (x<limSup[i]))
    params = modLandau.make_params(A = fitPar[i][0], mpv = fitPar[i][1], width = fitPar[i][2] )
    result = modLandau.fit(h[condition], params, x=x[condition])
    print(f'Chi-square = {result.chisqr:.4f}, Reduced Chi-square = {result.redchi:.4f}')
    #print(result.fit_report())
    mpv.append(result.params['mpv'].value)
    
    ax[i].plot(x[condition], result.best_fit, label=f'mpv = {round(mpv[i],2)} $\pm$ {round(result.params["mpv"].stderr,2)}', color = 'red', lw = 2)   
    ax[i].set_xlabel(f'PH {sipmlabels[i]} equilized [a.u]')
    ax[i].grid()
    ax[i].legend()
plt.tight_layout()
plt.show()
#%%
## vediamo le correlazioni
fig, ax = plt.subplots(1, 1, figsize=(8,8))
ax.hist2d(phsipm[:,0][cuttop[0]], phsipm[:,1][cuttop[0]],
          bins =[100,100], cmap = plt.cm.jet, norm=colors.LogNorm(), range = [[0,1500], [0,1500]])

ax.plot(phsipm[:,0], phsipm[:,0], '--', color = 'black', label = 'y = x')
ax.set_xlabel('PH George, lateral')
ax.set_ylabel('PH Paul, lateral')
ax.legend()
ax.grid()
plt.show()

fig, ax = plt.subplots(1, 1, figsize=(8,8))
ax.hist2d(phsipm[:,2][cutbottom[0]], phsipm[:,3][cutbottom[0]],
          bins =[100,100], cmap = plt.cm.jet, norm=colors.LogNorm(), range = [[0,1500], [0,1500]])

ax.plot(phsipm[:,2], phsipm[:,2], '--', color = 'black', label = 'y = x')
ax.set_xlabel('PH John, lateral')
ax.set_ylabel('PH Ringo, lateral')
ax.legend()
ax.grid()
plt.show()
#%%
## vediamo le correlazioni con equalizzazione
fig, ax = plt.subplots(1, 1, figsize=(8,8))
ax.hist2d(phsipm[:,0][cuttop[0]]*eqpar[0], phsipm[:,1][cuttop[0]]*eqpar[1],
          bins =[100,100], cmap = plt.cm.jet, norm=colors.LogNorm(), range = [[0,1500], [0,1500]])

ax.plot(phsipm[:,0], phsipm[:,0], '--', color = 'black', label = 'y = x')
ax.set_xlabel('PH George equilized, lateral')
ax.set_ylabel('PH Paul equilized, lateral')
ax.legend()
ax.grid()
plt.show()

fig, ax = plt.subplots(1, 1, figsize=(8,8))
ax.hist2d(phsipm[:,2][cutbottom[0]]*eqpar[2], phsipm[:,3][cutbottom[0]]*eqpar[3],
          bins =[100,100], cmap = plt.cm.jet, norm=colors.LogNorm(), range = [[0,1500], [0,1500]])

ax.plot(phsipm[:,2], phsipm[:,2], '--', color = 'black', label = 'y = x')
ax.set_xlabel('PH John equilized, lateral')
ax.set_ylabel('PH Ringo equilized, lateral')
ax.legend()
ax.grid()
plt.show()
#%% ## charge sharing equalizzato 

cs_top = ((phsipm[:,0][cuttop[0]]- phsipm[:,1][cuttop[0]])/(phsipm[:,0][cuttop[0]]+ phsipm[:,1][cuttop[0]]))
cs_bottom = ((phsipm[:,2][cutbottom[0]]- phsipm[:,3][cutbottom[0]])/(phsipm[:,2][cutbottom[0]]+ phsipm[:,3][cutbottom[0]]))


fig, ax = plt.subplots(2,2, figsize=(10,8))
ax = ax.flat
ax[0].hist2d(xcryst[0][cuttop[0]], cs_top, range = [[0,10],[-0.8,0.8]], bins =[100,100], 
              cmap = plt.cm.jet, norm=colors.LogNorm())
ax[0].set_xlabel('x [cm]')
ax[0].set_ylabel('(George-Paul)/(George+Paul) \n equalized')
ax[1].hist2d(ycryst[0][cuttop[0]], cs_top, range = [[0,+10],[-0.8,0.8]], bins =[100,100], 
              cmap = plt.cm.jet, norm=colors.LogNorm())
ax[1].set_xlabel('y [cm]')
ax[1].set_ylabel('(George-Paul)/(George+Paul)\n equalized')

ax[2].hist2d(xcryst[0][cutbottom[0]],cs_bottom, range = [[0,10],[-0.8,0.8]], bins =[100,100], 
              cmap = plt.cm.jet, norm=colors.LogNorm())
ax[2].set_xlabel('x [cm]')
ax[2].set_ylabel('(Paul-Ringo)/(Paul +Ringo)\n equalized')

ax[3].hist2d(ycryst[0][cutbottom[0]], cs_bottom, range = [[0,10],[-0.8,0.8]], bins =[100,100], label = 'back',
              cmap = plt.cm.jet, norm=colors.LogNorm())
ax[3].set_xlabel('y [cm]')
ax[3].set_ylabel('(Paul-Ringo)/(Paul +Ringo)\n equalized')
for i in range(4): 
    ax[i].grid()
    ax[i].axhline(y=0, color="black", ls = '--', lw = 3)
plt.tight_layout()
plt.show()
#%%
## facciamo l'eff di tutte e quattro 

    

fig, ax = plt.subplots(1,1, figsize=(8,8))
zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xcryst[0], ycryst[0], cmap=plt.cm.jet, bins=[80, 80], range=[[0,10], [0,10]])
plt.colorbar(mapable, ax=ax)
ax.set_xlabel('x [cm]')
ax.set_xlabel('y [cm]')
ax.set_title('Proiezione su piano del cristallo')
plt.tight_layout()
plt.show()

fig, ax = plt.subplots(2,2, figsize=(8,8))
fig1, ax1 = plt.subplots(2,2, figsize=(8,8))
ax = ax.flat
ax1 = ax1.flat
for i in range(4): 
    if (i == 0) |(i==1):
        zhist2dcut, xhist2dcut , yhist2dcut , mapable = ax[i].hist2d(xcryst[0][cuttop[0]&(cutph[i])], ycryst[0][cuttop[0]&(cutph[i])], 
                                                                     cmap=plt.cm.jet, bins = [80,80],
                                                                     range = [[0,10], [0,10]])
    else: 
        zhist2dcut, xhist2dcut , yhist2dcut , mapable = ax[i].hist2d(xcryst[0][cutbottom[0]&(cutph[i])], ycryst[0][cutbottom[0]&(cutph[i])], 
                                                                     cmap=plt.cm.jet, bins = [80,80],
                                                                     range = [[0,10], [0,10]])

    ax[i].set_xlabel('x [cm]')
    ax[i].set_ylabel('y [cm]')
    ax[i].set_title(f'{sipmlabels[i]}')
    plt.tight_layout()
    np.seterr(divide='ignore', invalid = 'ignore')
    eff = np.divide(zhist2dcut, zhist2d)
    eff[np.isnan(eff)]=0

    divider = make_axes_locatable(ax1[i])
    cax = divider.append_axes('right', size='5%', pad=0.1)
    im = ax1[i].imshow(eff.T, cmap=plt.cm.jet, extent=[0,10, 0, 10], origin = "lower")
    fig.colorbar(im, cax=cax, orientation='vertical')
    rect = patches.Rectangle((3.6, 3.8), 3.1, 3.1, linewidth=2, edgecolor='darkred', facecolor='none')
    ax1[i].add_patch(rect)
    ax1[i].set_xlim([0,10])
    ax1[i].set_ylim([0,10])
    ax1[i].set_xlabel('x [cm]')
    ax1[i].set_ylabel('y [cm]')
    ax1[i].set_title(f'{sipmlabels[i]}')
plt.tight_layout()
plt.show()

#%% facciamo le somme seza erqualizzare 
mpvtot = []
phsumtop = (phsipm[:,0][cuttop[0]&(cutph[0]|cutph[1])])+(phsipm[:,1][cuttop[0]&(cutph[0]|cutph[1])])
phsumbottom = (phsipm[:,2][cutbottom[0]&(cutph[2]|cutph[3])])+(phsipm[:,3][cutbottom[0]&(cutph[2]|cutph[3])])
phsum = [phsumtop, phsumbottom]
fitPar = [[800,600,200]]*4
limInf = [200, 200, 200, 200]
limSup = [1500]*4
colorsum = ['limegreen', 'deepskyblue']
labelsum = ['George | Paul PH [a.u] (lateral)', 'John | Ringo PH [a.u] (lateral)']
fig, ax = plt.subplots(1,2, figsize=(8,8))
ax = ax.flat
for i in range(2):
    h, bins, _ = ax[i].hist(phsum[i], bins=100, range = [0,2000], histtype = 'stepfilled', 
                               color = colorsum[i],
                               alpha = 0.5,edgecolor = colorsum[i], lw =2)
    err = h
    x = np.array([(bins[k+1]+bins[k])/2 for k in range(len(bins)-1)])
    condition= ((x>limInf[i]) & (x<limSup[i]))
    params = modLandau.make_params(A = fitPar[i][0], mpv = fitPar[i][1], width = fitPar[i][2] )
    result = modLandau.fit(h[condition], params, x=x[condition])
    print(f'Chi-square = {result.chisqr:.4f}, Reduced Chi-square = {result.redchi:.4f}')
    #print(result.fit_report())
    mpvtot.append(result.params['mpv'].value)

    ax[i].set_xlabel(f'{labelsum[i]}')
    ax[i].plot(x[condition], result.best_fit, label=f'mpv = {round(mpvtot[i],2)} $\pm$ {round(result.params["mpv"].stderr,2)}', color = 'black', lw = 2)   
 
    ax[i].grid()
    ax[i].set_ylabel('Entries')
    ax[i].legend()
plt.tight_layout()
plt.show()   
#%%
#efficienza su somma  con or 
xcrystcut_top = xcryst[0][cuttop[0]|(cutph[0]|cutph[1])]
ycrystcut_top = ycryst[0][cuttop[0]|(cutph[0]|cutph[1])]
xcrystcut_bottom = xcryst[0][cutbottom[0]|(cutph[2]|cutph[3])]
ycrystcut_bottom = ycryst[0][cutbottom[0]|(cutph[2]|cutph[3])]

fig, ax = plt.subplots(2,1, figsize=(10,10))
ax = ax.flat


zhist2dcut_top, xhist2dcut_top , yhist2dcut_top , mapable_top = ax[0].hist2d(xcrystcut_top, ycrystcut_top, 
                                                                     cmap=plt.cm.jet, bins = [80,80],
                                                                     range = [[0,10], [0,10]])
zhist2dcut_bottom, xhist2dcut_bottom , yhist2dcut_bottom , mapable_bottom = ax[1].hist2d(xcrystcut_bottom, ycrystcut_bottom, 
                                                                     cmap=plt.cm.jet, bins = [80,80],
                                                                     range = [[0,10], [0,10]])
for i in range(2): 
    ax[i].set_xlabel('x [cm]')
    ax[i].set_ylabel('y [cm]')
    ax[i].set_title(f'{labelsum[i]}')
    plt.tight_layout()
    
fig, ax = plt.subplots(1,2, figsize=(10,10))
ax = ax.flat
np.seterr(divide='ignore', invalid = 'ignore')
eff_top = np.divide(zhist2dcut_top, zhist2d)
eff_top[np.isnan(eff)]=0

divider = make_axes_locatable(ax[0])
cax = divider.append_axes('right', size='5%', pad=0.1)
im_top = ax[0].imshow(np.transpose(eff_top), cmap=plt.cm.jet, extent=[0,10, 0, 10])
rect = patches.Rectangle((3.6, 3.8), 3.1, 3.1, linewidth=3, edgecolor='black', facecolor='none')
fig.colorbar(im_top, cax=cax, orientation='vertical')
ax[0].add_patch(rect)


np.seterr(divide='ignore', invalid = 'ignore')
eff_bottom = np.divide(zhist2dcut_bottom, zhist2d)
eff_bottom[np.isnan(eff)]=0
divider = make_axes_locatable(ax[1])
cax1 = divider.append_axes('right', size='5%', pad=0.1)
im_bottom = ax[1].imshow(np.transpose(eff_bottom), cmap=plt.cm.jet, extent=[0,10, 0, 10])
fig.colorbar(im_bottom, cax=cax1, orientation='vertical')
rect = patches.Rectangle((3.6, 3.8), 3.1, 3.1, linewidth=3, edgecolor='black', facecolor='none')

ax[1].add_patch(rect)
for i in range(2):
   
    ax[i].set_xlim([0,10])
    ax[i].set_ylim([0,10])
    
    ax[i].set_xlabel('x [cm]')
    ax[i].set_ylabel('y [cm]')
    ax[i].set_title(f'{labelsum[i]}')
    plt.tight_layout()
plt.show()

#%%  facciamo proiezione 
