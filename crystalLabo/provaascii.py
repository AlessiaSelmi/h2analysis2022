#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  4 10:21:16 2023

@author: ale
"""
from functionale import *
import numpy as np
import matplotlib.pyplot as plt


datapath = '/home/ale/Desktop/trovadati/run360'
runnumber = 735

data = np.loadtxt(datapath +f'{runnumber}.dat')
xpos = data[:,0:4]
ph1 = data[:,4:12]
ph2= data[:,12:20]
ph2 = data[:,20:28]
time1 = data[:,28:36]
xlabels = ['BC1',' BC1', 'BC2', 'BC2']
xlim = [[-0,10]]*4
histpos1d(xpos, 2, xlabels = xlabels, xlim = xlim, Range = [0,10])
histpos2d(xpos, numberoftrackers = 2)


fig, ax = plt.subplots(1,1, figsize=(8,8))
timephplot(ph1[:,1],time1[:,1], ax, timethresholdinf = 0, timethresholdsup=100, Range = [[0,800], [0,1000]])
