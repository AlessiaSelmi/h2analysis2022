#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 30 17:04:51 2023

@author: ale
"""


from functionale import *
import numpy as np
import matplotlib.pyplot as plt
from lmfit.models import GaussianModel
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
import pickle
from lmfit.models import GaussianModel

model = GaussianModel()
plt.rcParams['font.size'] = '15'
#%%
#apriamo i dati 60 GeV centrati su ciascun canale
datapath = '/home/ale/Desktop/dati2022H2/run520'
h5keys = ['xpos', 'digi_ph', 'digi_time']
labels = ['T1', 'T1', 'T2', 'T2']
Colors = [ 'dodgerblue', 'orangered',  'lime', 'turquoise','hotpink',  'indigo', 'darkred']

xlim = [[-4, 4]]*4
distancesfromT1 = [1590]
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]
energyeq = 60
divsup = 150*10**(-6)
divinf = -150*10**(-6)
zcalo = 1776
leadglassrun = {'TL': 141, 'TR': 138 , 'CL': 142,
                'CR': 127,  'BL': 140, 'BR': 139,
                'CC': 126}
lgchannels = {'TL':13, 'TR': 14, 'CL': 10, 'CR': 12, 'BL': 8, 'BR': 9, 'CC': 11}
ph60GeV = {}
mueq = {}
muerreq= {}

#%%
#sempre 60 GeV su ogni canale, guardo la divergenza, allineo fascio, guardo ph e tempo e poi salvo le PH 
for label in (leadglassrun):
    data = opendata(leadglassrun[label], datapath, h5keys)
    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(data[0], 2, distancesfromT1,
                                                          p0divx, p0divy,
                                                          Rangex=[[-0.001,0.001]],
                                                          Rangey=[[-0.001,0.001]])
    datashift, shift = aligntracker(data[0], 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                                  distancesfromT1,
                                                                                   p0divx, p0divy,
                                                                                   Rangex=[[-0.001,0.001]],
                                                                                   Rangey=[[-0.001,0.001]], 
                                                                                   Color='navy', 
                                                                             Figsize = (8,6))
    cutdiv = ((divexalign >divinf) & (divexalign<divsup) & (diveyalign>divinf) &(diveyalign<divsup))
    ax[0].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=divsup, color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=divsup, color="black", linestyle = '--', lw = 2)    
    ax[0].set_title(f'Lead Glass {label}, 60 GeV ')
    xcalo = datashift[:, 0] + zcalo*np.tan(divexalign)
    ycalo = datashift[:, 1] + zcalo*np.tan(diveyalign)
    fig, ax = plt.subplots(1, 1, figsize=(8, 4))
    zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xcalo[0], ycalo[0],
                                                   bins=[100, 100], cmap=plt.cm.jet, range=[[0, 2], [0, 2]])
    ax.set_xlabel(f'x [cm] {label}, 60 GeV')
    ax.set_ylabel(f'y [cm] {label}, 60 GeV')
    
    fig, ax = plt.subplots(1, 1, figsize=(8, 4))
           
    ax.hist2d(data[1][:,lgchannels[label]],data[2][:,lgchannels[label]] ,
               bins =[100,100], cmap = plt.cm.jet, range = [[2000, 10000], [100, 200]])
    
    ax.grid()
    ax.set_xlabel('PH [a.u]')
    ax.set_ylabel('time [ns]')
    ax.set_title(f'Lead Glass {label}, 60 GeV')
    fig.set_tight_layout('tight')
    plt.show()
    ph60GeV[label] = data[1][:,lgchannels[label]][cutdiv[0]]
#%%   facciamo il fit delle ph a 60 GeV per trovare fattore di equalizzazione
rangex = [[4000, 10000], [4000, 8000], [4000, 14000], [4000, 8000],[2000, 4500], [4000, 8000], [3000, 8000]]
xfitrangeinf = [7600, 6000, 9200, 5900, 3500, 6700, 5700]
xfitrangesup = [8500, 7000, 15000, 6460, 4000, 7500, 6600]
fig, ax = plt.subplots(1, 1, figsize=(11, 6))
for i, label in enumerate(leadglassrun): 
    

    hph, binsph, _ = ax.hist(ph60GeV[label], bins = 100, histtype ='stepfilled',
                          color = Colors[i], edgecolor = Colors[i], alpha = 0.3, 
                          lw =2, 
                          label = f'{label}', range = [2000, 12000], density = True)
    xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
    phcondition = ((xdata > xfitrangeinf[i]) & (xdata < xfitrangesup[i]))
    paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
    resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                        weights=np.sqrt(hph[phcondition]))
    to_plot = np.linspace(xfitrangeinf[i], xfitrangesup[i], 10000)
    y_eval = model.eval(resultx.params, x=to_plot)
    #ax[i].plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = 'red')
    mueq[label] = resultx.params['center'].value
    muerreq[label] = resultx.params['center'].stderr
    ax.plot(to_plot, y_eval, linewidth=2, color=Colors[i],linestyle = '--', label=f'$\mu$ = {round(mueq[label],2)} $\pm$ {round(muerreq[label],2)}')

    ax.grid()
    ax.set_xlabel('PH [a.u]')
    ax.set_ylabel('Entries')
    ax.legend(fontsize = 14)
    fig.set_tight_layout('tight')
#%% calcolo fattori di equalizzazione
eqfactor = {}
for label in leadglassrun:    
    eqfactor[label] = mueq['CC']/mueq[label]
    
with open("lg_eqfactor.dat", "wb") as file:
    # Serialize the dictionary and write it to the file
    pickle.dump(eqfactor, file)
#%% vadiamo i picchi equalizzati
fig, ax = plt.subplots(1, 1, figsize=(11, 6))
for i, label in enumerate(leadglassrun): 
    ax.hist(ph60GeV[label]*eqfactor[label], bins = 100, histtype ='step',
                          color = Colors[i],alpha = 0.7, 
                          lw =3, 
                          label = f'{label}', density = True, range = [4000, 7000])
    ax.grid()
    ax.set_xlabel('PH [a.u]')
    ax.set_ylabel('Entries')
    ax.legend(fontsize = 15)
    fig.set_tight_layout('tight')
    
#%%
# fig, ax = plt.subplots(1, 1, figsize=(11, 6))
# for i, label in enumerate(leadglassrun): 
#     ax.hist(ph60GeV[label]*eqfactor[label], bins = 100, histtype ='step',
#                           color = Colors[i],alpha = 1, 
#                           lw =3, 
#                           label = f'{label}', density = True, range = [4000, 7000])
#     ax.grid()
#     ax.set_xlabel('PH [a.u]')
#     ax.set_ylabel('Entries')
#     ax.legend()
#     fig.set_tight_layout('tight')
    
#%%###########################################################################
###############################################################################
##############################################################################
##################               CALIBRIAMO       #############################
runlgCC = {120:124, 100:151, 80:150, 60:126, 40:132, 20:144}
pheq = {}
phsum = {}
#apro le run a diverse energie centrate sul canale centrale, 
#si calcola la divergenza, tempi, coordiante di impatto, 
#equalizzo per ogni energia i canali e poi sommo
for energy in runlgCC: 
    data = opendata(runlgCC[energy], datapath, h5keys)
    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(data[0], 2, distancesfromT1,
                                                          p0divx, p0divy,
                                                          Rangex=[[-0.001,0.001]],
                                                          Rangey=[[-0.001,0.001]])
    datashift, shift = aligntracker(data[0], 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                                  distancesfromT1,
                                                                                   p0divx, p0divy,
                                                                                   Rangex=[[-0.001,0.001]],
                                                                                   Rangey=[[-0.001,0.001]], 
                                                                                   Color='navy', 
                                                                             Figsize = (8,6))
   
        
    cutdiv = ((divexalign >divinf) & (divexalign<divsup) & (diveyalign>divinf) &(diveyalign<divsup))
    ax[0].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=divsup, color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=divsup, color="black", linestyle = '--', lw = 2)   
    ax[0].set_title(f'Lead Glass CC, {energy} GeV ')
    if energy == 120: 
        xcalo = datashift[:,0] + zcalo*np.tan(divexalign)
        ycalo = datashift[:,1] + zcalo*np.tan(diveyalign)
        fig, ax = plt.subplots(1,1, figsize=(8,6))
        zhist2d, xhist2d , yhist2d , mapable = ax.hist2d(xcalo[0], ycalo[0], 
                  bins =[100,100], cmap = plt.cm.jet, range = [[-7.5, 10], [-8, 9.5]], norm=colors.LogNorm())
        rect = patches.Rectangle((-3.75, -4), 10, 10, linewidth=3, 
                                  edgecolor='black', facecolor='none', 
                                  label = 'Lead Glass CC')
        ax.add_patch(rect)
        rectcryst = patches.Rectangle((-0.15, -0.75), 3, 3, linewidth=3, 
                                  edgecolor='orangered', facecolor='none', 
                                  label = 'Fiducial area')
        ax.add_patch(rectcryst)
        ax.set_xlabel(f'x [cm]')
        ax.set_ylabel(f'y [cm]')
        ax.legend()
        fig.colorbar(mapable,ax=ax)
    fig, ax = plt.subplots(1, 1, figsize=(8, 4))
    ax.hist2d(data[1][:,lgchannels['CC']],data[2][:,lgchannels['CC']] ,
               bins =[100,100], cmap = plt.cm.jet)
    
    ax.grid()
    ax.set_xlabel('PH [a.u]')
    ax.set_ylabel('time [ns]')
    ax.set_title(f'Lead Glass CC, {energy} GeV')
    fig.set_tight_layout('tight')
    
    for label in lgchannels: 
        pheq[label] = (data[1][:,lgchannels[label]][cutdiv[0]])*eqfactor[label]
    phsum[energy] = sum(pheq.values())  
    
#%%
## fittimao i picchi del full calorimeter per trovare la retta di calibrazione 
mucalib = {}
muerrcalib = {}
fitrangecalibinf = [12200, 9800, 7900, 6250, 3800, 1800]
fitrangecalibsup = [12900, 10800, 8400, 8000, 4400, 2500]
fig, ax = plt.subplots(1, 1, figsize=(11, 6))
for i, energy in enumerate(runlgCC): 

    hph, binsph, _ = ax.hist(phsum[energy], bins = 200, histtype ='stepfilled',
                          color = Colors[i],edgecolor = Colors[i], alpha = 0.4, 
                          lw =3, 
                          label = f'{energy} GeV', density = True, range = [100, 20000])

    xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
    phcondition = ((xdata > fitrangecalibinf[i]) & (xdata < fitrangecalibsup[i]))
    paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
    resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                        weights=np.sqrt(hph[phcondition]))
    to_plot = np.linspace(fitrangecalibinf[i], fitrangecalibsup[i], 10000)
    y_eval = model.eval(resultx.params, x=to_plot)
    #ax[i].plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = 'red')
    mucalib[energy] = resultx.params['center'].value
    muerrcalib[energy] = resultx.params['center'].stderr
    ax.plot(to_plot, y_eval, linewidth=2, color='black', label=f'$\mu$ = {round(mucalib[energy],2)} $\pm$ {round(muerrcalib[energy],2)}')

ax.grid()
ax.set_xlabel('PH [a.u]')
ax.set_ylabel('Entries')
ax.legend()
fig.set_tight_layout('tight')
#%% faccimao la rfetta di calibrazione, trasformo i dizionari in liste e poi fitto 
mucaliblist = []   
errmucaliblist = []
energies = []
for energy in runlgCC: 
    energies.append(energy)
    mucaliblist.append(mucalib[energy])
    errmucaliblist.append(muerrcalib[energy])
    
    
fig, ax = plt.subplots(1,1, figsize=(6,6))
from lmfit.models import LinearModel
linearmodel = LinearModel()     
linearparams = linearmodel.guess(energies, x=mucaliblist)
linearresult = linearmodel.fit(energies, linearparams, x= mucaliblist, weights = errmucaliblist)

ax.errorbar(mucaliblist, energies, xerr= errmucaliblist, 
            marker='D',  linestyle = 'none', color = 'navy',
            markersize = 6)
ax.plot(mucaliblist,linearresult.best_fit, color = 'red', lw = 2,
        label = f' GeV = ({round(linearresult.params["slope"].value,4)} $\pm$ {round(linearresult.params["slope"].stderr,4)})$\cdot$ ADC +  ({round(linearresult.params["intercept"].value,3)}  $\pm$ {round(linearresult.params["intercept"].stderr,3)})')
ax.legend(fontsize = 10)
ax.set_ylabel('GeV')
ax.set_xlabel('PH [a.u.]')
ax.grid()
slopeeq=(linearresult.params['slope'].value)
errslopeeq = (linearresult.params['slope'].stderr)
intercepteq= (linearresult.params['intercept'].value)
errintercepteq= (linearresult.params['intercept'].stderr)
print(f'Chi-square = {linearresult.chisqr:.4f}, Reduced Chi-square = {linearresult.redchi:.4f}')
plt.show()



calib = [slopeeq, errslopeeq, intercepteq, errintercepteq]

with open('Calibration.dat','w') as out_file:
        np.savetxt(out_file, calib)
#%%  
#calcolo dei residui
residual = 100* (energies-linearresult.best_fit)/(linearresult.best_fit)
fig, ax = plt.subplots(1,1, figsize=(10,4))
ax.plot(mucaliblist
        , residual, 'D', markersize = 8, color = 'midnightblue')
ax.axhline(y= 0, ls = ':', c = 'k', lw = 3)

ax.set_xlabel('PH [a.u.]')
ax.set_ylabel('Residual $\%$')
ax.grid()
plt.tight_layout()

plt.show()


#%% vedo se i picchi calibrati sono effettivamente a quelle energie
xfitrangeinf = [120, 95, 78, 60,38, 19]
xfitrangesup = [127, 109,93,80,50,25]
muerrGeV = {}
muGeV = {}
fig, ax = plt.subplots(1, 1, figsize=(11, 6))
for i, ene in enumerate(energies):
    
    hph, binsph, _ = ax.hist(((phsum[ene]*slopeeq)+intercepteq), bins=200, histtype='stepfilled',
                             color=Colors[i], edgecolor=Colors[i], alpha=0.4,
                             lw=2,
                             label=f'{ene} GeV', density='True', range = [2, 200])
    xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
    phcondition = ((xdata > xfitrangeinf[i]) & (xdata < xfitrangesup[i]))
    paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
    resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                        weights=np.sqrt(hph[phcondition]))
    to_plot = np.linspace(xfitrangeinf[i], xfitrangesup[i], 10000)
    y_eval = model.eval(resultx.params, x=to_plot)
    #ax[i].plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = 'red')
    muGeV[ene] = resultx.params['center'].value
    muerrGeV[ene] = resultx.params['center'].stderr
    ax.plot(to_plot, y_eval, linewidth=2, color='black', label=f'$\mu$ = {round(muGeV[ene],3)} $\pm$ {round(muerrGeV[ene],3)}')


ax.set_xlabel('PH full calorimeter [GeV]')
ax.set_ylabel('Entries')
ax.legend()
ax.grid()
plt.show()