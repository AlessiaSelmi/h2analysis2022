#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 23 16:25:53 2023

@author: ale
"""

from functionale import *
import numpy as np
import matplotlib.pyplot as plt
from lmfit.models import GaussianModel
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
import pickle

 ###########################################################################
###############################################################################
###################           Cristallo            ###########################
###############################################################################
##############################################################################
## apro i dati, allineo le camere, calcolo la divergenza e plot tempo PH 
plt.rcParams['font.size'] = '15'
datapath = '/home/ale/Desktop/dati2022H2/run520'
h5keys = ['xpos', 'digi_ph', 'digi_time']
labels = ['T1', 'T1', 'T2', 'T2']
xlim = [[-4, 4]]*4
distancesfromT1 = [1590]
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]
xcryst = []
ycryst = []
cutdivtime = []
charge_sharing = []
summat = []
summatcryst2 = []
zcryst = 1747
sipmchannel = {'mat1': 4, 'mat2': 1, 'mat3':2, 'mat4':3}

## quelle di run 'normale' è il back 
#%%
#runs = [155, 204] # amorfo cristallo on beam back, front
runs = [153, 206]## asse back, front

for i, nrun in enumerate(runs):
    data = opendata(nrun, datapath, h5keys)
    divex, divey = divergence(data[0], numberoftrackers = 2, distancesfromT1=[15.90])
    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(data[0], 2, distancesfromT1,
                                                      p0divx, p0divy, Rangex=[[-0.001,0.001]], 
                                                      Rangey= [[-0.001,0.001]])
    datashift, shift = aligntracker(data[0], 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                              distancesfromT1,
                                                                                p0divx, p0divy,
                                                                                Color='navy', Figsize = (8,6), 
                                                                                
                                                                                Rangex=[[-0.04,0.04]],
                                                                                Rangey=[[-0.04,0.04]])

    x_cryst = datashift[:,0] + zcryst*np.tan(divexalign)
    y_cryst = datashift[:,1] + zcryst*np.tan(diveyalign)
    xcryst.append(x_cryst[0])
    ycryst.append(y_cryst[0])
    ax[0].axvline(x=-150*10**(-6), color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=+150*10**(-6), color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=-150*10**(-6), color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=+150*10**(-6), color="black", linestyle = '--', lw = 2)
    cutdive = ((divexalign>-150*10**(-6))&(divexalign<150*10**(-6)) & (diveyalign>-150*10**(-6))&(diveyalign<150*10**(-6)))
    # vado ad osservare i SiPM, vedo se sono correlate le risposte (se stanno sulla bisettrice)

    fig, ax = plt.subplots(1, 1, figsize=(8,8))
    timephplot(data[2][:,sipmchannel['mat2']], (data[1][:,sipmchannel['mat2']]), ax, 200, 300)
    fig, ax = plt.subplots(1, 1, figsize=(8,8))
    timephplot(data[2][:,sipmchannel['mat3']], (data[1][:,sipmchannel['mat3']]), ax, 200, 300)

    
    cuttime = ((data[2][:,sipmchannel['mat3']]>200) & (data[2][:,sipmchannel['mat3']]<300) & (data[2][:,sipmchannel['mat2']]>200) & (data[2][:,sipmchannel['mat2']]<300))
    ## vediamo le correlzioni
    fig, ax = plt.subplots(1, 1, figsize=(8,8))
    ax.hist2d(data[1][:,sipmchannel['mat2']],data[1][:,sipmchannel['mat3']], 
              bins =[100,100], cmap = plt.cm.jet, norm=colors.LogNorm())

    ax.plot(data[1][:,sipmchannel['mat2']], data[1][:,sipmchannel['mat2']], '--', color = 'black', label = 'y = x')
    ax.set_xlabel('SiPM mat2')
    ax.set_ylabel('SiPM mat3')
    ax.set_title(f'Run {nrun}')
    ax.legend()
    ax.grid()
    plt.show()
# #
    cond = cutdive[0] & cuttime[0]
    cutdivtime.append(cond) #and di divergenza e time (che è and dei tempi delle due matrici)
    fig, ax = plt.subplots(1, 1, figsize=(8,8))
    ax.hist(data[1][:,sipmchannel['mat2']][cond], bins = 100, range = [0,4000], 
            label = 'mat2', histtype ='step', 
            color = 'lime', alpha = 0.7, lw =3)
    ax.hist(data[1][:,sipmchannel['mat3']][cond], bins = 100, range = [0,4000], 
            label = 'mat3', histtype ='step', 
            color = 'orangered', alpha = 0.7, lw =3)
    ax.legend()
    ax.grid()
    ax.set_yscale('log')
    ax.set_xlabel('PH [a.u]')
    ax.set_ylabel('Entries')
    ax.set_title(f'Run {nrun}')
    plt.tight_layout()
    ## direi che sono praticamente equalizzate, sommiamo
    fig, ax = plt.subplots(1, 1, figsize=(8,8))
    ax.hist(data[1][:,sipmchannel['mat2']][cond]+data[1][:,sipmchannel['mat3']][cond], 
            bins = 100, 
            label = 'mat2+mat3', histtype ='step', 
            color = 'hotpink', alpha = 0.7, lw =3)
    ax.legend()
    ax.grid()
    ax.set_yscale('log')
    ax.set_xlabel('PH [a.u]')
    ax.set_ylabel('Entries')
    plt.tight_layout()




    sumdata = data[1][:,sipmchannel['mat2']]+data[1][:,sipmchannel['mat3']]
    sumdatacryst2 = data[1][:,sipmchannel['mat1']]+data[1][:,sipmchannel['mat4']]
    diffdata = data[1][:,sipmchannel['mat2']]-data[1][:,sipmchannel['mat3']]
    summat.append(sumdata) ## conterrà back e front
    summatcryst2.append(sumdatacryst2)
    ratio = diffdata/sumdata
    charge_sharing.append(ratio)

#%% guardo le somme se sono diverse tra conf back e front 

plt.rcParams['font.size'] = '20'
fig, ax = plt.subplots(1, 1, figsize=(10,10))
ax.hist(summat[0], 
        bins = 100, 
        label = 'Mat2 + Mat3 BACK', histtype ='step', 
        color = 'teal', alpha = 1, lw =3, density = 'True')

ax.hist(summat[1], 
        bins = 100, 
        label = 'Mat2 + Mat3 FRONT', histtype ='step', 
        color = 'darkred', alpha = 1, lw =3, density = 'True')
ax.legend(fontsize = 20)
ax.grid()
ax.set_yscale('log')
ax.set_xlabel('PH [a.u], axial orientation')
ax.set_ylabel('Entries')
ax.set_ylim([0, 2*10**(-3)])
plt.tight_layout()
#%% guardo l'area al centro del cristallo 
fig, ax = plt.subplots(1,1, figsize=(10,8))
ax.hist2d(xcryst[0], ycryst[0], range = [[0,+2],[0,2]], bins =[100,100], 
              cmap = plt.cm.jet, norm=colors.LogNorm())
xinf = 0.9
xsup = 1.6
yinf= 0.4
ysup = 1.4
xrectlenght = xsup-xinf
yrectlenght = ysup-yinf
rect = patches.Rectangle((xinf, yinf), xrectlenght, yrectlenght, linewidth=6, edgecolor='black', facecolor='none'
                          , label = 'Crystal central area')
ax.add_patch(rect)

fig, ax = plt.subplots(1,1, figsize=(10,8))
ax.hist2d(xcryst[1], ycryst[1], range = [[0,+2],[0,2]], bins =[100,100], 
              cmap = plt.cm.jet, norm=colors.LogNorm())

rect = patches.Rectangle((xinf, yinf), xrectlenght, yrectlenght, linewidth=6, edgecolor='black', facecolor='none'
                          , label = 'Crystal central area')
ax.add_patch(rect)
cutpos = [(xcryst[0]>xinf)&(xcryst[0]<xsup)&(ycryst[0]>yinf)&(ycryst[0]<ysup), (xcryst[1]>xinf)&(xcryst[1]<xsup)&(ycryst[1]>yinf)&(ycryst[1]<ysup) ]
#%% somme con tagli 
fig, ax = plt.subplots(1, 1, figsize=(8,8))
ax.hist(summat[0][cutdivtime[0]&cutpos[0]], 
        bins = 100, 
        label = 'SiPM sum BACK', histtype ='step', 
        color = 'indigo', alpha = 0.7, lw =3, density = 'True')

ax.hist(summat[1][cutdivtime[1]&cutpos[1]], 
        bins = 100, 
        label = 'SiPM sum FRONT', histtype ='step', 
        color = 'teal', alpha = 0.7, lw =3, density = 'True')
ax.legend()
ax.grid()
ax.set_yscale('log')
ax.set_xlabel('PH [a.u]')
ax.set_ylabel('Entries')
ax.set_title(f'Run {nrun}')
plt.tight_layout()
#%%
fig, ax = plt.subplots(2,2, figsize=(10,8))
ax = ax.flat
ax[0].hist2d(xcryst[0][cutdivtime[0]], charge_sharing[0][cutdivtime[0]], range = [[0,+2],[-0.5,0.5]], bins =[100,100], 
              cmap = plt.cm.jet, norm=colors.LogNorm())
ax[0].set_xlabel('x [cm]')
ax[0].set_ylabel('(PH2-PH3)/(PH2+PH3)')
ax[0].set_title('back')
ax[1].hist2d(ycryst[0][cutdivtime[0]], charge_sharing[0][cutdivtime[0]], range = [[0,+2],[-0.5,0.5]], bins =[100,100], 
              cmap = plt.cm.jet, norm=colors.LogNorm())
ax[1].set_xlabel('y [cm]')
ax[1].set_ylabel('(PH2-PH3)/(PH2+PH3)')
ax[1].set_title('SiPM prima del cistallo')
ax[2].hist2d(xcryst[1][cutdivtime[1]],charge_sharing[1][cutdivtime[1]], range = [[0,2],[-0.5,0.5]], bins =[100,100], 
              cmap = plt.cm.jet, norm=colors.LogNorm())
ax[2].set_xlabel('x [cm]')
ax[2].set_ylabel('(PH2-PH3)/(PH2+PH3)')
ax[2].set_title('SiPM dopo cryst)')
ax[3].hist2d(ycryst[1][cutdivtime[1]], charge_sharing[1][cutdivtime[1]], range = [[0,2],[-0.5,0.5]], bins =[100,100], label = 'back',
              cmap = plt.cm.jet, norm=colors.LogNorm())
ax[3].set_xlabel('y [cm]')
ax[3].set_ylabel('(PH2-PH3)/(PH2+PH3)')
ax[3].set_title('Front (sipm dopo cryst)')
for i in range(4): 
    ax[i].grid()
plt.tight_layout()
# plt.show()

#%%

#%%
# calototAmo = sum(caloAmo.values())
# #caloAmoGev = (calototAmo*slope)+intercept


# #%% 
# #apro dati simulazioni con cristallo in mezzo --> run 13

# simulationfileAmo = 'run13.dat'

# simulationdataAmo = np.loadtxt(f'{simulationpath}/{simulationfileAmo}', skiprows=1)
# with open(f'{simulationpath}/{simulationfileAmo}', 'r') as f:
#     header = f.readline().strip().split('\t')
    
# simulationdatacaloAmo = simulationdataAmo[:,9]+simulationdataAmo[:,10]+simulationdataAmo[:,11]+simulationdataAmo[:,12]+simulationdataAmo[:,13]+simulationdataAmo[:,14]+simulationdataAmo[:,15]
# simulationdatapi = simulationpi[:,2]+simulationpi[:,3]+simulationpi[:,4]+simulationpi[:,5]+simulationpi[:,6]+simulationpi[:,7]+simulationpi[:,8]
# simulationCalototAmo = np.concatenate((simulationdatacaloAmo, simulationdatapi), axis=0)
# #%%
# fig, ax = plt.subplots(1, 1, figsize=(8,8))
# ax2 = ax.twiny()

# h, bins = np.histogram(dataAmo[1][:,sipmchannel['mat2']][condAmo[0]]+dataAmo[1][:,sipmchannel['mat3']][condAmo[0]], bins = 100, )
# binc = bins[:-1] + (bins[1] - bins[0])/2
# lns1 = ax.plot(binc, h/np.sum(h), ds = "steps-mid",
#         label = 'SiPM experimental data', color = 'hotpink', alpha = 1, lw = 2)

# simulationdataCryst = np.concatenate((simulationdataAmo[:,0],simulationpi[:,0]),axis= 0)
# h, bins = np.histogram(simulationdataCryst, bins = 100, )
# binc = bins[:-1] + (bins[1] - bins[0])/2
# lns2 = ax2.plot(binc, h/np.sum(h), ds = "steps-mid",
#         label = 'Simulation', color = 'navy', alpha = .8, lw = 3)


# lns = lns1+lns2
# labs = [l.get_label() for l in lns]
# ax.legend(lns, labs, loc=0, fontsize = 15)

# # ax2.hist(simulationdata[:,1]*10**(-3), bins = 100, 
# #         label = 'simulationcryst2', histtype ='step', 
# #         color = 'navy',alpha = 0.7, lw =2, density=True)
# ax2.set_xlabel('Energy [GeV]')
# ax2.set_xlabel('Energy [GeV]')
# ax.set_xlabel('Energy [ADC]')
# ax2.set_yscale('log')
# ax.set_yscale('log')
# # ax2.legend()
# #ax.legend()
# ax.grid()
# plt.tight_layout()
# plt.show()

# #%%
# fig, ax = plt.subplots(1, 1, figsize=(8,8))
# hph, binsph, _ = ax.hist(dataAmo[1][:,sipmchannel['mat1']][condAmo[0]]+dataAmo[1][:,sipmchannel['mat4']][condAmo[0]], 
#         bins = 100, 
#         label = 'lateral crystal', histtype ='step', 
#         color = 'navy', alpha = 0.7, lw =3, density = 'True')
# ax.legend()
# ax.grid()
# ax.set_yscale('log')
# ax.set_xlabel('PH [a.u]')
# ax.set_ylabel('Entries')
# plt.tight_layout()


########################
#%%
