#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 19 11:29:14 2023

@author: ale
"""

from functionale import *
import numpy as np
import matplotlib.pyplot as plt
from lmfit.models import GaussianModel
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches

from lmfit.models import GaussianModel

plt.rcParams['font.size'] = '15'

#%%
datapath = '/home/ale/Desktop/dati2022H2/run520'
h5keys = ['xpos', 'digi_ph', 'digi_time']
leadglasslabel = ['TL', 'TR', 'CL','CC', 'CR', 'BL', 'BR']
energies = [120,100, 80, 60, 40, 20]
labels = ['T1', 'T1', 'T2', 'T2']
xlim = [[-4, 4]]*4
distancesfromT1 = [1590]
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]
zcalo = 1776
leadglassrun = {'TL': [141, 136, 147], 'TR': [138, 137, 146], 'CL': [142, 131, 143],
                'CR': [127, 133, 145], 'BL': [140, 135, 148], 'BR': [139, 134, 149],
                'CC': [124, 151, 150, 126, 132, 144]}

digichannel = [13, 14, 10, 12, 8, 9, 11]
lgchannels = {'BL': 8, 'BR': 9, 'CL': 10, 'CC': 11, 'CR': 12, 'TL': 13, 'TR': 14}


xposinf = 1.0
xpossup = 1.50
yposinf = 0.30
ypossup = 1.30
phtot = {}
ph = {}
pheff = {}
time = {}
rangeLGs = [[0,500], [0,200],[0,1000],[7000,12200],[0,2000],[0,1000],[0,1000]]
rangetime = [[0,250]]*7
for i, nrun in enumerate(leadglassrun['CC']):
    print (i,nrun)
    data = opendata(nrun, datapath, h5keys)
    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(data[0], 2, distancesfromT1,
                                                          p0divx, p0divy,
                                                          Rangex=[[-0.001,0.001]],
                                                          Rangey=[[-0.001,0.001]])
    datashift, shift = aligntracker(data[0], 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                                  distancesfromT1,
                                                                                   p0divx, p0divy,
                                                                                   Rangex=[[-0.001,0.001]],
                                                                                   Rangey=[[-0.001,0.001]], 
                                                                                   Color='navy', 
                                                                                   Figsize = (8,6))
    
    ax[0].set_ylim(0,1300)
    ax[1].set_ylim(0,1700)
    ax[0].axvline(x=-150*10**(-6), color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=+150*10**(-6), color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=-150*10**(-6), color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=+150*10**(-6), color="black", linestyle = '--', lw = 2)
    
    
    cutdiv = ((divexalign > -150*10**(-6)) & (divexalign < 150*10**(-6)) &
              (diveyalign > -150*10**(-6)) & (diveyalign < 150*10**(-6)))

    xcalo = datashift[:, 0] + zcalo*np.tan(divexalign)
    ycalo = datashift[:, 1] + zcalo*np.tan(diveyalign)
    fig, ax = plt.subplots(1, 1, figsize=(8, 4))
    zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xcalo[0], ycalo[0],
                                                   bins=[100, 100], cmap=plt.cm.jet, range=[[0, 2], [0, 2]])
    ax.set_xlabel(f'x [cm] Lead Glass CC, {energies[i]} GeV')
    ax.set_ylabel(f'y [cm] Lead Glass CC, {energies[i]} GeV')

    cutpos = ((xcalo > xposinf) & (xcalo < xpossup) &
              (ycalo > yposinf) & (ycalo < ypossup))
    rect = patches.Rectangle((xposinf, yposinf), (xpossup-xposinf), (ypossup-yposinf), linewidth=4,
                             edgecolor='red', facecolor='none',
                             label='Fiducial area')
    cuttime = ((data[2][:, lgchannels['CC']]>150) & ((data[2][:, lgchannels['CC']]<200)))
    ax.add_patch(rect)
    ax.grid()
    ax.legend()
    plt.show()
    condizione = (cutdiv & cuttime)
    
    for lglabel in lgchannels:
        ph[lglabel] = (data[1][:, lgchannels[lglabel]][condizione[0]])
        time[lglabel] = (data[2][:, lgchannels[lglabel]][condizione[0]])
        pheff[lglabel] = (data[1][:, lgchannels[lglabel]])
        print(lglabel)
    if nrun == 124: 
        fig = plt.figure(figsize = (10,8))
        ax1 = plt.subplot2grid((6, 6), (0, 1), colspan=2,rowspan=2)
        ax2 = plt.subplot2grid((6, 6), (0, 3), colspan=2,rowspan=2)
        ax3 = plt.subplot2grid((6, 6), (2, 0), colspan=2,rowspan=2)
        ax4 = plt.subplot2grid((6, 6), (2, 2), colspan=2,rowspan=2)
        ax5 = plt.subplot2grid((6, 6), (2, 4), colspan=2,rowspan=2)
        ax6 = plt.subplot2grid((6, 6), (4, 1), colspan=2,rowspan=2)
        ax7 = plt.subplot2grid((6, 6), (4, 3), colspan=2,rowspan=2)
        axes = [ax1,ax2,ax3,ax4,ax5,ax6,ax7]

        for k,ax in enumerate(axes):
            # ax.hist(ph[leadglasslabel[k]], bins = 100, histtype ='stepfilled',
            #                               color = 'lightpink', edgecolor= 'black',alpha = 0.7, 
            #                               lw =2, label= f'{leadglasslabel[k]} ', range = rangeLGs[k])
            
            
            ax.hist2d(ph[leadglasslabel[k]],time[leadglasslabel[k]] ,
                       bins =[100,100], cmap = plt.cm.jet, norm=colors.LogNorm(), range = [rangeLGs[k], rangetime[k]])
            
            ax.grid()
            ax.set_xlabel('PH [a.u]')
            ax.set_ylabel('time [ns]')
            ax.set_title(f'LG {leadglasslabel[k]}')
            fig.set_tight_layout('tight')
        plt.show()
        
        fig = plt.figure(figsize = (10,8))
        ax1 = plt.subplot2grid((6, 6), (0, 1), colspan=2,rowspan=2)
        ax2 = plt.subplot2grid((6, 6), (0, 3), colspan=2,rowspan=2)
        ax3 = plt.subplot2grid((6, 6), (2, 0), colspan=2,rowspan=2)
        ax4 = plt.subplot2grid((6, 6), (2, 2), colspan=2,rowspan=2)
        ax5 = plt.subplot2grid((6, 6), (2, 4), colspan=2,rowspan=2)
        ax6 = plt.subplot2grid((6, 6), (4, 1), colspan=2,rowspan=2)
        ax7 = plt.subplot2grid((6, 6), (4, 3), colspan=2,rowspan=2)
        axes = [ax1,ax2,ax3,ax4,ax5,ax6,ax7]
        
        for j,ax in enumerate(axes):
            # fig1, ax1 = plt.subplots(1, 1, figsize=(10, 6))
            thcalo = 30
            selcalo = ((ph[leadglasslabel[j]])>thcalo)
            zhist2dcut, xhist2dcut , yhist2dcut , mapable  = ax1.hist2d(xcalo[0][condizione[0]][selcalo], 
                                                                       ycalo[0][condizione[0]][selcalo] , 
                       bins =[100,100], cmap = plt.cm.jet, range = [[0,2], [0,2]])
    
    
            np.seterr(divide='ignore', invalid = 'ignore')
            eff = np.divide(zhist2dcut, zhist2d)
            eff[np.isnan(eff)]=0
            divider = make_axes_locatable(ax)
            cax = divider.append_axes('right', size='5%', pad=0.1)
            im = ax.imshow(np.flip(np.transpose(eff)), cmap=plt.cm.jet, extent=[0,2,0,2])
            fig.colorbar(im, cax=cax, orientation='vertical')   
            ax.set_xlim([0,2])
            ax.set_ylim([0,2])
            ax.set_xlabel('x [cm]', fontsize = 14)
            ax.set_ylabel('y [cm]', fontsize = 14)
            # ax.set_title(f'LG {leadglasslabel[j]} efficency')

            fig.set_tight_layout('tight')
        plt.show()
    phtot[energies[i]] = sum(ph.values())



# %%






model = GaussianModel()
mu = {}
muerr = {}


xfitrangeinf = [12000, 9500, 7600, 6000, 3000, 1800]

xfitrangesup = [12600, 10500,8500,7500,5000,2500]
fig, ax = plt.subplots(1, 1, figsize=(10, 6))
Colors = ['hotpink', 'dodgerblue', 'lime', 'orangered', 'turquoise', 'indigo']
for i, ene in enumerate(energies):

    hph, binsph, _ = ax.hist(phtot[ene], bins=100, histtype='stepfilled',
                             color=Colors[i], edgecolor=Colors[i], alpha=0.5,
                             lw=2,
                             label=f'{ene} GeV', density='True')

    xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
    phcondition = ((xdata > xfitrangeinf[i]) & (xdata < xfitrangesup[i]))
    paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
    resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                        weights=np.sqrt(hph[phcondition]))
    to_plot = np.linspace(xfitrangeinf[i], xfitrangesup[i], 10000)
    y_eval = model.eval(resultx.params, x=to_plot)
    #ax[i].plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = 'red')
    mu[ene] = resultx.params['center'].value
    muerr[ene] = resultx.params['center'].stderr
    ax.plot(to_plot, y_eval, linewidth=2, color='black', label=f'$\mu$ = {round(mu[ene],2)} $\pm$ {round(muerr[ene],2)}')



ax.set_xlabel('PH full calorimeter [a.u]')
ax.set_ylabel('Entries')
ax.legend()
ax.grid()
plt.show()
# %%
mucalib = []   
errcalib = []
for h in energies: 
    mucalib.append(mu[h])
    errcalib.append(muerr[h])


fig, ax = plt.subplots(1,1, figsize=(8,8))
from lmfit.models import LinearModel
linearmodel = LinearModel()     
linearparams = linearmodel.guess(energies, x=mucalib)
linearresult = linearmodel.fit(energies, linearparams, x= mucalib, weights = errcalib)

ax.errorbar(mucalib, energies, xerr= errcalib, 
            marker='D',  linestyle = 'none', color = 'midnightblue',
            label = f'Lead Glass CC', markersize = 6)
ax.plot(mucalib,linearresult.best_fit, color = 'red', lw = 2,
        label = f'a.u. = ({round(linearresult.params["slope"].value,3)} $\pm$ {round(linearresult.params["slope"].stderr,3)})$\cdot$ GeV +  ({round(linearresult.params["intercept"].value,3)}  $\pm$ {round(linearresult.params["intercept"].stderr,3)})')
ax.legend(fontsize = 14)
ax.set_ylabel('GeV')
ax.set_xlabel('PH [a.u.]')
ax.grid()
slope=(linearresult.params['slope'].value)
errslope = (linearresult.params['slope'].stderr)
intercept= (linearresult.params['intercept'].value)
errintercept= (linearresult.params['intercept'].stderr)
print(f'Chi-square = {linearresult.chisqr:.4f}, Reduced Chi-square = {linearresult.redchi:.4f}')
plt.show()
#%%

residual = 100* (energies-linearresult.best_fit)/(linearresult.best_fit)
fig, ax = plt.subplots(1,1, figsize=(10,4))
ax.plot(mucalib, residual, 'D', markersize = 8, color = 'midnightblue')
ax.axhline(y= 0, ls = ':', c = 'k', lw = 3)

ax.set_xlabel('PH [a.u.]')
ax.set_ylabel('Residual $\%$')
ax.grid()
plt.tight_layout()

plt.show()
#%%
xfitrangeinf = [115, 95, 75, 55,30, 10]

xfitrangesup = [125, 109,85,70,50,30]
fig, ax = plt.subplots(1, 1, figsize=(10, 6))
for i, ene in enumerate(energies):

    hph, binsph, _ = ax.hist(((phtot[ene]*slope)+intercept), bins=100, histtype='stepfilled',
                             color=Colors[i], edgecolor=Colors[i], alpha=0.5,
                             lw=2,
                             label=f'{ene} GeV', density='True')
    xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
    phcondition = ((xdata > xfitrangeinf[i]) & (xdata < xfitrangesup[i]))
    paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
    resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                        weights=np.sqrt(hph[phcondition]))
    to_plot = np.linspace(xfitrangeinf[i], xfitrangesup[i], 10000)
    y_eval = model.eval(resultx.params, x=to_plot)
    #ax[i].plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = 'red')
    mu[ene] = resultx.params['center'].value
    muerr[ene] = resultx.params['center'].stderr
    ax.plot(to_plot, y_eval, linewidth=2, color='black', label=f'$\mu$ = {round(mu[ene],3)} $\pm$ {round(muerr[ene],3)}')


ax.set_xlabel('PH full calorimeter [GeV]')
ax.set_ylabel('Entries')
ax.legend()
ax.grid()
plt.show()

#%% ## scrivo file per lA SIMULAZIONE
# fig, ax = plt.subplots(1, 1, figsize=(10, 10))

# hist, binedges, _ = ax.hist(((((phtot[120]*slope)+intercept))*10**3)[(((phtot[120]*slope)+intercept))*10**3>80000], bins=100, histtype ='step',
                            
#                             color = 'hotpink', alpha=0.6,
#                             lw=2,
#                             label='Experimental data 120 GeV')
# ax.set_yscale('log')
# ## scrivo istogramma da dare alla simulazione 
# with open("histogram.txt", "w") as f:
#     for i in range(len(hist)):
#         f.write(f"/gps/hist/point {binedges[i+1]:.2f} {hist[i]:.2f}\n")
        
# runnumber = 155 # amorfo cristallo on beam
# lgamorphous = {'BL':8, 'BR':9, 'CL':10, 'CC':11, 'CR':12, 'TL':13, 'TR':14}
# phlgamorphous = {}
# datacrystal = opendata(runnumber, datapath, h5keys)
# for i in lgamorphous:
#     phlgamorphous[i] = datacrystal[1][:,lgamorphous[i]]
    
# phAmotot = sum(phlgamorphous.values())
# phAmototGeV = (phAmotot*slope)+intercept
#%% CARICO FILE SIMULAZION E
simulationfile = 'run14.dat'
simulationpath = '/home/ale/Desktop/Dottorato/GeantSimulation/data'
simulationdata = np.loadtxt(f'{simulationpath}/{simulationfile}', skiprows=1)
with open(f'{simulationpath}/{simulationfile}', 'r') as f:
    header = f.readline().strip().split('\t')
    
    
#%%  
    
runpi = 'run10.dat'
simulationpi = np.loadtxt(f'{simulationpath}/{runpi}', skiprows=1)

simulationdatacalo = simulationdata[:,9]+simulationdata[:,10]+simulationdata[:,11]+simulationdata[:,12]+simulationdata[:,13]+simulationdata[:,14]+simulationdata[:,15]
simulationdatapi = simulationpi[:,2]+simulationpi[:,3]+simulationpi[:,4]+simulationpi[:,5]+simulationpi[:,6]+simulationpi[:,7]+simulationpi[:,8]
allsimulationdata = np.concatenate((simulationdatacalo, simulationdatapi), axis=0)
 

fig, ax = plt.subplots(1, 1, figsize=(8,8))
ax.hist(((phtot[120]*slope)+intercept), bins=100, histtype ='step',density = 'True',
        color = 'hotpink', alpha=0.6,
                          lw=2,
                          label=f'Experimental data 120 GeV')

ax.hist(allsimulationdata, bins = 100, 
          label = 'Simulation crystal on beam', histtype ='step',
          
          color = 'navy',alpha = 0.7, lw =2,density = 'True' )

#ax.legend(fontsize = 12)
ax.grid()
ax.set_yscale('log')
ax.set_xlabel('Lead Glasses PH [GeV]')
ax.set_ylabel('Entries')
#ax.legend()

ax.plot(120, 0.5*10**-1, '*', markersize =10)


#%%  calorimetro 
# fig, ax = plt.subplots(1, 1, figsize=(8,8))
# hph, binsph, _ = ax.hist(simulationCalototAmo, 
#         bins = 100, 
#         label = 'Simulation', histtype ='step', 
#         color = 'navy', alpha = 0.7, lw =3, density = 'True')
# max_indexsim = np.argmax(hph)

# exp_maxsim= (binsph[max_indexsim] + binsph[max_indexsim+1])/2.0

# plt.plot(exp_maxsim,np.max(hph), color='red', label = f'max sim = {round(exp_maxsim,2)}', marker= '*',  markersize = 10)

# # xfitrangeinflg  = 110
# # xfitrangesuplg  = 120
# # xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
# # phcondition =  ((xdata > xfitrangeinflg) & (xdata<xfitrangesuplg))
# # paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
# # resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
# #                     weights = np.sqrt(hph[phcondition]))

# # to_plot = np.linspace(xfitrangeinflg, xfitrangesuplg, 10000)

# # y_eval = model.eval(resultx.params, x= to_plot)

# # mulgfitsim = resultx.params['center'].value
# # errmulgfitsim = resultx.params['center'].stderr
# # sigmalgfit = resultx.params['sigma'].value
# # ax.plot(to_plot, y_eval, linewidth = 2, color = 'red', label = f'$\mu$ = {round(mulgfitsim,2)} $\pm$ {round(errmulgfitsim,2)}')


# hph1, binsph1, _ = ax.hist((calototAmo*slope)+intercept, 
#         bins = 100, 
#         label = 'Experimental data', histtype ='step', 
#         color = 'hotpink', alpha = 0.7, lw =3, density = 'True')
# max_index = np.argmax(hph1)

# exp_max= (binsph1[max_index] + binsph1[max_index+1])/2.0

# plt.plot(exp_max,np.max(hph1), color='black', label = f'max exp = {round(exp_max,2)}', marker= '*',  markersize = 10)


# # xfitrangeinflg  = 80
# # xfitrangesuplg  = 110
# # xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
# # phcondition =  ((xdata > xfitrangeinflg) & (xdata<xfitrangesuplg))
# # paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
# # resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
# #                     weights = np.sqrt(hph[phcondition]))

# # to_plot = np.linspace(xfitrangeinflg, xfitrangesuplg, 10000)

# # y_eval = model.eval(resultx.params, x= to_plot)

# # mulgfitexp = resultx.params['center'].value
# # errmulgfitexp = resultx.params['center'].stderr
# # sigmalgfitexp = resultx.params['sigma'].value
# # ax.plot(to_plot, y_eval, linewidth = 2, color = 'black', label = f'$\mu$ = {round(mulgfitexp,2)} $\pm$ {round(errmulgfitexp,2)}')



# ax.legend()
# ax.grid()
# ax.set_yscale('log')
# ax.set_xlabel('Full calorimeter PH [GeV]')
# ax.set_ylabel('Entries')
# plt.show()
# plt.tight_layout()








# 