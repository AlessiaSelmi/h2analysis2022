#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 29 12:26:01 2023

@author: ale
"""

from functionale import *
import numpy as np
import matplotlib.pyplot as plt
from lmfit.models import GaussianModel
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
import pickle
from lmfit.models import GaussianModel

model = GaussianModel()
plt.rcParams['font.size'] = '15'
#%%
# #apriamo i dati 60 GeV centrati su ciascun canale
datapath = '/home/ale/Desktop/dati2022H2/run520'
h5keys = ['xpos', 'digi_ph', 'digi_time']
energies = [120,100, 80, 60, 40, 20]
nrunsCC =  [124, 151, 150, 126, 132, 144]

LG_eqfact = {'TL': 0.7432132479651892,'TR': 0.9420004405379382,'CL': 0.6099601605385676,'CR': 0.9645596120969584,'BL': 1.6335469637749256,'BR': 0.8535219537299307,'CC': 1.0}

digichannel = [13, 14, 10, 12, 8, 9, 11]
lgchannels = {'BL': 8, 'BR': 9, 'CL': 10, 'CC': 11, 'CR': 12, 'TL': 13, 'TR': 14}
leadglasslabel = ['TL', 'TR', 'CL','CC', 'CR', 'BL', 'BR']
labels = ['T1', 'T1', 'T2', 'T2']
xlim = [[-4, 4]]*4
distancesfromT1 = [1590]
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]
zcalo = 1776
xposinf = 1.0
xpossup = 1.50
yposinf = 0.30
ypossup = 1.30
phtot = {}
ph = {}
pheff = {}
time = {}
rangeLGs = [[0,500], [0,200],[0,1000],[7000,12200],[0,2000],[0,1000],[0,1000]]
rangetime = [[0,250]]*7
ratio = {}
meanleakage = []
#%%
for j, nrun in enumerate(nrunsCC): 
    data = opendata(nrun, datapath, h5keys)
    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(data[0], 2, distancesfromT1,
                                                          p0divx, p0divy,
                                                          Rangex=[[-0.001,0.001]],
                                                          Rangey=[[-0.001,0.001]])
    datashift, shift = aligntracker(data[0], 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                                  distancesfromT1,
                                                                                   p0divx, p0divy,
                                                                                   Rangex=[[-0.001,0.001]],
                                                                                   Rangey=[[-0.001,0.001]], 
                                                                                   Color='navy', 
                                                                                   Figsize = (8,6))
    
    ax[0].set_ylim(0,1300)
    ax[1].set_ylim(0,1700)
    ax[0].axvline(x=-150*10**(-6), color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=+150*10**(-6), color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=-150*10**(-6), color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=+150*10**(-6), color="black", linestyle = '--', lw = 2)
    
    
    cutdiv = ((divexalign > -150*10**(-6)) & (divexalign < 150*10**(-6)) &
              (diveyalign > -150*10**(-6)) & (diveyalign < 150*10**(-6)))
    
    xcalo = datashift[:, 0] + zcalo*np.tan(divexalign)
    ycalo = datashift[:, 1] + zcalo*np.tan(diveyalign)
    fig, ax = plt.subplots(1, 1, figsize=(8, 4))
    zhist2d, xhist2d, yhist2d, mapable = ax.hist2d(xcalo[0], ycalo[0],
                                                   bins=[100, 100], cmap=plt.cm.jet, range=[[0, 2], [0, 2]])
    ax.set_xlabel(f'x [cm] Lead Glass CC')
    ax.set_ylabel(f'y [cm] Lead Glass CC')
    
    cutpos = ((xcalo > xposinf) & (xcalo < xpossup) &
              (ycalo > yposinf) & (ycalo < ypossup))
    rect = patches.Rectangle((xposinf, yposinf), (xpossup-xposinf), (ypossup-yposinf), linewidth=4,
                             edgecolor='red', facecolor='none',
                             label='Fiducial area')
    cuttime = ((data[2][:, lgchannels['CC']]>150) & ((data[2][:, lgchannels['CC']]<200)))
    ax.add_patch(rect)
    ax.grid()
    ax.legend()
    plt.show()
    condizione = (cutdiv & cuttime & cutpos)
    
    for lglabel in lgchannels:
        ph[lglabel] = (data[1][:, lgchannels[lglabel]][condizione[0]])
        time[lglabel] = (data[2][:, lgchannels[lglabel]][condizione[0]])
        pheff[lglabel] = (data[1][:, lgchannels[lglabel]])
        print(lglabel)
    
    fig = plt.figure(figsize = (10,8))
    ax1 = plt.subplot2grid((6, 6), (0, 1), colspan=2,rowspan=2)
    ax2 = plt.subplot2grid((6, 6), (0, 3), colspan=2,rowspan=2)
    ax3 = plt.subplot2grid((6, 6), (2, 0), colspan=2,rowspan=2)
    ax4 = plt.subplot2grid((6, 6), (2, 2), colspan=2,rowspan=2)
    ax5 = plt.subplot2grid((6, 6), (2, 4), colspan=2,rowspan=2)
    ax6 = plt.subplot2grid((6, 6), (4, 1), colspan=2,rowspan=2)
    ax7 = plt.subplot2grid((6, 6), (4, 3), colspan=2,rowspan=2)
    axes = [ax1,ax2,ax3,ax4,ax5,ax6,ax7]
    
    for k,ax in enumerate(axes):
        ax.hist(ph[leadglasslabel[k]], bins = 100, histtype ='stepfilled',
                                      color = 'lightpink', edgecolor= 'black',alpha = 0.7, 
                                      lw =2, label= f'{leadglasslabel[k]} ')
        
        
    
        ax.grid()
        ax.set_xlabel('PH [a.u]')
        ax.set_ylabel('entries')
        ax.set_title(f'energy {energies[j]}')
        fig.set_tight_layout('tight')
    plt.show()
    
    #%%
    sommaLGlateral = ph['BL']+ph['BR']+ph['CL']+ph['CR']+ph['TL']+ph['TR']
    sommaTot = ph['BL']+ph['BR']+ph['CL']+ph['CR']+ph['TL']+ph['TR']+ph['CC']
    
    ratio[nrun] = (sommaLGlateral/sommaTot)*100
    meanleakage.append(np.median(ratio[nrun]))
    fig, ax = plt.subplots(1,1, figsize=(6,6))
    ax.hist(ratio[nrun], bins = 100, histtype ='step',
                          color = 'hotpink', alpha = 1, label = 'Experimental data',
                          lw =2, range = [5,15])
    ax.grid()
    ax.set_xlabel('LG lateral leakage [$\%$], {energies[k]}')
    ax.set_ylabel('Counts')
    ax.legend()
    ax.set_ylim([0,800])
    fig.set_tight_layout('tight')
#%%
fig, ax = plt.subplots(1,1, figsize=(6,6))
ax.plot(energies, meanleakage, '.', markersize = 10)
ax.grid()
ax.set_xlabel('Energies GeV')
ax.set_ylabel('LG lateral leakage [$\%$]')
ax.legend()
ax.set_ylim([0,20])
fig.set_tight_layout('tight')



# #%%
# simulationfile = 'run14.dat'
# simulationpath = '/home/ale/Desktop/Dottorato/GeantSimulation/data'
# simulationdata = np.loadtxt(f'{simulationpath}/{simulationfile}', skiprows=1)
# with open(f'{simulationpath}/{simulationfile}', 'r') as f:
#     header = f.readline().strip().split('\t')
# runpi = 'run10.dat'
# simulationpi = np.loadtxt(f'{simulationpath}/{runpi}', skiprows=1)

# simulationdatacalo = simulationdata[:,9]+simulationdata[:,10]+simulationdata[:,11]+simulationdata[:,12]+simulationdata[:,13]+simulationdata[:,14]+simulationdata[:,15]
# simulationdatapi = simulationpi[:,2]+simulationpi[:,3]+simulationpi[:,4]+simulationpi[:,5]+simulationpi[:,6]+simulationpi[:,7]+simulationpi[:,8]
# allsimulationdata = np.concatenate((simulationdatacalo, simulationdatapi), axis=0)
 
# fig = plt.figure(figsize = (10,8))
# # ax1 = plt.subplot2grid((6, 6), (0, 1), colspan=2,rowspan=2)
# # ax2 = plt.subplot2grid((6, 6), (0, 3), colspan=2,rowspan=2)
# # ax3 = plt.subplot2grid((6, 6), (2, 0), colspan=2,rowspan=2)
# # ax4 = plt.subplot2grid((6, 6), (2, 2), colspan=2,rowspan=2)
# # ax5 = plt.subplot2grid((6, 6), (2, 4), colspan=2,rowspan=2)
# # ax6 = plt.subplot2grid((6, 6), (4, 1), colspan=2,rowspan=2)
# # ax7 = plt.subplot2grid((6, 6), (4, 3), colspan=2,rowspan=2)
# # axes = [ax1,ax2,ax3,ax4,ax5,ax6,ax7]

# for k in range(7):
#     fig, ax = plt.subplots(1, 1, figsize=(11, 6))
#     ax.hist(simulationdata[:,9+k], bins = 100, histtype ='stepfilled',
#                                   color = 'lightpink', edgecolor= 'black',alpha = 0.7, 
#                                   lw =2, )


    
    

#     ax.grid()
#     ax.set_xlabel('PH [a.u]')
#     ax.set_ylabel('entries')
#     ax.set_title(f'{9+k}')
#     fig.set_tight_layout('tight')
# plt.show()

# #%%
# somma7simulation = simulationdata[:,9]+simulationdata[:,10]+simulationdata[:,11]+simulationdata[:,12]+simulationdata[:,13]+simulationdata[:,14]+simulationdata[:,15]
# somma6simulation = simulationdata[:,9]+simulationdata[:,10]+simulationdata[:,11]+simulationdata[:,13]+simulationdata[:,14]+simulationdata[:,15]

# ratiosimulation = (somma6simulation/somma7simulation)*100



# fig, ax = plt.subplots(1,1, figsize=(6,6))
# ax.hist(ratio, bins = 100, histtype ='step',
#                       color = 'hotpink', alpha = 1, label = 'Experimental data',
#                       lw =2, range = [5,15])
# ax.grid()
# ax.set_xlabel('LG lateral leakage [$\%$]')
# ax.set_ylabel('Counts')
# ax.legend()
# ax.set_ylim([0,800])
# fig.set_tight_layout('tight')
# fig, ax = plt.subplots(1,1, figsize=(6,6))
# ax.hist(ratiosimulation, bins = 100, histtype ='step',
#                       color = 'navy', alpha = 1, label = 'Simulation',
#                       lw =2, range = [3,5])
# ax.grid()
# ax.set_xlabel('LG lateral leakage [$\%$]')
# ax.set_ylabel('Counts')
# ax.legend()
# fig.set_tight_layout('tight')

# #%%
# x01 = 2.51
# x02 = 1.265
# # Z1 = 30*0.39+90*0.55+(19*2+8)*0.02+30*0.03
# Z1 = 0.75*90+0.25*30
# Z2 = 8*0.156+14*0.081+22*0.008+33*0.003+82*0.752
# RM1 = 0.0265 *x01*(Z1 + 1.2)
# RM2 = 0.0265 *x02*(Z2 + 1.2)