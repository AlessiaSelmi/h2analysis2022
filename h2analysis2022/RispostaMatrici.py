#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  9 09:59:03 2023

@author: ale
"""


from functionale import *
import numpy as np
import matplotlib.pyplot as plt
from lmfit.models import GaussianModel
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
from lmfit.models import GaussianModel
model = GaussianModel()
## apriamo file random su fascio e vedo se cambia qualcosa nella risoluzione 
#della risposta del SiPM s eli leggo singoli o sommati
#su H2 leggi le due matrici assieme 
#%%
datapath = '/home/ale/Desktop/dati2022H2/run520'
h5keys = ['xpos', 'digi_ph', 'digi_time']
runnumberH2 = 155 # amorfo cristallo on beam
datacrystal = opendata(runnumberH2, datapath, h5keys)
sipmchannel = {'mat1': 4, 'mat2': 1, 'mat3':2, 'mat4':3}
labels = ['T1', 'T1', 'T2','T2']
#limite su asse x per hiostogrammi 1d
xlim = [[-4,4]]*4
histpos1d(datacrystal,2, labels, xlim)
#plot posizioni 2d
histpos2d(datacrystal,2, Range = [[0,2], [0,2]])
#calcolo divergenza + fit per trovare valore medio
distancesfromT1 = [1590]
#calcolo e plot divergenza
# divex, divey = divergence(data, 2, distancesfromT1)
p0divx = [500, 0, 100]
p0divy = [700, 0,100]
#dvergenze con  fit 
divex, divey, sigma, errsigma, mu, fig= divgaussianfit(datacrystal, 2, distancesfromT1,
                                            p0divx, p0divy, 
                                            Rangex = [[-800, 300]],
                                            Rangey=[[-500,500]])
#shift per allineamento camere
datashift, shift = aligntracker(datacrystal, 2, mu, distancesfromT1)

#plot e fit divergenze allineate
divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, _ = divgaussianfit(datashift, 2, 
                                                                                    distancesfromT1, 
                                                                                    p0divx, p0divy, 
                                                                                    Rangex = [[-600, 500]],
                                                                                    Rangey=[[-500,500]])
    
#%%
fig, ax = plt.subplots(2,2, figsize=(4,4))
ax = ax.flat
for digich in range(4):
    ax[digich].hist(datacrystal[1][:,sipmchannel[f'mat{digich+1}']],
                    bins= 100, label = f'SiPM PH mat{digich+1} ', histtype ='step', 
                            color = 'navy', alpha = 0.7, lw =2)
    ax[digich].grid()
    ax[digich].set_xlabel('PH [ADC]')
    ax[digich].set_ylabel('Entries')
    ax[digich].legend(fontsize = 10)
    ax[digich].set_yscale('log')
    plt.tight_layout
sipmtimethinf = 200
sipmtimethsup = 300
fig, ax = plt.subplots(1,1, figsize=(8,4))
timephplot(datacrystal[2][:,sipmchannel['mat2']], 
           datacrystal[1][:,sipmchannel['mat2']], 
           ax, timethresholdinf = sipmtimethinf, 
           timethresholdsup = sipmtimethsup, Range=[[0,400], [0,5000]])
fig, ax = plt.subplots(1,1, figsize=(8,4))
timephplot(datacrystal[2][:,sipmchannel['mat3']],
           datacrystal[1][:,sipmchannel['mat3']],
           ax, timethresholdinf = sipmtimethinf, 
           timethresholdsup = sipmtimethsup, Range=[[0,400], [0,5000]])

#%%
fig, ax = plt.subplots(1,1, figsize=(4,4))
ax.hist(datacrystal[1][:,sipmchannel[f'mat2']],
                bins= 100, label = 'Ringo ch3 + John ch3 ', histtype ='step', 
                        color = 'navy', alpha = 0.7, lw =2, density = True)
ax.hist(datacrystal[1][:,sipmchannel[f'mat3']],
                bins= 100, label = 'Paul ch2 + Paul ch3 ', histtype ='step', 
                        color = 'hotpink', alpha = 0.7, lw =2, density = True)
ax.hist(datacrystal[1][:,sipmchannel[f'mat3']]+datacrystal[1][:,sipmchannel[f'mat2']],
                bins= 100, label = 'Paul + Ringo ', histtype ='step', 
                        color = 'orangered', alpha = 0.7, lw =2, density = True)

ax.grid()
ax.set_xlabel('PH [a.u]')
ax.set_ylabel('Entries')
ax.set_yscale('log')
ax.legend(fontsize = 8)
plt.tight_layout


#%%
datapathBTF = '/home/ale/Desktop/Dottorato/BTF2023/data/run400'
runnumber = 395
data = opendata(runnumber, datapathBTF, h5keys)
mumatrix = []
errmumatrix = []

labelmat = ['John, mat2+mat3', 'Paul mat2+mat3', 'George, mat2+mat3', 'Ringo, mat2+mat3']

color = ['hotpink', 'limegreen', 'limegreen', 'hotpink']
colorfit = ['black']*4
fig, ax = plt.subplots(2,2, figsize=(10,10))
ax = ax.flat
for i in range(4):
    hph, binsph, _ = ax[i].hist(data[1][:,i],bins = 50, histtype ='stepfilled', 
                 color = color[i], alpha = 0.3, edgecolor = color[i],
                 lw =3, label=f'{labelmat[i]}')
    # ax.hist(data[1][:,3],bins=50, histtype ='stepfilled', 
    #              color = 'navy', alpha = 0.3, edgecolor='black', 
    #              lw = 2, label='mat3')
    
    xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
    phcondition =  ((xdata > 0) & (xdata<2500))
    paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
    resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                        weights = np.sqrt(hph[phcondition]))
    to_plot = np.linspace(0,2500,10000)
    energyadcfit = resultx.params['center'].value
    mumatrix.append(energyadcfit)
    energyadcfiterr = resultx.params['center'].stderr
    
    sigma = resultx.params['sigma'].value
    sigmaerr = resultx.params['sigma'].stderr

    y_eval = model.eval(resultx.params, x= to_plot)
    # ax.plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = Colorfit)
    ax[i].plot(to_plot, y_eval, linewidth = 2, color = colorfit[i], label = f'$\mu$ = {round(energyadcfit,2)} $\pm$ {round(energyadcfiterr,2)} \n $\sigma$ = {round(sigma,2)} $\pm$ {round(sigmaerr,2)}')
        
    ax[i].set_xlabel('PH [a.u.]')
    ax[i].set_ylabel('Entries')
    ax[i].legend(fontsize = 10)
    ax[i].grid()
plt.tight_layout()
plt.show()
eqfactor = []
for k in range(4):
    eqfactor.append(mumatrix[0]/mumatrix[k])

# fig, ax = plt.subplots(1,1, figsize=(8,8))
# for i in range(4):
#     ax.hist(data[1][:,i]*eqfactor[i],bins = 50, histtype ='step', 
#                   color = color[i], alpha = 0.7, 
#                   lw =3, label=f'mat{i} equalized')
#     ax.set_xlabel('PH [a.u.]')
#     ax.set_ylabel('Entries')
#     ax.grid()
#     ax.legend()
# plt.show()

SiPM = [data[1][:,0]*eqfactor[0] +data[1][:,3]*eqfactor[3], data[1][:,2]*eqfactor[2] + data[1][:,1]*eqfactor[1]]

# fig, ax = plt.subplots(1,1, figsize=(8,8))
# ax.hist((SiPM1-SiPM2),bins = 50, histtype ='step', 
#                color = 'navy', alpha = 0.7, 
#                lw =3)
# ax.set_xlabel('PH1-PH2')
# ax.set_ylabel('Entries')
# ax.grid()

#%%
labelsum = ['John + Ringo', 'Paul + George']
fig, ax = plt.subplots(1,2, figsize=(10,8))
for i in range(2):
    hph, binsph, _ = ax[i].hist(SiPM[i],bins = 50, histtype ='stepfilled', 
               color = color[i], alpha = 0.3,edgecolor=color[i], 
               lw =3, label = labelsum[i])
    xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
    phcondition =  ((xdata > 0) & (xdata<4000))
    paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
    resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                        weights = np.sqrt(hph[phcondition]))
    to_plot = np.linspace(0,4000,10000)
    energyadcfit = resultx.params['center'].value
    mumatrix.append(energyadcfit)
    energyadcfiterr = resultx.params['center'].stderr
    
    sigma = resultx.params['sigma'].value
    sigmaerr = resultx.params['sigma'].stderr

    y_eval = model.eval(resultx.params, x= to_plot)
    # ax.plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = Colorfit)
    ax[i].plot(to_plot, y_eval, linewidth = 2, color = colorfit[i], label = f'$\mu$ = {round(energyadcfit,2)} $\pm$ {round(energyadcfiterr,2)} \n $\sigma$ = {round(sigma,2)} $\pm$ {round(sigmaerr,2)}')
        

    ax[i].set_xlabel('PH [a.u]')
    ax[i].set_ylabel('Entries')
    ax[i].legend()
    ax[i].grid()
plt.tight_layout()
plt.show()
