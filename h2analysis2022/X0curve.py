#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 12:19:54 2023

@author: ale
"""
from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import pandas as pd
plt.rcParams['font.size'] = '15'
df = pd.read_csv(r'meshdata.csv', skiprows = 2)
#print(df)
ztotcm = 30
zbin = 3000
widthSlices = ztotcm/zbin
column_names = df.columns

# print the column names
print(column_names)

iZ = df[' iZ']+1

Zcm = iZ*widthSlices 

xX0 = Zcm / 0.89
#print(xX0)

Energy = df[' total(value) [MeV]'] / df[' entry']
Energy = Energy*10**(-3)

fig, ax = plt.subplots(1,1, figsize=(8,8))
ax.plot(xX0, np.cumsum(Energy), '.', color = 'navy', label = '$E_0$ = 120 GeV')
ax.set_xlabel('x/$X_0$')
ax.set_ylabel('Deposited energy [GeV]')
ax.legend()
ax.grid()



# define x and y arrays of data points
x = np.array([1, 2, 3, 4, 5])
y = np.array([2, 3, 5, 8, 13])

# define a new x value for extrapolation
x0_inter = 4.5

# perform linear interpolation of the data
Edep_amo = np.interp(x0_inter, xX0, np.cumsum(Energy))
print(f'Energy deposited in random = {Edep_amo} GeV')


Edep_axial = Edep_amo*3
print (f'Energy deposited in axial = {Edep_axial} GeV')

x0_axial = np.interp(Edep_axial, np.cumsum(Energy), xX0)
print(f'x/x0 in axial = {x0_axial}')

print(f'Thickness increase = {((x0_axial-x0_inter)/x0_inter)*100}%')
print(f'x0 effettiva = {x0_inter/x0_axial}')