#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 11 15:03:06 2023

@author: ale
"""

from functionale import *
import numpy as np
import matplotlib.pyplot as plt
from lmfit.models import GaussianModel
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
#set plt parameter
plt.rcParams['font.size'] = '20'
#%%
datapath = '/home/ale/Desktop/dati2022H2/run520'
h5keys = ['xpos', 'digi_ph', 'digi_time']
labels = ['T1', 'T1', 'T2', 'T2']
#ch1 --> mat 2 + ch 2--> mat3 crystal 
#ch3--> mat4 + ch4--> mat1 no cryst
sipmchannel = {'mat1': 4, 'mat2': 1, 'mat3':2, 'mat4':3}
leadglassrun = {'TL': 141, 'TR': 138 , 'CL': 142,
                'CR': 127,  'BL': 140, 'BR': 139,
                'CC': 126}
lgchannels = {'TL':13, 'TR': 14, 'CL': 10, 'CR': 12, 'BL': 8, 'BR': 9, 'CC': 11}



xlim = [[-4, 4]]*4
distancesfromT1 = [1590]
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]
energyeq = 60
divsup = 150*10**(-6)
divinf = -150*10**(-6)
zcalo = 1776

## Facciamo prova con retta calibrazione di ogni singolo LG 
slope_LGs = {'TL': 126.41688550260956,'TR': 102.31940331669799,'CL': 153.73438341897463,'CR': 102.0435615514145,'BL': 56.29948367760764,'BR': 110.4331941236628,'CC': 91.39814934473486}

intercept_LGs = {'TL': 209.36476917222066,
 'TR': 5.949215406803438,
 'CL': 211.54826261489714,
 'CR': -159.4175022327702,
 'BL': 107.63665271825012,
 'BR': 153.3549825092341,
 'CC': 171.51980659688613}

######## equalizzando prima tutti i canali e poi calibrando il full calorimeter 


LGeqfact = {'TL': 0.7432132479651892, 'TR': 0.9420004405379382, 'CL': 0.6099601605385676,'CR': 0.9645596120969584, 'BL': 1.6335469637749256, 'BR': 0.8535219537299307, 'CC': 1.0}
LG_mcalib = 1.002456552727584368e-02
LG_qcalib = -3.694493671411388513e-01

SiPM_slope_calib =  0.0018887847736972596 #l'intercetta è zero (hai fatto retta passante per zero)

runnumbers = [155, 153, 124] #random cristallo on beam # axial cristallo on beam + calibrazione 

data = {}
cutdiv = {}
PH_LGeq = {}
LG_PHGeVtot = {}
SiPM_PHGeV = {}
SiPM_mean = {}
LG_mean = {}
PH_LGsinglecalib = {}
PH_LGGeVfromcalib = {}
LG_calibmean = {}
for i, nrun in enumerate(runnumbers):
    print(i)
    data[nrun] = opendata(runnumbers[i], datapath, h5keys)
    xpos = data[nrun][0]
    #camere 
    xlabels = ['BC1',' BC1', 'BC2', 'BC2']
    xlim = [[0,2]]*4
    histpos1d(xpos, 2, xlabels = xlabels, xlim = xlim, Range = [0,2])
    histpos2d(xpos, numberoftrackers = 2, Range = [[0,2], [0,2]])
    #correlazioni camere 
    fig, ax = plt.subplots(1,2 , figsize=(8,8))
    ax = ax.flat
    ax[0].hist2d(xpos[:,0], xpos[:,2], bins = [100,100], range = [[0,2], [0,2]], cmap=plt.cm.jet)
    ax[0].set_ylabel('x BC2 [cm]')
    ax[0].set_xlabel('x BC1 [cm]')
    
    ax[1].hist2d(xpos[:,1],xpos[:,3] , bins = [100,100], range = [[0,2], [0,2]], cmap=plt.cm.jet,)
    ax[1].set_xlabel('y BC1 [cm]')
    ax[1].set_ylabel('x BC2 [cm]')
    plt.tight_layout()
    plt.show()
#%%
    #divergenze
    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(xpos, 2, distancesfromT1,
                                                           p0divx, p0divy,
                                                           Rangex=[[-0.001,0.001]],
                                                           Rangey=[[-0.001,0.001]])
    
    
    datashift, shift = aligntracker(xpos, 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                                   distancesfromT1,
                                                                                    p0divx, p0divy,
                                                                                    Rangex=[[-0.001,0.001]],
                                                                                    Rangey=[[-0.001,0.001]], 
                                                                                    Color='navy', 
                                                                                    Figsize = (8,6))
    cutdiv[nrun] = ((divexalign >divinf) & (divexalign<divsup) & (diveyalign>divinf) &(diveyalign<divsup))
    ax[0].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=divsup, color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=divinf, color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=divsup, color="black", linestyle = '--', lw = 2)   
  
#%%
    for label in (leadglassrun):
        PH_LGeq[label] = data[nrun][1][:,lgchannels[label]]*LGeqfact[label]
        PH_LGsinglecalib[label] = (data[nrun][1][:,lgchannels[label]]-intercept_LGs[label])/slope_LGs[label]
    LG_PHGeVtot[nrun] = (sum(PH_LGeq.values()))*LG_mcalib + LG_qcalib
    PH_LGGeVfromcalib[nrun] = sum(PH_LGsinglecalib.values())
        
    SiPM_PHGeV[nrun] = (data[nrun][1][:,sipmchannel['mat2']]+data[nrun][1][:,sipmchannel['mat3']])*SiPM_slope_calib
    SiPM_mean[nrun] = np.mean(SiPM_PHGeV[nrun][SiPM_PHGeV[nrun]>0.2])
    LG_mean[nrun] = np.mean(LG_PHGeVtot[nrun][LG_PHGeVtot[nrun]>50])
    LG_calibmean[nrun] = np.mean(PH_LGGeVfromcalib[nrun][PH_LGGeVfromcalib[nrun]>50])
#%%    
fig, ax = plt.subplots(1, 1, figsize=(10, 6))
ax.hist(SiPM_PHGeV[runnumbers[0]][SiPM_PHGeV[runnumbers[0]]>0.2], bins = 100, histtype ='step', alpha = 1, lw =3, 
        label = f'Random, mean = {round(SiPM_mean[runnumbers[0]],2)}', density = True, color = 'navy')
ax.hist(SiPM_PHGeV[runnumbers[1]][SiPM_PHGeV[runnumbers[1]]>0.2], bins = 100, histtype ='step', alpha = 1, lw =3, 
        label = f'Axial, mean = {round(SiPM_mean[runnumbers[1]],2)}', density = True, color = 'hotpink')


ax.legend()
ax.grid()
ax.set_xlabel('SiPM [GeV]')
ax.set_ylabel('Counts')
plt.tight_layout()
#%%
fig, ax = plt.subplots(1, 1, figsize=(10, 6))
ax.hist(LG_PHGeVtot[runnumbers[0]][LG_PHGeVtot[runnumbers[0]]>5], bins = 100, histtype ='step', alpha = 1, lw =3, 
        label = f'Random, mean = {round(LG_mean[runnumbers[0]], 2)}', density = True, color = 'navy',)
ax.hist(LG_PHGeVtot[runnumbers[1]][LG_PHGeVtot[runnumbers[1]]>5], bins = 100, histtype ='step', alpha = 1, lw =3, 
        label = f'Axial, mean = {round(LG_mean[runnumbers[1]],2)}', density = True, color = 'hotpink')


ax.legend()
ax.grid()
ax.set_xlabel('LG [GeV]')
ax.set_ylabel('Counts')
plt.tight_layout()

#%%
fig, ax = plt.subplots(1, 1, figsize=(10, 6))
ax.hist(PH_LGGeVfromcalib[runnumbers[0]][PH_LGGeVfromcalib[runnumbers[0]]>5], bins = 100, histtype ='step', alpha = 1, lw =3, 
        label = f'Random, mean = {round(LG_calibmean[runnumbers[0]], 2)}', density = True, color = 'navy',)
ax.hist(PH_LGGeVfromcalib[runnumbers[1]][PH_LGGeVfromcalib[runnumbers[1]]>5], bins = 100, histtype ='step', alpha = 1, lw =3, 
        label = f'Axial, mean = {round(LG_calibmean[runnumbers[1]],2)}', density = True, color = 'hotpink')


ax.legend()
ax.grid()
ax.set_xlabel('LG tia calib [GeV]')
ax.set_ylabel('Counts')
plt.tight_layout()
##%
print(f'missing energy random(120-E_SiPM-E_cal) = {120-SiPM_mean[runnumbers[0]]-LG_mean[runnumbers[0]]}')
print(f'missing energy Axial(120-E_SiPM-E_cal) = {120-SiPM_mean[runnumbers[1]]-LG_mean[runnumbers[1]]}')


print(f'missing energy random(120-E_SiPM-E_cal Tia calib ) = {120-SiPM_mean[runnumbers[0]]-LG_calibmean[runnumbers[0]]}')
print(f'missing energy Axial(120-E_SiPM-E_cal Tia calib) = {120-SiPM_mean[runnumbers[1]]-LG_calibmean[runnumbers[1]]}')
print(f'missing energy random(120-E_cal) = {120-LG_mean[runnumbers[0]]}')
print(f'missing energy Axial(120-E_cal) = {120-LG_mean[runnumbers[1]]}')
#%%
# ## prova con run di calibrazione 
# datacalibLG = opendata(124, datapath, h5keys)
# LG1eq = {}
# for label in (leadglassrun): 
#     LG1eq = datacalibLG[1][:,lgchannels[label]]*LGeqfact[label]


#%%
fig, ax = plt.subplots(1, 1, figsize=(10, 6))
ax.hist(PH_LGGeVfromcalib[runnumbers[2]][PH_LGGeVfromcalib[runnumbers[2]]>5], bins = 200, histtype ='step', alpha = 1, lw =3, 
        label = 'sigle LG calib', density = True, color = 'navy',)
ax.hist(LG_PHGeVtot[runnumbers[2]][LG_PHGeVtot[runnumbers[2]]>5], bins = 200, histtype ='step', alpha = 1, lw =3, 
        label = 'eq + calib', density = True, color = 'hotpink')

ax.plot(120, 0.075, '*', markersize = 20, label = '120 ')
ax.legend()
ax.grid()
ax.set_xlabel('LG [GeV]')
ax.set_ylabel('Counts')
plt.tight_layout()

#%% facciamo un bel plot da metetre perlo strong field


ratioAxRan = [3.120, 3.71, 2.630]
thickness = [1, 2, 4.5]
errRatio = [0.016, 0.012, 0.027]
fig, ax = plt.subplots(1, 1, figsize=(10, 6))
ax.errorbar(thickness, ratioAxRan,yerr= errRatio, marker = 'o', linestyle = '', color = 'navy',)
ax.grid()
ax.set_xlabel('Thickness [X_0]')
ax.set_ylabel('$E_{ax}/E_{rnd}$')
