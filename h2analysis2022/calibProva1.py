#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 28 17:27:51 2023

@author: ale
"""
from functionale import *
import numpy as np
import matplotlib.pyplot as plt
from lmfit.models import GaussianModel
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches


datapath = '/home/ale/Desktop/dati2022H2/run520'
h5keys = ['xpos', 'digi_ph', 'digi_time']
leadglasslabel = ['TL', 'TR', 'CL', 'CR', 'BL', 'BR', 'CC']
energies = [60,40,20]
labels = ['T1', 'T1', 'T2','T2']
xlim = [[-4,4]]*4
#canali digitizer associati ai label
digichannel = [13,14,10,12,8,9, 11]
leadglassrun = {'TL':[141,136,147], 'TR':[138,137,146], 'CL':[142,131,143], 
                'CR':[127,133,145], 'BL': [140,135,148], 'BR':[139,134,149], 
                 'CC': [124,151,150,126,132,144]}


distancesfromT1 = [1590]
p0divx = [500, 0, 100]
p0divy = [700, 0,100]

phleadglass = []
timeleadglass = []
energyadcall = []
slope = {}
errslope = {}
intercept = {}
errintercept = {}

timethresholdinf = [166, 166, 166, 163, 163, 163, 166, 164, 
                    164, 163, 163,163,163,163, 163,163,163, 
                    163, 163,163, 163, 163, 163, 163]
timethresholdsup = [175, 175, 175, 167, 167, 167, 175, 173, 
                    173, 167, 167,167, 167, 167, 167,167,167,
                    167, 167, 167, 167, 167, 167, 167]
#range fit gaussiana
xfitrangeinf = [7400,5000, 2300, 5800, 4000,1800, 8800,6100,
                2500, 5800, 3500, 1500, 3100, 2000, 0, 6400, 
                4300,2000,10800, 9000, 7000, 5500, 3500, 1800]
xfitrangesup = [8000, 6000, 3000, 6500,4500, 2200, 10000, 7300, 
                3800, 6200, 4500, 2500,3700, 3000, 8000, 7000,
                5000,2500, 11700, 9700, 7800, 6100, 4000, 2000]


xliminf = [4500, 2500, 1000,4000, 2000, 900, 5000, 4000, 0, 4000, 
           2000, 0, 2000, 0, 0, 2500,0, 0, 7000, 6000, 5000, 4000, 2000,1000]
xlimsup = [11000, 8000, 4000, 8000, 7000, 3000,14000, 10000, 6000,
           8000, 6000, 4000, 5000, 5000, 2000, 10000, 8000, 4000, 
           14000, 12000, 14000, 10000,6000,3000]
counter = 0 #mi serve per ciclare su fitrange


zcalo = 17.76
lgchannels = {'BL':8, 'BR':9, 'CL':10, 'CC':11, 'CR':12, 'TL':13, 'TR':14}
nomedicontrollo = {8:'BL', 9: 'BR', 10:'CL',11:'CC', 12:'CR', 13:'TL', 14:'TR'}
mpv = {}
eqfactor = {}
#%% 


for k, label in enumerate(leadglasslabel):
    energyadcall = []
    sigmaenergyadcall = []
    #la run del LG centrale ha sei energie 
    for j in range(len(leadglassrun[label])): 
        if len(leadglassrun[label]) == 6:
            energies = [120,100,  80, 60, 40, 20]
        print(k, label, j)
        #apro i dati di ciascun lead glass
        datacalorimeters = opendata(leadglassrun[label][j], datapath, h5keys)
        divex, divey, sigma, errsigma, mu, _= divgaussianfit(datacalorimeters[0], 2, distancesfromT1,
                                                p0divx, p0divy, 
                                                Rangex=[[-0.001,0.001]], 
                                                Rangey= [[-0.001,0.001]])
        datashift, shift = aligntracker(datacalorimeters[0], 2, mu, distancesfromT1)
        divexalign, diveyalign, sigmaalign, errsigmaalign, mualign,_ = divgaussianfit(datashift, 2, 
                                                                                        distancesfromT1, 
                                                                                        p0divx, p0divy, 
                                                                                        Rangex=[[-0.001,0.001]], 
                                                                                        Rangey= [[-0.001,0.001]], Color = 'navy')

        cutdiv = ((divexalign >-130) & (divexalign<130) & (diveyalign>-130) &(diveyalign<130))
        
 
        xcalo = datashift[:,0] + zcalo*np.tan(divexalign*10**-6)
        ycalo = datashift[:,1] + zcalo*np.tan(diveyalign*10**-6)
        fig, ax = plt.subplots(1,1, figsize=(8,4))
        zhist2d, xhist2d , yhist2d , mapable = ax.hist2d(xcalo[0], ycalo[0], 
                  bins =[100,100], cmap = plt.cm.jet, range = [[0,2], [0,2]])
        ax.set_xlabel(f'x [cm] Lead Glass {label}, {energies[j]} GeV')
        ax.set_ylabel(f'y [cm] Lead Glass {label}, {energies[j]} GeV')
    
        
        cutpos = ((xcalo>1.20) & (xcalo<1.30) & (ycalo>0.55)&(ycalo<1.20))
        rect = patches.Rectangle((1.20, 0.55), 0.10, 0.65, linewidth=3, 
                                 edgecolor='black', facecolor='none', 
                                 label = 'Crystal central area')
        ax.add_patch(rect)
        ax.grid()
        plt.show()        
        
        ph = datacalorimeters[1][:,digichannel[k]]
        figphcalib, axphcalib = plt.subplots(1,1, figsize=(8,8))
        # hph, binsph, _ = ax.hist(ph, bins = 100, 
        #                          label = (f'Lead Glass {label}, {energies[j]} GeV'))
        condition = cutdiv & cutpos
        
        fig, ax = plt.subplots(4,2, figsize=(10,6))
        ax = ax.flat
        for i in range (7):
            
            ax[i].hist(datacalorimeters[1][:,digichannel[i]][condition[0]],bins = 100,  histtype ='stepfilled',
            color = 'lightpink', edgecolor= 'black',alpha = 0.7, 
            lw =2, label = f'LG {nomedicontrollo[digichannel[i]]}')
            ax[i].set_title(f'Calibration run LG {label}, {energies[j]} GeV')
            ax[i].grid()
            ax[i].legend()
            plt.tight_layout()
        energyadc, sigmaenergyadc = calibgaussianfit(ph[condition[0]],axphcalib,Range=[xliminf[counter],xlimsup[counter]],
                                     xfitrangeinf = xfitrangeinf[counter], 
                                     xfitrangesup = xfitrangesup[counter], 
                                     Label = (f'Lead Glass {label}, {energies[j]} GeV'),
                                     Color='lightpink', Colorfit = 'black')
        if energies[j] == 60:
            mpv[label] = energyadc
        
        
        print(f'Lead Glass {label}, {energies[j]}: ADC value {energyadc}')
        axphcalib.axvline(x=xfitrangeinf[counter], color="black", linestyle = '--')
        axphcalib.axvline(x=xfitrangesup[counter], color="black", linestyle = '--')
        axphcalib.set_xlim([xliminf[counter], xlimsup[counter]])
        axphcalib.set_xlabel('PH (ADC)')
        axphcalib.set_ylabel('Entries')
        # energyadcall.append({label:energyadc})
        energyadcall.append(energyadc)
        sigmaenergyadcall.append(sigmaenergyadc)
        counter = counter+1
    sigmaenergyadcall = np.array(sigmaenergyadcall)  
    #plot retta calibrazione  

    fig, ax = plt.subplots(1,1, figsize=(8,8))
    from lmfit.models import LinearModel
    linearmodel = LinearModel()     
    linearparams = linearmodel.guess(energyadcall, x=energies)
    linearresult = linearmodel.fit(energyadcall, linearparams, x=energies, weights = sigmaenergyadcall)
    
    ax.errorbar(energies, energyadcall, yerr=sigmaenergyadcall, 
                marker='D',  linestyle = 'none', color = 'midnightblue',
                label = f'data Lead Glass {label}', markersize = 6)
    ax.plot(energies,linearresult.best_fit, color = 'red', lw = 2,
            label = f'Fit : ADC = ({round(linearresult.params["slope"].value,2)} $\pm$ {round(linearresult.params["slope"].stderr,2)})$\cdot$ GeV +  ({round(linearresult.params["intercept"].value,2)}  $\pm$ {round(linearresult.params["intercept"].stderr,2)})')
    ax.legend(fontsize = 14)
    ax.set_ylabel('ADC')
    ax.set_xlabel('Energy (GeV)')
    ax.grid()
    slope[label]=(linearresult.params['slope'].value)
    errslope[label] = (linearresult.params['slope'].stderr)
    intercept[label]= (linearresult.params['intercept'].value)
    errintercept[label]= (linearresult.params['intercept'].stderr)
    plt.show()
    
    
    
    
    
#%%
from lmfit.models import GaussianModel
energies120 = [120,100,  80, 60, 40, 20]
for i in leadglasslabel: 
    eqfactor[i] = mpv['CC']/mpv[i]

phcheq = {}
phtot={}
mufiteq = {}
errmufiteq = {}
for ene, nrun in enumerate(leadglassrun['CC']):
    dataequal= opendata(nrun,datapath, h5keys)
    divex, divey, sigma, errsigma, mu, _= divgaussianfit(dataequal[0], 2, distancesfromT1,
                                            p0divx, p0divy, 
                                            Rangex = [[-800, 300]],
                                            Rangey=[[-500,500]])
    datashift, shift = aligntracker(dataequal[0], 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign,_ = divgaussianfit(datashift, 2, 
                                                                                    distancesfromT1, 
                                                                                    p0divx, p0divy, 
                                                                                    Rangex = [[-600, 500]],
                                                                                    Rangey=[[-500,500]], Color = 'navy')

    cutdiv = ((divexalign >-130) & (divexalign<130) & (diveyalign>-130) &(diveyalign<130))
    

    xcalo = datashift[:,0] + zcalo*np.tan(divexalign*10**-6)
    ycalo = datashift[:,1] + zcalo*np.tan(diveyalign*10**-6)
    fig, ax = plt.subplots(1,1, figsize=(8,4))
    zhist2d, xhist2d , yhist2d , mapable = ax.hist2d(xcalo[0], ycalo[0], 
              bins =[100,100], cmap = plt.cm.jet, range = [[0,2], [0,2]])
    ax.set_xlabel(f'x [cm] Lead Glass {label}, {energies[j]} GeV')
    ax.set_ylabel(f'y [cm] Lead Glass {label}, {energies[j]} GeV')

    
    cutpos = ((xcalo>1.20) & (xcalo<1.30) & (ycalo>0.55)&(ycalo<1.20))
    rect = patches.Rectangle((1.20, 0.55), 0.10, 0.65, linewidth=3, 
                             edgecolor='black', facecolor='none', 
                             label = 'Crystal central area')
    ax.add_patch(rect)
    ax.grid()
    plt.show()   
    condizione = (cutdiv & cutpos)      
    for i in lgchannels:
        phcheq[i] = (dataequal[1][:,lgchannels[i]][condizione[0]])*eqfactor[i]
    phtot[energies120[ene]] = sum(phcheq.values())                         


fig, ax = plt.subplots(3,2, figsize=(10,6))
rangeeq = [[10000, 15000], [8000,12000], [5000,10000], [4000,10000], [3000, 6000],[ 1000, 5000]]
xfitrangeinf = [12000,9600,7500,6000,4000,1500]
xfitrangesup=[13000,10500,8900,7000,5000,3000]
ax = ax.flat 
model = GaussianModel()
for i, ene in enumerate(energies120):
    hph, binsph, _= ax[i].hist(phtot[ene],bins = 100, histtype ='stepfilled',
                          color = 'lightpink', edgecolor= 'black',alpha = 0.7, 
                          lw =2, 
                          label = f'{ene} GeV' , range = rangeeq[i])

    
    xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
    phcondition =  ((xdata > xfitrangeinf[i]) & (xdata<xfitrangesup[i]))
    paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
    resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                        weights = np.sqrt(hph[phcondition]))
    to_plot = np.linspace(xfitrangeinf[i],xfitrangesup[i],10000)
    y_eval = model.eval(resultx.params, x= to_plot)
    #ax[i].plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = 'red')
    ax[i].plot(to_plot, y_eval, linewidth = 2, color = 'black', label ='fit')
    mufiteq[ene] = resultx.params['center'].value
    errmufiteq[ene] = resultx.params['center'].stderr
    ax[i].legend()    
    ax[i].set_xlabel('Lead Glass PH tot [a.u.]')
    ax[i].set_ylabel('Entries')
    ax[i].grid()
plt.show()
    
mueq = []   
errmueq = []
for h in energies120: 
    mueq.append(mufiteq[h])
    errmueq.append(errmufiteq[h])


fig, ax = plt.subplots(1,1, figsize=(8,8))
from lmfit.models import LinearModel
linearmodel = LinearModel()     
linearparams = linearmodel.guess(energies120, x=mueq)
linearresult = linearmodel.fit(energies120, linearparams, x= mueq, weights = errmueq)

ax.errorbar( mueq, energies120, xerr= errmueq, 
            marker='D',  linestyle = 'none', color = 'midnightblue',
            label = f'data Lead Glass {label}', markersize = 6)
ax.plot(mueq,linearresult.best_fit, color = 'red', lw = 2,
        label = f'Fit : ADC = ({round(linearresult.params["slope"].value,2)} $\pm$ {round(linearresult.params["slope"].stderr,2)})$\cdot$ GeV +  ({round(linearresult.params["intercept"].value,2)}  $\pm$ {round(linearresult.params["intercept"].stderr,2)})')
ax.legend(fontsize = 14)
ax.set_ylabel('GeV')
ax.set_xlabel('PH [a.u.]')
ax.grid()
slopeeq=(linearresult.params['slope'].value)
errslopeeq = (linearresult.params['slope'].stderr)
intercepteq= (linearresult.params['intercept'].value)
errintercepteq= (linearresult.params['intercept'].stderr)
plt.show()
    
#%% 
    
fig, ax = plt.subplots(1,1, figsize=(8,8))
ph120GeV = phtot[120]*slopeeq+intercepteq
ax.hist(ph120GeV,bins = 100, histtype ='stepfilled',
                      color = 'lightpink', edgecolor= 'black',alpha = 0.7, 
                      lw =2, 
                      label = 'LG 120 GeV')
ax.plot(120,500, '*', markersize = 10)
ax.grid()
ax.legend()
ax.set_yscale('log')
plt.show()  
#%%  
xposinf = 1.25
xpossup = 1.30
yposinf = 0.75
ypossup = 1.10




datacalibcaloCC = opendata(124, datapath, h5keys)
phdatacalibcaloCC = {}

divexCC, diveyCC, sigmadivCC, errsigmaCC, muCC, _= divgaussianfit(datacalibcaloCC[0], 2, distancesfromT1,
                                          p0divx, p0divy, 
                                          Rangex = [[-800, 300]],
                                          Rangey=[[-500,500]])
datashiftCC, shiftCC = aligntracker(datacalibcaloCC[0], 2, muCC, distancesfromT1)
divexalignCC, diveyalignCC, sigmaalignCC, errsigmaalignCC, mualignCC,axdiv = divgaussianfit(datashiftCC, 2, 
                                                                                  distancesfromT1, 
                                                                                  p0divx, p0divy, 
                                                                                  Rangex = [[-600, 500]],
                                                                            Rangey=[[-500,500]], Color = 'navy')
for n in range(2):     
    axdiv[n].axvline(x=-150, color="red", linestyle = '--', lw = 2)
    axdiv[n].axvline(x=150, color="red", linestyle = '--', lw = 2)
    
cutdivCC = ((divexalignCC >-150) & (divexalignCC<150) & (diveyalignCC>-150) &(diveyalignCC<150))
xcaloCC = datashiftCC[:,0] + zcalo*np.tan(divexalignCC*10**-6)
ycaloCC = datashiftCC[:,1] + zcalo*np.tan(diveyalignCC*10**-6)
fig, ax = plt.subplots(1,1, figsize=(8,4))
zhist2d, xhist2d , yhist2d , mapable = ax.hist2d(xcaloCC[0], ycaloCC[0], 
          bins =[100,100], cmap = plt.cm.jet, range = [[0,2], [0,2]])
ax.set_xlabel(f'x [cm] Lead Glass CC, 120 GeV')
ax.set_ylabel(f'y [cm] Lead CC, 120 GeV')


cutposCC = ((xcaloCC>xposinf) & (xcaloCC<xpossup) & (ycaloCC>yposinf)&(ycaloCC<ypossup))
rect = patches.Rectangle((xposinf, yposinf), (xpossup-xposinf), (ypossup-yposinf), linewidth=3, 
                          edgecolor='black', facecolor='none', 
                          label = 'Crystal central area')
ax.add_patch(rect)
ax.grid()
plt.show()        
#%%#############################################################
conditionCC = (cutdivCC & cutposCC)
rangeCC = [[0,500], [0,1000],[0,500],[6000,12100],[0,1000],[0,300],[0,100]]
fig, ax = plt.subplots(4,2, figsize=(12,12))
ax = ax.flat
for i, ch in enumerate(lgchannels):
    phdatacalibcaloCC[ch] = datacalibcaloCC[1][:,lgchannels[ch]][conditionCC[0]]
    ax[i].hist(datacalibcaloCC[1][:,lgchannels[ch]], bins = 100, histtype ='stepfilled',
                          color = 'lightpink', edgecolor= 'black',alpha = 0.7, 
                          lw =2, 
                          label = f' no cut', range = rangeCC[i])
    
    ax[i].hist(datacalibcaloCC[1][:,lgchannels[ch]][conditionCC[0]], bins = 100, histtype ='stepfilled',
                          color = 'navy', edgecolor= 'black',alpha = 0.7, 
                          lw =2, 
                          label = f' cut',range = rangeCC[i])
    ax[i].grid()
    ax[i].legend()
    ax[i].set_yscale('log')
    ax[i].set_title(f'LG {ch}')
    fig.suptitle('Run 120 GeV on LG  CC', fontsize=16)
    plt.tight_layout()
    
    
phcalibcaloGeVCC = {}

for label in (lgchannels):
    phcalibcaloGeVCC[label] = (phdatacalibcaloCC[label]-intercept[label])/slope[label]
fig, ax = plt.subplots(1,1, figsize=(10,6))
phcalibtot = (sum(phcalibcaloGeVCC.values()))# tolgo 8 GeV per far tornare il picco a 120 GeV
hist, binedges, _ = ax.hist(phcalibtot, bins = 100, 
                      histtype ='stepfilled',
                      color = 'lightpink', edgecolor= 'black',alpha = 0.7, 
                      lw =2, 
                      label = 'Lg PH tot ')
ax.hist(phcalibcaloGeVCC['CC'], bins = 100, 
                      histtype ='step',
                      color = 'navy', alpha = 1
                      , 
                      lw =2, 
                      range = [0,200], label = 'Lg PH CC')
ax.set_yscale('log')
plt.plot(120, 3*10**3, '*', markersize=10, label = '120 GeV')
ax.legend(fontsize = 10)
ax.grid()
ax.set_xlabel('GeV')
plt.show()

#%% proviamo a fare la mappa di efficenza del calo con run a 120 GeV 

# seleffcalo = (datacalibcaloCC[1][:,lgchannels['CC']][conditionCC[0]])

# zhist2dcut, xhist2dcut , yhist2dcut , mapable  = ax.hist2d(xcryst[0][seleffsipm], 
#                                                            ycryst[0][seleffsipm] , 
#           bins =[100,100], cmap = plt.cm.jet, range = [[0,2], [0,2]])


# np.seterr(divide='ignore', invalid = 'ignore')
# eff = np.divide(zhist2dcut, zhist2d)
# eff[np.isnan(eff)]=0
# fig, ax = plt.subplots(1,1, figsize=(12,6))
# divider = make_axes_locatable(ax)
# cax = divider.append_axes('right', size='5%', pad=0.1)
# im = ax.imshow(np.flip(np.transpose(eff)), cmap=plt.cm.jet, extent=[0,2,0,2])
# fig.colorbar(im, cax=cax, orientation='vertical')
# rect = patches.Rectangle((xrectinf, yrectinf), xrectlenght, yrectlenght, linewidth=6, edgecolor='black', facecolor='none'
#                          , label = 'Crystal central area')
# ax.add_patch(rect)
# ax.set_xlim([0,2])
# ax.set_ylim([0,2])
# ax.set_xlabel('x [cm]', fontsize = 14)
# ax.set_ylabel('y [cm]', fontsize = 14)
# ax.set_title('Crystal efficiency')
# ax.legend()
# plt.show()
