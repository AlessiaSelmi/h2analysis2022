#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 29 16:25:10 2023

@author: ale
"""

from functionale import *
import numpy as np
import matplotlib.pyplot as plt
from lmfit.models import GaussianModel
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
import pickle

 ###########################################################################
###############################################################################
###################           Cristallo            ###########################
###############################################################################
##############################################################################
## apro i dati, allineo le camere, calcolo la divergenza e plot tempo PH 
plt.rcParams['font.size'] = '15'
datapath = '/home/ale/Desktop/dati2022H2/run520'
h5keys = ['xpos', 'digi_ph', 'digi_time']
labels = ['T1', 'T1', 'T2', 'T2']
xlim = [[-4, 4]]*4
distancesfromT1 = [1590]
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]
xcryst = []
ycryst = []
cutdivtime = []
charge_sharing = []
crystCC = []
crystLateral = []
zcryst = 1747
sipmchannel = {'mat1': 4, 'mat2': 1, 'mat3':2, 'mat4':3}
ratiocalo = []
lgch = {'BL':8, 'BR':9, 'CL':10, 'CC':11, 'CR':12, 'TL':13, 'TR':14}
phcaloGeV  = []
## quelle di run 'normale' è il back 
with open("lg_eqfactor.dat", "rb") as file:
    # Deserialize the data and load it into a new dictionary
    eqfactor = pickle.load(file)
    
with open("Calibration.dat", "rb") as file1:
    # Deserialize the data and load it into a new dictionary
    calibfactor = np.loadtxt(file1)

#%%
#apriamo la simulation 
#elettroni con i nove cristalli 
simulationfile = 'run13.dat'

simulationpath = '/home/ale/Desktop/Dottorato/GeantSimulation/data/'
simulationdata = np.loadtxt(f'{simulationpath}/{simulationfile}', skiprows=1)
with open(f'{simulationpath}/{simulationfile}', 'r') as f:
    header = f.readline().strip().split('\t')
    
    

#############################################
#2000 eventi presi dalla run di simulazione dei pioni (run01)
runpi = 'run10.dat'
simulationpi = np.loadtxt(f'{simulationpath}/{runpi}', skiprows=1)
print(header)

### per legegre file simulazione dopo run 10
simulationdatacalo = simulationdata[:,9]+simulationdata[:,10]+simulationdata[:,11]+simulationdata[:,12]+simulationdata[:,13]+simulationdata[:,14]+simulationdata[:,15]
simulationdatapi = simulationpi[:,2]+simulationpi[:,3]+simulationpi[:,4]+simulationpi[:,5]+simulationpi[:,6]+simulationpi[:,7]+simulationpi[:,8]
LGs = np.concatenate((simulationdatacalo, simulationdatapi), axis=0)
#LG_lateral = np.concatenate((sum_LG, simulationdatapi), axis=0)
SiPMCC_sim = simulationdata[:,0]
SiPM_lateral = np.concatenate((simulationdata[:,1],simulationpi[:,1]), axis=0)
Sum8SiPM_sim= simulationdata[:,1]+simulationdata[:,2]+simulationdata[:,3]+simulationdata[:,4]+simulationdata[:,5]+simulationdata[:,6]+simulationdata[:,7]+simulationdata[:,8]
#%%
# apro amorfo e asse  in config back, calcolo la divergenza, allineamento e tagli 
runs = [155,153] 
for i, nrun in enumerate(runs):
    data = opendata(nrun, datapath, h5keys)
    divex, divey = divergence(data[0], numberoftrackers = 2, distancesfromT1=[15.90])
    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(data[0], 2, distancesfromT1,
                                                      p0divx, p0divy, Rangex=[[-0.001,0.001]], 
                                                      Rangey= [[-0.001,0.001]])
    datashift, shift = aligntracker(data[0], 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                              distancesfromT1,
                                                                                p0divx, p0divy,
                                                                                Color='navy', Figsize = (8,6), Rangex=[[-0.001,0.001]], 
                                                                                Rangey= [[-0.001,0.001]])

    x_cryst = datashift[:,0] + zcryst*np.tan(divexalign)
    y_cryst = datashift[:,1] + zcryst*np.tan(diveyalign)
    xcryst.append(x_cryst[0])
    ycryst.append(y_cryst[0])
    ax[0].axvline(x=-150*10**(-6), color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=+150*10**(-6), color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=-150*10**(-6), color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=+150*10**(-6), color="black", linestyle = '--', lw = 2)
    cutdive = ((divexalign>-150*10**(-6))&(divexalign<150*10**(-6)) & (diveyalign>-150*10**(-6))&(diveyalign<150*10**(-6)))
    # vado ad osservare i SiPM, vedo se sono correlate le risposte (se stanno sulla bisettrice)

    fig, ax = plt.subplots(1, 1, figsize=(8,8))
    timephplot(data[2][:,sipmchannel['mat2']], (data[1][:,sipmchannel['mat2']]), ax, 200, 300)
    fig, ax = plt.subplots(1, 1, figsize=(8,8))
    timephplot(data[2][:,sipmchannel['mat3']], (data[1][:,sipmchannel['mat3']]), ax, 200, 300)

    
    cuttime = ((data[2][:,sipmchannel['mat3']]>200) & (data[2][:,sipmchannel['mat3']]<300) & (data[2][:,sipmchannel['mat2']]>200) & (data[2][:,sipmchannel['mat2']]<300))
    ## vediamo le correlzioni
    fig, ax = plt.subplots(1, 1, figsize=(8,8))
    ax.hist2d(data[1][:,sipmchannel['mat2']],data[1][:,sipmchannel['mat3']], 
              bins =[100,100], cmap = plt.cm.jet, norm=colors.LogNorm())

    ax.plot(data[1][:,sipmchannel['mat2']], data[1][:,sipmchannel['mat2']], '--', color = 'black', label = 'y = x')
    ax.set_xlabel('SiPM mat2')
    ax.set_ylabel('SiPM mat3')
    ax.set_title(f'Run {nrun}')
    ax.legend()
    ax.grid()
    plt.show()
# #
    cond = cutdive[0] & cuttime[0]
    cutdivtime.append(cond) #and di divergenza e time (che è and dei tempi delle due matrici)
    fig, ax = plt.subplots(1, 1, figsize=(8,8))
    ax.hist(data[1][:,sipmchannel['mat2']][cond], bins = 100, range = [0,8000], 
            label = 'mat2', histtype ='step', 
            color = 'lime', alpha = 0.7, lw =3)
    ax.hist(data[1][:,sipmchannel['mat3']][cond], bins = 100, range = [0,8000], 
            label = 'mat3', histtype ='step', 
            color = 'orangered', alpha = 0.7, lw =3)
    ax.legend()
    ax.grid()
    ax.set_yscale('log')
    ax.set_xlabel('PH [a.u]')
    ax.set_ylabel('Entries')
    ax.set_title(f'Run {nrun}')
    plt.tight_layout()
    ## direi che sono praticamente equalizzate, sommiamo
    fig, ax = plt.subplots(1, 1, figsize=(8,8))
    ax.hist(data[1][:,sipmchannel['mat2']][cond]+data[1][:,sipmchannel['mat3']][cond], 
            bins = 100, 
            label = 'mat2+mat3', histtype ='step', 
            color = 'gainsboro', alpha = 0.7, lw =3)
    ax.legend()
    ax.grid()
    ax.set_yscale('log')
    ax.set_xlabel('PH [a.u]')
    ax.set_ylabel('Entries')
    plt.tight_layout()


    ## carichiamo i LGs
    lgs= {}
    for lglabel in lgch:
        lgs[lglabel] = data[1][:,lgch[lglabel]]*eqfactor[lglabel]
    fullcalo = (sum(lgs.values()))
    phcaloGeV.append(((fullcalo*calibfactor[0]) + calibfactor[2]))

    sumdata = data[1][:,sipmchannel['mat2']]+data[1][:,sipmchannel['mat3']]
    sumdatacryst2 = data[1][:,sipmchannel['mat1']]+data[1][:,sipmchannel['mat4']]
    diffdata = data[1][:,sipmchannel['mat2']]-data[1][:,sipmchannel['mat3']]
    crystCC.append(sumdata) ## conterrà amorfo e asse
    crystLateral.append(sumdatacryst2)
    # ratio = diffdata/sumdata
    # charge_sharing.append(ratio)
    
#%% CONFRONTO CON I DATI DELLA SIMULAZIONE --> cristallo centrale amorfo 
fig, ax = plt.subplots(1,1, figsize=(8,6))
ax2 = ax.twiny()


h, bins = np.histogram(crystCC[0][cutdivtime[0]], bins = 100, range = [0,9000])
binc = bins[:-1] + (bins[1] - bins[0])/2
lns1 = ax.plot(binc, h/np.sum(h), ds = "steps-mid",
        label = 'Experimental data, Random', color = 'gainsboro', alpha = 1, lw = 2)


h, bins = np.histogram(SiPMCC_sim, bins = 100)
binc = bins[:-1] + (bins[1] - bins[0])/2
lns2 = ax2.plot(binc, h/np.sum(h), ds = "steps-mid",
        label = 'Simulation', color = 'navy', alpha = .8, lw = 3)



lns = lns1+lns2
labs = [l.get_label() for l in lns]
ax.legend(lns, labs, loc=0)
plt.tight_layout()
ax2.set_xlabel('$PH\;crystall_{CC} [GeV]$ ')
ax.set_xlabel('$PH\;crystall_{CC} [a.u]$ ')
ax2.set_yscale('log')
ax.set_yscale('log')
# ax2.legend()
#ax.legend()
ax.grid()
plt.tight_layout()
plt.show()   
    
#%%
#prova calibrazione SiPM --> prendo mu gaussiana della simulazione e dell'exp e faccio
#una retta di calibarzione per due punti (0 e exp vs sim)
## proviamo a fittare con una gaussina
from lmfit.models import GaussianModel

model = GaussianModel()
limInf = 2000
limSup = 4000



#fit experimental data 
fig, ax = plt.subplots(1,1, figsize=(8,6))


h, bins, _ = ax.hist(crystCC[0][cutdivtime[0]], bins = 100, range = [0,9000], histtype ='step', 
            color = 'gainsboro', alpha = 1, lw =2, 
            label = 'Experimental data, Random')
x = np.array([(bins[k+1]+bins[k])/2 for k in range(len(bins)-1)])
condition= ((x>limInf) & (x<limSup))
params = model.guess(h[condition], x=x[condition])
result = model.fit(h[condition], params, x=x[condition], weights=np.sqrt(h[condition]))
print(f'Chi-square = {result.chisqr:.4f}, Reduced Chi-square = {result.redchi:.4f}')
#print(result.fit_report())
mpvExp = (result.params['center'].value)
errmpvexp  = result.params["center"].stderr
to_plot = np.linspace(0,8000,10000)
ax.plot(x[condition], result.best_fit, color = 'black', lw = 2, label=f'mpv = {round(mpvExp,2)} $\pm$ {round(result.params["center"].stderr,2)}')
ax.legend(fontsize = 20)
ax.set_xlabel('PH [ADC]')
ax.set_ylabel('Counts')
ax.set_yscale('log')
ax.legend()
plt.tight_layout()
ax.set_xlabel('$PH\;crystall_{CC} [a.u]$ ')
ax.grid()

plt.tight_layout()


# fit simulation data
fitPar = [3000,5,10]
limInf = 3
limSup = 8

fig, ax = plt.subplots(1,1, figsize=(8,6))
h, bins, _ = ax.hist(SiPMCC_sim, bins = 100,  histtype ='step', 
            color = 'navy', alpha = 1, lw =2, 
            label = 'Simulation, Random')

x = np.array([(bins[k+1]+bins[k])/2 for k in range(len(bins)-1)])
condition= ((x>limInf) & (x<limSup))
params = model.make_params(A = fitPar[0], mpv = fitPar[1], width = fitPar[2])
result = model.fit(h[condition], params, x=x[condition], weights=np.sqrt(h[condition]))
print(f'Chi-square = {result.chisqr:.4f}, Reduced Chi-square = {result.redchi:.4f}')
#print(result.fit_report())
mpvSim = (result.params['center'].value)

to_plot = np.linspace(0,3000,10000)
ax.plot(x[condition], result.best_fit, label=f'mpv = {round(mpvSim,2)} $\pm$ {round(result.params["center"].stderr,2)}', color = 'red', lw = 2)
ax.legend(fontsize = 20)
ax.set_xlabel('PH [ADC]')
ax.set_ylabel('Counts')

ax.legend()
plt.tight_layout()
ax.set_xlabel('$PH\;crystall_{CC} [GeV]$ ')
ax.set_yscale('log')
ax.grid()
plt.tight_layout()
plt.show()

#%% fit lineare retta calibrazione per due punti 
mpvexp = [0, mpvExp]
mpvsim = [0, mpvSim]
## facciamo calibrazione per un punto 
fig, ax = plt.subplots(1,1, figsize=(6,6))
from lmfit.models import LinearModel
linearmodel = LinearModel()     
linearparams = linearmodel.guess(mpvexp, x=mpvsim)
linearresult = linearmodel.fit(mpvsim, linearparams, x= mpvexp)

ax.errorbar(mpvexp, mpvsim , xerr= errmpvexp , 
            marker='D',  linestyle = 'none', color = 'navy',
            markersize = 6)
ax.plot(mpvexp,linearresult.best_fit, color = 'red', lw = 2,
        label = f'GeV= ({round(linearresult.params["slope"].value,4)} $\pm$ {round(linearresult.params["slope"].stderr,4)})$\cdot$ ADC +  ({round(linearresult.params["intercept"].value,3)}  $\pm$ {round(linearresult.params["intercept"].stderr,3)})')
ax.legend(fontsize = 10)
ax.set_ylabel('GeV')
ax.set_xlabel('PH [a.u.]')
ax.grid()
print(f'Chi-square = {linearresult.chisqr:.4f}, Reduced Chi-square = {linearresult.redchi:.4f}')
plt.show()
slope=(linearresult.params['slope'].value)
errslope = (linearresult.params['slope'].stderr)
intercept= (linearresult.params['intercept'].value)
errintercept= (linearresult.params['intercept'].stderr)


# with open('H2_LG_CalibrationOREO23_2V.dat','w') as out_file:
#         np.savetxt(out_file, calib)
#%% vediamo in energia a quanto siamo 

fig, ax = plt.subplots(1,1, figsize=(10,6))
ax.hist(crystCC[1]*slope,  bins = 100, histtype ='step', 
            color = 'indigo', alpha = 1, lw =2, 
            label = 'Axial', 
            density = True, range = [0,30])
ax.hist(crystCC[0]*slope, bins = 100, histtype ='step', 
            color = 'teal', alpha = 1, lw =2, 
            label = 'Amorphous', 
            density = True, range = [0,30])



ax.grid()
ax.set_yscale('log')
ax.legend(fontsize = 20)
ax.set_xlabel('PH [GeV]')
ax.set_ylabel('Entries')
#%%
#%% 8 cristalli laterali ADC
fig, ax = plt.subplots(1,1, figsize=(8,6))
ax2 = ax.twiny()


h, bins = np.histogram(8*crystLateral[0], bins = 100, density = True)
binc = bins[:-1] + (bins[1] - bins[0])/2
lns1 = ax.plot(binc, h/np.sum(h), ds = "steps-mid",
        label = 'Experimental data', color = 'gainsboro', alpha = 1, lw = 2)


h, bins = np.histogram(Sum8SiPM_sim, bins = 100,  density = True)
binc = bins[:-1] + (bins[1] - bins[0])/2
lns2 = ax2.plot(binc, h/np.sum(h), ds = "steps-mid",
        label = 'Simulation', color = 'navy', alpha = .8, lw = 3)



lns = lns1+lns2
labs = [l.get_label() for l in lns]
ax.legend(lns, labs, loc=0)
plt.tight_layout()
ax2.set_xlabel('$\\sum \;PH\;crystals_{Lateral}\;\;\;  [GeV]$')
ax.set_xlabel('$8\cdot \;PH\;crystal_{Lateral}\;\;\; [a.u]$ ')
ax2.set_yscale('log')
ax.set_yscale('log')
# ax2.legend()
#ax.legend()
ax.grid()
plt.tight_layout()
plt.show()
plt.show()


#%% 8 cristalli laterali in energia ---> 
## non stai facendo tagli quindi potersti avere particelle da 120 che arrivano 
#dirette nel cristallo a fianco--> fai tagli ; anyway, stai applicando dei fattori di calibrazione 
# del primo cristallo al secondo e questi nopn sono equalizzati, quindi potrebbero non avere gli 
#stessi fattori --> cerca run con fascio sul secondo critsallo e confronta 
fig, ax = plt.subplots(1,1, figsize=(8,6))
ax.hist(8*crystLateral[0]*slope, bins = 100, histtype ='step', 
            color = 'gainsboro', alpha = 1, lw =2, 
            label = '$experimental\; \;\;8\cdot sum \;PH\;crystals_{Lateral}$', 
            density = True, range = [0,30])

ax.hist(Sum8SiPM_sim, bins = 100, histtype ='step', 
            label = '$simulation  \\sum \;PH\;crystal_{Lateral}$', color = 'navy', alpha = .8, lw = 3, 
            density = True, range = [0,30])


ax.grid()
ax.set_yscale('log')
ax.legend(fontsize = 10)
ax.set_xlabel('PH [GeV]')
ax.set_ylabel('Entries')
#%% somma cristallo centrale e otto laterali adc

fig, ax = plt.subplots(1,1, figsize=(8,6))
ax2 = ax.twiny()


h, bins = np.histogram(crystCC[0]+8*crystLateral[0], bins = 100, range = [0,9000])
binc = bins[:-1] + (bins[1] - bins[0])/2
lns1 = ax.plot(binc, h/np.sum(h), ds = "steps-mid",
        label = 'Experimental data, Random', color = 'gainsboro', alpha = 1, lw = 2)


h, bins = np.histogram(SiPMCC_sim + Sum8SiPM_sim, bins = 100)
binc = bins[:-1] + (bins[1] - bins[0])/2
lns2 = ax2.plot(binc, h/np.sum(h), ds = "steps-mid",
        label = 'Simulation, Random', color = 'navy', alpha = .8, lw = 3)



lns = lns1+lns2
labs = [l.get_label() for l in lns]
ax.legend(lns, labs, loc=0)
plt.tight_layout()
ax2.set_xlabel('$PH\;crystall_{CC}\;+\\sum \;PH\;crystals_{Lateral}\;\;\;  [GeV]$ ')
ax.set_xlabel('$PH\;crystall_{CC}\;+\;8\cdot \;PH\;crystal_{Lateral}\;\;\;  [a.u]$ ')
ax2.set_yscale('log')
ax.set_yscale('log')
# ax2.legend()
#ax.legend()
ax.grid()
plt.tight_layout()
plt.show()
#%% somma cristallo centrale e otto laterali in energia 



fig, ax = plt.subplots(1,1, figsize=(8,6))
ax.hist(crystCC[0]*slope+(8*crystLateral[0]*slope), bins = 100, histtype ='step', 
            color = 'gainsboro', alpha = 1, lw =2, 
            label = '$experimental\;crystall_{CC}\;+\\sum \;PH\;crystals_{Lateral}$', 
            density = True, range = [0,30])

ax.hist(SiPMCC_sim + Sum8SiPM_sim, bins = 100, histtype ='step', 
            label = '$simulation \;crystall_{CC}\;+\;8\cdot \;PH\;crystal_{Lateral}$', color = 'navy', alpha = .8, lw = 3, 
            density = True, range = [0,30])


ax.grid()
ax.set_yscale('log')
ax.legend(fontsize = 10)
ax.set_xlabel('PH [GeV]')
ax.set_ylabel('Entries')


#%%
  
##############################################################################
##############################################################################
##############################################################################
#################          RAPPORTI           ################################
#%%  ##   PH cristallo centrale / PH cristalli laterali  amorfo e asse 
ratio = (crystCC[0][cutdivtime[0]])/(8*crystLateral[0][cutdivtime[0]])
ratio[ratio == -np.inf] = 0
ratio[ratio == +np.inf] = 0
ratio[ratio == np.NaN] = 0

ratioaxial = crystCC[1][cutdivtime[1]]/(8*crystLateral[1][cutdivtime[1]])
ratioaxial[ratioaxial == -np.inf] = 0
ratioaxial[ratioaxial == +np.inf] = 0
fig, ax = plt.subplots(1,1, figsize=(10,6))
ax.hist(ratioaxial, bins = 100, histtype ='step', 
            color = 'indigo', alpha = 1, lw =2, 
            label = 'Axial', 
            density = True, range = [0,20])
ax.hist(ratio, bins = 100, histtype ='step', 
            color = 'teal', alpha = 1, lw =2, 
            label = '$Amorphous$', 
            density = True, range = [0,20])

ax.grid()
ax.set_yscale('log')
ax.legend(fontsize= 20)
ax.set_xlabel('$PH\; crystal_{Central}\;\;/\;\;8\cdot PH\; crystal_{Lateral}$')
ax.set_ylabel('Entries')

plt.tight_layout()



#%% 8 cristalli laterali 
fig, ax = plt.subplots(1,1, figsize=(8,6))
ax2 = ax.twiny()


h, bins = np.histogram(8*crystLateral[0], bins = 100, density = True)
binc = bins[:-1] + (bins[1] - bins[0])/2
lns1 = ax.plot(binc, h/np.sum(h), ds = "steps-mid",
        label = 'Experimental data', color = 'gainsboro', alpha = 1, lw = 2)


h, bins = np.histogram(Sum8SiPM_sim, bins = 100,  density = True)
binc = bins[:-1] + (bins[1] - bins[0])/2
lns2 = ax2.plot(binc, h/np.sum(h), ds = "steps-mid",
        label = 'Simulation', color = 'navy', alpha = .8, lw = 3)



lns = lns1+lns2
labs = [l.get_label() for l in lns]
ax.legend(lns, labs, loc=0)
plt.tight_layout()
ax2.set_xlabel('$\\sum \;PH\;crystals_{Lateral}\;\;\;  [GeV]$')
ax.set_xlabel('$8\cdot \;PH\;crystal_{Lateral}\;\;\; [a.u]$ ')
ax2.set_yscale('log')
ax.set_yscale('log')
# ax2.legend()
#ax.legend()
ax.grid()
plt.tight_layout()
plt.show()
plt.show()





#%%
#rapporto canale centrale e otto laterali 

    
fig, ax = plt.subplots(1,1, figsize=(10,6))
ax.hist((crystCC[0][crystLateral[0]>0]/(8*crystLateral[0][crystLateral[0]>0])), bins = 100, histtype ='step', 
            color = 'gainsboro', alpha = 1, lw =2, 
            label = 'Experimental data', 
            density = True, range = [0,200])
ax.hist(SiPMCC_sim[(Sum8SiPM_sim<1000)&(Sum8SiPM_sim>0)]/Sum8SiPM_sim[(Sum8SiPM_sim<1000) & (Sum8SiPM_sim>0)], bins = 100, histtype ='step', 
           color = 'navy', alpha = 1, lw =2, 
            label = 'Simulation', 
            density = True, range = [0,200])
SiPMCC_sim


ax.grid()
ax.set_yscale('log')
ax.legend(fontsize = 20)
ax.set_xlabel('$PH\; crystal_{Central}\;\;/\;\; PH\; crystals_{Lateral}$')
ax.set_ylabel('Entries')



################################################################################
#%%# ################Spettri da asse a amorfo SiPM e calorimetri ##########
# # LENTO--> LE APRE TUTTE MENTRE PLOTTA 
# ##############################################################################
###############################################################################
# Apriamo i fattori di equalizzazione del calorimetro


#%%   sia cristallo che calorimetro
plt.rcParams['font.size'] = '20'
lgch = {'BL':8, 'BR':9, 'CL':10, 'CC':11, 'CR':12, 'TL':13, 'TR':14}

#runscan = {'Axial': 153,  '1 $\u03F4_0$': 157, 'Plane':168, '8 $\u03F4_0$' :160, '17 $\u03F4_0$':162, 'Random': 155}
runscan = { 'Random': 155, '17 $\u03F4_0$':162, '8 $\u03F4_0$' :160, 'Plane':168, '1 $\u03F4_0$': 157,'Axial': 153}
#colors = ['gainsboro', 'navy', 'lime', 'orangered', 'turquoise', 'indigo']
colors = ['teal', 'limegreen', 'indigo', 'orangered', 'magenta', 'deepskyblue']
fig, ax = plt.subplots(1,1, figsize=(10,6))
#fig1, ax1 = plt.subplots(1,1, figsize=(10,8))
for i, run in enumerate(runscan.keys()):
    datascan = opendata(runscan[run], datapath, h5keys)
    phSiPM = (datascan[1][:,sipmchannel['mat2']]+datascan[1][:,sipmchannel['mat3']])
    ax.hist(phSiPM[phSiPM>100]*slope, bins = 100, histtype ='step', 
                color = colors[i], alpha = 1, lw =3, label = f'{run}', density = True)
    
    ## calorimetro, prioma devo equalizzare e sommare i canali 

    # lgs= {}
    # for lglabel in lgch:
    #     lgs[lglabel] = datascan[1][:,lgch[lglabel]]*eqfactor[lglabel]
    # fullcaloscan = (sum(lgs.values()))
    # phcaloGeVscan = ((fullcaloscan*calibfactor[0]) + calibfactor[2])
    # ax1.hist(phcaloGeVscan, bins = 100, histtype ='step', 
    #           color = colors[i], alpha = 1, lw =2, label = f'{run} calo PH', density = True, range = [0,140])
 

ax.legend()
ax.grid()
#ax1.grid()
ax.set_xlabel('Energy deposited [GeV]')
ax.set_ylabel('Normalised counts')
#ax1.set_xlabel('PH calo [GeV]')
plt.tight_layout()
plt.savefig('/home/ale/Desktop/Dottorato/Technical Note Oreo2023/H22022+SFevidence/H22022enegydepositedVSangle.pdf')
plt.show()


# #%% ATTENZIONE CHE SOPRA HANNO GLI STESSI NOMI E FAI LA STESSA COSA
# # #guardiamo il calorimetro --> PH LG centrale / PH LG laterali  già uqualizzati amorfo e asse

#     ax1.hist(phcaloGeV, bins = 100, histtype ='step', 
#               color = colors[i], alpha = 1, lw =2, label = f'{run} calo PH', density = True, range = [0,140])
 

# fullcalo = []

# for i, nrun in enumerate(runs):
#     data = opendata(nrun, datapath, h5keys)
#     lgs= {}
#     for lglabel in lgch:
#         lgs[lglabel] = data[1][:,lgch[lglabel]]*eqfactor[lglabel]
#     ratiocalo.append(lgs['CC']/(lgs['BL']+lgs['CL']+lgs['BR']+lgs['CR']+lgs['TL']+lgs['TR']))
#     fullcalo.append(sum(lgs.values()))

    
# fig, ax = plt.subplots(1,1, figsize=(10,6))
# ax.hist(ratiocalo[1][cutdivtime[1]], bins = 100, histtype ='step', 
#             color = 'indigo', alpha = 1, lw =2, 
#             label = 'Axial', 
#             density = True, range = [0,20])
# ax.hist(ratiocalo[0][cutdivtime[0]], bins = 100, histtype ='step', 
#             color = 'teal', alpha = 1, lw =2, 
#             label = 'Amorphous', 
#             density = True, range = [0,20])



# ax.grid()
# ax.set_yscale('log')
# ax.legend(fontsize = 20)
# ax.set_xlabel('$PH\; LG_{CC}\;\;/\;\;\\sum PH\; LG_{Lateral}$')
# ax.set_ylabel('Entries')

 