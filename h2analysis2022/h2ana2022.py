#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 21 15:17:38 2022

@author: ale
"""




from functionale import *
import numpy as np
import matplotlib.pyplot as plt
from lmfit.models import GaussianModel
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
#set plt parameter

#%%
datapath = '/home/ale/Desktop/dati2022H2/run520'
h5keys = ['xpos', 'digi_ph', 'digi_time']


## APRIAMO LE RUN
## RUN DI CALIBRAZIONE CALORIMETRO CENTRALE (NO CRYSTAL ON BEAM)
datacalibcaloCC = opendata(124, datapath, h5keys)
## CRYSTAL ON BEAM
axialrun = 153 #asse cristallo on beam
runnumber = 155 # amorfo cristallo on beam
datacrystal = opendata(runnumber, datapath, h5keys)
axialdata = opendata(axialrun, datapath, h5keys)


## Run con SiPM prima del cristallo (chiamati nel modo più controintuito possibile back
# good job ale >.-)
#axial = 200
runback = 204
databack = opendata(runback, datapath, h5keys)

#%% cose su fascio 
#run lead glass centrale da 120 GeV a 20 GeV
allrunCC = [124, 151, 150, 126, 132, 144]
E = [120, 100, 80, 60, 40, 20]   
# allrunCC = [124]
sigmaallrun = []
divexalignallrun = []
diveyalignallrun = []
timeallrun = []
phallrun = []
timethresholdinf = 163
timethresholdsup = 167
#%%   ############alcune cose sul fascio##############à
for j, i in enumerate(allrunCC):  
    
    
    runleadcc = i
    #carico tutte le run
    
    #estraggo solo quello che mi interessa fino ad ora 
    

    datarunleadcc  = opendata(runleadcc, datapath, h5keys)
    #%%  
    #prendo solo le posizioni di hit sui silici
    data = datarunleadcc[0]
    #labels da sistemare
    labels = ['T1', 'T1', 'T2','T2']
    #limite su asse x per hiostogrammi 1d
    xlim = [[-4,4]]*4
#     #

    histpos1d(data,2, labels, xlim)
    #plot posizioni 2d
    histpos2d(data,2, Range = [[0,2], [0,2]])
    #calcolo divergenza + fit per trovare valore medio
    distancesfromT1 = [1590]
    #calcolo e plot divergenza
    # divex, divey = divergence(data, 2, distancesfromT1)
    p0divx = [500, 0, 100]
    p0divy = [700, 0,100]
    #dvergenze con  fit 
    divex, divey, sigma, errsigma, mu, fig= divgaussianfit(data, 2, distancesfromT1,
                                            p0divx, p0divy, 
                                            Rangex = [[-800, 300]],
                                            Rangey=[[-500,500]])
    #shift per allineamento camere
    datashift, shift = aligntracker(data, 2, mu, distancesfromT1)
    # #plot camere allineate
    # histpos1d(datashift, 2, labels, xlim)
    # histpos2d(datashift, 2, Range=[[0,2],[0,2]])
    
    #plot e fit divergenze allineate
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, _ = divgaussianfit(datashift, 2, 
                                                                                    distancesfromT1, 
                                                                                    p0divx, p0divy, 
                                                                                    Rangex = [[-600, 500]],
                                                                                    Rangey=[[-500,500]])
    
    
    # lista contenente valore medio di della divergenza in x e y di tutte le run
    sigmaallrun.append(sigmaalign)
    divexalignallrun.append(divexalign)
    diveyalignallrun.append(diveyalign)
    #tempi e ph di tutte le run  # 
    ph = datarunleadcc[1][:,11]
    phallrun.append(ph)
    time = datarunleadcc[2][:,11]
    timeallrun.append(time)
    
    # timephplot(time, ph, timethresholdinf, timethresholdsup,
    #             Bins = [100,100],Range=[[150,180],[0,20000]]) 
  
    
    

#%%    

#Plot divergence vs energy
selections = [0,1]
sigmax = []
sigmay = []
for i in range(len(sigmaallrun)):
    sigmax.append((sigmaallrun[i][selections[0]]))
    sigmay.append((sigmaallrun[i][selections[1]]))

fig, ax = plt.subplots(1,2, figsize=(15,5))
ax = ax.flat

ax[0].plot(E, sigmax, marker = 'H',ls = 'none', 
            markersize = 10, color = 'navy',
            label = 'Data')
ax[1].plot(E, sigmay, marker = 'H', ls = 'none', 
            markersize = 10, color = 'navy', 
            label = 'Data')
ax[0].grid()
ax[1].grid()
ax[0].set_xlabel('Energy [GeV]', fontsize = 22)
ax[1].set_xlabel('Energy [GeV]', fontsize = 22)
ax[0].set_ylabel('x divergence [$\mu$ rad]', fontsize = 23)
ax[1].set_ylabel('y divergence [$\mu$ rad]', fontsize = 23)
ax[0].legend()
ax[1].legend()
plt.tight_layout()





xlimphplot = [[8000,12000], [7000,10000], [4500, 8000], [4000,6500], [2000, 4500], [1000, 2500]]
xfitrangeinf = [11000, 9000, 7400, 5600, 3600, 1800]
energyadcfit = []
# xfitrangesup = []
model = GaussianModel()
fig, ax = plt.subplots(3,2, figsize=(15,5))
ax = ax.flat
for i in range(len(phallrun)):
    
    hph, binsph, _ = ax[i].hist(phallrun[i][(divexalignallrun[i][0]>-250)
                                            &(divexalignallrun[i][0]<250)&
                                            (diveyalignallrun[i][0]>-250)&
                                            (diveyalignallrun[i][0]<250)&
                                            (timeallrun[i]>timethresholdinf)&
                                            (timeallrun[i]<timethresholdsup)],
                                bins = 100,
                                label = (f'Ph lead glass centrale {E[i]} Gev'), 
                                range = xlimphplot[i])
   
    xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
    phcondition =  (xdata > xfitrangeinf[i])
    paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
    resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition]) #, weights = errx)
    ax[i].plot(xdata[phcondition], resultx.best_fit, linewidth = 2, color = 'red')
    energyadcfit.append(resultx.params['center'].value)
    ax[i].legend()
    ax[i].grid()
    
    
from lmfit.models import LinearModel
linearmodel = LinearModel() 
fig, ax = plt.subplots(1,1, figsize=(8,8))
linearparams = linearmodel.guess(energyadcfit, x=E)
linearresult = linearmodel.fit(energyadcfit, linearparams, x=E)

ax.plot(E, energyadcfit, '.')
ax.plot(E,linearresult.best_fit, color = 'red', lw = 2)
ax.set_xlabel('ADC')
ax.set_ylabel('Energy (GeV)')
slope = linearresult.params['slope'].value
intercept = linearresult.params['intercept'].value

#%% #################### Calibrazione calorimetro#####################
#########################################################################





#inizializzazione alcune variabili
phleadglass = []
timeleadglass = []
energyadcall = []
slope = {}
errslope = {}
intercept = {}
errintercept = {}
#energie calibrazione 
energies = [60,40,20]
#label lead glass per diffrenti run
leadglasslabel = ['TL', 'TR', 'CL', 'CR', 'BL', 'BR', 'CC']
#canali digitizer associati ai label
digichannel = [13,14,10,12,8,9, 11]
leadglassrun = {'TL':[141,136,147], 'TR':[138,137,146], 'CL':[142,131,143], 
                'CR':[127,133,145], 'BL': [140,135,148], 'BR':[139,134,149], 
                 'CC': [124,151,150,126,132,144]}

#time threshold in time ph plot
timethresholdinf = [166, 166, 166, 163, 163, 163, 166, 164, 
                    164, 163, 163,163,163,163, 163,163,163, 
                    163, 163,163, 163, 163, 163, 163]
timethresholdsup = [175, 175, 175, 167, 167, 167, 175, 173, 
                    173, 167, 167,167, 167, 167, 167,167,167,
                    167, 167, 167, 167, 167, 167, 167]
#range fit gaussiana
xfitrangeinf = [7400,5000, 2300, 5800, 4000,1800, 8800,6100,
                2500, 5800, 3500, 1500, 3100, 2000, 0, 6400, 
                4300,2000,10800, 9000, 7000, 5500, 3500, 1800]
xfitrangesup = [8000, 6000, 3000, 6500,4500, 2200, 10000, 7300, 
                3800, 6200, 4500, 2500,3700, 3000, 8000, 7000,
                5000,2500, 11700, 9700, 7800, 6100, 4000, 2000]


xliminf = [4500, 2500, 1000,4000, 2000, 900, 5000, 4000, 0, 4000, 
           2000, 0, 2000, 0, 0, 2500,0, 0, 7000, 6000, 5000, 4000, 2000,1000]
xlimsup = [11000, 8000, 4000, 8000, 7000, 3000,14000, 10000, 6000,
           8000, 6000, 4000, 5000, 5000, 2000, 10000, 8000, 4000, 
           14000, 12000, 14000, 10000,6000,3000]
counter = 0 #mi serve per ciclare su fitrange

saveADC60GeV = {}

for k, label in enumerate(leadglasslabel):
    energyadcall = []
    sigmaenergyadcall = []
    #la run del LG centrale ha sei energie 
    for j in range(len(leadglassrun[label])): 
        if len(leadglassrun[label]) == 6:
            energies = [120,100,  80, 60, 40, 20]
        print(k, label, j)
        #apro i dati di ciascun lead glass
        datacalorimeters = opendata(leadglassrun[label][j], datapath, h5keys)
        #estraggo Ph e tempi
        ph = datacalorimeters[1][:,digichannel[k]]
        phleadglass.append({label:ph})#non serve davvero
        time = datacalorimeters[2][:,digichannel[k]]
        timeleadglass.append({label:time})
        #plot2d tempo ph
        figtime, axtime = plt.subplots(1,1 , figsize=(15,5))
        timephplot(time, ph,axtime, timethresholdinf[counter], timethresholdsup[counter],
                   Range=([100,200], [0,15000]), Title = f'Lead Glass {label}, {energies[j]} GeV')
        #facciamo il fit dei picchi
        figphcalib, axphcalib = plt.subplots(1,1, figsize=(8,8))
        # hph, binsph, _ = ax.hist(ph, bins = 100, 
        #                          label = (f'Lead Glass {label}, {energies[j]} GeV'))
        condition = ((time > timethresholdinf[counter])&(time < timethresholdsup[counter]))
        
        energyadc, sigmaenergyadc = calibgaussianfit(ph[condition],axphcalib,Range=[xliminf[counter],xlimsup[counter]],
                                     xfitrangeinf = xfitrangeinf[counter], 
                                     xfitrangesup = xfitrangesup[counter], 
                                     Label = (f'Lead Glass {label}, {energies[j]} GeV'),
                                     Color='lightpink', Colorfit = 'black')
        if energies[j] == 60 :
            saveADC60GeV[label] = energyadc
        print(f'Lead Glass {label}, {energies[j]}: ADC value {energyadc}')
        axphcalib.axvline(x=xfitrangeinf[counter], color="black", linestyle = '--')
        axphcalib.axvline(x=xfitrangesup[counter], color="black", linestyle = '--')
        axphcalib.set_xlim([xliminf[counter], xlimsup[counter]])
        axphcalib.set_xlabel('PH (ADC)')
        axphcalib.set_ylabel('Entries')
        # energyadcall.append({label:energyadc})
        energyadcall.append(energyadc)
        sigmaenergyadcall.append(sigmaenergyadc)
        counter = counter+1
    sigmaenergyadcall = np.array(sigmaenergyadcall)  
    #plot retta calibrazione  

    fig, ax = plt.subplots(1,1, figsize=(8,8))
    from lmfit.models import LinearModel
    linearmodel = LinearModel()     
    linearparams = linearmodel.guess(energyadcall, x=energies)
    linearresult = linearmodel.fit(energyadcall, linearparams, x=energies, weights = sigmaenergyadcall)
    
    ax.errorbar(energies, energyadcall, yerr=sigmaenergyadcall, 
                marker='D',  linestyle = 'none', color = 'midnightblue',
                label = f'data Lead Glass {label}', markersize = 6)
    ax.plot(energies,linearresult.best_fit, color = 'red', lw = 2,
            label = f'Fit : ADC = ({round(linearresult.params["slope"].value,2)} $\pm$ {round(linearresult.params["slope"].stderr,2)})$\cdot$ GeV +  ({round(linearresult.params["intercept"].value,2)}  $\pm$ {round(linearresult.params["intercept"].stderr,2)})')
    ax.legend(fontsize = 14)
    ax.set_ylabel('ADC')
    ax.set_xlabel('Energy (GeV)')
    ax.grid()
    slope[label]=(linearresult.params['slope'].value)
    errslope[label] = (linearresult.params['slope'].stderr)
    intercept[label]= (linearresult.params['intercept'].value)
    errintercept[label]= (linearresult.params['intercept'].stderr)

    
### SLOPE E INTERCEPT SONO DIZIONARI <3
# calibrationfactors = [slope, intercept]

# with open('calibrationfactors.dat','w') as out_file:
#         np.savetxt(out_file, calibrationfactors)#%% ######################
#%% ######################
equalipar = {}
for i in leadglasslabel : 
    equalipar[i] = saveADC60GeV['CC']/saveADC60GeV[i]


#############################################################################
#%% ###################### SIPM ############################################
############################################################################
## GUARDO UN PO' CHE FORMA HANNO RUN 120 GeV ELETTRONI (CRYSTAL ON BEAM)
# fig, ax = plt.subplots(4,4, figsize=(4,4))
# ax = ax.flat
# for digich in range (4):
    
#     ax[digich].hist(datacrystal[1][:,digich], bins= 100)

#ch1 --> mat 2 + ch 2--> mat3 crystal 
#ch3--> mat4 + ch4--> mat1 no cryst
sipmchannel = {'mat1': 4, 'mat2': 1, 'mat3':2, 'mat4':3}

fig, ax = plt.subplots(2,2, figsize=(4,4))
ax = ax.flat
for digich in range(4):
    ax[digich].hist(datacrystal[1][:,sipmchannel[f'mat{digich+1}']],
                    bins= 100, label = f'SiPM PH mat{digich+1} ')
    ax[digich].grid()
    ax[digich].set_xlabel('PH [ADC]')
    ax[digich].set_ylabel('Entries')
    ax[digich].legend(fontsize = 10)
    ax[digich].set_yscale('log')
    plt.tight_layout
sipmtimethinf = 200
sipmtimethsup = 300
fig, ax = plt.subplots(1,1, figsize=(8,4))
timephplot(datacrystal[2][:,sipmchannel['mat2']], 
           datacrystal[1][:,sipmchannel['mat2']], 
           ax, timethresholdinf = sipmtimethinf, 
           timethresholdsup = sipmtimethsup, Range=[[0,400], [0,5000]])
fig, ax = plt.subplots(1,1, figsize=(8,4))
timephplot(datacrystal[2][:,sipmchannel['mat3']],
           datacrystal[1][:,sipmchannel['mat3']],
           ax, timethresholdinf = sipmtimethinf, 
           timethresholdsup = sipmtimethsup, Range=[[0,400], [0,5000]])


sipmcond = ((datacrystal[2][:,sipmchannel['mat2']]>sipmtimethinf) & 
            (datacrystal[2][:,sipmchannel['mat2']]<sipmtimethsup) &
            (datacrystal[2][:,sipmchannel['mat3']]>sipmtimethinf) & 
            (datacrystal[2][:,sipmchannel['mat3']]<sipmtimethsup))
##plot PH tutte le matrici
fig, ax = plt.subplots(1,2, figsize=(8,4))
ax = ax.flat
for i, digich in enumerate(range(2,4)):
    ax[i].hist(datacrystal[1][:,sipmchannel[f'mat{digich}']][sipmcond],
                    bins= 100, label = f'SiPM PH mat{digich} ')
    ax[i].grid()
    ax[i].set_xlabel('PH [ADC]')
    ax[i].set_ylabel('Entries')
    ax[i].legend(fontsize = 10)
    plt.tight_layout

phsipmtot = datacrystal[1][:,sipmchannel['mat2']]+datacrystal[1][:,sipmchannel['mat3']]
fig, ax = plt.subplots(1,1, figsize=(8,4))
ax.hist(phsipmtot, bins = 100, label = 'SiPM PH tot (mat2 + mat3)', histtype ='stepfilled', 
        color = 'chartreuse', alpha = 0.7, edgecolor='black', lw =2)
ax.grid()
ax.set_xlabel('PH [ADC]')
ax.set_ylabel('Entries')
ax.legend(fontsize = 10)
ax.set_yscale('log')
plt.tight_layout



#%%###################################### lead glass 
#label lead glass
lgamorphous = {'BL':8, 'BR':9, 'CL':10, 'CC':11, 'CR':12, 'TL':13, 'TR':14}
phlgamorphous = {}


for i in lgamorphous:
 phlgamorphous[i] = datacrystal[1][:,lgamorphous[i]]


#prima di sommare bisogna calibrare, per ora guardo solo il canale centrale
# datacrystal[1][:,lgamorphous['CC']] = sum(phlgamorphous.values())
model = GaussianModel()
fig, ax = plt.subplots(1,1, figsize=(8,4))
hph,binsph, _ = ax.hist(datacrystal[1][:,lgamorphous['CC']], bins = 100, label = 'Lead glass PH CC', histtype ='stepfilled', 
        color = 'lightpink', alpha = 0.7, edgecolor='black', lw =2)
ax.set_xlabel('PH [ADC]')
ax.set_ylabel('Entries')
xfitrangeinflg  = 7000
xfitrangesuplg  = 11000


xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
phcondition =  ((xdata > xfitrangeinflg) & (xdata<xfitrangesuplg))
paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                    weights = np.sqrt(hph[phcondition]))

to_plot = np.linspace(xfitrangeinflg, xfitrangesuplg, 10000)

y_eval = model.eval(resultx.params, x= to_plot)
ax.plot(to_plot, y_eval, linewidth = 2, color = 'black')
mulgfit = resultx.params['center'].value
sigmalgfit = resultx.params['sigma'].value
ax.axvline(x=xfitrangeinflg, color="black", linestyle = '--', label = 'fit range')
ax.axvline(x=xfitrangesuplg, color="black", linestyle = '--')

lgthreshold = (mulgfit - 3*sigmalgfit)
ax.axvline(x=lgthreshold, color="red", linestyle = '--', label = 'threshold 3sigma')
ax.legend()    
ax.grid()
ax.set_yscale('log')
plt.tight_layout()



#### PH SiPM dopo aver tagliato su lead glass

fig, ax = plt.subplots(1,1, figsize=(10,6))
ax.hist(phsipmtot, bins = 100, histtype ='step', 
        color = 'navy', alpha = 1, lw = 2, label = 'SiPM PH tot')
ax.hist(phsipmtot[datacrystal[1][:,lgamorphous['CC']]>lgthreshold], bins = 100, histtype ='step', 
        color = 'limegreen', alpha = 1, lw =2, label = 'SiPM PH above lg threshold')
ax.hist(phsipmtot[datacrystal[1][:,lgamorphous['CC']]<lgthreshold], bins = 100, histtype ='step', 
        color = 'orangered', alpha = 1, lw = 2, label = 'SiPM PH under lg threshold')
ax.set_yscale('log')
ax.grid()
ax.set_xlabel('PH [ADC]')
ax.set_ylabel('Entries')
ax.legend(fontsize = 10)
plt.tight_layout()
# fig, ax = plt.subplots(1,1, figsize=(8,8))
# _,_,_, mapable = ax.hist2d(datacrystal[1][:,lgamorphous['CC']][datacrystal[1][:,lgamorphous['CC']]<lgthreshold], 
#                            phsipmtot[datacrystal[1][:,lgamorphous['CC']]<lgthreshold], bins = [100,100], 
#                            cmap = plt.cm.jet, norm=colors.LogNorm())
# plt.colorbar(mapable, ax=ax)
# ax.set_xlabel('Lead glass PH CC [ADC]')
# ax.set_ylabel('SiPM PH tot [ADC]')
# ax.grid()
# plt.tight_layout()




#%%

phSiPMcrystal1 = (datacrystal[1][:,sipmchannel['mat2']]+datacrystal[1][:,sipmchannel['mat3']])
phSiPMcrystal2 = (datacrystal[1][:,sipmchannel['mat1']]+datacrystal[1][:,sipmchannel['mat4']])
phsipmtot = phSiPMcrystal1 + 8*phSiPMcrystal2

caloADCequali = {}
for label in leadglasslabel: 
    caloADCequali[label] = datacrystal[1][:,lgamorphous[label]]*equalipar[label]

caloADCtot = sum(caloADCequali.values())
# caloADCtot = datacrystal[1][:,lgamorphous['CC']]
fig, ax = plt.subplots(1,1, figsize=(8,8))
myPlot = ax.hist2d(caloADCtot, phsipmtot, bins = [60,60],
                           cmap = plt.cm.jet, range = [[8000,12000], [0,8000]])
# plt.colorbar(mapable, ax=ax)
ax.set_xlabel('Lead glass PH CC [ADC]')
ax.set_ylabel('SiPM PH tot [ADC]')
fig.colorbar(myPlot[3], ax = ax)
ax.grid()

plt.tight_layout()

###############################################################################
#%% ############################# FIT ELLISSE #################################
#########################################################################
##########################################################################

#permette di fittare la zona di correlazione e selezionare solo gli eventi interni 
#o esterni --> si potrebbe invece calcolare asse e prendere eventi a sinistra. 
#per ora va bene la cosa del tre sigma a sinistra del LG
###############################################################################

###############################################################################
###############################################################################
 #%%
# #%% Contour
xcentr = myPlot[1][:-1] + (myPlot[1][1] - myPlot[1][0])/2
ycentr = myPlot[2][:-1] + (myPlot[2][1] - myPlot[2][0])/2


fig, ax = plt.subplots()
fig.set_size_inches(12,5)
ax.set_title("Curve di livello (Contour)", fontsize=16)






myContour = ax.contour(*np.meshgrid(xcentr, ycentr, indexing = "ij"), 
                        myPlot[0], levels = range(800,1400,50))

# ax.set_xlim((150, 400))
# ax.set_ylim((150, 400))
fig.colorbar(myContour, ax = ax)

ax.set_xlabel('Lead glass PH CC [ADC]')
ax.set_ylabel('SiPM PH tot [ADC]')
plt.show()

 #%%


#fit elli
# Source https://stackoverflow.com/questions/47873759/how-to-fit-a-2d-ellipse-to-given-points

# ottengo una certa curva di livello
p = myContour.collections[8].get_paths()[0]

# Estraggo le coordinate x,y dei punti su quella curva
v = p.vertices
x = v[:,0]
y = v[:,1]





fig, ax = plt.subplots()

# Plotto i punti della curva scelta
ax.plot(x,y, "*g", label = "Dati della curva scelta")



# Formalizzo il problema dei LstSq
X=x[:, np.newaxis]
Y=y[:, np.newaxis]
A = np.hstack([X**2, X * Y, Y**2, X, Y])
b = np.ones_like(X)
x = np.linalg.lstsq(A, b)[0].squeeze()


#%%


# Plotto l'ellisse fittata
margine = 500
x_coord = np.linspace(X.min()-margine, X.max()+margine, 500)
y_coord = np.linspace(Y.min()-margine, Y.max()+margine, 500)
X_coord, Y_coord = np.meshgrid(x_coord, y_coord)
Z_coord = x[0] * X_coord ** 2 + x[1] * X_coord * Y_coord + x[2] * Y_coord**2 + x[3] * X_coord + x[4] * Y_coord
plt.contour(X_coord, Y_coord, Z_coord, levels=[1], colors=('r'), linewidths=2, label = "Elli fittata")

ax.grid()
ax.legend()

plt.show()

print('The ellipse is given by {0:.3}x^2 + {1:.3}xy+{2:.3}y^2+{3:.3}x+{4:.3}y = 1'.format(x[0], x[1],x[2],x[3],x[4]))
#Sovrappongo tutto [ Copio tutto tranne una riga aggiunta ]
fig, ax=plt.subplots(1)
fig.set_size_inches(15,5)

myPlot = ax.hist2d(caloADCtot, phsipmtot, cmap = "jet", bins =50, range=((8e3,12e3),(0,8e3)))
ax.set_xlabel("ADC calorimetro")
ax.set_ylabel("ADC SIPM")
fig.colorbar(myPlot[3], ax = ax)

# Elli

plt.contour(X_coord, Y_coord, Z_coord, levels=[1], colors=("k"), ls = ":", linewidths=2 )


plt.show()
#%%
#Ora trovo il modo di selezionare gli eventi nell'ellisse direi che i punti che soddisfano la disequazione
# uguale all'equa dell'ellisse con > al posto di = sono INTERNI all'ellisse stesso (hom parametri negativi)

cond = x[0]*caloADCtot**2+x[1]*caloADCtot*phsipmtot+x[2]*phsipmtot**2+x[3]*caloADCtot+x[4]*phsipmtot>1
anticond = x[0]*caloADCtot**2+x[1]*caloADCtot*phsipmtot+x[2]*phsipmtot**2+x[3]*caloADCtot+x[4]*phsipmtot<1

fig, ax=plt.subplots(1)
fig.set_size_inches(15,5)

myPlot = ax.hist2d(caloADCtot[cond], phsipmtot[cond], cmap = "jet", bins =50, range=((8e3,12e3),(0,8e3)))
ax.set_xlabel("ADC calorimetro")
ax.set_ylabel("ADC SIPM")
fig.colorbar(myPlot[3], ax = ax)


#%%
import copy
import matplotlib as mpl
fig, ax=plt.subplots(1)
fig.set_size_inches(15,5)

mycmap = copy.copy(mpl.cm.jet)
mycmap.set_bad(mycmap(0))

corr = ax.hist2d(caloADCtot[cond], phsipmtot[cond], cmap = "jet", bins =50)

corr = list(corr)
corr[0] = corr[0].T
x, y, err = hist2dToProfile(corr)
ax.plot(x, y, '.', color = 'black', label = 'Profile plot 120 GeV')
ax.grid()
ax.set_xlabel('PH calorimeter [ADC$_{calo}$]', fontsize = 24)
ax.set_ylabel('PH SiPM [ADC$_{SiPM}$],  2$X_0$ ', fontsize = 24)
ax.legend(fontsize=18)
fig.colorbar(corr[3], ax = ax) 
plt.tight_layout()
ax.legend()
ax.grid()

ax.set_xlabel('Calo PH [GeV]')
ax.set_ylabel('Entries')
plt.show()



#%%
from lmfit.models import LinearModel

model = LinearModel()
xfitlim  = 0
yfitlim = 0
fitcond = ((x>10000) & (x<11200))
params = model.guess(y[fitcond], x=x[fitcond])


result = model.fit(y[fitcond], params, x=x[fitcond])

print(f'Chi-square = {result.chisqr:.4f}, Reduced Chi-square = {result.redchi:.4f}')
print(result.fit_report())
fig, ax = plt.subplots(1,1, figsize=(10,8))
ax.errorbar(x[fitcond], y[fitcond],yerr = err[fitcond], marker='s',  linestyle = 'none', color = 'darkturquoise', label = 'Data', markersize=5)
ax.plot(x[fitcond],result.best_fit,label=f'm ={round(result.params["slope"].value,2)} $\pm${round(result.params["slope"].stderr,2)} \n q = {round(result.params["intercept"].value,2)}$\pm$ {round(result.params["intercept"].stderr,2)} ', color = 'red', linewidth = 2)
ax.set_xlabel('PH Genni calorimeter [ADC$_{genni}$]', fontsize = 24)
ax.set_ylabel('PH Ringo [ADC$_{SiPM}$],  1$X_0$ ', fontsize = 24)
ax.grid()
ax.legend(fontsize = 20)
plt.tight_layout()

plt.show()




# 
slope1= result.params['slope'].value
errSlope1 = result.params['slope'].stderr
intercept1= result.params['intercept'].value
errInter1 = result.params['intercept'].stderr
calibSiPM = [slope1, errSlope1, intercept1, errInter1]

crystADCcal = -(1/(slope1))*phSiPMcrystal1

SiPMGeV = (crystADCcal - intercept['CC'])/slope['CC']


fig, ax = plt.subplots(1,1, figsize=(10,6))
ax.hist(SiPMGeV, bins = 100, histtype ='step', 
            color = 'navy', alpha = 1, lw =2, label = ' calo PH', density = True)
ax.legend()
ax.grid()
ax.set_yscale('log')
ax.set_xlabel('PH cryst [GeV]')
ax.set_ylabel('Entries')
## idea--> selezionare solo eventi a sinistra dell'asse dell'ellisse--> non dovrei vedere nulla nel
#sipm perchè non sono elettroni


#%% charge sharing nel cristallo front (CIOÈ SiPM DIETRO AL CRISTALLO IN AMORFO) 

divex, divey = divergence(datacrystal[0], numberoftrackers = 2, distancesfromT1=[15.90]) 

zcryst = 17.47
xcryst = datacrystal[0][:,0] + zcryst*np.tan(divex)
ycryst = datacrystal[0][:,1] + zcryst*np.tan(divey)




fig, ax = plt.subplots(1,1, figsize=(8,4))
zhist2d, xhist2d , yhist2d , mapable = ax.hist2d(xcryst[0], ycryst[0], 
          bins =[100,100], cmap = plt.cm.jet, range = [[0,2], [0,2]])
ax.set_xlabel('x [cm]')
ax.set_ylabel('y [cm]')
ax.grid()
plt.show()
###facciamo l'efficienza del cristallo 
theffsipm = 1000
xrectinf = 0.75
xrectlenght = 0.75
yrectinf = 0.70
yrectlenght = 0.75
seleffsipm = (phsipmtot>theffsipm)

zhist2dcut, xhist2dcut , yhist2dcut , mapable  = ax.hist2d(xcryst[0][seleffsipm], 
                                                           ycryst[0][seleffsipm] , 
          bins =[100,100], cmap = plt.cm.jet, range = [[0,2], [0,2]])


np.seterr(divide='ignore', invalid = 'ignore')
eff = np.divide(zhist2dcut, zhist2d)
eff[np.isnan(eff)]=0
fig, ax = plt.subplots(1,1, figsize=(12,6))
divider = make_axes_locatable(ax)
cax = divider.append_axes('right', size='5%', pad=0.1)
im = ax.imshow(np.flip(np.transpose(eff)), cmap=plt.cm.jet, extent=[0,2,0,2])
fig.colorbar(im, cax=cax, orientation='vertical')
rect = patches.Rectangle((xrectinf, yrectinf), xrectlenght, yrectlenght, linewidth=6, edgecolor='black', facecolor='none'
                         , label = 'Crystal central area')
ax.add_patch(rect)


####correlazione ph MAT2 E PH MAT3
fig, ax = plt.subplots(1,1, figsize=(8,4))
ax.hist2d(datacrystal[1][:,sipmchannel['mat2']],datacrystal[1][:,sipmchannel['mat3']], 
          bins =[100,100], cmap = plt.cm.jet, norm=colors.LogNorm())
ax.set_xlabel('SiPM mat2. front')
ax.set_ylabel('SiPM mat3, front')
ax.grid()
plt.show()

fig, ax = plt.subplots(1,1, figsize=(8,4))
ax.hist2d(databack[1][:,sipmchannel['mat2']],databack[1][:,sipmchannel['mat3']], 
          bins =[100,100], cmap = plt.cm.jet, norm=colors.LogNorm())
ax.set_xlabel('SiPM mat2. front')
ax.set_ylabel('SiPM mat3, front')
ax.grid()
plt.show()

###selezione area centrale del crystallo
crystsel = ((xcryst[0]<(xrectinf+xrectlenght))&(xcryst[0]>xrectinf) & (ycryst[0]<(yrectinf+yrectlenght)) &(ycryst[0]>yrectinf))
#assimetria front
chargesharingfront = ((datacrystal[1][:,sipmchannel['mat2']]-datacrystal[1][:,sipmchannel['mat3']])/
                 (datacrystal[1][:,sipmchannel['mat2']]+datacrystal[1][:,sipmchannel['mat3']]))
# 

# STESSA COSA PER SiPM POSTI DAVANTI AL CRISTALLO  AMORPHOUS BACK


# fig, ax = plt.subplots(2,2, figsize=(4,4))
# ax = ax.flat
# for digich in range (4):
    
#     ax[digich].hist(databack[1][:,digich], bins= 100)

divexback, diveyback = divergence(databack[0], numberoftrackers = 2, distancesfromT1=[15.90]) 

xcrystback = databack[0][:,0] + zcryst*np.tan(divexback)
ycrystback = databack[0][:,1] + zcryst*np.tan(diveyback)





#calcolo assimetria back
chargesharingback = ((databack[1][:,sipmchannel['mat2']]-databack[1][:,sipmchannel['mat3']])/
                 (databack[1][:,sipmchannel['mat2']]+databack[1][:,sipmchannel['mat3']]))

##correlazione back
# fig, ax = plt.subplots(1,1, figsize=(8,4))
# ax.hist2d(databack[1][:,sipmchannel['mat2']],databack[1][:,sipmchannel['mat3']], 
#           bins =[100,100], cmap = plt.cm.jet, norm=colors.LogNorm())
# ax.set_xlabel('SiPM mat2 back')
# ax.set_ylabel('SiPM mat3 back')
# ax.grid()
# plt.show()

fig, ax = plt.subplots(2,2, figsize=(10,8))
ax = ax.flat
ax[0].hist2d(xcrystback[0],chargesharingback, range = [[0,+2],[-0.5,0.5]], bins =[100,100], 
             cmap = plt.cm.jet, norm=colors.LogNorm())
ax[0].set_xlabel('x [cm]')
ax[0].set_ylabel('(PH2-PH3)/(PH2+PH3)')
ax[0].set_title('back')
ax[1].hist2d(ycrystback[0],chargesharingback, range = [[0,+2],[-0.5,0.5]], bins =[100,100], 
             cmap = plt.cm.jet, norm=colors.LogNorm())
ax[1].set_xlabel('y [cm]')
ax[1].set_ylabel('(PH2-PH3)/(PH2+PH3)')
ax[1].set_title('SiPM prima del cistallo')
ax[2].hist2d(xcryst[0],chargesharingfront, range = [[0,2],[-0.5,0.5]], bins =[100,100], 
             cmap = plt.cm.jet, norm=colors.LogNorm())
ax[2].set_xlabel('x [cm]')
ax[2].set_ylabel('(PH2-PH3)/(PH2+PH3)')
ax[2].set_title('SiPM dopo cryst)')
ax[3].hist2d(ycryst[0],chargesharingfront, range = [[0,2],[-0.5,0.5]], bins =[100,100], label = 'back',
             cmap = plt.cm.jet, norm=colors.LogNorm())
ax[3].set_xlabel('y [cm]')
ax[3].set_ylabel('(PH2-PH3)/(PH2+PH3)')
ax[3].set_title('Front (sipm dopo cryst)')
for i in range(4): 
    ax[i].grid()
plt.tight_layout()
plt.show()

##################
fig, ax = plt.subplots(2,2, figsize=(10,8))
ax = ax.flat
ax[0].hist2d(xcrystback[0], databack[1][:,sipmchannel['mat2']], range = [[0,2],[0,4000]], bins =[100,100], label = 'back',
                 cmap = plt.cm.jet, norm=colors.LogNorm())
ax[1].hist2d(ycrystback[0], databack[1][:,sipmchannel['mat2']], range = [[0,2],[0,4000]], bins =[100,100], label = 'back',
                 cmap = plt.cm.jet,norm=colors.LogNorm())
ax[2].hist2d(xcrystback[0], databack[1][:,sipmchannel['mat3']], range = [[0,2],[0,4000]], bins =[100,100], label = 'back',
                 cmap = plt.cm.jet, norm=colors.LogNorm())
ax[3].hist2d(ycrystback[0], databack[1][:,sipmchannel['mat3']], range = [[0,2],[0,4000]], bins =[100,100], label = 'back',
                 cmap = plt.cm.jet, norm=colors.LogNorm())

ax[0].set_ylabel('PH mat2')
ax[1].set_ylabel('PH mat2')

ax[2].set_ylabel('PH mat3')
ax[3].set_ylabel('PH mat3')

for i in range(4):
    if i%2==0:
        ax[i].set_xlabel('x [cm]')
    else : 
        ax[i].set_xlabel('y [cm]')
    ax[i].grid()
    ax[i].set_title('Sipm prima del cristallo')
plt.tight_layout()
plt.show() 

#%%
########################################### 
# fig, ax = plt.subplots(2,2, figsize=(10,8))
# ax = ax.flat
# ax[0].hist2d(xcryst[0], datacrystal[1][:,sipmchannel['mat2']], range = [[0,2],[0,4000]], bins =[100,100], label = 'back',
#                  cmap = plt.cm.jet, norm=colors.LogNorm())
# ax[1].hist2d(ycryst[0], datacrystal[1][:,sipmchannel['mat2']], range = [[0,2],[0,4000]], bins =[100,100], label = 'back',
#                  cmap = plt.cm.jet, norm=colors.LogNorm())
# ax[2].hist2d(xcryst[0], datacrystal[1][:,sipmchannel['mat3']], range = [[0,2],[0,4000]], bins =[100,100], label = 'back',
#                  cmap = plt.cm.jet, norm=colors.LogNorm())
# ax[3].hist2d(ycryst[0], datacrystal[1][:,sipmchannel['mat3']], range = [[0,2],[0,4000]], bins =[100,100], label = 'back',
#                  cmap = plt.cm.jet, norm=colors.LogNorm())

# ax[0].set_ylabel('PH mat2')
# ax[1].set_ylabel('PH mat2')

# ax[2].set_ylabel('PH mat3')
# ax[3].set_ylabel('PH mat3')

# for i in range(4):
#     if i%2==0:
#         ax[i].set_xlabel('x [cm]')
#     else : 
#         ax[i].set_xlabel('y [cm]')
#     ax[i].grid()
#     ax[i].set_title('Sipm dopo del cristallo')
# plt.tight_layout()
# plt.show() 

fig, ax = plt.subplots(1,1, figsize=(10,8))
phfront = (datacrystal[1][:,sipmchannel['mat2']]+datacrystal[1][:,sipmchannel['mat3']])
phback = (databack[1][:,sipmchannel['mat2']]+databack[1][:,sipmchannel['mat3']])
ax.hist(phfront, bins =100, 
            histtype ='step', color = 'navy', alpha = 0.7, lw =2, label = 'PH SiPM dopo cristallo', density = 'True')
ax.hist(phback, bins =100, 
              histtype ='step', 
              color = 'limegreen', alpha = 0.7, lw =2, label = 'PH SiPM prima cristallo', density = 'True')
ax.legend()

##############################################################################
#%% ## prova equalizzazione SIPM
###############################################################################


# fig, ax = plt.subplots(1,1, figsize=(8,4))
# ax.hist(datacrystal[1][:,sipmchannel['mat2']][sipmcond & crystsel], bins = 100, label = 'mat2', histtype ='step', 
#         color = 'navy', alpha = 1, lw = 2,)
# ax.hist(datacrystal[1][:,sipmchannel['mat3']][sipmcond & crystsel], bins = 100, label = 'mat3', histtype ='step', 
#         color = 'limegreen', alpha = 1, lw = 2,)
# ax.set_xlabel('PH [ADC]')
# ax.set_ylabel('Entries')
# ax.grid()
# ax.set_yscale('log')
# ax.legend()
# plt.show()





###############################################################################
#%%#############################   Calibrazione Calorimetro     ###############
############################################################################
lgeqfact = {'TL': 0.7432132479651892, 'TR': 0.9420004405379382, 'CL': 0.6099601605385676,'CR': 0.9645596120969584, 'BL': 1.6335469637749256, 'BR': 0.8535219537299307, 'CC': 1.0}
mcalib = 1.002456552727584368e-02
qcalib = -3.694493671411388513e-01

plt.rcParams['font.size'] = '18'
phlgGeV = {}
lgthGeVinf = 80
lgthGeVsup = 100

model = GaussianModel()

for label in (lgamorphous):
    phlgGeV[label] = (phlgamorphous[label]*lgeqfact[label])
    
fig, ax = plt.subplots(1,1, figsize=(12,8))
hph,binsph, _ = ax.hist(((sum(phlgGeV.values()))*mcalib)+qcalib, bins = 100, range = [0,160], 
        label = 'Experimental data', histtype ='stepfilled', 
        color = 'lightpink', alpha = 0.7, edgecolor='black', lw =3)
ax.grid()
ax.axvline(x=lgthGeVsup, color="black", linestyle = '--', label = 'Threshold', lw=3)
ax.axvline(x=lgthGeVinf, color="black", linestyle = '--', lw=3)
ax.set_xlabel('Lead Glass PH [GeV]')
ax.set_ylabel('Entries')     
ax.set_yscale('log')                  
ax.legend(fontsize = 13)
xfitrangeinflg  = 100
xfitrangesuplg  = 120


xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
phcondition =  ((xdata > xfitrangeinflg) & (xdata<xfitrangesuplg))
paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
                    weights = np.sqrt(hph[phcondition]))

to_plot = np.linspace(xfitrangeinflg, xfitrangesuplg, 10000)

y_eval = model.eval(resultx.params, x= to_plot)

mulgfit = resultx.params['center'].value
errmulgfit = resultx.params['center'].stderr
sigmalgfit = resultx.params['sigma'].value
ax.plot(to_plot, y_eval, linewidth = 2, color = 'red', label = f'$\mu$ = {round(mulgfit,2)}$\pm$ {round(errmulgfit,2)} ')
# ax.axvline(x=xfitrangeinflg, color="black", linestyle = '--')
# ax.axvline(x=xfitrangesuplg, color="black", linestyle = '--')

# lgthreshold = (mulgfit - 3*sigmalgfit)
# ax.axvline(x=lgthreshold, color="red", linestyle = '--', label = 'threshold 3sigma')
ax.legend()    

plt.tight_layout()
plt.show()
#%%



#%%






fig, ax = plt.subplots(1,1, figsize=(10,6))
ax.hist(phSiPMcrystal1 , bins = 100, histtype ='step', 
        color = 'navy', alpha = 1, lw = 2, label = 'SiPMs total PH')
ax.hist(phSiPMcrystal1 [((sum(phlgGeV.values()))>lgthGeVinf) & ((sum(phlgGeV.values()))<lgthGeVsup)], bins = 100, histtype ='step', 
        color = 'limegreen', alpha = 1, lw =2, label = f'Energy Lead Glass [{lgthGeVinf}, {lgthGeVsup}] GeV')
ax.hist(phSiPMcrystal1 [((sum(phlgGeV.values()))>lgthGeVsup)], bins = 100, histtype ='step', 
        color = 'orangered', alpha = 1, lw =2, label = f'Energy Lead Glass > {lgthGeVsup} GeV')
ax.hist(phSiPMcrystal1 [((sum(phlgGeV.values()))<lgthGeVinf)], bins = 100, histtype ='step', 
        color = 'hotpink', alpha = 1, lw =2, label = f'Energy Lead Glass < {lgthGeVinf} GeV')
ax.set_yscale('log')
ax.grid()
ax.set_xlabel('SiPMs PH [a.u.]')
ax.set_ylabel('Entries')
ax.legend(fontsize = 10)
plt.tight_layout()


# fig, ax = plt.subplots(1,1, figsize=(8,8))
# myPlot = ax.hist2d((sum(phlgGeV.values())), phsipmtot, bins = [60,60],
#                             cmap = plt.cm.jet, range = [[60,120], [0,8000]])
# # plt.colorbar(mapable, ax=ax)
# ax.set_xlabel('Lead glass PH tot [GeV]')
# ax.set_ylabel('SiPM PH tot [ADC]')
# ax.grid()
# plt.tight_layout()




#%%#  ########################################################################
#######################   SCRIVO FILE ISTOGRAMMA PER LA SIMULAZIONE    ########
############################################################################
## run calibrazione calorimetro con fascio centrale 

datacalibcaloCC = opendata(124, datapath, h5keys)
phdatacalibcaloCC = {}
for i in lgamorphous:
    phdatacalibcaloCC[i] = datacalibcaloCC[1][:,lgamorphous[i]]

phcalibcaloGeVCC = {}
for label in (lgamorphous):
    phcalibcaloGeVCC[label] = (phdatacalibcaloCC[label]-intercept[label])/slope[label]
fig, ax = plt.subplots(1,1, figsize=(10,6))
phcalibtot = ((sum(phcalibcaloGeVCC.values())))# tolgo 8 GeV per far tornare il picco a 120 GeV
hist, binedges, _ = ax.hist(phcalibtot, bins = 100, 
                     histtype ='stepfilled',
                     color = 'lightpink', edgecolor= 'black',alpha = 0.7, 
                     lw =2, 
                     label = 'Lg PH tot ')
ax.hist(phcalibcaloGeVCC['CC'], bins = 100, 
                      histtype ='step',
                      color = 'navy', alpha = 1
                      , 
                      lw =2, 
                      range = [0,200], label = 'Lg PH CC')
ax.set_yscale('log')
plt.plot(120, 3*10**3, '*', markersize=10, label = '120 GeV')
ax.legend(fontsize = 10)
ax.grid()
ax.set_xlabel('GeV')
plt.show()


# fig, ax = plt.subplots(3,3, figsize=(10,6))
# ax = ax.flat
# for j,i in enumerate(phcalibcaloGeVCC): 
#     ax[j].hist(phcalibcaloGeVCC[i], bins = 100, 
#                      histtype ='stepfilled',
#                      color = 'lightpink', edgecolor= 'black',alpha = 0.7, 
#                      lw =2, 
#                      label = 'Lg PH tot ')

#     ax[j].set_yscale('log')

#     ax[j].set_xlabel('GeV')
# plt.show()


fig, ax = plt.subplots(3,3, figsize=(10,6))
ax = ax.flat
for j,i in enumerate(phdatacalibcaloCC): 
    ax[j].hist(phdatacalibcaloCC[i], bins = 100, 
                      histtype ='stepfilled',
                      color = 'lightpink', edgecolor= 'black',alpha = 0.7, 
                      lw =2, 
                      label = 'Lg PH tot ')

    ax[j].set_yscale('log')

    ax[j].set_xlabel('GeV')
plt.show()


## scrivo istogramma da dare alla simulazione 
with open("histogram.txt", "w") as f:
    for i in range(len(hist)):
        f.write(f"/gps/hist/point {binedges[i+1]:.2f} {hist[i]:.2f}\n")
        
        
        
        


#%%# ######################### ASSE E AMORFO ALCUNE CONSIDERAZIONI############
############################################################################
# guardo il rapposrto tra energia depositata nel cristallo centarle e gli otto 
# virtuali a fianco in amorfo e in asse (per vedere quanto si apre il fascio)


## SPETTRI SIPM ASSE E AMORFO

phSiPMcrystal1axial = (axialdata[1][:,sipmchannel['mat2']]+axialdata[1][:,sipmchannel['mat3']])

phSiPMcrystal2axial = (axialdata[1][:,sipmchannel['mat1']]+axialdata[1][:,sipmchannel['mat4']])

fig, ax = plt.subplots(1,1, figsize=(10,6))
ax.hist(phSiPMcrystal1[phSiPMcrystal1>100], bins = 100, histtype ='step', 
            color = 'navy', alpha = 1, lw =2, label = 'Random SiPM PH', density = True)
ax.hist(phSiPMcrystal1axial[phSiPMcrystal1axial>100], bins = 100, histtype ='step', 
            color = 'hotpink', alpha = 1, lw =2, label = 'Axial SiPM PH', density = True)
ax.legend()
ax.grid()
ax.set_xlabel('SiPM PH [ADC]')
ax.set_ylabel('Entries')


#COSA VEDE IL CALORIMETRO CALIBRATO?
## dati con cristallo su fascio
phdatacalibcaloCCGeV = calibrationCalo(lgamorphous, datacalibcaloCC, intercept, slope)
phaxialGeV = calibrationCalo(lgamorphous, axialdata, intercept, slope)
phrandomGeV = calibrationCalo(lgamorphous, datacrystal, intercept, slope)
#%%
fig, ax = plt.subplots(1,1, figsize=(10,6))
ax.hist(sum(phaxialGeV.values()), bins = 100, histtype ='step', 
            color = 'navy', alpha = 1, lw =2, label = 'Axial calo PH', density = True)
ax.hist(sum(phrandomGeV.values()), bins = 100, histtype ='step', 
            color = 'hotpink', alpha = 1, lw =2, label = 'Random calo PH', density = True)
ax.legend()
ax.grid()
ax.set_yscale('log')
ax.set_xlabel('Calo PH [GeV]')
ax.set_ylabel('Entries')

#%%

### RAPPORTO TRA PH VISTA DA CRISTALLO SU FASCIO E 8*CRISTALLO A FIANCO
ratio = phSiPMcrystal1/(8*phSiPMcrystal2)
ratio[ratio == -np.inf] = 0
ratio[ratio == +np.inf] = 0
ratio[ratio == np.NaN] = 0

ratioaxial = phSiPMcrystal1axial/(8*phSiPMcrystal2axial)
ratioaxial[ratioaxial == -np.inf] = 0
ratioaxial[ratioaxial == +np.inf] = 0
fig, ax = plt.subplots(1,1, figsize=(10,6))
ax.hist(ratioaxial, bins = 100, histtype ='step', 
            color = 'lime', alpha = 1, lw =2, 
            label = 'PH cristallo centrale/ 8*PH cristallo laterale axial', 
            density = True, range = [0,20])
ax.hist(ratio, bins = 100, histtype ='step', 
            color = 'orangered', alpha = 1, lw =2, 
            label = 'PH cristallo centrale/ 8*PH cristallo laterale  random', 
            density = True, range = [0,20])

ax.grid()
ax.set_yscale('log')
ax.legend()
ax.set_xlabel('[ADC]')
ax.set_ylabel('Entries')

plt.tight_layout()
#%%
# RAPPORTO TRA COSA VEDE IL CANALE CENTRALE DEL CALORIMETRO ME QUELLI A FIANCO
#(i.e qunato si apre il fascio?)
lglateral = ['TL', 'TR', 'CL', 'CR', 'BL', 'BR']
datalateral = {}
datalateralaxial = {}
datalateralnocryst = {}
for j in lglateral:
    datalateral[j] = phrandomGeV[j]
    datalateralaxial[j] = phaxialGeV[j]
    datalateralnocryst[j] = phdatacalibcaloCCGeV[j]
    
ratiocal = phrandomGeV['CC'] / (sum(datalateral.values()))
ratiocalaxial = phaxialGeV['CC'] / (sum(datalateralaxial.values()))
ratiocalnocryst = phdatacalibcaloCCGeV['CC'] / (sum(datalateralnocryst.values()))
fig, ax = plt.subplots(1,1, figsize=(10,6))
ax.hist(ratiocalaxial, bins = 100, histtype ='step', 
            color = 'lime', alpha = 1, lw =2, label = 'PH  calo ch CC/ somma canali laterali axial', density = True, range = [0,40])
ax.hist(ratiocal, bins = 100, histtype ='step', 
            color = 'orangered', alpha = 1, lw =2, label = 'PH calo ch CC/ somma canali laterali random', density = True, range = [0,50])
ax.hist(ratiocalnocryst, bins = 100, histtype ='step', 
            color = 'black', alpha = 1, lw =2, label = 'PH calo ch CC/ somma canali laterali nocryst', density = True, range = [0,50])

ax.grid()
ax.set_yscale('log')
ax.legend()
ax.set_xlabel('[ADC]')
ax.set_ylabel('Entries')
plt.show()
plt.tight_layout()

################################################################################
#%%# ################Spettri da asse a amorfo SiPM e calorimetri ##########

# LENTO--> LE APRE TUTTE MENTRE PLOTTA 
##############################################################################
runscan = {'Axial': 153,  '1 $\u03F4_0$': 157, 'Plane':168, '8 $\u03F4_0$' :160, '17 $\u03F4_0$':162, 'Random': 155}
colors = ['hotpink', 'navy', 'lime', 'orangered', 'turquoise', 'indigo']
fig, ax = plt.subplots(1,1, figsize=(10,6))
fig1, ax1 = plt.subplots(1,1, figsize=(10,8))
for i, run in enumerate(runscan.keys()):
    datarun = opendata(runscan[run], datapath, h5keys)
    phSiPM = (datarun[1][:,sipmchannel['mat2']]+datarun[1][:,sipmchannel['mat3']])
    ax.hist(phSiPM[phSiPM>100], bins = 100, histtype ='step', 
                color = colors[i], alpha = 1, lw =3, label = f'{run}', density = True)
    # phcaloGeV = calibrationCalo(lgamorphous, datarun, intercept, slope)
    # ax1.hist(sum(phcaloGeV.values()), bins = 100, histtype ='step', 
    #           color = colors[i], alpha = 1, lw =2, label = f'{run} calo PH', density = True, range = [0,140])
ax.legend()
ax.grid()
#ax1.grid()
#ax1.set_yscale('log')
ax.set_xlabel('PH SiPM [a.u.]')
ax.set_ylabel('Normalised counts')
#ax1.set_xlabel('PH calo [GeV]')
#ax1.legend(fontsize = 10)








###############################################################################
#%%# ################# DATI SIMULAZIONE

simulationfile = 'run11.dat'
simulationpath = '/home/ale/Desktop/GeantSimulation/data'
simulationdata = np.loadtxt(f'{simulationpath}/{simulationfile}', skiprows=1)
with open(f'{simulationpath}/{simulationfile}', 'r') as f:
    header = f.readline().strip().split('\t')
    
    

#############################################
#2000 eventi presi dalla run di simulazione dei pioni (run01)
runpi = 'run10.dat'
simulationpi = np.loadtxt(f'{simulationpath}/{runpi}', skiprows=1)
#%%#
#Display the header and data

print(header)
# simulationdatacalo = simulationdata[:,2]+simulationdata[:,3]+simulationdata[:,4]+simulationdata[:,5]+simulationdata[:,6]+simulationdata[:,7]+simulationdata[:,8]
# simulationdatapi = simulationpi[:,2]+simulationpi[:,3]+simulationpi[:,4]+simulationpi[:,5]+simulationpi[:,6]+simulationpi[:,7]+simulationpi[:,8]
# allsimulationdata = np.concatenate((simulationdatacalo, simulationdatapi), axis=0)
# alldatasipm1 = np.concatenate((simulationdata[:,0],simulationpi[:,0]), axis=0)
# alldatasipm2 = np.concatenate((simulationdata[:,1],simulationpi[:,1]), axis=0)

### per legegre file simulazione dopo run 10
simulationdatacalo = simulationdata[:,9]+simulationdata[:,10]+simulationdata[:,11]+simulationdata[:,12]+simulationdata[:,13]+simulationdata[:,14]+simulationdata[:,15]
simulationdatapi = simulationpi[:,2]+simulationpi[:,3]+simulationpi[:,4]+simulationpi[:,5]+simulationpi[:,6]+simulationpi[:,7]+simulationpi[:,8]
allsimulationdata = np.concatenate((simulationdatacalo, simulationdatapi), axis=0)
alldatasipm1 = np.concatenate((simulationdata[:,0],simulationpi[:,0]), axis=0)
alldatasipm2 = np.concatenate((simulationdata[:,1],simulationpi[:,1]), axis=0)


#%%#

fig, ax = plt.subplots(1,1, figsize=(10,6))
#No cristal on beam 
# ax.hist(phcalibtot, bins = 100, histtype ='step', 
#             color = 'hotpink', alpha = 1, lw =2, label = 'Experimental spectra', density = True, range = [0,200])
#cristal on beam 
hph, binsph, _ =ax.hist((sum(phlgGeV.values())), bins = 100, histtype ='step', 
              color = 'hotpink', alpha = 1, lw =3, label = 'Experimental data ', density = True, range = [0,160])
ax.hist(allsimulationdata, bins = 100, 
          label = 'Simulation crystal on beam', histtype ='step',
          
          color = 'navy',alpha = 0.7, lw =2, density=True, range = [0,160])

ax.legend(fontsize = 12)
ax.grid()
ax.set_yscale('log')
ax.set_xlabel('Lead Glasses PH [GeV]')
ax.set_ylabel('Entries')

# xfitrangeinflg  = 105
# xfitrangesuplg  = 120


# xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
# phcondition =  ((xdata > xfitrangeinflg) & (xdata<xfitrangesuplg))
# paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
# resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
#                     weights = np.sqrt(hph[phcondition]))

# to_plot = np.linspace(xfitrangeinflg, xfitrangesuplg, 10000)

# y_eval = model.eval(resultx.params, x= to_plot)

# mulgfitsim = resultx.params['center'].value
# errmulgfitsim = resultx.params['center'].stderr
# sigmalgfit = resultx.params['sigma'].value
# ax.plot(to_plot, y_eval, linewidth = 3, color = 'black', label = f'$\mu$ = {round(mulgfitsim,2)} $\pm$ {round(errmulgfitsim,2)}')
# # ax.axvline(x=xfitrangeinflg, color="black", linestyle = '--')
# # ax.axvline(x=xfitrangesuplg, color="black", linestyle = '--')

# # lgthreshold = (mulgfit - 3*sigmalgfit)
# # ax.axvline(x=lgthreshold, color="red", linestyle = '--', label = 'threshold 3sigma')
ax.legend(fontsize = 10)    
ax.set_yscale('log')
plt.tight_layout()
plt.show()

##################################################################################################
#%%
# fig, ax = plt.subplots(1,1, figsize=(10,6))
# #No cristal on beam 
# hph, binsph, _ = ax.hist(phcalibtot, bins = 100, histtype ='step', 
#             color = 'hotpink', alpha = 1, lw =2, label = 'Experimental data calibration run', density = True, range = [0,160])
# # hph, binsph, _ = ax.hist(allsimulationdata, bins = 100, 
# #           label = 'Simulation calibration run', histtype ='step',
# #           color = 'navy',alpha = 0.7, lw =2, density=True, range = [0,160])

# ax.legend(fontsize = 12)
# ax.grid()
# ax.set_yscale('log')
# ax.set_xlabel('Lead Glasses PH [GeV]')
# ax.set_ylabel('Entries')

# xfitrangeinflg  = 117
# xfitrangesuplg  = 125


# xdata = np.array([(binsph[k+1]+binsph[k])/2 for k in range(len(binsph)-1)])
# phcondition =  ((xdata > xfitrangeinflg) & (xdata<xfitrangesuplg))
# paramsx = model.guess(hph[phcondition], x=xdata[phcondition])
# resultx = model.fit(hph[phcondition], paramsx, x=xdata[phcondition],
#                     weights = np.sqrt(hph[phcondition]))

# to_plot = np.linspace(xfitrangeinflg, xfitrangesuplg, 10000)

# y_eval = model.eval(resultx.params, x= to_plot)

# mulgfitsim = resultx.params['center'].value
# errmulgfitsim = resultx.params['center'].stderr
# sigmalgfit = resultx.params['sigma'].value
# ax.plot(to_plot, y_eval, linewidth = 2, color = 'black', label = f'$\mu$ = {round(mulgfitsim,2)} $\pm$ {round(errmulgfitsim,2)}')
# # ax.axvline(x=xfitrangeinflg, color="black", linestyle = '--')
# # ax.axvline(x=xfitrangesuplg, color="black", linestyle = '--')

# # lgthreshold = (mulgfit - 3*sigmalgfit)
# # ax.axvline(x=lgthreshold, color="red", linestyle = '--', label = 'threshold 3sigma')
# ax.legend(fontsize = 15)    
# ax.set_yscale('log')
# plt.tight_layout()
# plt.show()

#%%# crystal on beam 
fig, ax = plt.subplots(1,1, figsize=(10,6))
ax2 = ax.twiny()
# ax.hist(phSiPMcrystal1, bins = 100, 
#         label = 'Crystal PH', histtype ='step', 
#         color = 'limegreen',alpha = 1, lw =2)
# ax.set_xlabel('ADC')
# ax2.hist(simulationdata[:,0]*10**(-3), bins = 100, 
#         label = 'Simulation cryst1', histtype ='step',
#         color = 'hotpink',alpha = 0.8, lw =3, density = True)


h, bins = np.histogram(phSiPMcrystal1, bins = 100, )
binc = bins[:-1] + (bins[1] - bins[0])/2
lns1 = ax.plot(binc, h/np.sum(h), ds = "steps-mid",
        label = 'PH crystal on beam', color = 'limegreen', alpha = 1, lw = 2)


h, bins = np.histogram(alldatasipm1, bins = 100, )
binc = bins[:-1] + (bins[1] - bins[0])/2
lns2 = ax2.plot(binc, h/np.sum(h), ds = "steps-mid",
        label = 'PH simulation', color = 'navy', alpha = .8, lw = 3)


lns = lns1+lns2
labs = [l.get_label() for l in lns]
ax.legend(lns, labs, loc=0, fontsize = 15)

# ax2.hist(simulationdata[:,1]*10**(-3), bins = 100, 
#         label = 'simulationcryst2', histtype ='step', 
#         color = 'navy',alpha = 0.7, lw =2, density=True)
ax2.set_xlabel('Energy [GeV]')
ax2.set_xlabel('Energy [GeV]')
ax.set_xlabel('Energy [ADC]')
ax2.set_yscale('log')
ax.set_yscale('log')
# ax2.legend()
#ax.legend()
ax.grid()
plt.tight_layout()
plt.show()

#%% crystal 2

fig, ax = plt.subplots(1,1, figsize=(10,6))
ax2 = ax.twiny()
# ax.hist(phSiPMcrystal1, bins = 100, 
#         label = 'Crystal PH', histtype ='step', 
#         color = 'limegreen',alpha = 1, lw =2)
# ax.set_xlabel('ADC')
# ax2.hist(simulationdata[:,0]*10**(-3), bins = 100, 
#         label = 'Simulation cryst1', histtype ='step',
#         color = 'hotpink',alpha = 0.8, lw =3, density = True)


h, bins = np.histogram(phSiPMcrystal2, bins = 100, )
binc = bins[:-1] + (bins[1] - bins[0])/2
lns1 = ax.plot(binc, h/np.sum(h), ds = "steps-mid",
        label = 'Crystal PH', color = 'hotpink', alpha = 1, lw = 2)


h, bins = np.histogram(alldatasipm2, bins = 100, )
binc = bins[:-1] + (bins[1] - bins[0])/2
lns2 = ax2.plot(binc, h/np.sum(h), ds = "steps-mid",
        label = 'Simulation cryst1', color = 'limegreen', alpha = .8, lw = 3)


lns = lns1+lns2
labs = [l.get_label() for l in lns]
ax.legend(lns, labs, loc=0)

# ax2.hist(simulationdata[:,1]*10**(-3), bins = 100, 
#         label = 'simulationcryst2', histtype ='step', 
#         color = 'navy',alpha = 0.7, lw =2, density=True)
ax2.set_xlabel('Energy [GeV]')
ax2.set_yscale('log')
ax.set_yscale('log')
# ax2.legend()
#ax.legend()
ax.grid()
plt.tight_layout()
plt.show()


#%%

simfile8crystlas = 'run09.dat'
simdata8crystals = np.loadtxt(f'{simulationpath}/{simfile8crystlas}', skiprows=1)
with open(f'{simulationpath}/{simfile8crystlas}', 'r') as f:
    header1 = f.readline().strip().split('\t')
#######
sum8 = simdata8crystals[:,1]+simdata8crystals[:,2]+simdata8crystals[:,3]+simdata8crystals[:,4]+simdata8crystals[:,5]+simdata8crystals[:,6]+simdata8crystals[:,7]+simdata8crystals[:,8] 
ratiosim= simdata8crystals[:,0]/(sum8)


#%%

fig, ax = plt.subplots(1,1, figsize=(10,6))
ax2 = ax.twiny()


h, bins = np.histogram(phSiPMcrystal1+8*phSiPMcrystal2, bins = 100, range = [0,9000])
binc = bins[:-1] + (bins[1] - bins[0])/2
lns1 = ax.plot(binc, h/np.sum(h), ds = "steps-mid",
        label = 'PH central crystal +8*lateral crystal', color = 'hotpink', alpha = 1, lw = 2)


h, bins = np.histogram(simdata8crystals[:,0]+sum8, bins = 100, )
binc = bins[:-1] + (bins[1] - bins[0])/2
lns2 = ax2.plot(binc, h/np.sum(h), ds = "steps-mid",
        label = 'PH simulation 9 crystals ', color = 'navy', alpha = .8, lw = 3)



lns = lns1+lns2
labs = [l.get_label() for l in lns]
ax.legend(lns, labs, loc=0)

ax2.set_xlabel('Energy [GeV]')
ax2.set_yscale('log')
ax.set_yscale('log')
# ax2.legend()
#ax.legend()
ax.grid()
plt.tight_layout()
plt.show()

#%% ############################prova calibrazione SiPM




