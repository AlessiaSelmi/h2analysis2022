#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 29 16:25:10 2023

@author: ale
"""

from functionale import *
import numpy as np
import matplotlib.pyplot as plt
from lmfit.models import GaussianModel
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
import pickle

 ###########################################################################
###############################################################################
###################           Cristallo            ###########################
###############################################################################
##############################################################################
## apro i dati, allineo le camere, calcolo la divergenza e plot tempo PH 
plt.rcParams['font.size'] = '15'
datapath = '/home/ale/Desktop/dati2022H2/run520'
h5keys = ['xpos', 'digi_ph', 'digi_time']
labels = ['T1', 'T1', 'T2', 'T2']
xlim = [[-4, 4]]*4
distancesfromT1 = [1590]
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]
xcryst = []
ycryst = []
cutdivtime = []
charge_sharing = []
summat = []
summatcryst2 = []
zcryst = 1747
sipmchannel = {'mat1': 4, 'mat2': 1, 'mat3':2, 'mat4':3}

## quelle di run 'normale' è il back 
#%%

runs = [155,153] #amorfo e asse  in config back 
for i, nrun in enumerate(runs):
    data = opendata(nrun, datapath, h5keys)
    divex, divey = divergence(data[0], numberoftrackers = 2, distancesfromT1=[15.90])
    divex, divey, sigma, errsigma, mu, _ = divgaussianfit(data[0], 2, distancesfromT1,
                                                      p0divx, p0divy, Rangex=[[-0.001,0.001]], 
                                                      Rangey= [[-0.001,0.001]])
    datashift, shift = aligntracker(data[0], 2, mu, distancesfromT1)
    divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                              distancesfromT1,
                                                                                p0divx, p0divy,
                                                                                Color='navy', Figsize = (8,6), Rangex=[[-0.001,0.001]], 
                                                                                Rangey= [[-0.001,0.001]])

    x_cryst = datashift[:,0] + zcryst*np.tan(divexalign)
    y_cryst = datashift[:,1] + zcryst*np.tan(diveyalign)
    xcryst.append(x_cryst[0])
    ycryst.append(y_cryst[0])
    ax[0].axvline(x=-150*10**(-6), color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[0].axvline(x=+150*10**(-6), color="black", linestyle = '--', lw = 2)
    ax[1].axvline(x=-150*10**(-6), color="black", linestyle = '--', label = 'cut', lw = 2)
    ax[1].axvline(x=+150*10**(-6), color="black", linestyle = '--', lw = 2)
    cutdive = ((divexalign>-150*10**(-6))&(divexalign<150*10**(-6)) & (diveyalign>-150*10**(-6))&(diveyalign<150*10**(-6)))
    # vado ad osservare i SiPM, vedo se sono correlate le risposte (se stanno sulla bisettrice)

    fig, ax = plt.subplots(1, 1, figsize=(8,8))
    timephplot(data[2][:,sipmchannel['mat2']], (data[1][:,sipmchannel['mat2']]), ax, 200, 300)
    fig, ax = plt.subplots(1, 1, figsize=(8,8))
    timephplot(data[2][:,sipmchannel['mat3']], (data[1][:,sipmchannel['mat3']]), ax, 200, 300)

    
    cuttime = ((data[2][:,sipmchannel['mat3']]>200) & (data[2][:,sipmchannel['mat3']]<300) & (data[2][:,sipmchannel['mat2']]>200) & (data[2][:,sipmchannel['mat2']]<300))
    ## vediamo le correlzioni
    fig, ax = plt.subplots(1, 1, figsize=(8,8))
    ax.hist2d(data[1][:,sipmchannel['mat2']],data[1][:,sipmchannel['mat3']], 
              bins =[100,100], cmap = plt.cm.jet, norm=colors.LogNorm())

    ax.plot(data[1][:,sipmchannel['mat2']], data[1][:,sipmchannel['mat2']], '--', color = 'black', label = 'y = x')
    ax.set_xlabel('SiPM mat2')
    ax.set_ylabel('SiPM mat3')
    ax.set_title(f'Run {nrun}')
    ax.legend()
    ax.grid()
    plt.show()
# #
    cond = cutdive[0] & cuttime[0]
    cutdivtime.append(cond) #and di divergenza e time (che è and dei tempi delle due matrici)
    fig, ax = plt.subplots(1, 1, figsize=(8,8))
    ax.hist(data[1][:,sipmchannel['mat2']][cond], bins = 100, range = [0,4000], 
            label = 'mat2', histtype ='step', 
            color = 'lime', alpha = 0.7, lw =3)
    ax.hist(data[1][:,sipmchannel['mat3']][cond], bins = 100, range = [0,4000], 
            label = 'mat3', histtype ='step', 
            color = 'orangered', alpha = 0.7, lw =3)
    ax.legend()
    ax.grid()
    ax.set_yscale('log')
    ax.set_xlabel('PH [a.u]')
    ax.set_ylabel('Entries')
    ax.set_title(f'Run {nrun}')
    plt.tight_layout()
    ## direi che sono praticamente equalizzate, sommiamo
    fig, ax = plt.subplots(1, 1, figsize=(8,8))
    ax.hist(data[1][:,sipmchannel['mat2']][cond]+data[1][:,sipmchannel['mat3']][cond], 
            bins = 100, 
            label = 'mat2+mat3', histtype ='step', 
            color = 'hotpink', alpha = 0.7, lw =3)
    ax.legend()
    ax.grid()
    ax.set_yscale('log')
    ax.set_xlabel('PH [a.u]')
    ax.set_ylabel('Entries')
    plt.tight_layout()




    sumdata = data[1][:,sipmchannel['mat2']]+data[1][:,sipmchannel['mat3']]
    sumdatacryst2 = data[1][:,sipmchannel['mat1']]+data[1][:,sipmchannel['mat4']]
    diffdata = data[1][:,sipmchannel['mat2']]-data[1][:,sipmchannel['mat3']]
    summat.append(sumdata) ## conterrà amorfo e asse
    summatcryst2.append(sumdatacryst2)
    ratio = diffdata/sumdata
    charge_sharing.append(ratio)
#%%    
ratio = (summat[0][cutdivtime[0]])/(8*summatcryst2[0][cutdivtime[0]])
ratio[ratio == -np.inf] = 0
ratio[ratio == +np.inf] = 0
ratio[ratio == np.NaN] = 0

ratioaxial = summat[1][cutdivtime[1]]/(8*summatcryst2[1][cutdivtime[1]])
ratioaxial[ratioaxial == -np.inf] = 0
ratioaxial[ratioaxial == +np.inf] = 0
fig, ax = plt.subplots(1,1, figsize=(10,6))
ax.hist(ratioaxial, bins = 100, histtype ='step', 
            color = 'indigo', alpha = 1, lw =2, 
            label = 'Axial', 
            density = True, range = [0,20])
ax.hist(ratio, bins = 100, histtype ='step', 
            color = 'teal', alpha = 1, lw =2, 
            label = '$Amorphous$', 
            density = True, range = [0,20])

ax.grid()
ax.set_yscale('log')
ax.legend(fontsize= 20)
ax.set_xlabel('$PH\; crystal_{Central}\;\;/\;\;8\cdot PH\; crystal_{Lateral}$')
ax.set_ylabel('Entries')

plt.tight_layout()

#%%
Colors = [ 'dodgerblue', 'orangered',  'lime', 'turquoise','hotpink',  'indigo', 'darkred']
with open("lg_eqfactor.dat", "rb") as file:
    # Deserialize the data and load it into a new dictionary
    eqfactor = pickle.load(file)
#%%
# #guardiamo il calorimetro 
ratiocalo = []

lgch = {'BL':8, 'BR':9, 'CL':10, 'CC':11, 'CR':12, 'TL':13, 'TR':14}
for i, nrun in enumerate(runs):
    data = opendata(nrun, datapath, h5keys)
    lgs= {}
    for lglabel in lgch:
        lgs[lglabel] = data[1][:,lgch[lglabel]]*eqfactor[lglabel]
    ratiocalo.append(lgs['CC']/(lgs['BL']+lgs['CL']+lgs['BR']+lgs['CR']+lgs['TL']+lgs['TR']))
#%%   
    
fig, ax = plt.subplots(1,1, figsize=(10,6))
ax.hist(ratiocalo[1][cutdivtime[1]], bins = 100, histtype ='step', 
            color = 'indigo', alpha = 1, lw =2, 
            label = 'Axial', 
            density = True, range = [0,20])
ax.hist(ratiocalo[0][cutdivtime[0]], bins = 100, histtype ='step', 
            color = 'teal', alpha = 1, lw =2, 
            label = 'Amorphous', 
            density = True, range = [0,20])



ax.grid()
ax.set_yscale('log')
ax.legend(fontsize = 20)
ax.set_xlabel('$PH\; LG_{CC}\;\;/\;\;\\sum PH\; LG_{Lateral}$')
ax.set_ylabel('Entries')