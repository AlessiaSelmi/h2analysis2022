#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 18 17:15:40 2023

@author: ale
"""

from functionale import *
import numpy as np
import matplotlib.pyplot as plt
from lmfit.models import GaussianModel
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
import pickle
import pandas as pd
import scipy.ndimage as ndmg
runnumbers = [539]
sipmchannel = {'mat1': 4, 'mat2': 1, 'mat3':2, 'mat4':3}

gonio = {}
cradle = {}
ph = {}
time = {}

datapath = '/home/ale/Desktop/Dottorato/dataOREO_0823/run680'
h5keys = ['xpos', 'digiPH', 'digiTime', 'xinfo']
outdata = []
SiPM_ph = {}
xstereo = []
ystereo = []
#%%


def truebins(bins):
    # Shift the binning edges and remove the last point
    step = np.abs(bins[1]-bins[0])/2
    tbins = bins[:-1]+step
    return(tbins)


# Histogram
nbinsx = 20
binsizey = 20
binsize = 1

plt.show()

for i, nrun in enumerate(runnumbers):
    data = opendata(nrun, datapath, h5keys)
    SiPM_ph[nrun]= data[1][:,1]
    gonio[nrun] = data[3][:,0]
    cradle[nrun] = data[3][:,1]
    
    
    binsx = np.linspace(0,1000000,nbinsx)
    binsy = np.arange(binsize/2,np.max(SiPM_ph[nrun])+binsize*3/2,binsizey)
    truex = truebins(binsx)
    truey = truebins(binsy)
    cnorm = 50

    fig, ax = plt.subplots(figsize=(12,10))

    isto2d = plt.hist2d(gonio[nrun][SiPM_ph[nrun]>500], SiPM_ph[nrun][SiPM_ph[nrun]>500],
                        
                        cmap = 'jet')
    #isto2d = isto2d[0].transpose()

    ax.set_ylabel('SiPM PH [ADC]')
    ax.set_xlabel('$\u03F4_{x,mis}$ [$\mu$ rad]')
    plt.colorbar()
    #ax.tick_params(labelsize = textfont/2)
    
    # Proietto l'istogramma su un asse



    histosum = np.zeros_like(truex)

    x1,y1,_ = hist2dToProfile(isto2d)
    fig, ax = plt.subplots(figsize=(8,6))
    ax.plot(x1,y1, '.', markersize = 15, ls='--', lw = 2, color='navy', label='Data: profile plot')
    xstereo.append(x1)
    ystereo.append(y1)
    plt.grid(linewidth=0.5)
    ax.set_xlabel('$\u03F4_{ang}$ [$\mu$rad]', fontsize = 24)
    ax.set_ylabel('PH SiPM [ADC]', fontsize = 24)
    ax.legend(fontsize = 20)
    plt.tight_layout()



plt.show()
 #%%   
# import matplotlib.colors as colors
# import matplotlib.cm as cm


# plt.rcParams['font.size'] = '18'


# xpos = xstereo
# ypos = [cradle[111][1]]*len(xstereo[1])
# zpos = 0  # z coordinates of each bar
# dx = [0.0001]  # Width of each bar
# dy = [0.0001]# Depth of each bar
# dz = np.array(ystereo)

# offset = dz + np.abs(dz.min())
# fracs = offset.astype(float)/offset.max()
# norm = colors.Normalize(fracs.min(), fracs.max())
# colors = cm.jet(norm(fracs))
# cmap = 'jet'





# area = dz*10**-2

# fig, ax = plt.subplots(figsize=(8,6), constrained_layout=True)
# plt.scatter(xpos, ypos, s=area, c= colors,cmap = cm.jet)
# plt.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap))

# # ax.set_xlim([1.642*10**6, 1.654*10**6])
# # ax.set_ylim([22000, 28000])
# # ax.set_xlim([1.64649*10**6, 1.64751*10**6])
# # ax.set_ylim([24500, 26800])
# # ax.plot( 1647000,25384, '*', c= 'black', markersize = 20, label = 'Axis')
# ax.set_ylabel('$\u03B8_{crad}$ [rad]', fontsize = 22)
# ax.set_xlabel('$\u03B8_{ang}$ [rad]', fontsize = 22)
# ax.grid()
# ax.legend(fontsize = 26)


# fig.show()  
    
