#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 13 10:12:17 2023

@author: ale
"""

from functionale import *
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable

plt.rcParams['font.size'] = '15'
runnumber = 547

datapath = '/home/ale/Desktop/Dottorato/dataOREO_0823/run680'
h5keys = ['digiPH', 'xpos', 'digiTime']
data = opendata(runnumber, datapath, h5keys)

xpos = data[1]

#camere 
xlabels = ['BC1',' BC1', 'BC2', 'BC2']
xlim = [[-0,10]]*4
histpos1d(xpos, 2, xlabels = xlabels, xlim = xlim, Range = [0,10])
histpos2d(xpos, numberoftrackers = 2)
fig, ax = plt.subplots(1,2 , figsize=(8,8))
ax = ax.flat
ax[0].hist2d(xpos[:,0], xpos[:,2], bins = [100,100], range = [[0,10], [0,10]])
ax[1].hist2d(xpos[:,1],xpos[:,3] , bins = [100,100], range = [[0,10], [0,10]])
plt.show()

distancesfromT1 = [268]
p0divx = [500, 0, 100]
p0divy = [700, 0, 100]

divex, divey, sigma, errsigma, mu, _ = divgaussianfit(xpos, 2, distancesfromT1,
                                                       p0divx, p0divy,
                                                       Rangex=[[-0.02,0.02]],
                                                       Rangey=[[-0.02,0.02]])


datashift, shift = aligntracker(xpos, 2, mu, distancesfromT1)
divexalign, diveyalign, sigmaalign, errsigmaalign, mualign, ax = divgaussianfit(datashift, 2,
                                                                               distancesfromT1,
                                                                                p0divx, p0divy,
                                                                                Rangex=[[-0.02,0.02]],
                                                                                Rangey=[[-0.02,0.02]], 
                                                                                Color='navy', 
                                                                                Figsize = (8,6))

#%%


for i in range(36): 
    fig, ax = plt.subplots(2,2, figsize=(8,8))
    ax.hist(data[0][:,i], bins = 100, 
               histtype = 'stepfilled', color = 'navy', edgecolor = 'navy',
               alpha = 0.6,lw =3, label = f'crilin ch{i}')
    ax.legend()
    ax.set_xlabel('PH [a.u]')
    ax.set_ylabel('Entries')
    ax.grid()
    ax.set_yscale('log')
plt.show()