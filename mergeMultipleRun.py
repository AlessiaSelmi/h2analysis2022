#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  8 14:41:15 2023

@author: ale
"""

import h5py
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.patches as patches
data = []
datapath = '/home/ale/Desktop/Dottorato/dati_remoti /dataOREO_1023/run720'
h5keys = [ 'xpos','digiPH', 'digiTime']

chLabel = {'LG':0, 'APC':1, 'Cherry44':2, 'Cherry48':3, 'George':4, 'John':5, 'Paul':6, 'Ringo':7}
SiPMlabel = ['George','John', 'Paul', 'Ringo']
runnumber = [120, 122, 124, 125] # 0°, 90°
data = {}

partialData = []
for j in h5keys: 
   
    outdata = []
    for k, nrun in enumerate(runnumber): 
        with h5py.File(datapath +f'{runnumber[k]}.h5', 'r', libver='latest', swmr=True) as hf: 
            print("opening %s" % runnumber[k])
            print(j)
            outdata.append(np.array(hf[j]))
    partialData.append(np.vstack((outdata[0], outdata[1], outdata[2], outdata[3])))
    
data[6666] = partialData
